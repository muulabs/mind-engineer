-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jul 28, 2019 at 02:08 PM
-- Server version: 5.6.35
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mind-engineer`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(18, 140, 'WooCommerce', '', '', '', '2019-05-20 05:01:32', '2019-05-20 05:01:32', 'Unpaid order cancelled - time limit reached. Order status changed from Pending payment to Cancelled.', 0, '1', 'WooCommerce', 'order_note', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_failed_jobs`
--

CREATE TABLE `wp_failed_jobs` (
  `id` bigint(20) NOT NULL,
  `job` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_mailchimp_carts`
--

CREATE TABLE `wp_mailchimp_carts` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `cart` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'https://wp2.dev-c01.muulabs.pl', 'yes'),
(2, 'home', 'https://wp2.dev-c01.muulabs.pl', 'yes'),
(3, 'blogname', 'MindEngineer', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'tobiaszkochanski@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'closed', 'yes'),
(20, 'default_ping_status', 'closed', 'yes'),
(21, 'default_pingback_flag', '', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:13:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:36:\"contact-form-7/wp-contact-form-7.php\";i:2;s:41:\"event-tickets-plus/event-tickets-plus.php\";i:3;s:31:\"event-tickets/event-tickets.php\";i:4;s:20:\"social-pug/index.php\";i:5;s:43:\"the-events-calendar/the-events-calendar.php\";i:6;s:69:\"woo-gutenberg-products-block/woocommerce-gutenberg-products-block.php\";i:7;s:79:\"woocommerce-extra-product-options-pro/woocommerce-extra-product-options-pro.php\";i:8;s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";i:9;s:45:\"woocommerce-services/woocommerce-services.php\";i:10;s:27:\"woocommerce/woocommerce.php\";i:11;s:29:\"wp-mail-smtp/wp_mail_smtp.php\";i:12;s:27:\"wp-super-cache/wp-cache.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:3:{i:0;s:52:\"/var/www/html/wp-content/themes/storefront/style.css\";i:1;s:100:\"/var/www/html/wp-content/plugins/klarna-checkout-for-woocommerce/klarna-checkout-for-woocommerce.php\";i:2;s:0:\"\";}', 'no'),
(40, 'template', 'storefront', 'yes'),
(41, 'stylesheet', 'storefront', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '44719', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:2:{s:45:\"woocommerce-services/woocommerce-services.php\";a:2:{i:0;s:17:\"WC_Connect_Loader\";i:1;s:16:\"plugin_uninstall\";}s:27:\"wp-super-cache/wp-cache.php\";s:22:\"wpsupercache_uninstall\";}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '16', 'yes'),
(84, 'page_on_front', '15', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '', 'yes'),
(93, 'initial_db_version', '43764', 'yes'),
(94, 'wp_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:154:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;s:31:\"read_private_aggregator-records\";b:1;s:23:\"edit_aggregator-records\";b:1;s:30:\"edit_others_aggregator-records\";b:1;s:31:\"edit_private_aggregator-records\";b:1;s:33:\"edit_published_aggregator-records\";b:1;s:25:\"delete_aggregator-records\";b:1;s:32:\"delete_others_aggregator-records\";b:1;s:33:\"delete_private_aggregator-records\";b:1;s:35:\"delete_published_aggregator-records\";b:1;s:26:\"publish_aggregator-records\";b:1;s:25:\"read_private_tribe_events\";b:1;s:17:\"edit_tribe_events\";b:1;s:24:\"edit_others_tribe_events\";b:1;s:25:\"edit_private_tribe_events\";b:1;s:27:\"edit_published_tribe_events\";b:1;s:19:\"delete_tribe_events\";b:1;s:26:\"delete_others_tribe_events\";b:1;s:27:\"delete_private_tribe_events\";b:1;s:29:\"delete_published_tribe_events\";b:1;s:20:\"publish_tribe_events\";b:1;s:25:\"read_private_tribe_venues\";b:1;s:17:\"edit_tribe_venues\";b:1;s:24:\"edit_others_tribe_venues\";b:1;s:25:\"edit_private_tribe_venues\";b:1;s:27:\"edit_published_tribe_venues\";b:1;s:19:\"delete_tribe_venues\";b:1;s:26:\"delete_others_tribe_venues\";b:1;s:27:\"delete_private_tribe_venues\";b:1;s:29:\"delete_published_tribe_venues\";b:1;s:20:\"publish_tribe_venues\";b:1;s:29:\"read_private_tribe_organizers\";b:1;s:21:\"edit_tribe_organizers\";b:1;s:28:\"edit_others_tribe_organizers\";b:1;s:29:\"edit_private_tribe_organizers\";b:1;s:31:\"edit_published_tribe_organizers\";b:1;s:23:\"delete_tribe_organizers\";b:1;s:30:\"delete_others_tribe_organizers\";b:1;s:31:\"delete_private_tribe_organizers\";b:1;s:33:\"delete_published_tribe_organizers\";b:1;s:24:\"publish_tribe_organizers\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:74:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:31:\"read_private_aggregator-records\";b:1;s:23:\"edit_aggregator-records\";b:1;s:30:\"edit_others_aggregator-records\";b:1;s:31:\"edit_private_aggregator-records\";b:1;s:33:\"edit_published_aggregator-records\";b:1;s:25:\"delete_aggregator-records\";b:1;s:32:\"delete_others_aggregator-records\";b:1;s:33:\"delete_private_aggregator-records\";b:1;s:35:\"delete_published_aggregator-records\";b:1;s:26:\"publish_aggregator-records\";b:1;s:25:\"read_private_tribe_events\";b:1;s:17:\"edit_tribe_events\";b:1;s:24:\"edit_others_tribe_events\";b:1;s:25:\"edit_private_tribe_events\";b:1;s:27:\"edit_published_tribe_events\";b:1;s:19:\"delete_tribe_events\";b:1;s:26:\"delete_others_tribe_events\";b:1;s:27:\"delete_private_tribe_events\";b:1;s:29:\"delete_published_tribe_events\";b:1;s:20:\"publish_tribe_events\";b:1;s:25:\"read_private_tribe_venues\";b:1;s:17:\"edit_tribe_venues\";b:1;s:24:\"edit_others_tribe_venues\";b:1;s:25:\"edit_private_tribe_venues\";b:1;s:27:\"edit_published_tribe_venues\";b:1;s:19:\"delete_tribe_venues\";b:1;s:26:\"delete_others_tribe_venues\";b:1;s:27:\"delete_private_tribe_venues\";b:1;s:29:\"delete_published_tribe_venues\";b:1;s:20:\"publish_tribe_venues\";b:1;s:29:\"read_private_tribe_organizers\";b:1;s:21:\"edit_tribe_organizers\";b:1;s:28:\"edit_others_tribe_organizers\";b:1;s:29:\"edit_private_tribe_organizers\";b:1;s:31:\"edit_published_tribe_organizers\";b:1;s:23:\"delete_tribe_organizers\";b:1;s:30:\"delete_others_tribe_organizers\";b:1;s:31:\"delete_private_tribe_organizers\";b:1;s:33:\"delete_published_tribe_organizers\";b:1;s:24:\"publish_tribe_organizers\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:30:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:23:\"edit_aggregator-records\";b:1;s:33:\"edit_published_aggregator-records\";b:1;s:25:\"delete_aggregator-records\";b:1;s:35:\"delete_published_aggregator-records\";b:1;s:26:\"publish_aggregator-records\";b:1;s:17:\"edit_tribe_events\";b:1;s:27:\"edit_published_tribe_events\";b:1;s:19:\"delete_tribe_events\";b:1;s:29:\"delete_published_tribe_events\";b:1;s:20:\"publish_tribe_events\";b:1;s:17:\"edit_tribe_venues\";b:1;s:27:\"edit_published_tribe_venues\";b:1;s:19:\"delete_tribe_venues\";b:1;s:29:\"delete_published_tribe_venues\";b:1;s:20:\"publish_tribe_venues\";b:1;s:21:\"edit_tribe_organizers\";b:1;s:31:\"edit_published_tribe_organizers\";b:1;s:23:\"delete_tribe_organizers\";b:1;s:33:\"delete_published_tribe_organizers\";b:1;s:24:\"publish_tribe_organizers\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:13:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:23:\"edit_aggregator-records\";b:1;s:25:\"delete_aggregator-records\";b:1;s:17:\"edit_tribe_events\";b:1;s:19:\"delete_tribe_events\";b:1;s:17:\"edit_tribe_venues\";b:1;s:19:\"delete_tribe_venues\";b:1;s:21:\"edit_tribe_organizers\";b:1;s:23:\"delete_tribe_organizers\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"edit_theme_options\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:8:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:0:{}s:8:\"header-1\";a:0:{}s:8:\"footer-1\";a:0:{}s:8:\"footer-2\";a:0:{}s:8:\"footer-3\";a:0:{}s:8:\"footer-4\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(102, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'nonce_key', '@4]4i^z~7J9q}J5U7;JvHo8;Y3&yvCG8O1.bB W&#{w+]VIG(r|Q0G]yiHXvjM()', 'no'),
(109, 'nonce_salt', ')WEj:pxaN[;;$ZoFRpNdM|5q Q1R ;B7eUY,Fokavq^7_gcd>4$;6,j{3le/4?AF', 'no'),
(110, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'cron', 'a:16:{i:1564315739;a:1:{s:26:\"action_scheduler_run_queue\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:12:\"every_minute\";s:4:\"args\";a:0:{}s:8:\"interval\";i:60;}}}i:1564315834;a:1:{s:32:\"woocommerce_cancel_unpaid_orders\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1564315952;a:1:{s:39:\"tribe_aggregator_process_insert_records\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:17:\"tribe-every15mins\";s:4:\"args\";a:0:{}s:8:\"interval\";i:900;}}}i:1564317059;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1564317299;a:1:{s:40:\"dpsp_cron_get_posts_networks_share_count\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:14:\"dpsp_2x_hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:7200;}}}i:1564346634;a:1:{s:28:\"woocommerce_cleanup_sessions\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1564353059;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1564353062;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1564353503;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1564355091;a:1:{s:24:\"tribe_common_log_cleanup\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1564358400;a:1:{s:27:\"woocommerce_scheduled_sales\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1564368234;a:1:{s:33:\"woocommerce_cleanup_personal_data\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1564368244;a:1:{s:30:\"woocommerce_tracker_send_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1564379034;a:1:{s:24:\"woocommerce_cleanup_logs\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1564704000;a:1:{s:25:\"woocommerce_geoip_updater\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:7:\"monthly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:2635200;}}}s:7:\"version\";i:2;}', 'yes'),
(115, 'auth_key', 'h/FFM>Uq6`gk*!owK`vqOj{z<QMJR[)k@4UpW*k$HN#8~ #zQ,z}m)$mUW&|1YUf', 'no'),
(116, 'auth_salt', 'qQ[$as@lBPu]M@XU@4;+RB9_Ys3B86Y~XR5//9xFpI}Vbo%,<|sPY3U)sAR;jx1q', 'no'),
(117, 'logged_in_key', '?vWUqk[-G#M5Rt-{C<r![c=b?}9n[duRB73?Er]ftxp5cpz3<e<I3@pVhD}Tiz6<', 'no'),
(118, 'logged_in_salt', 'QrihSJ}:GAr(4;tP*n1}6ACfc:zP^3R7+N2F2iB&?AC0zh5~ceaG6f2a@l`*8{86', 'no'),
(142, 'db_upgraded', '', 'yes'),
(143, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:4:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.2\";s:7:\"version\";s:5:\"5.2.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.2\";s:7:\"version\";s:5:\"5.2.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.1-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.1\";s:7:\"version\";s:5:\"5.2.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:3;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.2.zip\";s:10:\"no_content\";s:68:\"https://downloads.wordpress.org/release/wordpress-5.2-no-content.zip\";s:11:\"new_bundled\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:3:\"5.2\";s:7:\"version\";s:3:\"5.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}}s:12:\"last_checked\";i:1564309867;s:15:\"version_checked\";s:5:\"5.1.1\";s:12:\"translations\";a:0:{}}', 'no'),
(146, 'can_compress_scripts', '0', 'no'),
(149, 'recently_activated', 'a:0:{}', 'yes'),
(150, 'widget_akismet_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(161, 'woocommerce_store_address', 'abc', 'yes'),
(162, 'woocommerce_store_address_2', '', 'yes'),
(163, 'woocommerce_store_city', 'abc', 'yes'),
(164, 'woocommerce_default_country', 'NL', 'yes'),
(165, 'woocommerce_store_postcode', 'abc', 'yes'),
(166, 'woocommerce_allowed_countries', 'all', 'yes'),
(167, 'woocommerce_all_except_countries', 'a:0:{}', 'yes'),
(168, 'woocommerce_specific_allowed_countries', 'a:0:{}', 'yes'),
(169, 'woocommerce_ship_to_countries', 'all', 'yes'),
(170, 'woocommerce_specific_ship_to_countries', 'a:0:{}', 'yes'),
(171, 'woocommerce_default_customer_address', 'geolocation', 'yes'),
(172, 'woocommerce_calc_taxes', 'yes', 'yes'),
(173, 'woocommerce_enable_coupons', 'no', 'yes'),
(174, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(175, 'woocommerce_currency', 'EUR', 'yes'),
(176, 'woocommerce_currency_pos', 'left', 'yes'),
(177, 'woocommerce_price_thousand_sep', ',', 'yes'),
(178, 'woocommerce_price_decimal_sep', '.', 'yes'),
(179, 'woocommerce_price_num_decimals', '2', 'yes'),
(180, 'woocommerce_shop_page_id', '5', 'yes'),
(181, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(182, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(183, 'woocommerce_placeholder_image', '', 'yes'),
(184, 'woocommerce_weight_unit', 'kg', 'yes'),
(185, 'woocommerce_dimension_unit', 'cm', 'yes'),
(186, 'woocommerce_enable_reviews', 'no', 'yes'),
(187, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(188, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(189, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(190, 'woocommerce_review_rating_required', 'yes', 'no'),
(191, 'woocommerce_manage_stock', 'yes', 'yes'),
(192, 'woocommerce_hold_stock_minutes', '60', 'no'),
(193, 'woocommerce_notify_low_stock', 'yes', 'no'),
(194, 'woocommerce_notify_no_stock', 'yes', 'no'),
(195, 'woocommerce_stock_email_recipient', 'tobiaszkochanski@gmail.com', 'no'),
(196, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(197, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(198, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(199, 'woocommerce_stock_format', '', 'yes'),
(200, 'woocommerce_file_download_method', 'force', 'no'),
(201, 'woocommerce_downloads_require_login', 'no', 'no'),
(202, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(203, 'woocommerce_prices_include_tax', 'no', 'yes'),
(204, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(205, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(206, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(207, 'woocommerce_tax_classes', 'Reduced rate\nZero rate', 'yes'),
(208, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(209, 'woocommerce_tax_display_cart', 'excl', 'yes'),
(210, 'woocommerce_price_display_suffix', '', 'yes'),
(211, 'woocommerce_tax_total_display', 'itemized', 'no'),
(212, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(213, 'woocommerce_shipping_cost_requires_address', 'no', 'yes'),
(214, 'woocommerce_ship_to_destination', 'billing', 'no'),
(215, 'woocommerce_shipping_debug_mode', 'no', 'yes'),
(216, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(217, 'woocommerce_enable_checkout_login_reminder', 'no', 'no'),
(218, 'woocommerce_enable_signup_and_login_from_checkout', 'no', 'no'),
(219, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(220, 'woocommerce_registration_generate_username', 'yes', 'no'),
(221, 'woocommerce_registration_generate_password', 'yes', 'no'),
(222, 'woocommerce_erasure_request_removes_order_data', 'no', 'no'),
(223, 'woocommerce_erasure_request_removes_download_data', 'no', 'no'),
(224, 'woocommerce_registration_privacy_policy_text', 'Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our [privacy_policy].', 'yes'),
(225, 'woocommerce_checkout_privacy_policy_text', 'Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our [privacy_policy].', 'yes'),
(226, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(227, 'woocommerce_trash_pending_orders', '', 'no'),
(228, 'woocommerce_trash_failed_orders', '', 'no'),
(229, 'woocommerce_trash_cancelled_orders', '', 'no'),
(230, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(231, 'woocommerce_email_from_name', 'MindEngineer', 'no'),
(232, 'woocommerce_email_from_address', 'tobiaszkochanski@gmail.com', 'no'),
(233, 'woocommerce_email_header_image', '', 'no'),
(234, 'woocommerce_email_footer_text', '', 'no'),
(235, 'woocommerce_email_base_color', '#96588a', 'no'),
(236, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(237, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(238, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(239, 'woocommerce_cart_page_id', '6', 'yes'),
(240, 'woocommerce_checkout_page_id', '7', 'yes'),
(241, 'woocommerce_myaccount_page_id', '8', 'yes'),
(242, 'woocommerce_terms_page_id', '3', 'no'),
(243, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(244, 'woocommerce_unforce_ssl_checkout', 'yes', 'yes'),
(245, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(246, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(247, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(248, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(249, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(250, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(251, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(252, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(253, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(254, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(255, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(256, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(257, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(258, 'woocommerce_api_enabled', 'no', 'yes'),
(259, 'woocommerce_single_image_width', '600', 'yes'),
(260, 'woocommerce_thumbnail_image_width', '300', 'yes'),
(261, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes'),
(262, 'woocommerce_demo_store', 'no', 'no'),
(263, 'woocommerce_permalinks', 'a:5:{s:12:\"product_base\";s:7:\"product\";s:13:\"category_base\";s:16:\"product-category\";s:8:\"tag_base\";s:11:\"product-tag\";s:14:\"attribute_base\";s:0:\"\";s:22:\"use_verbose_page_rules\";b:0;}', 'yes'),
(264, 'current_theme_supports_woocommerce', 'yes', 'yes'),
(265, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(266, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes'),
(268, 'default_product_cat', '15', 'yes'),
(273, 'woocommerce_admin_notices', 'a:1:{i:0;s:20:\"no_secure_connection\";}', 'yes'),
(274, '_transient_woocommerce_webhook_ids', 'a:0:{}', 'yes'),
(275, 'widget_woocommerce_widget_cart', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(276, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(277, 'widget_woocommerce_layered_nav', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(278, 'widget_woocommerce_price_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(279, 'widget_woocommerce_product_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(280, 'widget_woocommerce_product_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(281, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(282, 'widget_woocommerce_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(283, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(284, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(285, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(286, 'widget_woocommerce_rating_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(289, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(290, 'woocommerce_product_type', 'virtual', 'yes'),
(291, 'woocommerce_allow_tracking', 'no', 'yes'),
(295, 'woocommerce_klarna_checkout_settings', 'a:1:{s:7:\"enabled\";s:3:\"yes\";}', 'yes'),
(297, 'woocommerce_ppec_paypal_settings', 'a:53:{s:7:\"enabled\";s:3:\"yes\";s:16:\"reroute_requests\";s:3:\"yes\";s:5:\"email\";s:26:\"tobiaszkochanski@gmail.com\";s:5:\"title\";s:6:\"PayPal\";s:11:\"description\";s:85:\"Pay via PayPal; you can pay with your credit card if you don\'t have a PayPal account.\";s:11:\"environment\";s:7:\"sandbox\";s:12:\"api_username\";s:44:\"maciej.krupowies-facilitator_api1.muulabs.pl\";s:12:\"api_password\";s:16:\"8U38CDNE277AZ3PH\";s:13:\"api_signature\";s:56:\"AkUXvyyNwvsctJSR20z9FHmrGL54AlKxvso.99jt5JSSvQEtMCsSkLqY\";s:15:\"api_certificate\";s:0:\"\";s:11:\"api_subject\";s:0:\"\";s:20:\"sandbox_api_username\";s:44:\"maciej.krupowies-facilitator_api1.muulabs.pl\";s:20:\"sandbox_api_password\";s:16:\"8U38CDNE277AZ3PH\";s:21:\"sandbox_api_signature\";s:56:\"AkUXvyyNwvsctJSR20z9FHmrGL54AlKxvso.99jt5JSSvQEtMCsSkLqY\";s:23:\"sandbox_api_certificate\";s:0:\"\";s:19:\"sandbox_api_subject\";s:0:\"\";s:10:\"brand_name\";s:12:\"MindEngineer\";s:14:\"logo_image_url\";s:0:\"\";s:16:\"header_image_url\";s:0:\"\";s:10:\"page_style\";s:0:\"\";s:12:\"landing_page\";s:7:\"Billing\";s:5:\"debug\";s:2:\"no\";s:14:\"invoice_prefix\";s:3:\"WC-\";s:15:\"require_billing\";s:2:\"no\";s:20:\"require_phone_number\";s:2:\"no\";s:13:\"paymentaction\";s:4:\"sale\";s:16:\"instant_payments\";s:3:\"yes\";s:26:\"subtotal_mismatch_behavior\";s:3:\"add\";s:7:\"use_spb\";s:3:\"yes\";s:12:\"button_color\";s:4:\"gold\";s:12:\"button_shape\";s:4:\"rect\";s:13:\"button_layout\";s:8:\"vertical\";s:11:\"button_size\";s:10:\"responsive\";s:20:\"hide_funding_methods\";a:1:{i:0;s:4:\"CARD\";}s:14:\"credit_enabled\";s:2:\"no\";s:21:\"cart_checkout_enabled\";s:2:\"no\";s:25:\"mini_cart_settings_toggle\";s:2:\"no\";s:23:\"mini_cart_button_layout\";s:8:\"vertical\";s:21:\"mini_cart_button_size\";s:10:\"responsive\";s:30:\"mini_cart_hide_funding_methods\";a:1:{i:0;s:4:\"CARD\";}s:24:\"mini_cart_credit_enabled\";s:2:\"no\";s:34:\"checkout_on_single_product_enabled\";s:2:\"no\";s:30:\"single_product_settings_toggle\";s:3:\"yes\";s:28:\"single_product_button_layout\";s:10:\"horizontal\";s:26:\"single_product_button_size\";s:10:\"responsive\";s:35:\"single_product_hide_funding_methods\";a:1:{i:0;s:4:\"CARD\";}s:29:\"single_product_credit_enabled\";s:2:\"no\";s:12:\"mark_enabled\";s:3:\"yes\";s:20:\"mark_settings_toggle\";s:3:\"yes\";s:18:\"mark_button_layout\";s:8:\"vertical\";s:16:\"mark_button_size\";s:10:\"responsive\";s:25:\"mark_hide_funding_methods\";a:1:{i:0;s:4:\"CARD\";}s:19:\"mark_credit_enabled\";s:2:\"no\";}', 'yes'),
(298, 'woocommerce_stripe_settings', 'a:3:{s:7:\"enabled\";s:2:\"no\";s:14:\"create_account\";b:0;s:5:\"email\";b:0;}', 'yes'),
(299, 'woocommerce_cheque_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(300, 'woocommerce_bacs_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(301, 'woocommerce_cod_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(302, 'woocommerce_setup_automated_taxes', '1', 'yes'),
(303, 'mailchimp_woocommerce_plugin_do_activation_redirect', '', 'yes'),
(320, 'theme_mods_twentynineteen', 'a:1:{s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1552948676;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(321, 'current_theme', 'Storefront', 'yes'),
(322, 'theme_mods_storefront', 'a:15:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:18;}s:18:\"custom_css_post_id\";i:-1;s:34:\"storefront_header_background_color\";s:7:\"#1c2834\";s:28:\"storefront_header_text_color\";s:7:\"#ffffff\";s:28:\"storefront_header_link_color\";s:7:\"#f0474a\";s:34:\"storefront_footer_background_color\";s:7:\"#1c2834\";s:24:\"storefront_heading_color\";s:7:\"#f0474a\";s:23:\"storefront_accent_color\";s:7:\"#f0474a\";s:31:\"storefront_footer_heading_color\";s:7:\"#f0474a\";s:28:\"storefront_footer_text_color\";s:7:\"#ffffff\";s:28:\"storefront_footer_link_color\";s:7:\"#f0474a\";s:34:\"storefront_button_background_color\";s:7:\"#f0474a\";s:28:\"storefront_button_text_color\";s:7:\"#ffffff\";s:29:\"storefront_product_pagination\";b:0;}', 'yes'),
(323, 'theme_switched', '', 'yes'),
(324, 'mailchimp_woocommerce_version', '2.1.14', 'no'),
(328, 'storefront_nux_fresh_site', '0', 'yes'),
(329, 'woocommerce_catalog_rows', '4', 'yes'),
(330, 'woocommerce_catalog_columns', '3', 'yes'),
(331, 'woocommerce_maybe_regenerate_images_hash', '27acde77266b4d2a3491118955cb3f66', 'yes'),
(334, 'mailchimp_woocommerce_db_mailchimp_carts', '1', 'no'),
(335, 'wc_ppec_version', '1.6.10', 'yes'),
(339, 'do_activate', '0', 'yes'),
(344, '_transient_product_query-transient-version', '1559764797', 'yes'),
(350, '_transient_product-transient-version', '1559764797', 'yes'),
(357, '_transient_shipping-transient-version', '1552949094', 'yes'),
(394, 'tribe_events_calendar_options', 'a:38:{s:14:\"schema-version\";s:5:\"4.9.3\";s:27:\"recurring_events_are_hidden\";s:6:\"hidden\";s:21:\"previous_ecp_versions\";a:2:{i:0;s:1:\"0\";i:1;s:5:\"4.8.2\";}s:18:\"latest_ecp_version\";s:5:\"4.9.3\";s:39:\"last-update-message-the-events-calendar\";s:5:\"4.8.2\";s:25:\"ticket-enabled-post-types\";a:1:{i:0;s:12:\"tribe_events\";}s:31:\"previous_event_tickets_versions\";a:3:{i:0;s:1:\"0\";i:1;s:8:\"4.10.1.2\";i:2;s:6:\"4.10.2\";}s:28:\"latest_event_tickets_version\";s:6:\"4.10.6\";s:33:\"last-update-message-event-tickets\";s:6:\"4.10.2\";s:13:\"earliest_date\";s:19:\"2019-04-10 00:00:00\";s:21:\"earliest_date_markers\";a:1:{i:0;s:2:\"27\";}s:11:\"latest_date\";s:19:\"2019-05-01 23:59:59\";s:19:\"latest_date_markers\";a:1:{i:0;s:2:\"34\";}s:16:\"tribeEnableViews\";a:3:{i:0;s:4:\"list\";i:1;s:5:\"month\";i:2;s:3:\"day\";}s:22:\"google_maps_js_api_key\";s:39:\"AIzaSyDNsicAsP6-VuGtAb1O9riI3oc_NOb7IOU\";s:33:\"event-tickets-plus-schema-version\";s:6:\"4.10.5\";s:28:\"event-tickets-schema-version\";s:6:\"4.10.6\";s:11:\"donate-link\";b:0;s:12:\"postsPerPage\";s:2:\"10\";s:17:\"liveFiltersUpdate\";b:1;s:20:\"toggle_blocks_editor\";b:0;s:33:\"toggle_blocks_editor_hidden_field\";b:1;s:12:\"showComments\";b:0;s:29:\"disable_metabox_custom_fields\";b:1;s:20:\"showEventsInMainLoop\";b:0;s:10:\"eventsSlug\";s:6:\"events\";s:15:\"singleEventSlug\";s:5:\"event\";s:14:\"multiDayCutoff\";s:5:\"00:00\";s:21:\"defaultCurrencySymbol\";s:1:\"$\";s:23:\"reverseCurrencyPosition\";b:0;s:17:\"trash-past-events\";s:0:\"\";s:18:\"delete-past-events\";s:0:\"\";s:15:\"embedGoogleMaps\";b:1;s:19:\"embedGoogleMapsZoom\";s:2:\"10\";s:11:\"debugEvents\";b:0;s:26:\"tribe_events_timezone_mode\";s:5:\"event\";s:32:\"tribe_events_timezones_show_zone\";b:0;s:24:\"front_page_event_archive\";b:0;}', 'yes'),
(395, 'widget_tribe-events-list-widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(396, 'tribe_last_save_post', '1559846960', 'yes'),
(466, 'jetpack_constants_sync_checksum', 'a:31:{s:16:\"EMPTY_TRASH_DAYS\";i:2473281379;s:17:\"WP_POST_REVISIONS\";i:4261170317;s:26:\"AUTOMATIC_UPDATER_DISABLED\";i:634125391;s:7:\"ABSPATH\";i:349357834;s:14:\"WP_CONTENT_DIR\";i:3188548084;s:9:\"FS_METHOD\";i:634125391;s:18:\"DISALLOW_FILE_EDIT\";i:634125391;s:18:\"DISALLOW_FILE_MODS\";i:634125391;s:19:\"WP_AUTO_UPDATE_CORE\";i:634125391;s:22:\"WP_HTTP_BLOCK_EXTERNAL\";i:634125391;s:19:\"WP_ACCESSIBLE_HOSTS\";i:634125391;s:16:\"JETPACK__VERSION\";i:3417957315;s:12:\"IS_PRESSABLE\";i:634125391;s:15:\"DISABLE_WP_CRON\";i:634125391;s:17:\"ALTERNATE_WP_CRON\";i:634125391;s:20:\"WP_CRON_LOCK_TIMEOUT\";i:3994858278;s:11:\"PHP_VERSION\";i:297655616;s:15:\"WP_MEMORY_LIMIT\";i:3065409971;s:19:\"WP_MAX_MEMORY_LIMIT\";i:1474498405;s:14:\"WC_PLUGIN_FILE\";i:2286215777;s:10:\"WC_ABSPATH\";i:1661258440;s:18:\"WC_PLUGIN_BASENAME\";i:1149093810;s:10:\"WC_VERSION\";i:2416681029;s:19:\"WOOCOMMERCE_VERSION\";i:2416681029;s:21:\"WC_ROUNDING_PRECISION\";i:498629140;s:25:\"WC_DISCOUNT_ROUNDING_MODE\";i:450215437;s:20:\"WC_TAX_ROUNDING_MODE\";i:2212294583;s:12:\"WC_DELIMITER\";i:2455911554;s:10:\"WC_LOG_DIR\";i:955136587;s:22:\"WC_SESSION_CACHE_GROUP\";i:4278978988;s:22:\"WC_TEMPLATE_DEBUG_MODE\";i:734881840;}', 'yes'),
(469, 'jetpack_sync_https_history_main_network_site_url', 'a:1:{i:0;s:4:\"http\";}', 'yes'),
(470, 'jetpack_sync_https_history_site_url', 'a:2:{i:0;s:4:\"http\";i:1;s:4:\"http\";}', 'yes'),
(471, 'jetpack_sync_https_history_home_url', 'a:2:{i:0;s:4:\"http\";i:1;s:4:\"http\";}', 'yes'),
(507, 'jetpack_callables_sync_checksum', 'a:33:{s:18:\"wp_max_upload_size\";i:677931734;s:15:\"is_main_network\";i:734881840;s:13:\"is_multi_site\";i:734881840;s:17:\"main_network_site\";i:2999117530;s:8:\"site_url\";i:2999117530;s:8:\"home_url\";i:2999117530;s:16:\"single_user_site\";i:4261170317;s:7:\"updates\";i:3425443202;s:28:\"has_file_system_write_access\";i:4261170317;s:21:\"is_version_controlled\";i:734881840;s:10:\"taxonomies\";i:2079173225;s:10:\"post_types\";i:1628945042;s:18:\"post_type_features\";i:731968993;s:10:\"shortcodes\";i:1657596888;s:27:\"rest_api_allowed_post_types\";i:4281438522;s:32:\"rest_api_allowed_public_metadata\";i:223132457;s:24:\"sso_is_two_step_required\";i:734881840;s:26:\"sso_should_hide_login_form\";i:734881840;s:18:\"sso_match_by_email\";i:4261170317;s:21:\"sso_new_user_override\";i:734881840;s:29:\"sso_bypass_default_login_form\";i:734881840;s:10:\"wp_version\";i:2255569608;s:11:\"get_plugins\";i:2334684527;s:24:\"get_plugins_action_links\";i:223132457;s:14:\"active_modules\";i:223132457;s:16:\"hosting_provider\";i:769900095;s:6:\"locale\";i:110763218;s:13:\"site_icon_url\";i:734881840;s:5:\"roles\";i:4060916478;s:8:\"timezone\";i:3808505409;s:24:\"available_jetpack_blocks\";i:2258760174;s:13:\"paused_themes\";i:734881840;s:14:\"paused_plugins\";i:734881840;}', 'no'),
(508, 'jpsq_sync_checkout', '0:0', 'no'),
(511, 'jetpack_plugin_api_action_links', 'a:5:{s:19:\"jetpack/jetpack.php\";a:3:{s:7:\"Jetpack\";s:53:\"https://wp2.dev-c01.muulabs.pl/wp-admin/admin.php?page=jetpack\";s:8:\"Settings\";s:63:\"https://wp2.dev-c01.muulabs.pl/wp-admin/admin.php?page=jetpack#/settings\";s:7:\"Support\";s:62:\"https://wp2.dev-c01.muulabs.pl/wp-admin/admin.php?page=jetpack-debugger\";}s:67:\"klarna-checkout-for-woocommerce/klarna-checkout-for-woocommerce.php\";a:2:{s:8:\"Settings\";s:82:\"https://wp2.dev-c01.muulabs.pl/wp-admin/admin.php?page=wc-settings&tab=checkout&section=kco\";s:7:\"Support\";s:19:\"http://krokedil.se/\";}s:43:\"the-events-calendar/the-events-calendar.php\";a:2:{s:8:\"Settings\";s:80:\"https://wp2.dev-c01.muulabs.pl/wp-admin/edit.php?post_type=tribe_events&page=tribe-common\";s:8:\"Calendar\";s:29:\"https://wp2.dev-c01.muulabs.pl/events/\";}s:27:\"woocommerce/woocommerce.php\";a:1:{s:8:\"Settings\";s:57:\"https://wp2.dev-c01.muulabs.pl/wp-admin/admin.php?page=wc-settings\";}s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";a:2:{s:8:\"Settings\";s:90:\"https://wp2.dev-c01.muulabs.pl/wp-admin/admin.php?page=wc-settings&tab=checkout&section=ppec_paypal\";s:4:\"Docs\";s:62:\"https://docs.woocommerce.com/document/paypal-express-checkout/\";}}', 'yes'),
(534, 'storefront_nux_dismissed', '1', 'yes'),
(535, 'storefront_nux_guided_tour', '1', 'yes'),
(577, 'pue_install_key_event_tickets_plus', 'f4b28dcb33fe101fb6e0c94d957f73430114aab9', 'yes'),
(578, 'tribe_pue_key_notices', 'a:0:{}', 'yes'),
(602, 'external_updates-event-aggregator', 'O:8:\"stdClass\":3:{s:9:\"lastCheck\";i:1552961163;s:14:\"checkedVersion\";N;s:6:\"update\";O:19:\"Tribe__PUE__Utility\":13:{s:2:\"id\";i:0;s:6:\"plugin\";N;s:4:\"slug\";N;s:7:\"version\";s:3:\"1.0\";s:8:\"homepage\";N;s:12:\"download_url\";N;s:8:\"sections\";O:8:\"stdClass\":1:{s:9:\"changelog\";s:0:\"\";}s:14:\"upgrade_notice\";N;s:13:\"custom_update\";N;s:11:\"api_invalid\";b:1;s:19:\"api_invalid_message\";s:214:\"<p>You are using %plugin_name% but your license key is invalid. Visit the Events Calendar website to check your <a href=\"https://theeventscalendar.com/license-keys/?utm_medium=pue&utm_campaign=in-app\">licenses</a>.\";s:26:\"api_inline_invalid_message\";s:196:\"<p>Your %plugin_name% license key is invalid. Visit the Events Calendar website to check your <a href=\"https://theeventscalendar.com/license-keys/?utm_medium=pue&utm_campaign=in-app\">licenses</a>.\";s:13:\"license_error\";s:203:\"<p>Your Event Aggregator license key is invalid. Visit the Events Calendar website to check your <a href=\"https://theeventscalendar.com/license-keys/?utm_medium=pue&amp;utm_campaign=in-app\">licenses</a>.\";}}', 'no'),
(605, 'external_updates-event-tickets-plus', 'O:8:\"stdClass\":3:{s:9:\"lastCheck\";i:1564309871;s:14:\"checkedVersion\";s:6:\"4.10.5\";s:6:\"update\";O:19:\"Tribe__PUE__Utility\":11:{s:2:\"id\";i:0;s:6:\"plugin\";s:41:\"event-tickets-plus/event-tickets-plus.php\";s:4:\"slug\";s:18:\"event-tickets-plus\";s:7:\"version\";s:8:\"4.10.5.1\";s:8:\"homepage\";s:20:\"http://m.tri.be/18wg\";s:12:\"download_url\";s:284:\"https://pue.tri.be/api/plugins/v2/download?plugin=event-tickets-plus&version=4.10.5.1&installed_version=4.10.5&domain=localhost&multisite=0&network_activated=0&active_sites=1&wp_version=5.1.1&key=f4b28dcb33fe101fb6e0c94d957f73430114aab9&dk=f4b28dcb33fe101fb6e0c94d957f73430114aab9&o=m\";s:8:\"sections\";O:8:\"stdClass\":3:{s:11:\"description\";s:154:\"Event Tickets Plus adds features and functionality onto the core Event Tickets plugin, so you can sell tickets with WooCommerce or Easy Digital Downloads.\";s:12:\"installation\";s:348:\"Installing Events Tickets Plus is easy: just back up your site, download/install Event Tickets from the WordPress.org repo, and download/install Events Ticket Plus from theeventscalendar.com. Activate them both and you\'ll be good to go! If you\'re still confused or encounter problems, check out part 1 of our new user primer (http://m.tri.be/18ve).\";s:9:\"changelog\";s:566:\"<p>= [4.10.5.1] 2019-06-13 =</p>\r\n\r\n<ul>\r\n<li>Fix - Correct broken attendee list in classic editor [128946]</li>\r\n<li>Fix - Account for visitors getting to the WooCommerce cart or checkout screens in a manner other than our checkout flow, such as directly visiting the URL [128505]</li>\r\n<li>Fix - Correctly query and join the necessary tables when WooCommerce or EDD are not activated for queries against <code>purchaser_name</code> or <code>purchaser_email</code> [128881]</li>\r\n<li>Language - 0 new strings added, 2 updated, 0 fuzzied, and 0 obsoleted</li>\r\n</ul>\";}s:14:\"upgrade_notice\";s:0:\"\";s:13:\"custom_update\";O:8:\"stdClass\":1:{s:5:\"icons\";O:8:\"stdClass\":1:{s:3:\"svg\";s:92:\"https://theeventscalendar.com/content/themes/tribe-ecp/img/svg/product-icons/ticketsplus.svg\";}}s:11:\"api_expired\";b:0;s:11:\"api_upgrade\";b:0;}}', 'no'),
(668, 'product_cat_children', 'a:0:{}', 'yes'),
(717, 'category_children', 'a:0:{}', 'yes'),
(773, 'nav_menu_options', 'a:1:{s:8:\"auto_add\";a:0:{}}', 'yes'),
(796, 'tribe_events_cat_children', 'a:0:{}', 'yes'),
(812, 'woocommerce_version', '3.5.7', 'yes'),
(813, 'woocommerce_db_version', '3.5.7', 'yes'),
(1084, '_transient_orders-transient-version', '1558328492', 'yes'),
(1089, 'woocommerce_ppec_payer_id_sandbox_329dbe827de0ff0adbd1aa39a525885c', 'H383ZDR9LCLEQ', 'yes'),
(1093, '_transient_tribe_ticket_prefix_pool', 'a:1:{s:1:\"A\";i:27;}', 'yes'),
(1164, 'wp_mail_smtp_initial_version', '1.4.2', 'no'),
(1165, 'wp_mail_smtp_version', '1.4.2', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1166, 'wp_mail_smtp', 'a:5:{s:4:\"mail\";a:6:{s:10:\"from_email\";s:26:\"tobiaszkochanski@gmail.com\";s:9:\"from_name\";s:12:\"MindEngineer\";s:6:\"mailer\";s:4:\"smtp\";s:11:\"return_path\";b:0;s:16:\"from_email_force\";b:0;s:15:\"from_name_force\";b:0;}s:4:\"smtp\";a:7:{s:7:\"autotls\";b:1;s:4:\"host\";s:17:\"smtp.sendgrid.net\";s:10:\"encryption\";s:3:\"tls\";s:4:\"port\";i:587;s:4:\"auth\";b:1;s:4:\"user\";s:8:\"TunityNV\";s:4:\"pass\";s:17:\"tunityNutricoach1\";}s:5:\"gmail\";a:2:{s:9:\"client_id\";s:0:\"\";s:13:\"client_secret\";s:0:\"\";}s:7:\"mailgun\";a:3:{s:7:\"api_key\";s:0:\"\";s:6:\"domain\";s:0:\"\";s:6:\"region\";s:2:\"US\";}s:8:\"sendgrid\";a:1:{s:7:\"api_key\";s:0:\"\";}}', 'no'),
(1167, '_amn_smtp_last_checked', '1564272000', 'yes'),
(1168, 'wp_mail_smtp_debug', 'a:0:{}', 'no'),
(1237, 'rewrite_rules', 'a:364:{s:21:\"tickets/([0-9]{1,})/?\";s:43:\"index.php?p=$matches[1]&tribe-edit-orders=1\";s:28:\"event-aggregator/(insert)/?$\";s:53:\"index.php?tribe-aggregator=1&tribe-action=$matches[1]\";s:25:\"(?:event)/([^/]+)/ical/?$\";s:56:\"index.php?ical=1&name=$matches[1]&post_type=tribe_events\";s:28:\"(?:events)/(?:page)/(\\d+)/?$\";s:68:\"index.php?post_type=tribe_events&eventDisplay=list&paged=$matches[1]\";s:41:\"(?:events)/(?:featured)/(?:page)/(\\d+)/?$\";s:79:\"index.php?post_type=tribe_events&featured=1&eventDisplay=list&paged=$matches[1]\";s:38:\"(?:events)/(feed|rdf|rss|rss2|atom)/?$\";s:67:\"index.php?post_type=tribe_events&eventDisplay=list&feed=$matches[1]\";s:51:\"(?:events)/(?:featured)/(feed|rdf|rss|rss2|atom)/?$\";s:78:\"index.php?post_type=tribe_events&featured=1&eventDisplay=list&feed=$matches[1]\";s:23:\"(?:events)/(?:month)/?$\";s:51:\"index.php?post_type=tribe_events&eventDisplay=month\";s:36:\"(?:events)/(?:month)/(?:featured)/?$\";s:62:\"index.php?post_type=tribe_events&eventDisplay=month&featured=1\";s:37:\"(?:events)/(?:month)/(\\d{4}-\\d{2})/?$\";s:73:\"index.php?post_type=tribe_events&eventDisplay=month&eventDate=$matches[1]\";s:37:\"(?:events)/(?:list)/(?:page)/(\\d+)/?$\";s:68:\"index.php?post_type=tribe_events&eventDisplay=list&paged=$matches[1]\";s:50:\"(?:events)/(?:list)/(?:featured)/(?:page)/(\\d+)/?$\";s:79:\"index.php?post_type=tribe_events&eventDisplay=list&featured=1&paged=$matches[1]\";s:22:\"(?:events)/(?:list)/?$\";s:50:\"index.php?post_type=tribe_events&eventDisplay=list\";s:35:\"(?:events)/(?:list)/(?:featured)/?$\";s:61:\"index.php?post_type=tribe_events&eventDisplay=list&featured=1\";s:23:\"(?:events)/(?:today)/?$\";s:49:\"index.php?post_type=tribe_events&eventDisplay=day\";s:36:\"(?:events)/(?:today)/(?:featured)/?$\";s:60:\"index.php?post_type=tribe_events&eventDisplay=day&featured=1\";s:27:\"(?:events)/(\\d{4}-\\d{2})/?$\";s:73:\"index.php?post_type=tribe_events&eventDisplay=month&eventDate=$matches[1]\";s:40:\"(?:events)/(\\d{4}-\\d{2})/(?:featured)/?$\";s:84:\"index.php?post_type=tribe_events&eventDisplay=month&eventDate=$matches[1]&featured=1\";s:33:\"(?:events)/(\\d{4}-\\d{2}-\\d{2})/?$\";s:71:\"index.php?post_type=tribe_events&eventDisplay=day&eventDate=$matches[1]\";s:46:\"(?:events)/(\\d{4}-\\d{2}-\\d{2})/(?:featured)/?$\";s:82:\"index.php?post_type=tribe_events&eventDisplay=day&eventDate=$matches[1]&featured=1\";s:26:\"(?:events)/(?:featured)/?$\";s:43:\"index.php?post_type=tribe_events&featured=1\";s:13:\"(?:events)/?$\";s:53:\"index.php?post_type=tribe_events&eventDisplay=default\";s:18:\"(?:events)/ical/?$\";s:39:\"index.php?post_type=tribe_events&ical=1\";s:31:\"(?:events)/(?:featured)/ical/?$\";s:50:\"index.php?post_type=tribe_events&ical=1&featured=1\";s:38:\"(?:events)/(\\d{4}-\\d{2}-\\d{2})/ical/?$\";s:78:\"index.php?post_type=tribe_events&ical=1&eventDisplay=day&eventDate=$matches[1]\";s:47:\"(?:events)/(\\d{4}-\\d{2}-\\d{2})/ical/featured/?$\";s:89:\"index.php?post_type=tribe_events&ical=1&eventDisplay=day&eventDate=$matches[1]&featured=1\";s:60:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/(?:page)/(\\d+)/?$\";s:97:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&eventDisplay=list&paged=$matches[2]\";s:73:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/(?:featured)/(?:page)/(\\d+)/?$\";s:108:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&featured=1&eventDisplay=list&paged=$matches[2]\";s:55:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/(?:month)/?$\";s:80:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&eventDisplay=month\";s:68:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/(?:month)/(?:featured)/?$\";s:91:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&eventDisplay=month&featured=1\";s:69:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/(?:list)/(?:page)/(\\d+)/?$\";s:97:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&eventDisplay=list&paged=$matches[2]\";s:82:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/(?:list)/(?:featured)/(?:page)/(\\d+)/?$\";s:108:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&eventDisplay=list&featured=1&paged=$matches[2]\";s:54:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/(?:list)/?$\";s:79:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&eventDisplay=list\";s:67:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/(?:list)/(?:featured)/?$\";s:90:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&eventDisplay=list&featured=1\";s:55:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/(?:today)/?$\";s:78:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&eventDisplay=day\";s:68:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/(?:today)/(?:featured)/?$\";s:89:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&eventDisplay=day&featured=1\";s:73:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/(?:day)/(\\d{4}-\\d{2}-\\d{2})/?$\";s:100:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&eventDisplay=day&eventDate=$matches[2]\";s:86:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/(?:day)/(\\d{4}-\\d{2}-\\d{2})/(?:featured)/?$\";s:111:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&eventDisplay=day&eventDate=$matches[2]&featured=1\";s:59:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/(\\d{4}-\\d{2})/?$\";s:102:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&eventDisplay=month&eventDate=$matches[2]\";s:72:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/(\\d{4}-\\d{2})/(?:featured)/?$\";s:113:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&eventDisplay=month&eventDate=$matches[2]&featured=1\";s:65:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/(\\d{4}-\\d{2}-\\d{2})/?$\";s:100:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&eventDisplay=day&eventDate=$matches[2]\";s:78:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/(\\d{4}-\\d{2}-\\d{2})/(?:featured)/?$\";s:111:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&eventDisplay=day&eventDate=$matches[2]&featured=1\";s:50:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/feed/?$\";s:89:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&eventDisplay=list&feed=rss2\";s:63:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/(?:featured)/feed/?$\";s:100:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&featured=1&eventDisplay=list&feed=rss2\";s:50:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/ical/?$\";s:68:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&ical=1\";s:63:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/(?:featured)/ical/?$\";s:79:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&featured=1&ical=1\";s:75:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:78:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&feed=$matches[2]\";s:88:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/(?:featured)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:89:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&featured=1&feed=$matches[2]\";s:58:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/(?:featured)/?$\";s:93:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&featured=1&eventDisplay=default\";s:45:\"(?:events)/(?:category)/(?:[^/]+/)*([^/]+)/?$\";s:82:\"index.php?post_type=tribe_events&tribe_events_cat=$matches[1]&eventDisplay=default\";s:44:\"(?:events)/(?:tag)/([^/]+)/(?:page)/(\\d+)/?$\";s:84:\"index.php?post_type=tribe_events&tag=$matches[1]&eventDisplay=list&paged=$matches[2]\";s:57:\"(?:events)/(?:tag)/([^/]+)/(?:featured)/(?:page)/(\\d+)/?$\";s:95:\"index.php?post_type=tribe_events&tag=$matches[1]&featured=1&eventDisplay=list&paged=$matches[2]\";s:39:\"(?:events)/(?:tag)/([^/]+)/(?:month)/?$\";s:67:\"index.php?post_type=tribe_events&tag=$matches[1]&eventDisplay=month\";s:52:\"(?:events)/(?:tag)/([^/]+)/(?:month)/(?:featured)/?$\";s:78:\"index.php?post_type=tribe_events&tag=$matches[1]&eventDisplay=month&featured=1\";s:53:\"(?:events)/(?:tag)/([^/]+)/(?:list)/(?:page)/(\\d+)/?$\";s:84:\"index.php?post_type=tribe_events&tag=$matches[1]&eventDisplay=list&paged=$matches[2]\";s:66:\"(?:events)/(?:tag)/([^/]+)/(?:list)/(?:featured)/(?:page)/(\\d+)/?$\";s:95:\"index.php?post_type=tribe_events&tag=$matches[1]&eventDisplay=list&featured=1&paged=$matches[2]\";s:38:\"(?:events)/(?:tag)/([^/]+)/(?:list)/?$\";s:66:\"index.php?post_type=tribe_events&tag=$matches[1]&eventDisplay=list\";s:51:\"(?:events)/(?:tag)/([^/]+)/(?:list)/(?:featured)/?$\";s:77:\"index.php?post_type=tribe_events&tag=$matches[1]&eventDisplay=list&featured=1\";s:39:\"(?:events)/(?:tag)/([^/]+)/(?:today)/?$\";s:65:\"index.php?post_type=tribe_events&tag=$matches[1]&eventDisplay=day\";s:52:\"(?:events)/(?:tag)/([^/]+)/(?:today)/(?:featured)/?$\";s:76:\"index.php?post_type=tribe_events&tag=$matches[1]&eventDisplay=day&featured=1\";s:57:\"(?:events)/(?:tag)/([^/]+)/(?:day)/(\\d{4}-\\d{2}-\\d{2})/?$\";s:87:\"index.php?post_type=tribe_events&tag=$matches[1]&eventDisplay=day&eventDate=$matches[2]\";s:70:\"(?:events)/(?:tag)/([^/]+)/(?:day)/(\\d{4}-\\d{2}-\\d{2})/(?:featured)/?$\";s:98:\"index.php?post_type=tribe_events&tag=$matches[1]&eventDisplay=day&eventDate=$matches[2]&featured=1\";s:43:\"(?:events)/(?:tag)/([^/]+)/(\\d{4}-\\d{2})/?$\";s:89:\"index.php?post_type=tribe_events&tag=$matches[1]&eventDisplay=month&eventDate=$matches[2]\";s:56:\"(?:events)/(?:tag)/([^/]+)/(\\d{4}-\\d{2})/(?:featured)/?$\";s:100:\"index.php?post_type=tribe_events&tag=$matches[1]&eventDisplay=month&eventDate=$matches[2]&featured=1\";s:49:\"(?:events)/(?:tag)/([^/]+)/(\\d{4}-\\d{2}-\\d{2})/?$\";s:87:\"index.php?post_type=tribe_events&tag=$matches[1]&eventDisplay=day&eventDate=$matches[2]\";s:62:\"(?:events)/(?:tag)/([^/]+)/(\\d{4}-\\d{2}-\\d{2})/(?:featured)/?$\";s:98:\"index.php?post_type=tribe_events&tag=$matches[1]&eventDisplay=day&eventDate=$matches[2]&featured=1\";s:34:\"(?:events)/(?:tag)/([^/]+)/feed/?$\";s:76:\"index.php?post_type=tribe_events&tag=$matches[1]&eventDisplay=list&feed=rss2\";s:47:\"(?:events)/(?:tag)/([^/]+)/(?:featured)/feed/?$\";s:87:\"index.php?post_type=tribe_events&tag=$matches[1]&eventDisplay=list&feed=rss2&featured=1\";s:34:\"(?:events)/(?:tag)/([^/]+)/ical/?$\";s:55:\"index.php?post_type=tribe_events&tag=$matches[1]&ical=1\";s:47:\"(?:events)/(?:tag)/([^/]+)/(?:featured)/ical/?$\";s:66:\"index.php?post_type=tribe_events&tag=$matches[1]&featured=1&ical=1\";s:59:\"(?:events)/(?:tag)/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:65:\"index.php?post_type=tribe_events&tag=$matches[1]&feed=$matches[2]\";s:72:\"(?:events)/(?:tag)/([^/]+)/(?:featured)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:76:\"index.php?post_type=tribe_events&tag=$matches[1]&featured=1&feed=$matches[2]\";s:42:\"(?:events)/(?:tag)/([^/]+)/(?:featured)/?$\";s:59:\"index.php?post_type=tribe_events&tag=$matches[1]&featured=1\";s:29:\"(?:events)/(?:tag)/([^/]+)/?$\";s:69:\"index.php?post_type=tribe_events&tag=$matches[1]&eventDisplay=default\";s:34:\"(?:event)/([^/]+)/{{ tickets }}/?$\";s:78:\"index.php?tribe_events=$matches[1]&post_type=tribe_events&eventDisplay=tickets\";s:54:\"(?:event)/([^/]+)/(\\d{4}-\\d{2}-\\d{2})/{{ tickets }}/?$\";s:100:\"index.php?tribe_events=$matches[1]&eventDate=$matches[2]&post_type=tribe_events&eventDisplay=tickets\";s:29:\"(?:attendee\\-registration)/?$\";s:33:\"index.php?attendee-registration=1\";s:24:\"^wc-auth/v([1]{1})/(.*)?\";s:63:\"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]\";s:22:\"^wc-api/v([1-3]{1})/?$\";s:51:\"index.php?wc-api-version=$matches[1]&wc-api-route=/\";s:24:\"^wc-api/v([1-3]{1})(.*)?\";s:61:\"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]\";s:7:\"shop/?$\";s:27:\"index.php?post_type=product\";s:37:\"shop/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:32:\"shop/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:24:\"shop/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=product&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:22:\"tribe-promoter-auth/?$\";s:37:\"index.php?tribe-promoter-auth-check=1\";s:8:\"event/?$\";s:32:\"index.php?post_type=tribe_events\";s:38:\"event/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?post_type=tribe_events&feed=$matches[1]\";s:33:\"event/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?post_type=tribe_events&feed=$matches[1]\";s:25:\"event/page/([0-9]{1,})/?$\";s:50:\"index.php?post_type=tribe_events&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:32:\"category/(.+?)/wc-api(/(.*))?/?$\";s:54:\"index.php?category_name=$matches[1]&wc-api=$matches[3]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:29:\"tag/([^/]+)/wc-api(/(.*))?/?$\";s:44:\"index.php?tag=$matches[1]&wc-api=$matches[3]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:55:\"product-category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:50:\"product-category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:31:\"product-category/(.+?)/embed/?$\";s:44:\"index.php?product_cat=$matches[1]&embed=true\";s:43:\"product-category/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:25:\"product-category/(.+?)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:52:\"product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:47:\"product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:28:\"product-tag/([^/]+)/embed/?$\";s:44:\"index.php?product_tag=$matches[1]&embed=true\";s:40:\"product-tag/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_tag=$matches[1]&paged=$matches[2]\";s:22:\"product-tag/([^/]+)/?$\";s:33:\"index.php?product_tag=$matches[1]\";s:35:\"product/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"product/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"product/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"product/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"product/([^/]+)/embed/?$\";s:40:\"index.php?product=$matches[1]&embed=true\";s:28:\"product/([^/]+)/trackback/?$\";s:34:\"index.php?product=$matches[1]&tb=1\";s:48:\"product/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:43:\"product/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:36:\"product/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&paged=$matches[2]\";s:43:\"product/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&cpage=$matches[2]\";s:33:\"product/([^/]+)/wc-api(/(.*))?/?$\";s:48:\"index.php?product=$matches[1]&wc-api=$matches[3]\";s:39:\"product/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:50:\"product/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:32:\"product/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?product=$matches[1]&page=$matches[2]\";s:24:\"product/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"product/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"product/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:30:\"product/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:48:\"ticket-meta-fieldset/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:58:\"ticket-meta-fieldset/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:78:\"ticket-meta-fieldset/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:73:\"ticket-meta-fieldset/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:73:\"ticket-meta-fieldset/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:54:\"ticket-meta-fieldset/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:37:\"ticket-meta-fieldset/([^/]+)/embed/?$\";s:53:\"index.php?ticket-meta-fieldset=$matches[1]&embed=true\";s:41:\"ticket-meta-fieldset/([^/]+)/trackback/?$\";s:47:\"index.php?ticket-meta-fieldset=$matches[1]&tb=1\";s:49:\"ticket-meta-fieldset/([^/]+)/page/?([0-9]{1,})/?$\";s:60:\"index.php?ticket-meta-fieldset=$matches[1]&paged=$matches[2]\";s:56:\"ticket-meta-fieldset/([^/]+)/comment-page-([0-9]{1,})/?$\";s:60:\"index.php?ticket-meta-fieldset=$matches[1]&cpage=$matches[2]\";s:46:\"ticket-meta-fieldset/([^/]+)/wc-api(/(.*))?/?$\";s:61:\"index.php?ticket-meta-fieldset=$matches[1]&wc-api=$matches[3]\";s:52:\"ticket-meta-fieldset/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:63:\"ticket-meta-fieldset/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:45:\"ticket-meta-fieldset/([^/]+)(?:/([0-9]+))?/?$\";s:59:\"index.php?ticket-meta-fieldset=$matches[1]&page=$matches[2]\";s:37:\"ticket-meta-fieldset/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:47:\"ticket-meta-fieldset/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:67:\"ticket-meta-fieldset/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"ticket-meta-fieldset/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"ticket-meta-fieldset/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:43:\"ticket-meta-fieldset/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:33:\"venue/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:43:\"venue/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:63:\"venue/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"venue/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"venue/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:39:\"venue/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:22:\"venue/([^/]+)/embed/?$\";s:44:\"index.php?tribe_venue=$matches[1]&embed=true\";s:26:\"venue/([^/]+)/trackback/?$\";s:38:\"index.php?tribe_venue=$matches[1]&tb=1\";s:34:\"venue/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?tribe_venue=$matches[1]&paged=$matches[2]\";s:41:\"venue/([^/]+)/comment-page-([0-9]{1,})/?$\";s:51:\"index.php?tribe_venue=$matches[1]&cpage=$matches[2]\";s:31:\"venue/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?tribe_venue=$matches[1]&wc-api=$matches[3]\";s:37:\"venue/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:48:\"venue/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:30:\"venue/([^/]+)(?:/([0-9]+))?/?$\";s:50:\"index.php?tribe_venue=$matches[1]&page=$matches[2]\";s:22:\"venue/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:32:\"venue/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:52:\"venue/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:47:\"venue/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:47:\"venue/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:28:\"venue/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:37:\"organizer/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:47:\"organizer/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:67:\"organizer/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"organizer/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"organizer/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:43:\"organizer/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:26:\"organizer/([^/]+)/embed/?$\";s:48:\"index.php?tribe_organizer=$matches[1]&embed=true\";s:30:\"organizer/([^/]+)/trackback/?$\";s:42:\"index.php?tribe_organizer=$matches[1]&tb=1\";s:38:\"organizer/([^/]+)/page/?([0-9]{1,})/?$\";s:55:\"index.php?tribe_organizer=$matches[1]&paged=$matches[2]\";s:45:\"organizer/([^/]+)/comment-page-([0-9]{1,})/?$\";s:55:\"index.php?tribe_organizer=$matches[1]&cpage=$matches[2]\";s:35:\"organizer/([^/]+)/wc-api(/(.*))?/?$\";s:56:\"index.php?tribe_organizer=$matches[1]&wc-api=$matches[3]\";s:41:\"organizer/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:52:\"organizer/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:34:\"organizer/([^/]+)(?:/([0-9]+))?/?$\";s:54:\"index.php?tribe_organizer=$matches[1]&page=$matches[2]\";s:26:\"organizer/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:36:\"organizer/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:56:\"organizer/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:51:\"organizer/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:51:\"organizer/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:32:\"organizer/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:33:\"event/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:43:\"event/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:63:\"event/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"event/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"event/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:39:\"event/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:22:\"event/([^/]+)/embed/?$\";s:45:\"index.php?tribe_events=$matches[1]&embed=true\";s:26:\"event/([^/]+)/trackback/?$\";s:39:\"index.php?tribe_events=$matches[1]&tb=1\";s:46:\"event/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?tribe_events=$matches[1]&feed=$matches[2]\";s:41:\"event/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?tribe_events=$matches[1]&feed=$matches[2]\";s:34:\"event/([^/]+)/page/?([0-9]{1,})/?$\";s:52:\"index.php?tribe_events=$matches[1]&paged=$matches[2]\";s:41:\"event/([^/]+)/comment-page-([0-9]{1,})/?$\";s:52:\"index.php?tribe_events=$matches[1]&cpage=$matches[2]\";s:31:\"event/([^/]+)/wc-api(/(.*))?/?$\";s:53:\"index.php?tribe_events=$matches[1]&wc-api=$matches[3]\";s:37:\"event/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:48:\"event/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:30:\"event/([^/]+)(?:/([0-9]+))?/?$\";s:51:\"index.php?tribe_events=$matches[1]&page=$matches[2]\";s:22:\"event/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:32:\"event/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:52:\"event/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:47:\"event/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:47:\"event/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:28:\"event/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:54:\"events/category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:55:\"index.php?tribe_events_cat=$matches[1]&feed=$matches[2]\";s:49:\"events/category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:55:\"index.php?tribe_events_cat=$matches[1]&feed=$matches[2]\";s:30:\"events/category/(.+?)/embed/?$\";s:49:\"index.php?tribe_events_cat=$matches[1]&embed=true\";s:42:\"events/category/(.+?)/page/?([0-9]{1,})/?$\";s:56:\"index.php?tribe_events_cat=$matches[1]&paged=$matches[2]\";s:24:\"events/category/(.+?)/?$\";s:38:\"index.php?tribe_events_cat=$matches[1]\";s:41:\"deleted_event/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:51:\"deleted_event/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:71:\"deleted_event/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"deleted_event/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"deleted_event/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:47:\"deleted_event/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:30:\"deleted_event/([^/]+)/embed/?$\";s:46:\"index.php?deleted_event=$matches[1]&embed=true\";s:34:\"deleted_event/([^/]+)/trackback/?$\";s:40:\"index.php?deleted_event=$matches[1]&tb=1\";s:42:\"deleted_event/([^/]+)/page/?([0-9]{1,})/?$\";s:53:\"index.php?deleted_event=$matches[1]&paged=$matches[2]\";s:49:\"deleted_event/([^/]+)/comment-page-([0-9]{1,})/?$\";s:53:\"index.php?deleted_event=$matches[1]&cpage=$matches[2]\";s:39:\"deleted_event/([^/]+)/wc-api(/(.*))?/?$\";s:54:\"index.php?deleted_event=$matches[1]&wc-api=$matches[3]\";s:45:\"deleted_event/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:56:\"deleted_event/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:38:\"deleted_event/([^/]+)(?:/([0-9]+))?/?$\";s:52:\"index.php?deleted_event=$matches[1]&page=$matches[2]\";s:30:\"deleted_event/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:40:\"deleted_event/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:60:\"deleted_event/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"deleted_event/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"deleted_event/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:36:\"deleted_event/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:36:\"amn_smtp/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"amn_smtp/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"amn_smtp/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"amn_smtp/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"amn_smtp/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"amn_smtp/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"amn_smtp/([^/]+)/embed/?$\";s:41:\"index.php?amn_smtp=$matches[1]&embed=true\";s:29:\"amn_smtp/([^/]+)/trackback/?$\";s:35:\"index.php?amn_smtp=$matches[1]&tb=1\";s:37:\"amn_smtp/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?amn_smtp=$matches[1]&paged=$matches[2]\";s:44:\"amn_smtp/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?amn_smtp=$matches[1]&cpage=$matches[2]\";s:34:\"amn_smtp/([^/]+)/wc-api(/(.*))?/?$\";s:49:\"index.php?amn_smtp=$matches[1]&wc-api=$matches[3]\";s:40:\"amn_smtp/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:51:\"amn_smtp/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:33:\"amn_smtp/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?amn_smtp=$matches[1]&page=$matches[2]\";s:25:\"amn_smtp/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"amn_smtp/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"amn_smtp/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"amn_smtp/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"amn_smtp/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"amn_smtp/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=15&cpage=$matches[1]\";s:17:\"wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:26:\"comments/wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:29:\"search/(.+)/wc-api(/(.*))?/?$\";s:42:\"index.php?s=$matches[1]&wc-api=$matches[3]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:32:\"author/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?author_name=$matches[1]&wc-api=$matches[3]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:54:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:41:\"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:66:\"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:28:\"([0-9]{4})/wc-api(/(.*))?/?$\";s:45:\"index.php?year=$matches[1]&wc-api=$matches[3]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:25:\"(.?.+?)/wc-api(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&wc-api=$matches[3]\";s:28:\"(.?.+?)/order-pay(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&order-pay=$matches[3]\";s:33:\"(.?.+?)/order-received(/(.*))?/?$\";s:57:\"index.php?pagename=$matches[1]&order-received=$matches[3]\";s:25:\"(.?.+?)/orders(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&orders=$matches[3]\";s:29:\"(.?.+?)/view-order(/(.*))?/?$\";s:53:\"index.php?pagename=$matches[1]&view-order=$matches[3]\";s:28:\"(.?.+?)/downloads(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&downloads=$matches[3]\";s:31:\"(.?.+?)/edit-account(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-account=$matches[3]\";s:31:\"(.?.+?)/edit-address(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-address=$matches[3]\";s:34:\"(.?.+?)/payment-methods(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&payment-methods=$matches[3]\";s:32:\"(.?.+?)/lost-password(/(.*))?/?$\";s:56:\"index.php?pagename=$matches[1]&lost-password=$matches[3]\";s:34:\"(.?.+?)/customer-logout(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&customer-logout=$matches[3]\";s:37:\"(.?.+?)/add-payment-method(/(.*))?/?$\";s:61:\"index.php?pagename=$matches[1]&add-payment-method=$matches[3]\";s:40:\"(.?.+?)/delete-payment-method(/(.*))?/?$\";s:64:\"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]\";s:45:\"(.?.+?)/set-default-payment-method(/(.*))?/?$\";s:69:\"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]\";s:31:\".?.+?/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:25:\"([^/]+)/wc-api(/(.*))?/?$\";s:45:\"index.php?name=$matches[1]&wc-api=$matches[3]\";s:31:\"[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\"[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(1348, 'ossdl_off_cdn_url', 'https://wp2.dev-c01.muulabs.pl', 'yes'),
(1349, 'ossdl_off_blog_url', 'https://wp2.dev-c01.muulabs.pl', 'yes'),
(1350, 'ossdl_off_include_dirs', 'wp-content,wp-includes', 'yes'),
(1351, 'ossdl_off_exclude', '.php', 'yes'),
(1352, 'ossdl_cname', '', 'yes'),
(1353, 'wpsupercache_start', '1554822375', 'yes'),
(1354, 'wpsupercache_count', '0', 'yes'),
(1355, 'supercache_stats', 'a:3:{s:9:\"generated\";i:1554822482;s:10:\"supercache\";a:5:{s:7:\"expired\";i:0;s:6:\"cached\";i:0;s:5:\"fsize\";i:0;s:11:\"cached_list\";a:0:{}s:12:\"expired_list\";a:0:{}}s:7:\"wpcache\";a:5:{s:7:\"expired\";i:0;s:6:\"cached\";i:0;s:5:\"fsize\";i:0;s:11:\"cached_list\";a:0:{}s:12:\"expired_list\";a:0:{}}}', 'yes'),
(1418, 'acf_version', '5.8.1', 'yes'),
(1427, '_site_transient_update_themes', 'O:8:\"stdClass\":2:{s:12:\"last_checked\";i:1564308690;s:7:\"checked\";a:0:{}}', 'no'),
(1743, 'woocommerce_paypal_settings', 'a:23:{s:7:\"enabled\";s:2:\"no\";s:5:\"title\";s:6:\"PayPal\";s:11:\"description\";s:85:\"Pay via PayPal; you can pay with your credit card if you don\'t have a PayPal account.\";s:5:\"email\";s:26:\"tobiaszkochanski@gmail.com\";s:8:\"advanced\";s:0:\"\";s:8:\"testmode\";s:3:\"yes\";s:5:\"debug\";s:2:\"no\";s:16:\"ipn_notification\";s:3:\"yes\";s:14:\"receiver_email\";s:26:\"tobiaszkochanski@gmail.com\";s:14:\"identity_token\";s:0:\"\";s:14:\"invoice_prefix\";s:3:\"WC-\";s:13:\"send_shipping\";s:3:\"yes\";s:16:\"address_override\";s:2:\"no\";s:13:\"paymentaction\";s:4:\"sale\";s:10:\"page_style\";s:0:\"\";s:9:\"image_url\";s:0:\"\";s:11:\"api_details\";s:0:\"\";s:12:\"api_username\";s:12:\"mindengineer\";s:12:\"api_password\";s:18:\"2nYGkle2PkOm!ywX4E\";s:13:\"api_signature\";s:0:\"\";s:20:\"sandbox_api_username\";s:44:\"maciej.krupowies-facilitator_api1.muulabs.pl\";s:20:\"sandbox_api_password\";s:16:\"8U38CDNE277AZ3PH\";s:21:\"sandbox_api_signature\";s:56:\"AkUXvyyNwvsctJSR20z9FHmrGL54AlKxvso.99jt5JSSvQEtMCsSkLqY\";}', 'yes'),
(1761, 'woocommerce_gateway_order', 'a:5:{s:4:\"bacs\";i:0;s:6:\"cheque\";i:1;s:3:\"cod\";i:2;s:6:\"paypal\";i:3;s:11:\"ppec_paypal\";i:4;}', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(2036, 'thwepo_custom_sections', 'a:1:{s:7:\"default\";O:25:\"WEPO_Product_Page_Section\":30:{s:2:\"id\";s:7:\"default\";s:4:\"name\";s:7:\"default\";s:8:\"position\";s:29:\"woo_before_add_to_cart_button\";s:5:\"order\";s:0:\"\";s:4:\"type\";s:0:\"\";s:8:\"cssclass\";s:0:\"\";s:15:\"title_cell_with\";s:0:\"\";s:15:\"field_cell_with\";s:0:\"\";s:10:\"show_title\";i:0;s:5:\"title\";s:7:\"Default\";s:10:\"title_type\";s:0:\"\";s:11:\"title_color\";s:0:\"\";s:14:\"title_position\";s:0:\"\";s:11:\"title_class\";s:0:\"\";s:8:\"subtitle\";s:0:\"\";s:13:\"subtitle_type\";s:0:\"\";s:14:\"subtitle_color\";s:0:\"\";s:17:\"subtitle_position\";s:0:\"\";s:14:\"subtitle_class\";s:0:\"\";s:12:\"cssclass_str\";s:0:\"\";s:15:\"title_class_str\";s:0:\"\";s:18:\"subtitle_class_str\";s:0:\"\";s:12:\"rules_action\";s:0:\"\";s:17:\"rules_action_ajax\";s:0:\"\";s:22:\"conditional_rules_json\";s:0:\"\";s:17:\"conditional_rules\";a:0:{}s:27:\"conditional_rules_ajax_json\";s:0:\"\";s:22:\"conditional_rules_ajax\";a:0:{}s:14:\"condition_sets\";a:0:{}s:6:\"fields\";a:16:{s:14:\"mentoring_type\";O:24:\"WEPO_Product_Field_Radio\":64:{s:7:\"options\";a:4:{s:8:\"facetime\";a:4:{s:3:\"key\";s:8:\"facetime\";s:4:\"text\";s:36:\"Emergency FaceTime Phonecall, 30 min\";s:5:\"price\";s:2:\"50\";s:10:\"price_type\";s:0:\"\";}s:9:\"5_sessies\";a:4:{s:3:\"key\";s:9:\"5_sessies\";s:4:\"text\";s:63:\"TRAJECT: Gratis Kennismakingsronde, 45 min + 5 sessies (60 min)\";s:5:\"price\";s:3:\"449\";s:10:\"price_type\";s:0:\"\";}s:10:\"10_sessies\";a:4:{s:3:\"key\";s:10:\"10_sessies\";s:4:\"text\";s:58:\"TRAJECT: Gratis Kennismaking, 45 min + 10 sessies (60 min)\";s:5:\"price\";s:3:\"799\";s:10:\"price_type\";s:0:\"\";}s:7:\"spreker\";a:4:{s:3:\"key\";s:7:\"spreker\";s:4:\"text\";s:19:\"Hur mij als spreker\";s:5:\"price\";s:0:\"\";s:10:\"price_type\";s:0:\"\";}}s:5:\"order\";s:1:\"0\";s:4:\"type\";s:5:\"radio\";s:2:\"id\";s:14:\"mentoring_type\";s:4:\"name\";s:14:\"mentoring_type\";s:8:\"name_old\";s:14:\"mentoring_type\";s:9:\"minlength\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:5:\"value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:12:\"options_json\";s:685:\"%5B%7B%22key%22%3A%22facetime%22%2C%22text%22%3A%22Emergency%20FaceTime%20Phonecall%2C%2030%20min%22%2C%22price%22%3A%2250%22%2C%22price_type%22%3A%22%22%7D%2C%7B%22key%22%3A%225_sessies%22%2C%22text%22%3A%22TRAJECT%3A%20Gratis%20Kennismakingsronde%2C%2045%20min%20%2B%205%20sessies%20(60%20min)%22%2C%22price%22%3A%22449%22%2C%22price_type%22%3A%22%22%7D%2C%7B%22key%22%3A%2210_sessies%22%2C%22text%22%3A%22TRAJECT%3A%20Gratis%20Kennismaking%2C%2045%20min%20%2B%2010%20sessies%20(60%20min)%22%2C%22price%22%3A%22799%22%2C%22price_type%22%3A%22%22%7D%2C%7B%22key%22%3A%22spreker%22%2C%22text%22%3A%22Hur%20mij%20als%20spreker%22%2C%22price%22%3A%22%22%2C%22price_type%22%3A%22%22%7D%5D\";s:8:\"validate\";s:0:\"\";s:11:\"input_class\";s:0:\"\";s:8:\"cssclass\";s:14:\"mentoring-type\";s:12:\"cssclass_str\";s:0:\"\";s:5:\"title\";s:4:\"Type\";s:10:\"title_type\";s:5:\"label\";s:11:\"title_color\";s:0:\"\";s:14:\"title_position\";s:4:\"left\";s:11:\"title_class\";s:0:\"\";s:15:\"title_class_str\";s:0:\"\";s:8:\"subtitle\";s:0:\"\";s:13:\"subtitle_type\";s:5:\"label\";s:14:\"subtitle_color\";s:0:\"\";s:17:\"subtitle_position\";s:0:\"\";s:14:\"subtitle_class\";s:0:\"\";s:18:\"subtitle_class_str\";s:0:\"\";s:11:\"price_field\";i:1;s:5:\"price\";s:0:\"\";s:10:\"price_unit\";s:0:\"\";s:10:\"price_type\";s:0:\"\";s:14:\"price_min_unit\";s:0:\"\";s:12:\"price_prefix\";s:0:\"\";s:12:\"price_suffix\";s:0:\"\";s:8:\"required\";s:3:\"yes\";s:7:\"enabled\";s:1:\"1\";s:12:\"rules_action\";s:4:\"show\";s:17:\"rules_action_ajax\";s:4:\"show\";s:22:\"conditional_rules_json\";s:0:\"\";s:17:\"conditional_rules\";a:0:{}s:27:\"conditional_rules_ajax_json\";s:0:\"\";s:22:\"conditional_rules_ajax\";a:0:{}s:14:\"separator_type\";s:0:\"\";s:15:\"separator_hight\";s:0:\"\";s:7:\"checked\";i:0;s:7:\"maxsize\";s:0:\"\";s:6:\"accept\";s:0:\"\";s:4:\"cols\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:12:\"default_date\";s:0:\"\";s:11:\"date_format\";s:0:\"\";s:8:\"min_date\";s:0:\"\";s:8:\"max_date\";s:0:\"\";s:10:\"year_range\";s:0:\"\";s:16:\"number_of_months\";s:0:\"\";s:13:\"disabled_days\";s:0:\"\";s:14:\"disabled_dates\";s:0:\"\";s:8:\"min_time\";s:0:\"\";s:8:\"max_time\";s:0:\"\";s:10:\"start_time\";s:0:\"\";s:9:\"time_step\";s:0:\"\";s:11:\"time_format\";s:0:\"\";s:11:\"linked_date\";s:0:\"\";s:12:\"custom_field\";i:1;}s:41:\"description_emergency_facetime_phone_call\";O:24:\"WEPO_Product_Field_Label\":66:{s:5:\"order\";s:1:\"1\";s:4:\"type\";s:5:\"label\";s:2:\"id\";s:41:\"description_emergency_facetime_phone_call\";s:4:\"name\";s:41:\"description_emergency_facetime_phone_call\";s:8:\"name_old\";s:11:\"description\";s:9:\"minlength\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:5:\"value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:12:\"options_json\";s:0:\"\";s:7:\"options\";a:0:{}s:8:\"validate\";s:0:\"\";s:11:\"input_class\";s:0:\"\";s:8:\"cssclass\";s:0:\"\";s:12:\"cssclass_str\";s:0:\"\";s:5:\"title\";s:254:\"Duis tempus ac ipsum a placerat. Quisque elementum imperdiet libero, in sagittis velit feugiat sit amet. Fusce eu ex lectus. Curabitur semper lectus lorem, a imperdiet massa pretium et. Pellentesque elementum orci erat, eu accumsan mauris ullamcorper et.\";s:10:\"title_type\";s:3:\"div\";s:11:\"title_color\";s:0:\"\";s:14:\"title_position\";s:0:\"\";s:11:\"title_class\";s:11:\"description\";s:15:\"title_class_str\";s:0:\"\";s:8:\"subtitle\";s:0:\"\";s:13:\"subtitle_type\";s:5:\"label\";s:14:\"subtitle_color\";s:0:\"\";s:17:\"subtitle_position\";s:0:\"\";s:14:\"subtitle_class\";s:0:\"\";s:18:\"subtitle_class_str\";s:0:\"\";s:11:\"price_field\";i:0;s:5:\"price\";i:0;s:10:\"price_unit\";i:0;s:10:\"price_type\";s:0:\"\";s:14:\"price_min_unit\";s:0:\"\";s:12:\"price_prefix\";s:0:\"\";s:12:\"price_suffix\";s:0:\"\";s:8:\"required\";i:0;s:7:\"enabled\";s:1:\"1\";s:12:\"rules_action\";s:4:\"show\";s:17:\"rules_action_ajax\";s:4:\"show\";s:22:\"conditional_rules_json\";s:0:\"\";s:17:\"conditional_rules\";a:0:{}s:27:\"conditional_rules_ajax_json\";s:0:\"\";s:22:\"conditional_rules_ajax\";a:0:{}s:14:\"separator_type\";s:0:\"\";s:15:\"separator_hight\";s:0:\"\";s:7:\"checked\";i:0;s:7:\"maxsize\";s:0:\"\";s:6:\"accept\";s:0:\"\";s:4:\"cols\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:12:\"default_date\";s:0:\"\";s:11:\"date_format\";s:0:\"\";s:8:\"min_date\";s:0:\"\";s:8:\"max_date\";s:0:\"\";s:10:\"year_range\";s:0:\"\";s:16:\"number_of_months\";s:0:\"\";s:13:\"disabled_days\";s:0:\"\";s:14:\"disabled_dates\";s:0:\"\";s:8:\"min_time\";s:0:\"\";s:8:\"max_time\";s:0:\"\";s:10:\"start_time\";s:0:\"\";s:9:\"time_step\";s:0:\"\";s:11:\"time_format\";s:0:\"\";s:11:\"linked_date\";s:0:\"\";s:7:\"taxable\";s:0:\"\";s:9:\"tax_class\";s:0:\"\";s:12:\"custom_field\";i:1;}s:37:\"description_gratis_kennismakingsronde\";O:24:\"WEPO_Product_Field_Label\":66:{s:5:\"order\";s:1:\"2\";s:4:\"type\";s:5:\"label\";s:2:\"id\";s:37:\"description_gratis_kennismakingsronde\";s:4:\"name\";s:37:\"description_gratis_kennismakingsronde\";s:8:\"name_old\";s:0:\"\";s:9:\"minlength\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:5:\"value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:12:\"options_json\";s:0:\"\";s:7:\"options\";a:0:{}s:8:\"validate\";s:0:\"\";s:11:\"input_class\";s:0:\"\";s:8:\"cssclass\";s:0:\"\";s:12:\"cssclass_str\";s:0:\"\";s:5:\"title\";s:261:\"Test 1 Duis tempus ac ipsum a placerat. Quisque elementum imperdiet libero, in sagittis velit feugiat sit amet. Fusce eu ex lectus. Curabitur semper lectus lorem, a imperdiet massa pretium et. Pellentesque elementum orci erat, eu accumsan mauris ullamcorper et.\";s:10:\"title_type\";s:3:\"div\";s:11:\"title_color\";s:0:\"\";s:14:\"title_position\";s:0:\"\";s:11:\"title_class\";s:11:\"description\";s:15:\"title_class_str\";s:0:\"\";s:8:\"subtitle\";s:0:\"\";s:13:\"subtitle_type\";s:5:\"label\";s:14:\"subtitle_color\";s:0:\"\";s:17:\"subtitle_position\";s:0:\"\";s:14:\"subtitle_class\";s:0:\"\";s:18:\"subtitle_class_str\";s:0:\"\";s:11:\"price_field\";i:0;s:5:\"price\";i:0;s:10:\"price_unit\";i:0;s:10:\"price_type\";s:0:\"\";s:14:\"price_min_unit\";s:0:\"\";s:12:\"price_prefix\";s:0:\"\";s:12:\"price_suffix\";s:0:\"\";s:8:\"required\";i:0;s:7:\"enabled\";s:1:\"1\";s:12:\"rules_action\";s:4:\"show\";s:17:\"rules_action_ajax\";s:4:\"show\";s:22:\"conditional_rules_json\";s:0:\"\";s:17:\"conditional_rules\";a:0:{}s:27:\"conditional_rules_ajax_json\";s:0:\"\";s:22:\"conditional_rules_ajax\";a:0:{}s:14:\"separator_type\";s:0:\"\";s:15:\"separator_hight\";s:0:\"\";s:7:\"checked\";i:0;s:7:\"maxsize\";s:0:\"\";s:6:\"accept\";s:0:\"\";s:4:\"cols\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:12:\"default_date\";s:0:\"\";s:11:\"date_format\";s:0:\"\";s:8:\"min_date\";s:0:\"\";s:8:\"max_date\";s:0:\"\";s:10:\"year_range\";s:0:\"\";s:16:\"number_of_months\";s:0:\"\";s:13:\"disabled_days\";s:0:\"\";s:14:\"disabled_dates\";s:0:\"\";s:8:\"min_time\";s:0:\"\";s:8:\"max_time\";s:0:\"\";s:10:\"start_time\";s:0:\"\";s:9:\"time_step\";s:0:\"\";s:11:\"time_format\";s:0:\"\";s:11:\"linked_date\";s:0:\"\";s:7:\"taxable\";s:0:\"\";s:9:\"tax_class\";s:0:\"\";s:12:\"custom_field\";i:1;}s:31:\"description_gratis_kennismaking\";O:24:\"WEPO_Product_Field_Label\":66:{s:5:\"order\";s:1:\"3\";s:4:\"type\";s:5:\"label\";s:2:\"id\";s:31:\"description_gratis_kennismaking\";s:4:\"name\";s:31:\"description_gratis_kennismaking\";s:8:\"name_old\";s:0:\"\";s:9:\"minlength\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:5:\"value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:12:\"options_json\";s:0:\"\";s:7:\"options\";a:0:{}s:8:\"validate\";s:0:\"\";s:11:\"input_class\";s:0:\"\";s:8:\"cssclass\";s:0:\"\";s:12:\"cssclass_str\";s:0:\"\";s:5:\"title\";s:261:\"Test 2 Duis tempus ac ipsum a placerat. Quisque elementum imperdiet libero, in sagittis velit feugiat sit amet. Fusce eu ex lectus. Curabitur semper lectus lorem, a imperdiet massa pretium et. Pellentesque elementum orci erat, eu accumsan mauris ullamcorper et.\";s:10:\"title_type\";s:3:\"div\";s:11:\"title_color\";s:0:\"\";s:14:\"title_position\";s:0:\"\";s:11:\"title_class\";s:11:\"description\";s:15:\"title_class_str\";s:0:\"\";s:8:\"subtitle\";s:0:\"\";s:13:\"subtitle_type\";s:5:\"label\";s:14:\"subtitle_color\";s:0:\"\";s:17:\"subtitle_position\";s:0:\"\";s:14:\"subtitle_class\";s:0:\"\";s:18:\"subtitle_class_str\";s:0:\"\";s:11:\"price_field\";i:0;s:5:\"price\";i:0;s:10:\"price_unit\";i:0;s:10:\"price_type\";s:0:\"\";s:14:\"price_min_unit\";s:0:\"\";s:12:\"price_prefix\";s:0:\"\";s:12:\"price_suffix\";s:0:\"\";s:8:\"required\";i:0;s:7:\"enabled\";s:1:\"1\";s:12:\"rules_action\";s:4:\"show\";s:17:\"rules_action_ajax\";s:4:\"show\";s:22:\"conditional_rules_json\";s:0:\"\";s:17:\"conditional_rules\";a:0:{}s:27:\"conditional_rules_ajax_json\";s:0:\"\";s:22:\"conditional_rules_ajax\";a:0:{}s:14:\"separator_type\";s:0:\"\";s:15:\"separator_hight\";s:0:\"\";s:7:\"checked\";i:0;s:7:\"maxsize\";s:0:\"\";s:6:\"accept\";s:0:\"\";s:4:\"cols\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:12:\"default_date\";s:0:\"\";s:11:\"date_format\";s:0:\"\";s:8:\"min_date\";s:0:\"\";s:8:\"max_date\";s:0:\"\";s:10:\"year_range\";s:0:\"\";s:16:\"number_of_months\";s:0:\"\";s:13:\"disabled_days\";s:0:\"\";s:14:\"disabled_dates\";s:0:\"\";s:8:\"min_time\";s:0:\"\";s:8:\"max_time\";s:0:\"\";s:10:\"start_time\";s:0:\"\";s:9:\"time_step\";s:0:\"\";s:11:\"time_format\";s:0:\"\";s:11:\"linked_date\";s:0:\"\";s:7:\"taxable\";s:0:\"\";s:9:\"tax_class\";s:0:\"\";s:12:\"custom_field\";i:1;}s:31:\"description_hur_mij_als_spreker\";O:24:\"WEPO_Product_Field_Label\":66:{s:5:\"order\";s:1:\"4\";s:4:\"type\";s:5:\"label\";s:2:\"id\";s:31:\"description_hur_mij_als_spreker\";s:4:\"name\";s:31:\"description_hur_mij_als_spreker\";s:8:\"name_old\";s:0:\"\";s:9:\"minlength\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:5:\"value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:12:\"options_json\";s:0:\"\";s:7:\"options\";a:0:{}s:8:\"validate\";s:0:\"\";s:11:\"input_class\";s:0:\"\";s:8:\"cssclass\";s:0:\"\";s:12:\"cssclass_str\";s:0:\"\";s:5:\"title\";s:261:\"Test 3 Duis tempus ac ipsum a placerat. Quisque elementum imperdiet libero, in sagittis velit feugiat sit amet. Fusce eu ex lectus. Curabitur semper lectus lorem, a imperdiet massa pretium et. Pellentesque elementum orci erat, eu accumsan mauris ullamcorper et.\";s:10:\"title_type\";s:3:\"div\";s:11:\"title_color\";s:0:\"\";s:14:\"title_position\";s:0:\"\";s:11:\"title_class\";s:11:\"description\";s:15:\"title_class_str\";s:0:\"\";s:8:\"subtitle\";s:0:\"\";s:13:\"subtitle_type\";s:5:\"label\";s:14:\"subtitle_color\";s:0:\"\";s:17:\"subtitle_position\";s:0:\"\";s:14:\"subtitle_class\";s:0:\"\";s:18:\"subtitle_class_str\";s:0:\"\";s:11:\"price_field\";i:0;s:5:\"price\";i:0;s:10:\"price_unit\";i:0;s:10:\"price_type\";s:0:\"\";s:14:\"price_min_unit\";s:0:\"\";s:12:\"price_prefix\";s:0:\"\";s:12:\"price_suffix\";s:0:\"\";s:8:\"required\";i:0;s:7:\"enabled\";s:1:\"1\";s:12:\"rules_action\";s:4:\"show\";s:17:\"rules_action_ajax\";s:4:\"show\";s:22:\"conditional_rules_json\";s:0:\"\";s:17:\"conditional_rules\";a:0:{}s:27:\"conditional_rules_ajax_json\";s:0:\"\";s:22:\"conditional_rules_ajax\";a:0:{}s:14:\"separator_type\";s:0:\"\";s:15:\"separator_hight\";s:0:\"\";s:7:\"checked\";i:0;s:7:\"maxsize\";s:0:\"\";s:6:\"accept\";s:0:\"\";s:4:\"cols\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:12:\"default_date\";s:0:\"\";s:11:\"date_format\";s:0:\"\";s:8:\"min_date\";s:0:\"\";s:8:\"max_date\";s:0:\"\";s:10:\"year_range\";s:0:\"\";s:16:\"number_of_months\";s:0:\"\";s:13:\"disabled_days\";s:0:\"\";s:14:\"disabled_dates\";s:0:\"\";s:8:\"min_time\";s:0:\"\";s:8:\"max_time\";s:0:\"\";s:10:\"start_time\";s:0:\"\";s:9:\"time_step\";s:0:\"\";s:11:\"time_format\";s:0:\"\";s:11:\"linked_date\";s:0:\"\";s:7:\"taxable\";s:0:\"\";s:9:\"tax_class\";s:0:\"\";s:12:\"custom_field\";i:1;}s:14:\"mentoring_naam\";O:28:\"WEPO_Product_Field_InputText\":64:{s:5:\"order\";s:1:\"5\";s:4:\"type\";s:9:\"inputtext\";s:2:\"id\";s:14:\"mentoring_naam\";s:4:\"name\";s:14:\"mentoring_naam\";s:8:\"name_old\";s:14:\"mentoring_naam\";s:9:\"minlength\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:5:\"value\";s:0:\"\";s:11:\"placeholder\";s:4:\"naam\";s:12:\"options_json\";s:0:\"\";s:7:\"options\";a:0:{}s:8:\"validate\";s:0:\"\";s:11:\"input_class\";s:12:\"form-control\";s:8:\"cssclass\";s:18:\"tr-contact-element\";s:12:\"cssclass_str\";s:0:\"\";s:5:\"title\";s:4:\"Naam\";s:10:\"title_type\";s:5:\"label\";s:11:\"title_color\";s:0:\"\";s:14:\"title_position\";s:4:\"left\";s:11:\"title_class\";s:0:\"\";s:15:\"title_class_str\";s:0:\"\";s:8:\"subtitle\";s:0:\"\";s:13:\"subtitle_type\";s:5:\"label\";s:14:\"subtitle_color\";s:0:\"\";s:17:\"subtitle_position\";s:0:\"\";s:14:\"subtitle_class\";s:0:\"\";s:18:\"subtitle_class_str\";s:0:\"\";s:11:\"price_field\";b:0;s:5:\"price\";s:0:\"\";s:10:\"price_unit\";s:0:\"\";s:10:\"price_type\";s:6:\"normal\";s:14:\"price_min_unit\";s:0:\"\";s:12:\"price_prefix\";s:0:\"\";s:12:\"price_suffix\";s:0:\"\";s:8:\"required\";i:0;s:7:\"enabled\";s:1:\"1\";s:12:\"rules_action\";s:4:\"show\";s:17:\"rules_action_ajax\";s:4:\"show\";s:22:\"conditional_rules_json\";s:129:\"%5B%5B%5B%5B%7B%22operand_type%22%3A%22product%22%2C%22operator%22%3A%22equals%22%2C%22operand%22%3A%5B%2268%22%5D%7D%5D%5D%5D%5D\";s:17:\"conditional_rules\";a:1:{i:0;O:23:\"WEPO_Condition_Rule_Set\":2:{s:5:\"logic\";s:3:\"and\";s:15:\"condition_rules\";a:1:{i:0;O:19:\"WEPO_Condition_Rule\":2:{s:5:\"logic\";s:2:\"or\";s:14:\"condition_sets\";a:1:{i:0;O:18:\"WEPO_Condition_Set\":2:{s:5:\"logic\";s:3:\"and\";s:10:\"conditions\";a:1:{i:0;O:14:\"WEPO_Condition\":4:{s:12:\"operand_type\";s:7:\"product\";s:7:\"operand\";a:1:{i:0;s:2:\"68\";}s:8:\"operator\";s:6:\"equals\";s:5:\"value\";s:0:\"\";}}}}}}}}s:27:\"conditional_rules_ajax_json\";s:0:\"\";s:22:\"conditional_rules_ajax\";a:0:{}s:14:\"separator_type\";s:0:\"\";s:15:\"separator_hight\";s:0:\"\";s:7:\"checked\";i:0;s:7:\"maxsize\";s:0:\"\";s:6:\"accept\";s:0:\"\";s:4:\"cols\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:12:\"default_date\";s:0:\"\";s:11:\"date_format\";s:0:\"\";s:8:\"min_date\";s:0:\"\";s:8:\"max_date\";s:0:\"\";s:10:\"year_range\";s:0:\"\";s:16:\"number_of_months\";s:0:\"\";s:13:\"disabled_days\";s:0:\"\";s:14:\"disabled_dates\";s:0:\"\";s:8:\"min_time\";s:0:\"\";s:8:\"max_time\";s:0:\"\";s:10:\"start_time\";s:0:\"\";s:9:\"time_step\";s:0:\"\";s:11:\"time_format\";s:0:\"\";s:11:\"linked_date\";s:0:\"\";s:12:\"custom_field\";i:1;}s:15:\"mentoring_email\";O:28:\"WEPO_Product_Field_InputText\":64:{s:5:\"order\";s:1:\"6\";s:4:\"type\";s:9:\"inputtext\";s:2:\"id\";s:15:\"mentoring_email\";s:4:\"name\";s:15:\"mentoring_email\";s:8:\"name_old\";s:15:\"mentoring_email\";s:9:\"minlength\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:5:\"value\";s:0:\"\";s:11:\"placeholder\";s:11:\"e-mailadres\";s:12:\"options_json\";s:0:\"\";s:7:\"options\";a:0:{}s:8:\"validate\";s:5:\"email\";s:11:\"input_class\";s:12:\"form-control\";s:8:\"cssclass\";s:18:\"tr-contact-element\";s:12:\"cssclass_str\";s:0:\"\";s:5:\"title\";s:11:\"E-mailadres\";s:10:\"title_type\";s:5:\"label\";s:11:\"title_color\";s:0:\"\";s:14:\"title_position\";s:0:\"\";s:11:\"title_class\";s:0:\"\";s:15:\"title_class_str\";s:0:\"\";s:8:\"subtitle\";s:0:\"\";s:13:\"subtitle_type\";s:0:\"\";s:14:\"subtitle_color\";s:0:\"\";s:17:\"subtitle_position\";s:0:\"\";s:14:\"subtitle_class\";s:0:\"\";s:18:\"subtitle_class_str\";s:0:\"\";s:11:\"price_field\";b:0;s:5:\"price\";s:0:\"\";s:10:\"price_unit\";s:1:\"0\";s:10:\"price_type\";s:0:\"\";s:14:\"price_min_unit\";s:1:\"0\";s:12:\"price_prefix\";s:0:\"\";s:12:\"price_suffix\";s:0:\"\";s:8:\"required\";i:0;s:7:\"enabled\";s:1:\"1\";s:12:\"rules_action\";s:4:\"show\";s:17:\"rules_action_ajax\";s:4:\"show\";s:22:\"conditional_rules_json\";s:129:\"%5B%5B%5B%5B%7B%22operand_type%22%3A%22product%22%2C%22operator%22%3A%22equals%22%2C%22operand%22%3A%5B%2268%22%5D%7D%5D%5D%5D%5D\";s:17:\"conditional_rules\";a:1:{i:0;O:23:\"WEPO_Condition_Rule_Set\":2:{s:5:\"logic\";s:3:\"and\";s:15:\"condition_rules\";a:1:{i:0;O:19:\"WEPO_Condition_Rule\":2:{s:5:\"logic\";s:2:\"or\";s:14:\"condition_sets\";a:1:{i:0;O:18:\"WEPO_Condition_Set\":2:{s:5:\"logic\";s:3:\"and\";s:10:\"conditions\";a:1:{i:0;O:14:\"WEPO_Condition\":4:{s:12:\"operand_type\";s:7:\"product\";s:7:\"operand\";a:1:{i:0;s:2:\"68\";}s:8:\"operator\";s:6:\"equals\";s:5:\"value\";s:0:\"\";}}}}}}}}s:27:\"conditional_rules_ajax_json\";s:0:\"\";s:22:\"conditional_rules_ajax\";a:0:{}s:14:\"separator_type\";s:0:\"\";s:15:\"separator_hight\";s:0:\"\";s:7:\"checked\";i:0;s:7:\"maxsize\";s:0:\"\";s:6:\"accept\";s:0:\"\";s:4:\"cols\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:12:\"default_date\";s:0:\"\";s:11:\"date_format\";s:0:\"\";s:8:\"min_date\";s:0:\"\";s:8:\"max_date\";s:0:\"\";s:10:\"year_range\";s:0:\"\";s:16:\"number_of_months\";s:0:\"\";s:13:\"disabled_days\";s:0:\"\";s:14:\"disabled_dates\";s:0:\"\";s:8:\"min_time\";s:0:\"\";s:8:\"max_time\";s:0:\"\";s:10:\"start_time\";s:0:\"\";s:9:\"time_step\";s:0:\"\";s:11:\"time_format\";s:0:\"\";s:11:\"linked_date\";s:0:\"\";s:12:\"custom_field\";i:1;}s:15:\"mentoring_phone\";O:28:\"WEPO_Product_Field_InputText\":64:{s:5:\"order\";s:1:\"7\";s:4:\"type\";s:9:\"inputtext\";s:2:\"id\";s:15:\"mentoring_phone\";s:4:\"name\";s:15:\"mentoring_phone\";s:8:\"name_old\";s:15:\"mentoring_phone\";s:9:\"minlength\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:5:\"value\";s:0:\"\";s:11:\"placeholder\";s:14:\"telefoonnummer\";s:12:\"options_json\";s:0:\"\";s:7:\"options\";a:0:{}s:8:\"validate\";s:0:\"\";s:11:\"input_class\";s:12:\"form-control\";s:8:\"cssclass\";s:18:\"tr-contact-element\";s:12:\"cssclass_str\";s:0:\"\";s:5:\"title\";s:14:\"Telefoonnummer\";s:10:\"title_type\";s:5:\"label\";s:11:\"title_color\";s:0:\"\";s:14:\"title_position\";s:0:\"\";s:11:\"title_class\";s:0:\"\";s:15:\"title_class_str\";s:0:\"\";s:8:\"subtitle\";s:0:\"\";s:13:\"subtitle_type\";s:0:\"\";s:14:\"subtitle_color\";s:0:\"\";s:17:\"subtitle_position\";s:0:\"\";s:14:\"subtitle_class\";s:0:\"\";s:18:\"subtitle_class_str\";s:0:\"\";s:11:\"price_field\";b:0;s:5:\"price\";s:0:\"\";s:10:\"price_unit\";s:1:\"0\";s:10:\"price_type\";s:0:\"\";s:14:\"price_min_unit\";s:1:\"0\";s:12:\"price_prefix\";s:0:\"\";s:12:\"price_suffix\";s:0:\"\";s:8:\"required\";i:0;s:7:\"enabled\";s:1:\"1\";s:12:\"rules_action\";s:4:\"show\";s:17:\"rules_action_ajax\";s:4:\"show\";s:22:\"conditional_rules_json\";s:129:\"%5B%5B%5B%5B%7B%22operand_type%22%3A%22product%22%2C%22operator%22%3A%22equals%22%2C%22operand%22%3A%5B%2268%22%5D%7D%5D%5D%5D%5D\";s:17:\"conditional_rules\";a:1:{i:0;O:23:\"WEPO_Condition_Rule_Set\":2:{s:5:\"logic\";s:3:\"and\";s:15:\"condition_rules\";a:1:{i:0;O:19:\"WEPO_Condition_Rule\":2:{s:5:\"logic\";s:2:\"or\";s:14:\"condition_sets\";a:1:{i:0;O:18:\"WEPO_Condition_Set\":2:{s:5:\"logic\";s:3:\"and\";s:10:\"conditions\";a:1:{i:0;O:14:\"WEPO_Condition\":4:{s:12:\"operand_type\";s:7:\"product\";s:7:\"operand\";a:1:{i:0;s:2:\"68\";}s:8:\"operator\";s:6:\"equals\";s:5:\"value\";s:0:\"\";}}}}}}}}s:27:\"conditional_rules_ajax_json\";s:0:\"\";s:22:\"conditional_rules_ajax\";a:0:{}s:14:\"separator_type\";s:0:\"\";s:15:\"separator_hight\";s:0:\"\";s:7:\"checked\";i:0;s:7:\"maxsize\";s:0:\"\";s:6:\"accept\";s:0:\"\";s:4:\"cols\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:12:\"default_date\";s:0:\"\";s:11:\"date_format\";s:0:\"\";s:8:\"min_date\";s:0:\"\";s:8:\"max_date\";s:0:\"\";s:10:\"year_range\";s:0:\"\";s:16:\"number_of_months\";s:0:\"\";s:13:\"disabled_days\";s:0:\"\";s:14:\"disabled_dates\";s:0:\"\";s:8:\"min_time\";s:0:\"\";s:8:\"max_time\";s:0:\"\";s:10:\"start_time\";s:0:\"\";s:9:\"time_step\";s:0:\"\";s:11:\"time_format\";s:0:\"\";s:11:\"linked_date\";s:0:\"\";s:12:\"custom_field\";i:1;}s:17:\"mentoring_message\";O:27:\"WEPO_Product_Field_Textarea\":64:{s:4:\"cols\";s:0:\"\";s:4:\"rows\";s:1:\"4\";s:5:\"order\";s:1:\"8\";s:4:\"type\";s:8:\"textarea\";s:2:\"id\";s:17:\"mentoring_message\";s:4:\"name\";s:17:\"mentoring_message\";s:8:\"name_old\";s:17:\"mentoring_message\";s:9:\"minlength\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:5:\"value\";s:0:\"\";s:11:\"placeholder\";s:7:\"bericht\";s:12:\"options_json\";s:0:\"\";s:7:\"options\";a:0:{}s:8:\"validate\";s:0:\"\";s:11:\"input_class\";s:12:\"form-control\";s:8:\"cssclass\";s:18:\"tr-contact-element\";s:12:\"cssclass_str\";s:0:\"\";s:5:\"title\";s:7:\"Bericht\";s:10:\"title_type\";s:5:\"label\";s:11:\"title_color\";s:0:\"\";s:14:\"title_position\";s:0:\"\";s:11:\"title_class\";s:9:\"text-left\";s:15:\"title_class_str\";s:0:\"\";s:8:\"subtitle\";s:0:\"\";s:13:\"subtitle_type\";s:0:\"\";s:14:\"subtitle_color\";s:0:\"\";s:17:\"subtitle_position\";s:0:\"\";s:14:\"subtitle_class\";s:0:\"\";s:18:\"subtitle_class_str\";s:0:\"\";s:11:\"price_field\";b:0;s:5:\"price\";s:0:\"\";s:10:\"price_unit\";s:1:\"0\";s:10:\"price_type\";s:0:\"\";s:14:\"price_min_unit\";s:1:\"0\";s:12:\"price_prefix\";s:0:\"\";s:12:\"price_suffix\";s:0:\"\";s:8:\"required\";i:0;s:7:\"enabled\";s:1:\"1\";s:12:\"rules_action\";s:4:\"show\";s:17:\"rules_action_ajax\";s:4:\"show\";s:22:\"conditional_rules_json\";s:129:\"%5B%5B%5B%5B%7B%22operand_type%22%3A%22product%22%2C%22operator%22%3A%22equals%22%2C%22operand%22%3A%5B%2268%22%5D%7D%5D%5D%5D%5D\";s:17:\"conditional_rules\";a:1:{i:0;O:23:\"WEPO_Condition_Rule_Set\":2:{s:5:\"logic\";s:3:\"and\";s:15:\"condition_rules\";a:1:{i:0;O:19:\"WEPO_Condition_Rule\":2:{s:5:\"logic\";s:2:\"or\";s:14:\"condition_sets\";a:1:{i:0;O:18:\"WEPO_Condition_Set\":2:{s:5:\"logic\";s:3:\"and\";s:10:\"conditions\";a:1:{i:0;O:14:\"WEPO_Condition\":4:{s:12:\"operand_type\";s:7:\"product\";s:7:\"operand\";a:1:{i:0;s:2:\"68\";}s:8:\"operator\";s:6:\"equals\";s:5:\"value\";s:0:\"\";}}}}}}}}s:27:\"conditional_rules_ajax_json\";s:0:\"\";s:22:\"conditional_rules_ajax\";a:0:{}s:14:\"separator_type\";s:0:\"\";s:15:\"separator_hight\";s:0:\"\";s:7:\"checked\";i:0;s:7:\"maxsize\";s:0:\"\";s:6:\"accept\";s:0:\"\";s:12:\"default_date\";s:0:\"\";s:11:\"date_format\";s:0:\"\";s:8:\"min_date\";s:0:\"\";s:8:\"max_date\";s:0:\"\";s:10:\"year_range\";s:0:\"\";s:16:\"number_of_months\";s:0:\"\";s:13:\"disabled_days\";s:0:\"\";s:14:\"disabled_dates\";s:0:\"\";s:8:\"min_time\";s:0:\"\";s:8:\"max_time\";s:0:\"\";s:10:\"start_time\";s:0:\"\";s:9:\"time_step\";s:0:\"\";s:11:\"time_format\";s:0:\"\";s:11:\"linked_date\";s:0:\"\";s:12:\"custom_field\";i:1;}s:17:\"mentoring_website\";O:28:\"WEPO_Product_Field_InputText\":64:{s:5:\"order\";s:1:\"9\";s:4:\"type\";s:9:\"inputtext\";s:2:\"id\";s:17:\"mentoring_website\";s:4:\"name\";s:17:\"mentoring_website\";s:8:\"name_old\";s:17:\"mentoring_website\";s:9:\"minlength\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:5:\"value\";s:0:\"\";s:11:\"placeholder\";s:7:\"website\";s:12:\"options_json\";s:0:\"\";s:7:\"options\";a:0:{}s:8:\"validate\";s:0:\"\";s:11:\"input_class\";s:12:\"form-control\";s:8:\"cssclass\";s:0:\"\";s:12:\"cssclass_str\";s:0:\"\";s:5:\"title\";s:7:\"Website\";s:10:\"title_type\";s:5:\"label\";s:11:\"title_color\";s:0:\"\";s:14:\"title_position\";s:4:\"left\";s:11:\"title_class\";s:0:\"\";s:15:\"title_class_str\";s:0:\"\";s:8:\"subtitle\";s:0:\"\";s:13:\"subtitle_type\";s:5:\"label\";s:14:\"subtitle_color\";s:0:\"\";s:17:\"subtitle_position\";s:0:\"\";s:14:\"subtitle_class\";s:0:\"\";s:18:\"subtitle_class_str\";s:0:\"\";s:11:\"price_field\";b:0;s:5:\"price\";s:0:\"\";s:10:\"price_unit\";s:0:\"\";s:10:\"price_type\";s:6:\"normal\";s:14:\"price_min_unit\";s:0:\"\";s:12:\"price_prefix\";s:0:\"\";s:12:\"price_suffix\";s:0:\"\";s:8:\"required\";i:0;s:7:\"enabled\";s:1:\"0\";s:12:\"rules_action\";s:4:\"show\";s:17:\"rules_action_ajax\";s:4:\"show\";s:22:\"conditional_rules_json\";s:129:\"%5B%5B%5B%5B%7B%22operand_type%22%3A%22product%22%2C%22operator%22%3A%22equals%22%2C%22operand%22%3A%5B%2268%22%5D%7D%5D%5D%5D%5D\";s:17:\"conditional_rules\";a:1:{i:0;O:23:\"WEPO_Condition_Rule_Set\":2:{s:5:\"logic\";s:3:\"and\";s:15:\"condition_rules\";a:1:{i:0;O:19:\"WEPO_Condition_Rule\":2:{s:5:\"logic\";s:2:\"or\";s:14:\"condition_sets\";a:1:{i:0;O:18:\"WEPO_Condition_Set\":2:{s:5:\"logic\";s:3:\"and\";s:10:\"conditions\";a:1:{i:0;O:14:\"WEPO_Condition\":4:{s:12:\"operand_type\";s:7:\"product\";s:7:\"operand\";a:1:{i:0;s:2:\"68\";}s:8:\"operator\";s:6:\"equals\";s:5:\"value\";s:0:\"\";}}}}}}}}s:27:\"conditional_rules_ajax_json\";s:171:\"%5B%5B%5B%5B%7B%22operand_type%22%3A%22field%22%2C%22value%22%3A%22spreker%22%2C%22operator%22%3A%22value_eq%22%2C%22operand%22%3A%5B%22mentoring_type%22%5D%7D%5D%5D%5D%5D\";s:22:\"conditional_rules_ajax\";a:1:{i:0;O:23:\"WEPO_Condition_Rule_Set\":2:{s:5:\"logic\";s:3:\"and\";s:15:\"condition_rules\";a:1:{i:0;O:19:\"WEPO_Condition_Rule\":2:{s:5:\"logic\";s:2:\"or\";s:14:\"condition_sets\";a:1:{i:0;O:18:\"WEPO_Condition_Set\":2:{s:5:\"logic\";s:3:\"and\";s:10:\"conditions\";a:1:{i:0;O:14:\"WEPO_Condition\":4:{s:12:\"operand_type\";s:5:\"field\";s:7:\"operand\";a:1:{i:0;s:14:\"mentoring_type\";}s:8:\"operator\";s:8:\"value_eq\";s:5:\"value\";s:7:\"spreker\";}}}}}}}}s:14:\"separator_type\";s:0:\"\";s:15:\"separator_hight\";s:0:\"\";s:7:\"checked\";i:0;s:7:\"maxsize\";s:0:\"\";s:6:\"accept\";s:0:\"\";s:4:\"cols\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:12:\"default_date\";s:0:\"\";s:11:\"date_format\";s:0:\"\";s:8:\"min_date\";s:0:\"\";s:8:\"max_date\";s:0:\"\";s:10:\"year_range\";s:0:\"\";s:16:\"number_of_months\";s:0:\"\";s:13:\"disabled_days\";s:0:\"\";s:14:\"disabled_dates\";s:0:\"\";s:8:\"min_time\";s:0:\"\";s:8:\"max_time\";s:0:\"\";s:10:\"start_time\";s:0:\"\";s:9:\"time_step\";s:0:\"\";s:11:\"time_format\";s:0:\"\";s:11:\"linked_date\";s:0:\"\";s:12:\"custom_field\";i:1;}s:15:\"mentoring_event\";O:28:\"WEPO_Product_Field_InputText\":64:{s:5:\"order\";s:2:\"10\";s:4:\"type\";s:9:\"inputtext\";s:2:\"id\";s:15:\"mentoring_event\";s:4:\"name\";s:15:\"mentoring_event\";s:8:\"name_old\";s:15:\"mentoring_event\";s:9:\"minlength\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:5:\"value\";s:0:\"\";s:11:\"placeholder\";s:5:\"event\";s:12:\"options_json\";s:0:\"\";s:7:\"options\";a:0:{}s:8:\"validate\";s:0:\"\";s:11:\"input_class\";s:12:\"form-control\";s:8:\"cssclass\";s:0:\"\";s:12:\"cssclass_str\";s:0:\"\";s:5:\"title\";s:5:\"Event\";s:10:\"title_type\";s:5:\"label\";s:11:\"title_color\";s:0:\"\";s:14:\"title_position\";s:4:\"left\";s:11:\"title_class\";s:0:\"\";s:15:\"title_class_str\";s:0:\"\";s:8:\"subtitle\";s:0:\"\";s:13:\"subtitle_type\";s:5:\"label\";s:14:\"subtitle_color\";s:0:\"\";s:17:\"subtitle_position\";s:0:\"\";s:14:\"subtitle_class\";s:0:\"\";s:18:\"subtitle_class_str\";s:0:\"\";s:11:\"price_field\";b:0;s:5:\"price\";s:0:\"\";s:10:\"price_unit\";s:0:\"\";s:10:\"price_type\";s:6:\"normal\";s:14:\"price_min_unit\";s:0:\"\";s:12:\"price_prefix\";s:0:\"\";s:12:\"price_suffix\";s:0:\"\";s:8:\"required\";i:0;s:7:\"enabled\";s:1:\"0\";s:12:\"rules_action\";s:4:\"show\";s:17:\"rules_action_ajax\";s:4:\"show\";s:22:\"conditional_rules_json\";s:129:\"%5B%5B%5B%5B%7B%22operand_type%22%3A%22product%22%2C%22operator%22%3A%22equals%22%2C%22operand%22%3A%5B%2268%22%5D%7D%5D%5D%5D%5D\";s:17:\"conditional_rules\";a:1:{i:0;O:23:\"WEPO_Condition_Rule_Set\":2:{s:5:\"logic\";s:3:\"and\";s:15:\"condition_rules\";a:1:{i:0;O:19:\"WEPO_Condition_Rule\":2:{s:5:\"logic\";s:2:\"or\";s:14:\"condition_sets\";a:1:{i:0;O:18:\"WEPO_Condition_Set\":2:{s:5:\"logic\";s:3:\"and\";s:10:\"conditions\";a:1:{i:0;O:14:\"WEPO_Condition\":4:{s:12:\"operand_type\";s:7:\"product\";s:7:\"operand\";a:1:{i:0;s:2:\"68\";}s:8:\"operator\";s:6:\"equals\";s:5:\"value\";s:0:\"\";}}}}}}}}s:27:\"conditional_rules_ajax_json\";s:171:\"%5B%5B%5B%5B%7B%22operand_type%22%3A%22field%22%2C%22value%22%3A%22spreker%22%2C%22operator%22%3A%22value_eq%22%2C%22operand%22%3A%5B%22mentoring_type%22%5D%7D%5D%5D%5D%5D\";s:22:\"conditional_rules_ajax\";a:1:{i:0;O:23:\"WEPO_Condition_Rule_Set\":2:{s:5:\"logic\";s:3:\"and\";s:15:\"condition_rules\";a:1:{i:0;O:19:\"WEPO_Condition_Rule\":2:{s:5:\"logic\";s:2:\"or\";s:14:\"condition_sets\";a:1:{i:0;O:18:\"WEPO_Condition_Set\":2:{s:5:\"logic\";s:3:\"and\";s:10:\"conditions\";a:1:{i:0;O:14:\"WEPO_Condition\":4:{s:12:\"operand_type\";s:5:\"field\";s:7:\"operand\";a:1:{i:0;s:14:\"mentoring_type\";}s:8:\"operator\";s:8:\"value_eq\";s:5:\"value\";s:7:\"spreker\";}}}}}}}}s:14:\"separator_type\";s:0:\"\";s:15:\"separator_hight\";s:0:\"\";s:7:\"checked\";i:0;s:7:\"maxsize\";s:0:\"\";s:6:\"accept\";s:0:\"\";s:4:\"cols\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:12:\"default_date\";s:0:\"\";s:11:\"date_format\";s:0:\"\";s:8:\"min_date\";s:0:\"\";s:8:\"max_date\";s:0:\"\";s:10:\"year_range\";s:0:\"\";s:16:\"number_of_months\";s:0:\"\";s:13:\"disabled_days\";s:0:\"\";s:14:\"disabled_dates\";s:0:\"\";s:8:\"min_time\";s:0:\"\";s:8:\"max_time\";s:0:\"\";s:10:\"start_time\";s:0:\"\";s:9:\"time_step\";s:0:\"\";s:11:\"time_format\";s:0:\"\";s:11:\"linked_date\";s:0:\"\";s:12:\"custom_field\";i:1;}s:23:\"mentoring_event_address\";O:28:\"WEPO_Product_Field_InputText\":64:{s:5:\"order\";s:2:\"11\";s:4:\"type\";s:9:\"inputtext\";s:2:\"id\";s:23:\"mentoring_event_address\";s:4:\"name\";s:23:\"mentoring_event_address\";s:8:\"name_old\";s:23:\"mentoring_event_address\";s:9:\"minlength\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:5:\"value\";s:0:\"\";s:11:\"placeholder\";s:13:\"event address\";s:12:\"options_json\";s:0:\"\";s:7:\"options\";a:0:{}s:8:\"validate\";s:0:\"\";s:11:\"input_class\";s:12:\"form-control\";s:8:\"cssclass\";s:0:\"\";s:12:\"cssclass_str\";s:0:\"\";s:5:\"title\";s:13:\"Event address\";s:10:\"title_type\";s:5:\"label\";s:11:\"title_color\";s:0:\"\";s:14:\"title_position\";s:4:\"left\";s:11:\"title_class\";s:0:\"\";s:15:\"title_class_str\";s:0:\"\";s:8:\"subtitle\";s:0:\"\";s:13:\"subtitle_type\";s:5:\"label\";s:14:\"subtitle_color\";s:0:\"\";s:17:\"subtitle_position\";s:0:\"\";s:14:\"subtitle_class\";s:0:\"\";s:18:\"subtitle_class_str\";s:0:\"\";s:11:\"price_field\";b:0;s:5:\"price\";s:0:\"\";s:10:\"price_unit\";s:0:\"\";s:10:\"price_type\";s:6:\"normal\";s:14:\"price_min_unit\";s:0:\"\";s:12:\"price_prefix\";s:0:\"\";s:12:\"price_suffix\";s:0:\"\";s:8:\"required\";i:0;s:7:\"enabled\";s:1:\"0\";s:12:\"rules_action\";s:4:\"show\";s:17:\"rules_action_ajax\";s:4:\"show\";s:22:\"conditional_rules_json\";s:129:\"%5B%5B%5B%5B%7B%22operand_type%22%3A%22product%22%2C%22operator%22%3A%22equals%22%2C%22operand%22%3A%5B%2268%22%5D%7D%5D%5D%5D%5D\";s:17:\"conditional_rules\";a:1:{i:0;O:23:\"WEPO_Condition_Rule_Set\":2:{s:5:\"logic\";s:3:\"and\";s:15:\"condition_rules\";a:1:{i:0;O:19:\"WEPO_Condition_Rule\":2:{s:5:\"logic\";s:2:\"or\";s:14:\"condition_sets\";a:1:{i:0;O:18:\"WEPO_Condition_Set\":2:{s:5:\"logic\";s:3:\"and\";s:10:\"conditions\";a:1:{i:0;O:14:\"WEPO_Condition\":4:{s:12:\"operand_type\";s:7:\"product\";s:7:\"operand\";a:1:{i:0;s:2:\"68\";}s:8:\"operator\";s:6:\"equals\";s:5:\"value\";s:0:\"\";}}}}}}}}s:27:\"conditional_rules_ajax_json\";s:171:\"%5B%5B%5B%5B%7B%22operand_type%22%3A%22field%22%2C%22value%22%3A%22spreker%22%2C%22operator%22%3A%22value_eq%22%2C%22operand%22%3A%5B%22mentoring_type%22%5D%7D%5D%5D%5D%5D\";s:22:\"conditional_rules_ajax\";a:1:{i:0;O:23:\"WEPO_Condition_Rule_Set\":2:{s:5:\"logic\";s:3:\"and\";s:15:\"condition_rules\";a:1:{i:0;O:19:\"WEPO_Condition_Rule\":2:{s:5:\"logic\";s:2:\"or\";s:14:\"condition_sets\";a:1:{i:0;O:18:\"WEPO_Condition_Set\":2:{s:5:\"logic\";s:3:\"and\";s:10:\"conditions\";a:1:{i:0;O:14:\"WEPO_Condition\":4:{s:12:\"operand_type\";s:5:\"field\";s:7:\"operand\";a:1:{i:0;s:14:\"mentoring_type\";}s:8:\"operator\";s:8:\"value_eq\";s:5:\"value\";s:7:\"spreker\";}}}}}}}}s:14:\"separator_type\";s:0:\"\";s:15:\"separator_hight\";s:0:\"\";s:7:\"checked\";i:0;s:7:\"maxsize\";s:0:\"\";s:6:\"accept\";s:0:\"\";s:4:\"cols\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:12:\"default_date\";s:0:\"\";s:11:\"date_format\";s:0:\"\";s:8:\"min_date\";s:0:\"\";s:8:\"max_date\";s:0:\"\";s:10:\"year_range\";s:0:\"\";s:16:\"number_of_months\";s:0:\"\";s:13:\"disabled_days\";s:0:\"\";s:14:\"disabled_dates\";s:0:\"\";s:8:\"min_time\";s:0:\"\";s:8:\"max_time\";s:0:\"\";s:10:\"start_time\";s:0:\"\";s:9:\"time_step\";s:0:\"\";s:11:\"time_format\";s:0:\"\";s:11:\"linked_date\";s:0:\"\";s:12:\"custom_field\";i:1;}s:20:\"mentoring_event_date\";O:29:\"WEPO_Product_Field_DatePicker\":66:{s:7:\"pattern\";a:11:{i:0;s:3:\"/d/\";i:1;s:3:\"/j/\";i:2;s:3:\"/l/\";i:3;s:3:\"/z/\";i:4;s:3:\"/S/\";i:5;s:3:\"/F/\";i:6;s:3:\"/M/\";i:7;s:3:\"/n/\";i:8;s:3:\"/m/\";i:9;s:3:\"/Y/\";i:10;s:3:\"/y/\";}s:7:\"replace\";a:11:{i:0;s:2:\"dd\";i:1;s:1:\"d\";i:2;s:2:\"DD\";i:3;s:1:\"o\";i:4;s:0:\"\";i:5;s:2:\"MM\";i:6;s:1:\"M\";i:7;s:1:\"m\";i:8;s:2:\"mm\";i:9;s:2:\"yy\";i:10;s:1:\"y\";}s:12:\"default_date\";s:0:\"\";s:11:\"date_format\";s:8:\"dd/mm/yy\";s:8:\"min_date\";s:0:\"\";s:8:\"max_date\";s:0:\"\";s:10:\"year_range\";s:7:\"-100:+1\";s:16:\"number_of_months\";s:1:\"1\";s:13:\"disabled_days\";s:0:\"\";s:14:\"disabled_dates\";s:0:\"\";s:5:\"order\";s:2:\"12\";s:4:\"type\";s:10:\"datepicker\";s:2:\"id\";s:20:\"mentoring_event_date\";s:4:\"name\";s:20:\"mentoring_event_date\";s:8:\"name_old\";s:20:\"mentoring_event_date\";s:9:\"minlength\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:5:\"value\";s:0:\"\";s:11:\"placeholder\";s:13:\"date of event\";s:12:\"options_json\";s:0:\"\";s:7:\"options\";a:0:{}s:8:\"validate\";s:0:\"\";s:11:\"input_class\";s:12:\"form-control\";s:8:\"cssclass\";s:0:\"\";s:12:\"cssclass_str\";s:0:\"\";s:5:\"title\";s:13:\"Date of event\";s:10:\"title_type\";s:5:\"label\";s:11:\"title_color\";s:0:\"\";s:14:\"title_position\";s:4:\"left\";s:11:\"title_class\";s:0:\"\";s:15:\"title_class_str\";s:0:\"\";s:8:\"subtitle\";s:0:\"\";s:13:\"subtitle_type\";s:5:\"label\";s:14:\"subtitle_color\";s:0:\"\";s:17:\"subtitle_position\";s:0:\"\";s:14:\"subtitle_class\";s:0:\"\";s:18:\"subtitle_class_str\";s:0:\"\";s:11:\"price_field\";b:0;s:5:\"price\";s:0:\"\";s:10:\"price_unit\";s:0:\"\";s:10:\"price_type\";s:6:\"normal\";s:14:\"price_min_unit\";s:0:\"\";s:12:\"price_prefix\";s:0:\"\";s:12:\"price_suffix\";s:0:\"\";s:8:\"required\";i:0;s:7:\"enabled\";s:1:\"0\";s:12:\"rules_action\";s:4:\"show\";s:17:\"rules_action_ajax\";s:4:\"show\";s:22:\"conditional_rules_json\";s:129:\"%5B%5B%5B%5B%7B%22operand_type%22%3A%22product%22%2C%22operator%22%3A%22equals%22%2C%22operand%22%3A%5B%2268%22%5D%7D%5D%5D%5D%5D\";s:17:\"conditional_rules\";a:1:{i:0;O:23:\"WEPO_Condition_Rule_Set\":2:{s:5:\"logic\";s:3:\"and\";s:15:\"condition_rules\";a:1:{i:0;O:19:\"WEPO_Condition_Rule\":2:{s:5:\"logic\";s:2:\"or\";s:14:\"condition_sets\";a:1:{i:0;O:18:\"WEPO_Condition_Set\":2:{s:5:\"logic\";s:3:\"and\";s:10:\"conditions\";a:1:{i:0;O:14:\"WEPO_Condition\":4:{s:12:\"operand_type\";s:7:\"product\";s:7:\"operand\";a:1:{i:0;s:2:\"68\";}s:8:\"operator\";s:6:\"equals\";s:5:\"value\";s:0:\"\";}}}}}}}}s:27:\"conditional_rules_ajax_json\";s:171:\"%5B%5B%5B%5B%7B%22operand_type%22%3A%22field%22%2C%22value%22%3A%22spreker%22%2C%22operator%22%3A%22value_eq%22%2C%22operand%22%3A%5B%22mentoring_type%22%5D%7D%5D%5D%5D%5D\";s:22:\"conditional_rules_ajax\";a:1:{i:0;O:23:\"WEPO_Condition_Rule_Set\":2:{s:5:\"logic\";s:3:\"and\";s:15:\"condition_rules\";a:1:{i:0;O:19:\"WEPO_Condition_Rule\":2:{s:5:\"logic\";s:2:\"or\";s:14:\"condition_sets\";a:1:{i:0;O:18:\"WEPO_Condition_Set\":2:{s:5:\"logic\";s:3:\"and\";s:10:\"conditions\";a:1:{i:0;O:14:\"WEPO_Condition\":4:{s:12:\"operand_type\";s:5:\"field\";s:7:\"operand\";a:1:{i:0;s:14:\"mentoring_type\";}s:8:\"operator\";s:8:\"value_eq\";s:5:\"value\";s:7:\"spreker\";}}}}}}}}s:14:\"separator_type\";s:0:\"\";s:15:\"separator_hight\";s:0:\"\";s:7:\"checked\";i:0;s:7:\"maxsize\";s:0:\"\";s:6:\"accept\";s:0:\"\";s:4:\"cols\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:8:\"min_time\";s:0:\"\";s:8:\"max_time\";s:0:\"\";s:10:\"start_time\";s:0:\"\";s:9:\"time_step\";s:0:\"\";s:11:\"time_format\";s:0:\"\";s:11:\"linked_date\";s:0:\"\";s:12:\"custom_field\";i:1;}s:18:\"mentoring_duration\";O:28:\"WEPO_Product_Field_InputText\":64:{s:5:\"order\";s:2:\"13\";s:4:\"type\";s:9:\"inputtext\";s:2:\"id\";s:18:\"mentoring_duration\";s:4:\"name\";s:18:\"mentoring_duration\";s:8:\"name_old\";s:0:\"\";s:9:\"minlength\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:5:\"value\";s:0:\"\";s:11:\"placeholder\";s:18:\"duration of speach\";s:12:\"options_json\";s:0:\"\";s:7:\"options\";a:0:{}s:8:\"validate\";s:0:\"\";s:11:\"input_class\";s:12:\"form-control\";s:8:\"cssclass\";s:0:\"\";s:12:\"cssclass_str\";s:0:\"\";s:5:\"title\";s:18:\"Duration of speach\";s:10:\"title_type\";s:5:\"label\";s:11:\"title_color\";s:0:\"\";s:14:\"title_position\";s:4:\"left\";s:11:\"title_class\";s:0:\"\";s:15:\"title_class_str\";s:0:\"\";s:8:\"subtitle\";s:0:\"\";s:13:\"subtitle_type\";s:5:\"label\";s:14:\"subtitle_color\";s:0:\"\";s:17:\"subtitle_position\";s:0:\"\";s:14:\"subtitle_class\";s:0:\"\";s:18:\"subtitle_class_str\";s:0:\"\";s:11:\"price_field\";b:0;s:5:\"price\";s:0:\"\";s:10:\"price_unit\";s:0:\"\";s:10:\"price_type\";s:6:\"normal\";s:14:\"price_min_unit\";s:0:\"\";s:12:\"price_prefix\";s:0:\"\";s:12:\"price_suffix\";s:0:\"\";s:8:\"required\";i:0;s:7:\"enabled\";s:1:\"0\";s:12:\"rules_action\";s:4:\"show\";s:17:\"rules_action_ajax\";s:4:\"show\";s:22:\"conditional_rules_json\";s:129:\"%5B%5B%5B%5B%7B%22operand_type%22%3A%22product%22%2C%22operator%22%3A%22equals%22%2C%22operand%22%3A%5B%2268%22%5D%7D%5D%5D%5D%5D\";s:17:\"conditional_rules\";a:1:{i:0;O:23:\"WEPO_Condition_Rule_Set\":2:{s:5:\"logic\";s:3:\"and\";s:15:\"condition_rules\";a:1:{i:0;O:19:\"WEPO_Condition_Rule\":2:{s:5:\"logic\";s:2:\"or\";s:14:\"condition_sets\";a:1:{i:0;O:18:\"WEPO_Condition_Set\":2:{s:5:\"logic\";s:3:\"and\";s:10:\"conditions\";a:1:{i:0;O:14:\"WEPO_Condition\":4:{s:12:\"operand_type\";s:7:\"product\";s:7:\"operand\";a:1:{i:0;s:2:\"68\";}s:8:\"operator\";s:6:\"equals\";s:5:\"value\";s:0:\"\";}}}}}}}}s:27:\"conditional_rules_ajax_json\";s:171:\"%5B%5B%5B%5B%7B%22operand_type%22%3A%22field%22%2C%22value%22%3A%22spreker%22%2C%22operator%22%3A%22value_eq%22%2C%22operand%22%3A%5B%22mentoring_type%22%5D%7D%5D%5D%5D%5D\";s:22:\"conditional_rules_ajax\";a:1:{i:0;O:23:\"WEPO_Condition_Rule_Set\":2:{s:5:\"logic\";s:3:\"and\";s:15:\"condition_rules\";a:1:{i:0;O:19:\"WEPO_Condition_Rule\":2:{s:5:\"logic\";s:2:\"or\";s:14:\"condition_sets\";a:1:{i:0;O:18:\"WEPO_Condition_Set\":2:{s:5:\"logic\";s:3:\"and\";s:10:\"conditions\";a:1:{i:0;O:14:\"WEPO_Condition\":4:{s:12:\"operand_type\";s:5:\"field\";s:7:\"operand\";a:1:{i:0;s:14:\"mentoring_type\";}s:8:\"operator\";s:8:\"value_eq\";s:5:\"value\";s:7:\"spreker\";}}}}}}}}s:14:\"separator_type\";s:0:\"\";s:15:\"separator_hight\";s:0:\"\";s:7:\"checked\";i:0;s:7:\"maxsize\";s:0:\"\";s:6:\"accept\";s:0:\"\";s:4:\"cols\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:12:\"default_date\";s:0:\"\";s:11:\"date_format\";s:0:\"\";s:8:\"min_date\";s:0:\"\";s:8:\"max_date\";s:0:\"\";s:10:\"year_range\";s:0:\"\";s:16:\"number_of_months\";s:0:\"\";s:13:\"disabled_days\";s:0:\"\";s:14:\"disabled_dates\";s:0:\"\";s:8:\"min_time\";s:0:\"\";s:8:\"max_time\";s:0:\"\";s:10:\"start_time\";s:0:\"\";s:9:\"time_step\";s:0:\"\";s:11:\"time_format\";s:0:\"\";s:11:\"linked_date\";s:0:\"\";s:12:\"custom_field\";i:1;}s:22:\"mentoring_company_name\";O:28:\"WEPO_Product_Field_InputText\":64:{s:5:\"order\";s:2:\"14\";s:4:\"type\";s:9:\"inputtext\";s:2:\"id\";s:22:\"mentoring_company_name\";s:4:\"name\";s:22:\"mentoring_company_name\";s:8:\"name_old\";s:0:\"\";s:9:\"minlength\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:5:\"value\";s:0:\"\";s:11:\"placeholder\";s:12:\"company name\";s:12:\"options_json\";s:0:\"\";s:7:\"options\";a:0:{}s:8:\"validate\";s:0:\"\";s:11:\"input_class\";s:12:\"form-control\";s:8:\"cssclass\";s:0:\"\";s:12:\"cssclass_str\";s:0:\"\";s:5:\"title\";s:12:\"Company name\";s:10:\"title_type\";s:5:\"label\";s:11:\"title_color\";s:0:\"\";s:14:\"title_position\";s:4:\"left\";s:11:\"title_class\";s:0:\"\";s:15:\"title_class_str\";s:0:\"\";s:8:\"subtitle\";s:0:\"\";s:13:\"subtitle_type\";s:5:\"label\";s:14:\"subtitle_color\";s:0:\"\";s:17:\"subtitle_position\";s:0:\"\";s:14:\"subtitle_class\";s:0:\"\";s:18:\"subtitle_class_str\";s:0:\"\";s:11:\"price_field\";b:0;s:5:\"price\";s:0:\"\";s:10:\"price_unit\";s:0:\"\";s:10:\"price_type\";s:6:\"normal\";s:14:\"price_min_unit\";s:0:\"\";s:12:\"price_prefix\";s:0:\"\";s:12:\"price_suffix\";s:0:\"\";s:8:\"required\";i:0;s:7:\"enabled\";s:1:\"0\";s:12:\"rules_action\";s:4:\"show\";s:17:\"rules_action_ajax\";s:4:\"show\";s:22:\"conditional_rules_json\";s:129:\"%5B%5B%5B%5B%7B%22operand_type%22%3A%22product%22%2C%22operator%22%3A%22equals%22%2C%22operand%22%3A%5B%2268%22%5D%7D%5D%5D%5D%5D\";s:17:\"conditional_rules\";a:1:{i:0;O:23:\"WEPO_Condition_Rule_Set\":2:{s:5:\"logic\";s:3:\"and\";s:15:\"condition_rules\";a:1:{i:0;O:19:\"WEPO_Condition_Rule\":2:{s:5:\"logic\";s:2:\"or\";s:14:\"condition_sets\";a:1:{i:0;O:18:\"WEPO_Condition_Set\":2:{s:5:\"logic\";s:3:\"and\";s:10:\"conditions\";a:1:{i:0;O:14:\"WEPO_Condition\":4:{s:12:\"operand_type\";s:7:\"product\";s:7:\"operand\";a:1:{i:0;s:2:\"68\";}s:8:\"operator\";s:6:\"equals\";s:5:\"value\";s:0:\"\";}}}}}}}}s:27:\"conditional_rules_ajax_json\";s:171:\"%5B%5B%5B%5B%7B%22operand_type%22%3A%22field%22%2C%22value%22%3A%22spreker%22%2C%22operator%22%3A%22value_eq%22%2C%22operand%22%3A%5B%22mentoring_type%22%5D%7D%5D%5D%5D%5D\";s:22:\"conditional_rules_ajax\";a:1:{i:0;O:23:\"WEPO_Condition_Rule_Set\":2:{s:5:\"logic\";s:3:\"and\";s:15:\"condition_rules\";a:1:{i:0;O:19:\"WEPO_Condition_Rule\":2:{s:5:\"logic\";s:2:\"or\";s:14:\"condition_sets\";a:1:{i:0;O:18:\"WEPO_Condition_Set\":2:{s:5:\"logic\";s:3:\"and\";s:10:\"conditions\";a:1:{i:0;O:14:\"WEPO_Condition\":4:{s:12:\"operand_type\";s:5:\"field\";s:7:\"operand\";a:1:{i:0;s:14:\"mentoring_type\";}s:8:\"operator\";s:8:\"value_eq\";s:5:\"value\";s:7:\"spreker\";}}}}}}}}s:14:\"separator_type\";s:0:\"\";s:15:\"separator_hight\";s:0:\"\";s:7:\"checked\";i:0;s:7:\"maxsize\";s:0:\"\";s:6:\"accept\";s:0:\"\";s:4:\"cols\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:12:\"default_date\";s:0:\"\";s:11:\"date_format\";s:0:\"\";s:8:\"min_date\";s:0:\"\";s:8:\"max_date\";s:0:\"\";s:10:\"year_range\";s:0:\"\";s:16:\"number_of_months\";s:0:\"\";s:13:\"disabled_days\";s:0:\"\";s:14:\"disabled_dates\";s:0:\"\";s:8:\"min_time\";s:0:\"\";s:8:\"max_time\";s:0:\"\";s:10:\"start_time\";s:0:\"\";s:9:\"time_step\";s:0:\"\";s:11:\"time_format\";s:0:\"\";s:11:\"linked_date\";s:0:\"\";s:12:\"custom_field\";i:1;}s:3:\"vat\";O:28:\"WEPO_Product_Field_InputText\":64:{s:5:\"order\";s:2:\"15\";s:4:\"type\";s:9:\"inputtext\";s:2:\"id\";s:3:\"vat\";s:4:\"name\";s:3:\"vat\";s:8:\"name_old\";s:0:\"\";s:9:\"minlength\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:5:\"value\";s:0:\"\";s:11:\"placeholder\";s:3:\"vat\";s:12:\"options_json\";s:0:\"\";s:7:\"options\";a:0:{}s:8:\"validate\";s:0:\"\";s:11:\"input_class\";s:12:\"form-control\";s:8:\"cssclass\";s:0:\"\";s:12:\"cssclass_str\";s:0:\"\";s:5:\"title\";s:3:\"VAT\";s:10:\"title_type\";s:5:\"label\";s:11:\"title_color\";s:0:\"\";s:14:\"title_position\";s:4:\"left\";s:11:\"title_class\";s:0:\"\";s:15:\"title_class_str\";s:0:\"\";s:8:\"subtitle\";s:0:\"\";s:13:\"subtitle_type\";s:5:\"label\";s:14:\"subtitle_color\";s:0:\"\";s:17:\"subtitle_position\";s:0:\"\";s:14:\"subtitle_class\";s:0:\"\";s:18:\"subtitle_class_str\";s:0:\"\";s:11:\"price_field\";b:0;s:5:\"price\";s:0:\"\";s:10:\"price_unit\";s:0:\"\";s:10:\"price_type\";s:6:\"normal\";s:14:\"price_min_unit\";s:0:\"\";s:12:\"price_prefix\";s:0:\"\";s:12:\"price_suffix\";s:0:\"\";s:8:\"required\";i:0;s:7:\"enabled\";s:1:\"0\";s:12:\"rules_action\";s:4:\"show\";s:17:\"rules_action_ajax\";s:4:\"show\";s:22:\"conditional_rules_json\";s:129:\"%5B%5B%5B%5B%7B%22operand_type%22%3A%22product%22%2C%22operator%22%3A%22equals%22%2C%22operand%22%3A%5B%2268%22%5D%7D%5D%5D%5D%5D\";s:17:\"conditional_rules\";a:1:{i:0;O:23:\"WEPO_Condition_Rule_Set\":2:{s:5:\"logic\";s:3:\"and\";s:15:\"condition_rules\";a:1:{i:0;O:19:\"WEPO_Condition_Rule\":2:{s:5:\"logic\";s:2:\"or\";s:14:\"condition_sets\";a:1:{i:0;O:18:\"WEPO_Condition_Set\":2:{s:5:\"logic\";s:3:\"and\";s:10:\"conditions\";a:1:{i:0;O:14:\"WEPO_Condition\":4:{s:12:\"operand_type\";s:7:\"product\";s:7:\"operand\";a:1:{i:0;s:2:\"68\";}s:8:\"operator\";s:6:\"equals\";s:5:\"value\";s:0:\"\";}}}}}}}}s:27:\"conditional_rules_ajax_json\";s:171:\"%5B%5B%5B%5B%7B%22operand_type%22%3A%22field%22%2C%22value%22%3A%22spreker%22%2C%22operator%22%3A%22value_eq%22%2C%22operand%22%3A%5B%22mentoring_type%22%5D%7D%5D%5D%5D%5D\";s:22:\"conditional_rules_ajax\";a:1:{i:0;O:23:\"WEPO_Condition_Rule_Set\":2:{s:5:\"logic\";s:3:\"and\";s:15:\"condition_rules\";a:1:{i:0;O:19:\"WEPO_Condition_Rule\":2:{s:5:\"logic\";s:2:\"or\";s:14:\"condition_sets\";a:1:{i:0;O:18:\"WEPO_Condition_Set\":2:{s:5:\"logic\";s:3:\"and\";s:10:\"conditions\";a:1:{i:0;O:14:\"WEPO_Condition\":4:{s:12:\"operand_type\";s:5:\"field\";s:7:\"operand\";a:1:{i:0;s:14:\"mentoring_type\";}s:8:\"operator\";s:8:\"value_eq\";s:5:\"value\";s:7:\"spreker\";}}}}}}}}s:14:\"separator_type\";s:0:\"\";s:15:\"separator_hight\";s:0:\"\";s:7:\"checked\";i:0;s:7:\"maxsize\";s:0:\"\";s:6:\"accept\";s:0:\"\";s:4:\"cols\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:12:\"default_date\";s:0:\"\";s:11:\"date_format\";s:0:\"\";s:8:\"min_date\";s:0:\"\";s:8:\"max_date\";s:0:\"\";s:10:\"year_range\";s:0:\"\";s:16:\"number_of_months\";s:0:\"\";s:13:\"disabled_days\";s:0:\"\";s:14:\"disabled_dates\";s:0:\"\";s:8:\"min_time\";s:0:\"\";s:8:\"max_time\";s:0:\"\";s:10:\"start_time\";s:0:\"\";s:9:\"time_step\";s:0:\"\";s:11:\"time_format\";s:0:\"\";s:11:\"linked_date\";s:0:\"\";s:12:\"custom_field\";i:1;}}}}', 'yes'),
(2037, 'thwepo_section_hook_map', 'a:1:{s:29:\"woo_before_add_to_cart_button\";N;}', 'yes'),
(2038, 'thwepo_options_name_title_map', 'a:0:{}', 'yes'),
(2066, 'thwepo_advanced_settings', 'a:5:{s:25:\"section_custom_validators\";s:0:\"\";s:17:\"custom_validators\";a:0:{}s:18:\"confirm_validators\";a:0:{}s:22:\"section_other_settings\";s:0:\"\";s:33:\"disable_select2_for_select_fields\";s:0:\"\";}', 'yes'),
(2067, 'woocommerce_extra_product_options_thlmdata', 'a:12:{s:10:\"identifier\";s:33:\"woocommerce-extra-product-options\";s:11:\"license_key\";s:30:\"SUF9T9DBJYA8H3G1HJVLD6D1PYEZPU\";s:14:\"license_status\";s:6:\"active\";s:17:\"activation_status\";s:9:\"activated\";s:13:\"purchase_date\";s:14:\"June 3rd, 2019\";s:11:\"expiry_date\";s:14:\"June 2nd, 2020\";s:11:\"expiry_days\";s:0:\"\";s:13:\"response_code\";s:4:\"R007\";s:12:\"response_msg\";s:35:\"License Key activated successfully.\";s:13:\"expiry_notice\";s:0:\"\";s:13:\"response_flag\";s:7:\"success\";s:11:\"update_flag\";s:1:\"Y\";}', 'yes'),
(2100, 'dpsp_location_sidebar', 'a:4:{s:8:\"networks\";a:3:{s:8:\"facebook\";a:1:{s:5:\"label\";s:8:\"Facebook\";}s:7:\"twitter\";a:1:{s:5:\"label\";s:7:\"Twitter\";}s:9:\"pinterest\";a:1:{s:5:\"label\";s:9:\"Pinterest\";}}s:12:\"button_style\";s:1:\"1\";s:7:\"display\";a:4:{s:5:\"shape\";s:7:\"rounded\";s:14:\"icon_animation\";s:3:\"yes\";s:16:\"show_count_total\";s:3:\"yes\";s:8:\"position\";s:4:\"left\";}s:17:\"post_type_display\";a:1:{i:0;s:4:\"post\";}}', 'yes'),
(2101, 'dpsp_location_content', 'a:5:{s:6:\"active\";s:1:\"1\";s:8:\"networks\";a:3:{s:8:\"facebook\";a:1:{s:5:\"label\";s:5:\"Share\";}s:7:\"twitter\";a:1:{s:5:\"label\";s:5:\"Tweet\";}s:9:\"pinterest\";a:1:{s:5:\"label\";s:3:\"Pin\";}}s:7:\"display\";a:12:{s:5:\"shape\";s:11:\"rectangular\";s:14:\"icon_animation\";s:3:\"yes\";s:8:\"position\";s:6:\"bottom\";s:12:\"column_count\";s:1:\"3\";s:7:\"message\";s:20:\"Share this blog post\";s:11:\"show_labels\";s:3:\"yes\";s:7:\"spacing\";s:3:\"yes\";s:11:\"show_mobile\";s:3:\"yes\";s:11:\"screen_size\";s:0:\"\";s:10:\"show_count\";s:3:\"yes\";s:16:\"show_count_total\";s:3:\"yes\";s:20:\"total_count_position\";s:5:\"after\";}s:17:\"post_type_display\";a:1:{i:0;s:4:\"post\";}s:12:\"button_style\";s:1:\"1\";}', 'yes'),
(2102, 'dpsp_version', '1.5.3', 'yes'),
(2103, 'dpsp_first_activation', '1559846102', 'yes'),
(2104, 'dpsp_settings', 'a:0:{}', 'yes'),
(2105, 'dpsp_version_update_1_5_1', '1', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(2108, 'dpsp_top_shared_posts', '{\"post\":{\"102\":0,\"30\":0,\"1\":0},\"page\":{\"15\":19},\"product\":{\"62\":0},\"tribe_events\":{\"27\":0,\"52\":0,\"34\":0}}', 'yes'),
(2111, 'WPLANG', '', 'yes'),
(2112, 'new_admin_email', 'info@mindengineer.com', 'yes'),
(2113, 'adminhash', 'a:2:{s:4:\"hash\";s:32:\"900a575a38c0a369dd8f6986d7769282\";s:8:\"newemail\";s:21:\"info@mindengineer.com\";}', 'yes'),
(2223, '_transient_timeout_external_ip_address_::1', '1564506897', 'no'),
(2224, '_transient_external_ip_address_::1', '89.64.23.250', 'no'),
(2228, '_transient_timeout_tribe_feature_detection', '1564506899', 'no'),
(2229, '_transient_tribe_feature_detection', 'a:1:{s:22:\"supports_async_process\";b:1;}', 'no'),
(2230, '_transient_timeout_wc_term_counts', '1566500705', 'no'),
(2231, '_transient_wc_term_counts', 'a:3:{i:17;s:1:\"0\";i:15;s:1:\"1\";i:16;s:1:\"2\";}', 'no'),
(2242, '_transient_wc_count_comments', 'O:8:\"stdClass\":7:{s:14:\"total_comments\";i:0;s:3:\"all\";i:0;s:9:\"moderated\";i:0;s:8:\"approved\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(2243, '_transient_as_comment_count', 'O:8:\"stdClass\":7:{s:14:\"total_comments\";i:0;s:3:\"all\";i:0;s:9:\"moderated\";i:0;s:8:\"approved\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(2244, '_site_transient_timeout_browser_630d2c68223a9b4451a300b70984ca4f', '1564508948', 'no'),
(2245, '_site_transient_browser_630d2c68223a9b4451a300b70984ca4f', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"75.0.3770.100\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(2248, '_site_transient_timeout_php_check_80cadeb689bccc0409faf12154bcfbb7', '1564508948', 'no'),
(2249, '_site_transient_php_check_80cadeb689bccc0409faf12154bcfbb7', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(2262, '_transient_timeout_wc_low_stock_count', '1566496150', 'no'),
(2263, '_transient_wc_low_stock_count', '0', 'no'),
(2264, '_transient_timeout_wc_outofstock_count', '1566496150', 'no'),
(2265, '_transient_wc_outofstock_count', '0', 'no'),
(2355, '_transient_timeout_wc_featured_products', '1566500705', 'no'),
(2356, '_transient_wc_featured_products', 'a:0:{}', 'no'),
(2357, '_transient_timeout_wc_products_onsale', '1566500705', 'no'),
(2358, '_transient_wc_products_onsale', 'a:0:{}', 'no'),
(2359, '_transient_timeout_wc_product_loop040c1559764797', '1566500705', 'no'),
(2360, '_transient_wc_product_loop040c1559764797', 'O:8:\"stdClass\":5:{s:3:\"ids\";a:2:{i:0;i:68;i:1;i:62;}s:5:\"total\";i:2;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:2;s:12:\"current_page\";i:1;}', 'no'),
(2361, '_transient_timeout_wc_product_loopa47f1559764797', '1566500705', 'no'),
(2362, '_transient_wc_product_loopa47f1559764797', 'O:8:\"stdClass\":5:{s:3:\"ids\";a:3:{i:0;i:61;i:1;i:68;i:2;i:62;}s:5:\"total\";i:3;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:4;s:12:\"current_page\";i:1;}', 'no'),
(2570, '_transient_timeout_wc_shipping_method_count_1_1552949094', '1566672805', 'no'),
(2571, '_transient_wc_shipping_method_count_1_1552949094', '0', 'no'),
(2587, '_site_transient_timeout_theme_roots', '1564310407', 'no'),
(2588, '_site_transient_theme_roots', 'a:1:{s:10:\"storefront\";s:7:\"/themes\";}', 'no'),
(2594, '_transient_timeout_plugin_slugs', '1564395102', 'no'),
(2595, '_transient_plugin_slugs', 'a:13:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:36:\"contact-form-7/wp-contact-form-7.php\";i:2;s:31:\"event-tickets/event-tickets.php\";i:3;s:41:\"event-tickets-plus/event-tickets-plus.php\";i:4;s:20:\"social-pug/index.php\";i:5;s:43:\"the-events-calendar/the-events-calendar.php\";i:6;s:27:\"woocommerce/woocommerce.php\";i:7;s:69:\"woo-gutenberg-products-block/woocommerce-gutenberg-products-block.php\";i:8;s:79:\"woocommerce-extra-product-options-pro/woocommerce-extra-product-options-pro.php\";i:9;s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";i:10;s:45:\"woocommerce-services/woocommerce-services.php\";i:11;s:29:\"wp-mail-smtp/wp_mail_smtp.php\";i:12;s:27:\"wp-super-cache/wp-cache.php\";}', 'no'),
(2596, '_transient_timeout_tribe_aggregator_services_list', '1564395036', 'no'),
(2597, '_transient_tribe_aggregator_services_list', 'a:1:{s:6:\"origin\";a:1:{i:0;O:8:\"stdClass\":2:{s:2:\"id\";s:3:\"csv\";s:4:\"name\";s:8:\"CSV File\";}}}', 'no'),
(2598, '_transient_timeout_tribe_plugin_upgrade_notice-cac53096', '1564395102', 'no'),
(2599, '_transient_tribe_plugin_upgrade_notice-cac53096', '', 'no'),
(2600, '_transient_timeout_wc_upgrade_notice_3.6.5', '1564395041', 'no'),
(2601, '_transient_wc_upgrade_notice_3.6.5', '', 'no'),
(2602, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1564319459', 'no'),
(2603, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'O:8:\"stdClass\":100:{s:6:\"widget\";a:3:{s:4:\"name\";s:6:\"widget\";s:4:\"slug\";s:6:\"widget\";s:5:\"count\";i:4601;}s:11:\"woocommerce\";a:3:{s:4:\"name\";s:11:\"woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:5:\"count\";i:3556;}s:4:\"post\";a:3:{s:4:\"name\";s:4:\"post\";s:4:\"slug\";s:4:\"post\";s:5:\"count\";i:2630;}s:5:\"admin\";a:3:{s:4:\"name\";s:5:\"admin\";s:4:\"slug\";s:5:\"admin\";s:5:\"count\";i:2502;}s:5:\"posts\";a:3:{s:4:\"name\";s:5:\"posts\";s:4:\"slug\";s:5:\"posts\";s:5:\"count\";i:1928;}s:9:\"shortcode\";a:3:{s:4:\"name\";s:9:\"shortcode\";s:4:\"slug\";s:9:\"shortcode\";s:5:\"count\";i:1746;}s:8:\"comments\";a:3:{s:4:\"name\";s:8:\"comments\";s:4:\"slug\";s:8:\"comments\";s:5:\"count\";i:1738;}s:7:\"twitter\";a:3:{s:4:\"name\";s:7:\"twitter\";s:4:\"slug\";s:7:\"twitter\";s:5:\"count\";i:1470;}s:6:\"images\";a:3:{s:4:\"name\";s:6:\"images\";s:4:\"slug\";s:6:\"images\";s:5:\"count\";i:1445;}s:6:\"google\";a:3:{s:4:\"name\";s:6:\"google\";s:4:\"slug\";s:6:\"google\";s:5:\"count\";i:1443;}s:8:\"facebook\";a:3:{s:4:\"name\";s:8:\"facebook\";s:4:\"slug\";s:8:\"facebook\";s:5:\"count\";i:1435;}s:5:\"image\";a:3:{s:4:\"name\";s:5:\"image\";s:4:\"slug\";s:5:\"image\";s:5:\"count\";i:1373;}s:3:\"seo\";a:3:{s:4:\"name\";s:3:\"seo\";s:4:\"slug\";s:3:\"seo\";s:5:\"count\";i:1336;}s:7:\"sidebar\";a:3:{s:4:\"name\";s:7:\"sidebar\";s:4:\"slug\";s:7:\"sidebar\";s:5:\"count\";i:1295;}s:7:\"gallery\";a:3:{s:4:\"name\";s:7:\"gallery\";s:4:\"slug\";s:7:\"gallery\";s:5:\"count\";i:1148;}s:5:\"email\";a:3:{s:4:\"name\";s:5:\"email\";s:4:\"slug\";s:5:\"email\";s:5:\"count\";i:1129;}s:4:\"page\";a:3:{s:4:\"name\";s:4:\"page\";s:4:\"slug\";s:4:\"page\";s:5:\"count\";i:1101;}s:6:\"social\";a:3:{s:4:\"name\";s:6:\"social\";s:4:\"slug\";s:6:\"social\";s:5:\"count\";i:1070;}s:9:\"ecommerce\";a:3:{s:4:\"name\";s:9:\"ecommerce\";s:4:\"slug\";s:9:\"ecommerce\";s:5:\"count\";i:1026;}s:5:\"login\";a:3:{s:4:\"name\";s:5:\"login\";s:4:\"slug\";s:5:\"login\";s:5:\"count\";i:944;}s:5:\"links\";a:3:{s:4:\"name\";s:5:\"links\";s:4:\"slug\";s:5:\"links\";s:5:\"count\";i:855;}s:7:\"widgets\";a:3:{s:4:\"name\";s:7:\"widgets\";s:4:\"slug\";s:7:\"widgets\";s:5:\"count\";i:845;}s:5:\"video\";a:3:{s:4:\"name\";s:5:\"video\";s:4:\"slug\";s:5:\"video\";s:5:\"count\";i:840;}s:8:\"security\";a:3:{s:4:\"name\";s:8:\"security\";s:4:\"slug\";s:8:\"security\";s:5:\"count\";i:806;}s:4:\"spam\";a:3:{s:4:\"name\";s:4:\"spam\";s:4:\"slug\";s:4:\"spam\";s:5:\"count\";i:745;}s:7:\"content\";a:3:{s:4:\"name\";s:7:\"content\";s:4:\"slug\";s:7:\"content\";s:5:\"count\";i:738;}s:10:\"buddypress\";a:3:{s:4:\"name\";s:10:\"buddypress\";s:4:\"slug\";s:10:\"buddypress\";s:5:\"count\";i:731;}s:6:\"slider\";a:3:{s:4:\"name\";s:6:\"slider\";s:4:\"slug\";s:6:\"slider\";s:5:\"count\";i:721;}s:10:\"e-commerce\";a:3:{s:4:\"name\";s:10:\"e-commerce\";s:4:\"slug\";s:10:\"e-commerce\";s:5:\"count\";i:713;}s:9:\"analytics\";a:3:{s:4:\"name\";s:9:\"analytics\";s:4:\"slug\";s:9:\"analytics\";s:5:\"count\";i:701;}s:3:\"rss\";a:3:{s:4:\"name\";s:3:\"rss\";s:4:\"slug\";s:3:\"rss\";s:5:\"count\";i:699;}s:5:\"pages\";a:3:{s:4:\"name\";s:5:\"pages\";s:4:\"slug\";s:5:\"pages\";s:5:\"count\";i:686;}s:5:\"media\";a:3:{s:4:\"name\";s:5:\"media\";s:4:\"slug\";s:5:\"media\";s:5:\"count\";i:677;}s:6:\"search\";a:3:{s:4:\"name\";s:6:\"search\";s:4:\"slug\";s:6:\"search\";s:5:\"count\";i:664;}s:4:\"form\";a:3:{s:4:\"name\";s:4:\"form\";s:4:\"slug\";s:4:\"form\";s:5:\"count\";i:662;}s:6:\"jquery\";a:3:{s:4:\"name\";s:6:\"jquery\";s:4:\"slug\";s:6:\"jquery\";s:5:\"count\";i:653;}s:4:\"feed\";a:3:{s:4:\"name\";s:4:\"feed\";s:4:\"slug\";s:4:\"feed\";s:5:\"count\";i:633;}s:4:\"menu\";a:3:{s:4:\"name\";s:4:\"menu\";s:4:\"slug\";s:4:\"menu\";s:5:\"count\";i:625;}s:4:\"ajax\";a:3:{s:4:\"name\";s:4:\"ajax\";s:4:\"slug\";s:4:\"ajax\";s:5:\"count\";i:624;}s:8:\"category\";a:3:{s:4:\"name\";s:8:\"category\";s:4:\"slug\";s:8:\"category\";s:5:\"count\";i:619;}s:6:\"editor\";a:3:{s:4:\"name\";s:6:\"editor\";s:4:\"slug\";s:6:\"editor\";s:5:\"count\";i:593;}s:5:\"embed\";a:3:{s:4:\"name\";s:5:\"embed\";s:4:\"slug\";s:5:\"embed\";s:5:\"count\";i:591;}s:10:\"javascript\";a:3:{s:4:\"name\";s:10:\"javascript\";s:4:\"slug\";s:10:\"javascript\";s:5:\"count\";i:572;}s:3:\"css\";a:3:{s:4:\"name\";s:3:\"css\";s:4:\"slug\";s:3:\"css\";s:5:\"count\";i:568;}s:4:\"link\";a:3:{s:4:\"name\";s:4:\"link\";s:4:\"slug\";s:4:\"link\";s:5:\"count\";i:561;}s:7:\"youtube\";a:3:{s:4:\"name\";s:7:\"youtube\";s:4:\"slug\";s:7:\"youtube\";s:5:\"count\";i:556;}s:12:\"contact-form\";a:3:{s:4:\"name\";s:12:\"contact form\";s:4:\"slug\";s:12:\"contact-form\";s:5:\"count\";i:550;}s:5:\"share\";a:3:{s:4:\"name\";s:5:\"share\";s:4:\"slug\";s:5:\"share\";s:5:\"count\";i:544;}s:5:\"theme\";a:3:{s:4:\"name\";s:5:\"theme\";s:4:\"slug\";s:5:\"theme\";s:5:\"count\";i:535;}s:10:\"responsive\";a:3:{s:4:\"name\";s:10:\"responsive\";s:4:\"slug\";s:10:\"responsive\";s:5:\"count\";i:527;}s:7:\"comment\";a:3:{s:4:\"name\";s:7:\"comment\";s:4:\"slug\";s:7:\"comment\";s:5:\"count\";i:527;}s:9:\"dashboard\";a:3:{s:4:\"name\";s:9:\"dashboard\";s:4:\"slug\";s:9:\"dashboard\";s:5:\"count\";i:519;}s:6:\"custom\";a:3:{s:4:\"name\";s:6:\"custom\";s:4:\"slug\";s:6:\"custom\";s:5:\"count\";i:515;}s:9:\"affiliate\";a:3:{s:4:\"name\";s:9:\"affiliate\";s:4:\"slug\";s:9:\"affiliate\";s:5:\"count\";i:512;}s:3:\"ads\";a:3:{s:4:\"name\";s:3:\"ads\";s:4:\"slug\";s:3:\"ads\";s:5:\"count\";i:507;}s:7:\"payment\";a:3:{s:4:\"name\";s:7:\"payment\";s:4:\"slug\";s:7:\"payment\";s:5:\"count\";i:497;}s:10:\"categories\";a:3:{s:4:\"name\";s:10:\"categories\";s:4:\"slug\";s:10:\"categories\";s:5:\"count\";i:496;}s:6:\"button\";a:3:{s:4:\"name\";s:6:\"button\";s:4:\"slug\";s:6:\"button\";s:5:\"count\";i:478;}s:4:\"tags\";a:3:{s:4:\"name\";s:4:\"tags\";s:4:\"slug\";s:4:\"tags\";s:5:\"count\";i:477;}s:7:\"contact\";a:3:{s:4:\"name\";s:7:\"contact\";s:4:\"slug\";s:7:\"contact\";s:5:\"count\";i:476;}s:4:\"user\";a:3:{s:4:\"name\";s:4:\"user\";s:4:\"slug\";s:4:\"user\";s:5:\"count\";i:473;}s:3:\"api\";a:3:{s:4:\"name\";s:3:\"api\";s:4:\"slug\";s:3:\"api\";s:5:\"count\";i:472;}s:6:\"mobile\";a:3:{s:4:\"name\";s:6:\"mobile\";s:4:\"slug\";s:6:\"mobile\";s:5:\"count\";i:458;}s:5:\"users\";a:3:{s:4:\"name\";s:5:\"users\";s:4:\"slug\";s:5:\"users\";s:5:\"count\";i:453;}s:6:\"events\";a:3:{s:4:\"name\";s:6:\"events\";s:4:\"slug\";s:6:\"events\";s:5:\"count\";i:442;}s:15:\"payment-gateway\";a:3:{s:4:\"name\";s:15:\"payment gateway\";s:4:\"slug\";s:15:\"payment-gateway\";s:5:\"count\";i:431;}s:5:\"photo\";a:3:{s:4:\"name\";s:5:\"photo\";s:4:\"slug\";s:5:\"photo\";s:5:\"count\";i:430;}s:9:\"slideshow\";a:3:{s:4:\"name\";s:9:\"slideshow\";s:4:\"slug\";s:9:\"slideshow\";s:5:\"count\";i:422;}s:5:\"stats\";a:3:{s:4:\"name\";s:5:\"stats\";s:4:\"slug\";s:5:\"stats\";s:5:\"count\";i:416;}s:6:\"photos\";a:3:{s:4:\"name\";s:6:\"photos\";s:4:\"slug\";s:6:\"photos\";s:5:\"count\";i:416;}s:10:\"navigation\";a:3:{s:4:\"name\";s:10:\"navigation\";s:4:\"slug\";s:10:\"navigation\";s:5:\"count\";i:413;}s:9:\"marketing\";a:3:{s:4:\"name\";s:9:\"marketing\";s:4:\"slug\";s:9:\"marketing\";s:5:\"count\";i:406;}s:10:\"statistics\";a:3:{s:4:\"name\";s:10:\"statistics\";s:4:\"slug\";s:10:\"statistics\";s:5:\"count\";i:400;}s:8:\"calendar\";a:3:{s:4:\"name\";s:8:\"calendar\";s:4:\"slug\";s:8:\"calendar\";s:5:\"count\";i:399;}s:4:\"chat\";a:3:{s:4:\"name\";s:4:\"chat\";s:4:\"slug\";s:4:\"chat\";s:5:\"count\";i:393;}s:5:\"popup\";a:3:{s:4:\"name\";s:5:\"popup\";s:4:\"slug\";s:5:\"popup\";s:5:\"count\";i:393;}s:4:\"news\";a:3:{s:4:\"name\";s:4:\"news\";s:4:\"slug\";s:4:\"news\";s:5:\"count\";i:386;}s:10:\"shortcodes\";a:3:{s:4:\"name\";s:10:\"shortcodes\";s:4:\"slug\";s:10:\"shortcodes\";s:5:\"count\";i:384;}s:10:\"newsletter\";a:3:{s:4:\"name\";s:10:\"newsletter\";s:4:\"slug\";s:10:\"newsletter\";s:5:\"count\";i:381;}s:12:\"social-media\";a:3:{s:4:\"name\";s:12:\"social media\";s:4:\"slug\";s:12:\"social-media\";s:5:\"count\";i:377;}s:5:\"forms\";a:3:{s:4:\"name\";s:5:\"forms\";s:4:\"slug\";s:5:\"forms\";s:5:\"count\";i:369;}s:9:\"gutenberg\";a:3:{s:4:\"name\";s:9:\"gutenberg\";s:4:\"slug\";s:9:\"gutenberg\";s:5:\"count\";i:365;}s:4:\"code\";a:3:{s:4:\"name\";s:4:\"code\";s:4:\"slug\";s:4:\"code\";s:5:\"count\";i:363;}s:7:\"plugins\";a:3:{s:4:\"name\";s:7:\"plugins\";s:4:\"slug\";s:7:\"plugins\";s:5:\"count\";i:362;}s:3:\"url\";a:3:{s:4:\"name\";s:3:\"url\";s:4:\"slug\";s:3:\"url\";s:5:\"count\";i:356;}s:9:\"multisite\";a:3:{s:4:\"name\";s:9:\"multisite\";s:4:\"slug\";s:9:\"multisite\";s:5:\"count\";i:356;}s:8:\"redirect\";a:3:{s:4:\"name\";s:8:\"redirect\";s:4:\"slug\";s:8:\"redirect\";s:5:\"count\";i:355;}s:4:\"meta\";a:3:{s:4:\"name\";s:4:\"meta\";s:4:\"slug\";s:4:\"meta\";s:5:\"count\";i:350;}s:14:\"contact-form-7\";a:3:{s:4:\"name\";s:14:\"contact form 7\";s:4:\"slug\";s:14:\"contact-form-7\";s:5:\"count\";i:346;}s:4:\"list\";a:3:{s:4:\"name\";s:4:\"list\";s:4:\"slug\";s:4:\"list\";s:5:\"count\";i:346;}s:11:\"performance\";a:3:{s:4:\"name\";s:11:\"performance\";s:4:\"slug\";s:11:\"performance\";s:5:\"count\";i:341;}s:12:\"notification\";a:3:{s:4:\"name\";s:12:\"notification\";s:4:\"slug\";s:12:\"notification\";s:5:\"count\";i:332;}s:16:\"custom-post-type\";a:3:{s:4:\"name\";s:16:\"custom post type\";s:4:\"slug\";s:16:\"custom-post-type\";s:5:\"count\";i:324;}s:11:\"advertising\";a:3:{s:4:\"name\";s:11:\"advertising\";s:4:\"slug\";s:11:\"advertising\";s:5:\"count\";i:324;}s:8:\"tracking\";a:3:{s:4:\"name\";s:8:\"tracking\";s:4:\"slug\";s:8:\"tracking\";s:5:\"count\";i:322;}s:16:\"google-analytics\";a:3:{s:4:\"name\";s:16:\"google analytics\";s:4:\"slug\";s:16:\"google-analytics\";s:5:\"count\";i:321;}s:6:\"simple\";a:3:{s:4:\"name\";s:6:\"simple\";s:4:\"slug\";s:6:\"simple\";s:5:\"count\";i:318;}s:4:\"html\";a:3:{s:4:\"name\";s:4:\"html\";s:4:\"slug\";s:4:\"html\";s:5:\"count\";i:315;}s:7:\"adsense\";a:3:{s:4:\"name\";s:7:\"adsense\";s:4:\"slug\";s:7:\"adsense\";s:5:\"count\";i:311;}s:6:\"author\";a:3:{s:4:\"name\";s:6:\"author\";s:4:\"slug\";s:6:\"author\";s:5:\"count\";i:310;}}', 'no'),
(2606, '_transient_timeout__woocommerce_helper_updates', '1564351887', 'no'),
(2607, '_transient__woocommerce_helper_updates', 'a:4:{s:4:\"hash\";s:32:\"d751713988987e9331980363e24189ce\";s:7:\"updated\";i:1564308687;s:8:\"products\";a:0:{}s:6:\"errors\";a:1:{i:0;s:10:\"http-error\";}}', 'no'),
(2610, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.1.3\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1564308701;s:7:\"version\";s:5:\"5.1.3\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(2623, '_transient_timeout_wc_related_68', '1564401877', 'no'),
(2624, '_transient_wc_related_68', 'a:1:{s:50:\"limit=3&exclude_ids%5B0%5D=0&exclude_ids%5B1%5D=68\";a:0:{}}', 'no'),
(2629, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1564309870;s:7:\"checked\";a:13:{s:30:\"advanced-custom-fields/acf.php\";s:5:\"5.8.1\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.1.3\";s:31:\"event-tickets/event-tickets.php\";s:6:\"4.10.6\";s:41:\"event-tickets-plus/event-tickets-plus.php\";s:6:\"4.10.5\";s:20:\"social-pug/index.php\";s:5:\"1.5.3\";s:43:\"the-events-calendar/the-events-calendar.php\";s:5:\"4.9.3\";s:27:\"woocommerce/woocommerce.php\";s:5:\"3.5.7\";s:69:\"woo-gutenberg-products-block/woocommerce-gutenberg-products-block.php\";s:5:\"1.4.0\";s:79:\"woocommerce-extra-product-options-pro/woocommerce-extra-product-options-pro.php\";s:5:\"2.3.9\";s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";s:6:\"1.6.10\";s:45:\"woocommerce-services/woocommerce-services.php\";s:6:\"1.19.0\";s:29:\"wp-mail-smtp/wp_mail_smtp.php\";s:5:\"1.4.2\";s:27:\"wp-super-cache/wp-cache.php\";s:5:\"1.6.7\";}s:8:\"response\";a:11:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:5:\"5.8.2\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.5.8.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";s:3:\"5.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:31:\"event-tickets/event-tickets.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:27:\"w.org/plugins/event-tickets\";s:4:\"slug\";s:13:\"event-tickets\";s:6:\"plugin\";s:31:\"event-tickets/event-tickets.php\";s:11:\"new_version\";s:8:\"4.10.6.2\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/event-tickets/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/event-tickets.4.10.6.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/event-tickets/assets/icon-256x256.png?rev=2071486\";s:2:\"1x\";s:66:\"https://ps.w.org/event-tickets/assets/icon-128x128.png?rev=2071486\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/event-tickets/assets/banner-1544x500.png?rev=2048309\";s:2:\"1x\";s:68:\"https://ps.w.org/event-tickets/assets/banner-772x250.png?rev=2048309\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";s:3:\"5.6\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:20:\"social-pug/index.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:24:\"w.org/plugins/social-pug\";s:4:\"slug\";s:10:\"social-pug\";s:6:\"plugin\";s:20:\"social-pug/index.php\";s:11:\"new_version\";s:5:\"1.6.1\";s:3:\"url\";s:41:\"https://wordpress.org/plugins/social-pug/\";s:7:\"package\";s:53:\"https://downloads.wordpress.org/plugin/social-pug.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/social-pug/assets/icon-256x256.png?rev=1323409\";s:2:\"1x\";s:63:\"https://ps.w.org/social-pug/assets/icon-128x128.png?rev=1323409\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/social-pug/assets/banner-772x250.png?rev=1973060\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:43:\"the-events-calendar/the-events-calendar.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:33:\"w.org/plugins/the-events-calendar\";s:4:\"slug\";s:19:\"the-events-calendar\";s:6:\"plugin\";s:43:\"the-events-calendar/the-events-calendar.php\";s:11:\"new_version\";s:5:\"4.9.5\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/the-events-calendar/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/the-events-calendar.4.9.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/the-events-calendar/assets/icon-256x256.png?rev=2071468\";s:2:\"1x\";s:72:\"https://ps.w.org/the-events-calendar/assets/icon-128x128.png?rev=2071468\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/the-events-calendar/assets/banner-1544x500.png?rev=2048291\";s:2:\"1x\";s:74:\"https://ps.w.org/the-events-calendar/assets/banner-772x250.png?rev=2048291\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";s:3:\"5.6\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:27:\"woocommerce/woocommerce.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:25:\"w.org/plugins/woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:6:\"plugin\";s:27:\"woocommerce/woocommerce.php\";s:11:\"new_version\";s:5:\"3.6.5\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woocommerce/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce.3.6.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=2075035\";s:2:\"1x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=2075035\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=2075035\";s:2:\"1x\";s:66:\"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=2075035\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:69:\"woo-gutenberg-products-block/woocommerce-gutenberg-products-block.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:42:\"w.org/plugins/woo-gutenberg-products-block\";s:4:\"slug\";s:28:\"woo-gutenberg-products-block\";s:6:\"plugin\";s:69:\"woo-gutenberg-products-block/woocommerce-gutenberg-products-block.php\";s:11:\"new_version\";s:5:\"2.2.1\";s:3:\"url\";s:59:\"https://wordpress.org/plugins/woo-gutenberg-products-block/\";s:7:\"package\";s:77:\"https://downloads.wordpress.org/plugin/woo-gutenberg-products-block.2.2.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:81:\"https://ps.w.org/woo-gutenberg-products-block/assets/icon-256x256.png?rev=1863757\";s:2:\"1x\";s:81:\"https://ps.w.org/woo-gutenberg-products-block/assets/icon-128x128.png?rev=1863757\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:83:\"https://ps.w.org/woo-gutenberg-products-block/assets/banner-772x250.png?rev=1863757\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";s:3:\"5.2\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:57:\"w.org/plugins/woocommerce-gateway-paypal-express-checkout\";s:4:\"slug\";s:43:\"woocommerce-gateway-paypal-express-checkout\";s:6:\"plugin\";s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";s:11:\"new_version\";s:6:\"1.6.16\";s:3:\"url\";s:74:\"https://wordpress.org/plugins/woocommerce-gateway-paypal-express-checkout/\";s:7:\"package\";s:93:\"https://downloads.wordpress.org/plugin/woocommerce-gateway-paypal-express-checkout.1.6.16.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:96:\"https://ps.w.org/woocommerce-gateway-paypal-express-checkout/assets/icon-256x256.png?rev=1900204\";s:2:\"1x\";s:96:\"https://ps.w.org/woocommerce-gateway-paypal-express-checkout/assets/icon-128x128.png?rev=1900204\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:99:\"https://ps.w.org/woocommerce-gateway-paypal-express-checkout/assets/banner-1544x500.png?rev=1948167\";s:2:\"1x\";s:98:\"https://ps.w.org/woocommerce-gateway-paypal-express-checkout/assets/banner-772x250.png?rev=1948167\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.0\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:45:\"woocommerce-services/woocommerce-services.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:34:\"w.org/plugins/woocommerce-services\";s:4:\"slug\";s:20:\"woocommerce-services\";s:6:\"plugin\";s:45:\"woocommerce-services/woocommerce-services.php\";s:11:\"new_version\";s:6:\"1.21.0\";s:3:\"url\";s:51:\"https://wordpress.org/plugins/woocommerce-services/\";s:7:\"package\";s:70:\"https://downloads.wordpress.org/plugin/woocommerce-services.1.21.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/woocommerce-services/assets/icon-256x256.png?rev=1910075\";s:2:\"1x\";s:73:\"https://ps.w.org/woocommerce-services/assets/icon-128x128.png?rev=1910075\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:76:\"https://ps.w.org/woocommerce-services/assets/banner-1544x500.png?rev=1962920\";s:2:\"1x\";s:75:\"https://ps.w.org/woocommerce-services/assets/banner-772x250.png?rev=1962920\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:29:\"wp-mail-smtp/wp_mail_smtp.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:26:\"w.org/plugins/wp-mail-smtp\";s:4:\"slug\";s:12:\"wp-mail-smtp\";s:6:\"plugin\";s:29:\"wp-mail-smtp/wp_mail_smtp.php\";s:11:\"new_version\";s:5:\"1.5.2\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/wp-mail-smtp/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wp-mail-smtp.1.5.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/wp-mail-smtp/assets/icon-256x256.png?rev=1755440\";s:2:\"1x\";s:65:\"https://ps.w.org/wp-mail-smtp/assets/icon-128x128.png?rev=1755440\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/wp-mail-smtp/assets/banner-1544x500.png?rev=2120094\";s:2:\"1x\";s:67:\"https://ps.w.org/wp-mail-smtp/assets/banner-772x250.png?rev=2120094\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";s:3:\"5.3\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:27:\"wp-super-cache/wp-cache.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/wp-super-cache\";s:4:\"slug\";s:14:\"wp-super-cache\";s:6:\"plugin\";s:27:\"wp-super-cache/wp-cache.php\";s:11:\"new_version\";s:5:\"1.6.9\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/wp-super-cache/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/wp-super-cache.1.6.9.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/wp-super-cache/assets/icon-256x256.png?rev=1095422\";s:2:\"1x\";s:67:\"https://ps.w.org/wp-super-cache/assets/icon-128x128.png?rev=1095422\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/wp-super-cache/assets/banner-1544x500.png?rev=1082414\";s:2:\"1x\";s:69:\"https://ps.w.org/wp-super-cache/assets/banner-772x250.png?rev=1082414\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";s:5:\"5.2.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:41:\"event-tickets-plus/event-tickets-plus.php\";O:8:\"stdClass\":7:{s:2:\"id\";i:0;s:6:\"plugin\";s:41:\"event-tickets-plus/event-tickets-plus.php\";s:4:\"slug\";s:18:\"event-tickets-plus\";s:11:\"new_version\";s:8:\"4.10.5.1\";s:3:\"url\";s:20:\"http://m.tri.be/18wg\";s:7:\"package\";s:284:\"https://pue.tri.be/api/plugins/v2/download?plugin=event-tickets-plus&version=4.10.5.1&installed_version=4.10.5&domain=localhost&multisite=0&network_activated=0&active_sites=1&wp_version=5.1.1&key=f4b28dcb33fe101fb6e0c94d957f73430114aab9&dk=f4b28dcb33fe101fb6e0c94d957f73430114aab9&o=m\";s:5:\"icons\";a:1:{s:3:\"svg\";s:92:\"https://theeventscalendar.com/content/themes/tribe-ecp/img/svg/product-icons/ticketsplus.svg\";}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:1:{s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.1.3\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.1.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(2729, '_transient_is_multi_author', '0', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(60, 14, '_VenueOrigin', 'events-calendar'),
(61, 14, '_tribe_modified_fields', 'a:18:{s:12:\"_VenueOrigin\";i:1552950438;s:17:\"_EventShowMapLink\";i:1552950438;s:13:\"_EventShowMap\";i:1552950438;s:17:\"_VenueShowMapLink\";i:1552950438;s:13:\"_VenueShowMap\";i:1552950438;s:13:\"_VenueAddress\";i:1552950438;s:10:\"_VenueCity\";i:1552950438;s:13:\"_VenueCountry\";i:1552950438;s:14:\"_VenueProvince\";i:1552950438;s:11:\"_VenueState\";i:1552950438;s:9:\"_VenueZip\";i:1552950438;s:11:\"_VenuePhone\";i:1552950438;s:9:\"_VenueURL\";i:1552950438;s:19:\"_VenueStateProvince\";i:1552950438;s:10:\"_edit_last\";i:1553744069;s:10:\"post_title\";i:1553744069;s:22:\"_tribe_ticket_capacity\";i:1554818197;s:21:\"_tribe_ticket_version\";i:1554818197;}'),
(62, 14, '_EventShowMapLink', '1'),
(63, 14, '_EventShowMap', '1'),
(64, 14, '_VenueShowMapLink', '1'),
(65, 14, '_VenueShowMap', '1'),
(66, 14, '_VenueAddress', 'dfgfdgf'),
(67, 14, '_VenueCity', 'fgjhgjg'),
(68, 14, '_VenueCountry', 'Norway'),
(69, 14, '_VenueProvince', 'dfgdfg'),
(70, 14, '_VenueState', ''),
(71, 14, '_VenueZip', 'dfgfd'),
(72, 14, '_VenuePhone', ''),
(73, 14, '_VenueURL', ''),
(74, 14, '_VenueStateProvince', 'dfgdfg'),
(85, 15, '_wp_page_template', ''),
(87, 15, '_customize_changeset_uuid', 'b7e9bc24-b295-4d06-8ca1-f5f65e90b22a'),
(89, 16, '_customize_changeset_uuid', 'b7e9bc24-b295-4d06-8ca1-f5f65e90b22a'),
(114, 27, '_EventOrigin', 'events-calendar'),
(116, 27, '_edit_last', '1'),
(117, 27, '_edit_lock', '1554936957:1'),
(118, 28, '_EventShowMapLink', ''),
(119, 28, '_EventShowMap', ''),
(120, 28, '_EventAllDay', 'yes'),
(121, 28, '_EventStartDate', '2019-03-19 00:00:00'),
(122, 28, '_EventEndDate', '2019-03-19 23:59:59'),
(123, 28, '_EventStartDateUTC', '2019-03-19 00:00:00'),
(124, 28, '_EventEndDateUTC', '2019-03-19 23:59:59'),
(125, 28, '_EventCurrencySymbol', '$'),
(126, 28, '_EventCurrencyPosition', 'prefix'),
(127, 28, '_EventCost', '100'),
(128, 28, '_EventURL', ''),
(129, 28, '_EventTimezone', 'UTC+0'),
(130, 28, '_EventTimezoneAbbr', 'UTC+0'),
(131, 27, '_EventShowMapLink', ''),
(132, 27, '_EventShowMap', ''),
(133, 27, '_EventVenueID', '14'),
(134, 27, '_EventAllDay', 'yes'),
(135, 27, '_EventStartDate', '2019-04-10 00:00:00'),
(136, 27, '_EventEndDate', '2019-04-10 23:59:59'),
(137, 27, '_EventStartDateUTC', '2019-04-10 00:00:00'),
(138, 27, '_EventEndDateUTC', '2019-04-10 23:59:59'),
(139, 27, '_EventCurrencySymbol', '$'),
(140, 27, '_EventCurrencyPosition', 'prefix'),
(142, 27, '_EventURL', ''),
(143, 27, '_EventTimezone', 'UTC+0'),
(144, 27, '_EventTimezoneAbbr', 'UTC+0'),
(147, 27, '_tribe_ticket_capacity', ''),
(148, 27, '_tribe_ticket_version', '4.10.2'),
(149, 29, '_wc_review_count', '0'),
(150, 29, '_wc_rating_count', 'a:0:{}'),
(151, 29, '_wc_average_rating', '0'),
(152, 29, '_sku', '29-1-NORMAL'),
(153, 29, '_regular_price', '19.99'),
(154, 29, '_sale_price', ''),
(155, 29, '_sale_price_dates_from', ''),
(156, 29, '_sale_price_dates_to', ''),
(157, 29, 'total_sales', '5'),
(158, 29, '_tax_status', 'taxable'),
(159, 29, '_tax_class', ''),
(160, 29, '_manage_stock', 'yes'),
(161, 29, '_backorders', 'no'),
(162, 29, '_low_stock_amount', ''),
(163, 29, '_sold_individually', 'no'),
(164, 29, '_weight', ''),
(165, 29, '_length', ''),
(166, 29, '_width', ''),
(167, 29, '_height', ''),
(168, 29, '_upsell_ids', 'a:0:{}'),
(169, 29, '_crosssell_ids', 'a:0:{}'),
(170, 29, '_purchase_note', ''),
(171, 29, '_default_attributes', 'a:0:{}'),
(172, 29, '_virtual', 'yes'),
(173, 29, '_downloadable', 'no'),
(174, 29, '_product_image_gallery', ''),
(175, 29, '_download_limit', '-1'),
(176, 29, '_download_expiry', '-1'),
(179, 29, '_product_version', '3.5.7'),
(180, 29, '_price', '19.99'),
(181, 29, '_tribe_wooticket_for_event', '27'),
(182, 29, '_tribe_ticket_show_description', 'yes'),
(183, 29, '_tribe_ticket_capacity', '100'),
(184, 29, '_tribe_ticket_version', '4.10.2'),
(185, 29, '_ticket_start_date', '2019-03-09 00:30:00'),
(186, 29, '_ticket_end_date', '2019-04-12 00:00:00'),
(187, 29, '_tribe_tickets_meta', 'a:0:{}'),
(189, 29, '_edit_lock', '1554749161:1'),
(190, 29, '_edit_last', '1'),
(198, 30, '_tribe_ticket_capacity', '0'),
(199, 30, '_tribe_ticket_version', '4.10.1.2'),
(200, 30, '_edit_lock', '1559846574:1'),
(203, 7, '_tribe_ticket_capacity', '0'),
(204, 7, '_tribe_ticket_version', '4.10.1.2'),
(205, 7, '_edit_lock', '1554747627:1'),
(280, 34, '_EventOrigin', 'events-calendar'),
(281, 34, '_tribe_modified_fields', 'a:33:{s:12:\"_EventOrigin\";i:1552962225;s:22:\"_tribe_ticket_capacity\";i:1554932764;s:21:\"_tribe_ticket_version\";i:1554932763;s:10:\"_EventCost\";i:1554932804;s:10:\"_edit_last\";i:1552962288;s:10:\"post_title\";i:1554932705;s:12:\"post_content\";i:1554932691;s:11:\"post_status\";i:1552962288;s:26:\"_tribe_hide_attendees_list\";i:1552962288;s:30:\"_tribe_default_ticket_provider\";i:1552962288;s:17:\"_EventShowMapLink\";i:1552962288;s:13:\"_EventShowMap\";i:1552962288;s:13:\"_EventVenueID\";i:1552962288;s:12:\"_EventAllDay\";i:1552962288;s:15:\"_EventStartDate\";i:1554932819;s:13:\"_EventEndDate\";i:1554932819;s:18:\"_EventStartDateUTC\";i:1554932819;s:16:\"_EventEndDateUTC\";i:1554932819;s:9:\"_EventURL\";i:1552962289;s:14:\"_EventTimezone\";i:1552962289;s:18:\"_EventTimezoneAbbr\";i:1552962289;s:27:\"_tribe_tickets_flush_blocks\";i:1552962289;s:34:\"_transient_timeout_tribe_attendees\";i:1552962313;s:26:\"_transient_tribe_attendees\";i:1552962313;s:12:\"post_excerpt\";i:1554932691;s:7:\"program\";i:1554932691;s:8:\"_program\";i:1554932691;s:30:\"_tribe_ticket_use_global_stock\";i:1554932764;s:32:\"_tribe_ticket_global_stock_level\";i:1554932764;s:13:\"_thumbnail_id\";i:1554937047;s:20:\"dpsp_networks_shares\";i:1559847073;s:26:\"dpsp_networks_shares_total\";i:1559847073;s:33:\"dpsp_networks_shares_last_updated\";i:1563986818;}'),
(282, 34, '_tribe_ticket_capacity', '25'),
(283, 34, '_tribe_ticket_version', '4.10.2'),
(324, 34, '_edit_last', '1'),
(325, 36, '_EventShowMapLink', '1'),
(326, 36, '_EventShowMap', '1'),
(327, 36, '_EventAllDay', 'yes'),
(328, 36, '_EventStartDate', '2019-03-20 00:00:00'),
(329, 36, '_EventEndDate', '2019-03-20 23:59:59'),
(330, 36, '_EventStartDateUTC', '2019-03-20 00:00:00'),
(331, 36, '_EventEndDateUTC', '2019-03-20 23:59:59'),
(332, 36, '_EventURL', ''),
(333, 36, '_EventTimezone', 'UTC+0'),
(334, 36, '_EventTimezoneAbbr', 'UTC+0'),
(335, 34, '_tribe_hide_attendees_list', ''),
(336, 34, '_tribe_default_ticket_provider', 'Tribe__Tickets_Plus__Commerce__WooCommerce__Main'),
(337, 34, '_EventShowMapLink', '1'),
(338, 34, '_EventShowMap', '1'),
(339, 34, '_EventVenueID', '14'),
(340, 34, '_EventAllDay', 'yes'),
(341, 34, '_EventStartDate', '2019-04-28 00:00:00'),
(342, 34, '_EventEndDate', '2019-05-01 23:59:59'),
(343, 34, '_EventStartDateUTC', '2019-04-28 00:00:00'),
(344, 34, '_EventEndDateUTC', '2019-05-01 23:59:59'),
(345, 34, '_EventURL', ''),
(346, 34, '_EventTimezone', 'UTC+0'),
(347, 34, '_EventTimezoneAbbr', 'UTC+0'),
(348, 34, '_edit_lock', '1554936912:1'),
(349, 34, '_tribe_tickets_flush_blocks', '1'),
(350, 34, '_transient_timeout_tribe_attendees', '1552965913'),
(351, 34, '_transient_tribe_attendees', 'a:0:{}'),
(366, 39, '_menu_item_type', 'custom'),
(367, 39, '_menu_item_menu_item_parent', '0'),
(368, 39, '_menu_item_object_id', '39'),
(369, 39, '_menu_item_object', 'custom'),
(370, 39, '_menu_item_target', ''),
(371, 39, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(372, 39, '_menu_item_xfn', ''),
(373, 39, '_menu_item_url', '/#events'),
(374, 40, '_menu_item_type', 'custom'),
(375, 40, '_menu_item_menu_item_parent', '0'),
(376, 40, '_menu_item_object_id', '40'),
(377, 40, '_menu_item_object', 'custom'),
(378, 40, '_menu_item_target', ''),
(379, 40, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(380, 40, '_menu_item_xfn', ''),
(381, 40, '_menu_item_url', '/#ebooks'),
(392, 42, '_menu_item_type', 'custom'),
(393, 42, '_menu_item_menu_item_parent', '0'),
(394, 42, '_menu_item_object_id', '42'),
(395, 42, '_menu_item_object', 'custom'),
(396, 42, '_menu_item_target', ''),
(397, 42, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(398, 42, '_menu_item_xfn', ''),
(399, 42, '_menu_item_url', '/#contact'),
(410, 44, '_menu_item_type', 'post_type'),
(411, 44, '_menu_item_menu_item_parent', '0'),
(412, 44, '_menu_item_object_id', '16'),
(413, 44, '_menu_item_object', 'page'),
(414, 44, '_menu_item_target', ''),
(415, 44, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(416, 44, '_menu_item_xfn', ''),
(417, 44, '_menu_item_url', ''),
(473, 15, '_tribe_ticket_capacity', '0'),
(474, 15, '_tribe_ticket_version', '4.10.1.2'),
(475, 15, '_edit_lock', '1563910794:1'),
(476, 2, '_tribe_ticket_capacity', '0'),
(477, 2, '_tribe_ticket_version', '4.10.1.2'),
(478, 2, '_edit_lock', '1553736934:1'),
(479, 52, '_EventOrigin', 'events-calendar'),
(480, 52, '_tribe_modified_fields', 'a:32:{s:12:\"_EventOrigin\";i:1553740920;s:22:\"_tribe_ticket_capacity\";i:1554932905;s:21:\"_tribe_ticket_version\";i:1554932905;s:10:\"_edit_last\";i:1553740925;s:10:\"post_title\";i:1554932872;s:11:\"post_status\";i:1553740936;s:12:\"post_content\";i:1554932935;s:26:\"_tribe_hide_attendees_list\";i:1553740936;s:30:\"_tribe_default_ticket_provider\";i:1553740936;s:17:\"_EventShowMapLink\";i:1553740936;s:13:\"_EventShowMap\";i:1553740936;s:15:\"_EventStartDate\";i:1554932619;s:13:\"_EventEndDate\";i:1554932619;s:18:\"_EventStartDateUTC\";i:1554932619;s:16:\"_EventEndDateUTC\";i:1554932619;s:14:\"_EventDuration\";i:1554932619;s:9:\"_EventURL\";i:1553740936;s:14:\"_EventTimezone\";i:1553740936;s:18:\"_EventTimezoneAbbr\";i:1553740936;s:34:\"_transient_timeout_tribe_attendees\";i:1553743805;s:26:\"_transient_tribe_attendees\";i:1553743805;s:7:\"program\";i:1554932990;s:8:\"_program\";i:1554932591;s:12:\"post_excerpt\";i:1554932872;s:30:\"_tribe_ticket_use_global_stock\";i:1554932905;s:32:\"_tribe_ticket_global_stock_level\";i:1554932905;s:10:\"_EventCost\";i:1554932905;s:27:\"_tribe_tickets_flush_blocks\";i:1554932938;s:13:\"_thumbnail_id\";i:1554937148;s:20:\"dpsp_networks_shares\";i:1559847067;s:26:\"dpsp_networks_shares_total\";i:1559847067;s:33:\"dpsp_networks_shares_last_updated\";i:1563986803;}'),
(481, 52, '_tribe_ticket_capacity', '100'),
(482, 52, '_tribe_ticket_version', '4.10.2'),
(483, 52, '_edit_last', '1'),
(484, 52, '_edit_lock', '1554937116:1'),
(485, 53, '_EventShowMapLink', '1'),
(486, 53, '_EventShowMap', '1'),
(487, 53, '_EventStartDate', '2019-03-28 08:00:00'),
(488, 53, '_EventEndDate', '2019-03-30 17:00:00'),
(489, 53, '_EventStartDateUTC', '2019-03-28 08:00:00'),
(490, 53, '_EventEndDateUTC', '2019-03-30 17:00:00'),
(491, 53, '_EventDuration', '205200'),
(492, 53, '_EventURL', ''),
(493, 53, '_EventTimezone', 'UTC+0'),
(494, 53, '_EventTimezoneAbbr', 'UTC+0'),
(495, 52, '_tribe_hide_attendees_list', ''),
(496, 52, '_tribe_default_ticket_provider', 'Tribe__Tickets_Plus__Commerce__WooCommerce__Main'),
(497, 52, '_EventShowMapLink', '1'),
(498, 52, '_EventShowMap', '1'),
(499, 52, '_EventStartDate', '2019-04-25 08:00:00'),
(500, 52, '_EventEndDate', '2019-04-28 17:00:00'),
(501, 52, '_EventStartDateUTC', '2019-04-25 08:00:00'),
(502, 52, '_EventEndDateUTC', '2019-04-28 17:00:00'),
(503, 52, '_EventDuration', '291600'),
(504, 52, '_EventURL', ''),
(505, 52, '_EventTimezone', 'UTC+0'),
(506, 52, '_EventTimezoneAbbr', 'UTC+0'),
(507, 52, '_transient_timeout_tribe_attendees', '1553743925'),
(508, 52, '_transient_tribe_attendees', 'a:0:{}'),
(509, 55, '_EventShowMapLink', '1'),
(510, 55, '_EventShowMap', '1'),
(511, 55, '_EventAllDay', 'yes'),
(512, 55, '_EventStartDate', '2019-03-20 00:00:00'),
(513, 55, '_EventEndDate', '2019-03-20 23:59:59'),
(514, 55, '_EventStartDateUTC', '2019-03-20 00:00:00'),
(515, 55, '_EventEndDateUTC', '2019-03-20 23:59:59'),
(516, 55, '_EventURL', ''),
(517, 55, '_EventTimezone', 'UTC+0'),
(518, 55, '_EventTimezoneAbbr', 'UTC+0'),
(519, 14, '_edit_lock', '1553743989:1'),
(520, 14, '_edit_last', '1'),
(521, 56, '_EventShowMapLink', '1'),
(522, 56, '_EventShowMap', '1'),
(523, 56, '_EventStartDate', '2019-03-28 08:00:00'),
(524, 56, '_EventEndDate', '2019-03-30 17:00:00'),
(525, 56, '_EventStartDateUTC', '2019-03-28 08:00:00'),
(526, 56, '_EventEndDateUTC', '2019-03-30 17:00:00'),
(527, 56, '_EventDuration', '205200'),
(528, 56, '_EventURL', ''),
(529, 56, '_EventTimezone', 'UTC+0'),
(530, 56, '_EventTimezoneAbbr', 'UTC+0'),
(531, 57, '_EventShowMapLink', ''),
(532, 57, '_EventShowMap', ''),
(533, 57, '_EventAllDay', 'yes'),
(534, 57, '_EventStartDate', '2019-03-19 00:00:00'),
(535, 57, '_EventEndDate', '2019-03-19 23:59:59'),
(536, 57, '_EventStartDateUTC', '2019-03-19 00:00:00'),
(537, 57, '_EventEndDateUTC', '2019-03-19 23:59:59'),
(538, 57, '_EventURL', ''),
(539, 57, '_EventTimezone', 'UTC+0'),
(540, 57, '_EventTimezoneAbbr', 'UTC+0'),
(541, 27, '_tribe_hide_attendees_list', ''),
(542, 27, '_tribe_default_ticket_provider', 'Tribe__Tickets_Plus__Commerce__WooCommerce__Main'),
(543, 58, '_EventShowMapLink', ''),
(544, 58, '_EventShowMap', ''),
(545, 58, '_EventAllDay', 'yes'),
(546, 58, '_EventStartDate', '2019-03-19 00:00:00'),
(547, 58, '_EventEndDate', '2019-03-19 23:59:59'),
(548, 58, '_EventStartDateUTC', '2019-03-19 00:00:00'),
(549, 58, '_EventEndDateUTC', '2019-03-19 23:59:59'),
(550, 58, '_EventURL', ''),
(551, 58, '_EventTimezone', 'UTC+0'),
(552, 58, '_EventTimezoneAbbr', 'UTC+0'),
(556, 60, '_wp_attached_file', '2019/03/book.png'),
(557, 60, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:713;s:6:\"height\";i:476;s:4:\"file\";s:16:\"2019/03/book.png\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"book-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"book-300x200.png\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:16:\"book-324x324.png\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:16:\"book-416x278.png\";s:5:\"width\";i:416;s:6:\"height\";i:278;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"book-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:16:\"book-324x324.png\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:16:\"book-416x278.png\";s:5:\"width\";i:416;s:6:\"height\";i:278;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"book-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(572, 61, '_wc_review_count', '0'),
(573, 61, '_wc_rating_count', 'a:0:{}'),
(574, 61, '_wc_average_rating', '0'),
(575, 61, '_edit_last', '1'),
(576, 61, '_edit_lock', '1554937156:1'),
(577, 61, '_sku', ''),
(578, 61, '_regular_price', '35'),
(579, 61, '_sale_price', ''),
(580, 61, '_sale_price_dates_from', ''),
(581, 61, '_sale_price_dates_to', ''),
(582, 61, 'total_sales', '3'),
(583, 61, '_tax_status', 'taxable'),
(584, 61, '_tax_class', ''),
(585, 61, '_manage_stock', 'no'),
(586, 61, '_backorders', 'no'),
(587, 61, '_low_stock_amount', ''),
(588, 61, '_sold_individually', 'no'),
(589, 61, '_weight', ''),
(590, 61, '_length', ''),
(591, 61, '_width', ''),
(592, 61, '_height', ''),
(593, 61, '_upsell_ids', 'a:0:{}'),
(594, 61, '_crosssell_ids', 'a:0:{}'),
(595, 61, '_purchase_note', ''),
(596, 61, '_default_attributes', 'a:0:{}'),
(597, 61, '_virtual', 'yes'),
(598, 61, '_downloadable', 'yes'),
(599, 61, '_product_image_gallery', ''),
(600, 61, '_download_limit', '-1'),
(601, 61, '_download_expiry', '-1'),
(602, 61, '_stock', ''),
(603, 61, '_stock_status', 'instock'),
(604, 61, '_product_version', '3.5.7'),
(605, 61, '_price', '35'),
(606, 62, '_wc_review_count', '0'),
(607, 62, '_wc_rating_count', 'a:0:{}'),
(608, 62, '_wc_average_rating', '0'),
(609, 62, '_edit_last', '1'),
(610, 62, '_sku', ''),
(611, 62, '_regular_price', '19.99'),
(612, 62, '_sale_price', ''),
(613, 62, '_sale_price_dates_from', ''),
(614, 62, '_sale_price_dates_to', ''),
(615, 62, 'total_sales', '1'),
(616, 62, '_tax_status', 'taxable'),
(617, 62, '_tax_class', ''),
(618, 62, '_manage_stock', 'no'),
(619, 62, '_backorders', 'no'),
(620, 62, '_low_stock_amount', ''),
(621, 62, '_sold_individually', 'no'),
(622, 62, '_weight', ''),
(623, 62, '_length', ''),
(624, 62, '_width', ''),
(625, 62, '_height', ''),
(626, 62, '_upsell_ids', 'a:0:{}'),
(627, 62, '_crosssell_ids', 'a:0:{}'),
(628, 62, '_purchase_note', ''),
(629, 62, '_default_attributes', 'a:0:{}'),
(630, 62, '_virtual', 'yes'),
(631, 62, '_downloadable', 'yes'),
(632, 62, '_product_image_gallery', ''),
(633, 62, '_download_limit', '-1'),
(634, 62, '_download_expiry', '-1'),
(635, 62, '_stock', ''),
(636, 62, '_stock_status', 'instock'),
(637, 62, '_product_version', '3.5.7'),
(638, 62, '_price', '19.99'),
(639, 62, '_edit_lock', '1554937278:1'),
(640, 63, '_menu_item_type', 'post_type'),
(641, 63, '_menu_item_menu_item_parent', '0'),
(642, 63, '_menu_item_object_id', '15'),
(643, 63, '_menu_item_object', 'page'),
(644, 63, '_menu_item_target', ''),
(645, 63, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(646, 63, '_menu_item_xfn', ''),
(647, 63, '_menu_item_url', ''),
(649, 62, '_thumbnail_id', '60'),
(656, 29, '_global_stock_mode', 'own'),
(657, 29, '_stock', '95'),
(658, 29, '_stock_status', 'instock'),
(661, 29, '_tribe_ticket_manual_updated', '_ticket_end_date'),
(664, 66, '_OrganizerOrigin', 'events-calendar'),
(665, 66, '_tribe_modified_fields', 'a:7:{s:16:\"_OrganizerOrigin\";i:1554187215;s:21:\"_OrganizerOrganizerID\";i:1554187215;s:15:\"_OrganizerPhone\";i:1554187215;s:17:\"_OrganizerWebsite\";i:1554187215;s:15:\"_OrganizerEmail\";i:1554187215;s:22:\"_tribe_ticket_capacity\";i:1554818194;s:21:\"_tribe_ticket_version\";i:1554818194;}'),
(666, 66, '_OrganizerOrganizerID', '0'),
(667, 66, '_OrganizerPhone', 's435'),
(668, 66, '_OrganizerWebsite', 'sdfsdf'),
(669, 66, '_OrganizerEmail', ''),
(672, 68, '_wc_review_count', '0'),
(673, 68, '_wc_rating_count', 'a:0:{}'),
(674, 68, '_wc_average_rating', '0'),
(675, 68, '_edit_last', '1'),
(676, 68, '_sku', ''),
(677, 68, '_regular_price', '0'),
(678, 68, '_sale_price', ''),
(679, 68, '_sale_price_dates_from', ''),
(680, 68, '_sale_price_dates_to', ''),
(681, 68, 'total_sales', '3'),
(682, 68, '_tax_status', 'taxable'),
(683, 68, '_tax_class', ''),
(684, 68, '_manage_stock', 'no'),
(685, 68, '_backorders', 'no'),
(686, 68, '_low_stock_amount', ''),
(687, 68, '_sold_individually', 'yes'),
(688, 68, '_weight', ''),
(689, 68, '_length', ''),
(690, 68, '_width', ''),
(691, 68, '_height', ''),
(692, 68, '_upsell_ids', 'a:0:{}'),
(693, 68, '_crosssell_ids', 'a:0:{}'),
(694, 68, '_purchase_note', ''),
(695, 68, '_default_attributes', 'a:0:{}'),
(696, 68, '_virtual', 'yes'),
(697, 68, '_downloadable', 'no'),
(698, 68, '_product_image_gallery', ''),
(699, 68, '_download_limit', '-1'),
(700, 68, '_download_expiry', '-1'),
(701, 68, '_stock', ''),
(702, 68, '_stock_status', 'instock'),
(703, 68, '_product_version', '3.5.7'),
(704, 68, '_price', '0'),
(705, 68, '_edit_lock', '1564081203:1'),
(848, 74, '_tribe_wooticket_product', '70'),
(849, 74, '_tribe_wooticket_order', '73'),
(850, 74, '_tribe_wooticket_order_item', '3'),
(851, 74, '_tribe_wooticket_event', '27'),
(852, 74, '_tribe_wooticket_attendee_optout', ''),
(853, 74, '_tribe_wooticket_security_code', 'eb2300ed44'),
(854, 74, '_paid_price', '10'),
(855, 74, '_price_currency_symbol', '$'),
(856, 27, '_tribe_post_root', 'A-'),
(857, 27, '_tribe_progressive_ticket_current_number', '9'),
(858, 74, '_unique_id', 'A-1-Z4A508'),
(859, 74, '_tribe_tickets_attendee_user_id', '1'),
(864, 76, '_wp_attached_file', 'woocommerce_uploads/2019/03/book.pdf'),
(865, 61, '_downloadable_files', 'a:1:{s:36:\"dc69faa4-155e-4e10-8f49-692dee984979\";a:3:{s:2:\"id\";s:36:\"dc69faa4-155e-4e10-8f49-692dee984979\";s:4:\"name\";s:10:\"Book Lorem\";s:4:\"file\";s:77:\"https://wp2.dev-c01.muulabs.pl/wp-content/uploads/woocommerce_uploads/2019/03/book.pdf\";}}'),
(866, 62, '_downloadable_files', 'a:1:{s:36:\"4be623b5-7cc9-4909-8731-c34062b2d12c\";a:3:{s:2:\"id\";s:36:\"4be623b5-7cc9-4909-8731-c34062b2d12c\";s:4:\"name\";s:6:\"Secret\";s:4:\"file\";s:77:\"https://wp2.dev-c01.muulabs.pl/wp-content/uploads/woocommerce_uploads/2019/03/book.pdf\";}}'),
(933, 78, '_tribe_wooticket_product', '29'),
(934, 78, '_tribe_wooticket_order', '77'),
(935, 78, '_tribe_wooticket_order_item', '4'),
(936, 78, '_tribe_wooticket_event', '27'),
(937, 78, '_tribe_wooticket_attendee_optout', ''),
(938, 78, '_tribe_wooticket_security_code', '029b5b8a1a'),
(939, 78, '_paid_price', '19.99'),
(940, 78, '_price_currency_symbol', '$'),
(941, 78, '_unique_id', 'A-2-51T0YL'),
(942, 78, '_tribe_tickets_attendee_user_id', '1'),
(944, 79, '_tribe_wooticket_product', '70'),
(945, 79, '_tribe_wooticket_order', '77'),
(946, 79, '_tribe_wooticket_order_item', '5'),
(947, 79, '_tribe_wooticket_event', '27'),
(948, 79, '_tribe_wooticket_attendee_optout', ''),
(949, 79, '_tribe_wooticket_security_code', '70bdf65248'),
(950, 79, '_paid_price', '10'),
(951, 79, '_price_currency_symbol', '$'),
(952, 79, '_unique_id', 'A-3-TUM5ZC'),
(953, 79, '_tribe_tickets_attendee_user_id', '1'),
(1018, 81, '_tribe_wooticket_product', '29'),
(1019, 81, '_tribe_wooticket_order', '80'),
(1020, 81, '_tribe_wooticket_order_item', '8'),
(1021, 81, '_tribe_wooticket_event', '27'),
(1022, 81, '_tribe_wooticket_attendee_optout', ''),
(1023, 81, '_tribe_wooticket_security_code', '77e1fec230'),
(1024, 81, '_paid_price', '19.99'),
(1025, 81, '_price_currency_symbol', '$'),
(1026, 81, '_unique_id', 'A-4-YNRHZS'),
(1027, 81, '_tribe_tickets_attendee_user_id', '0'),
(1028, 82, '_tribe_wooticket_product', '70'),
(1029, 82, '_tribe_wooticket_order', '80'),
(1030, 82, '_tribe_wooticket_order_item', '9'),
(1031, 82, '_tribe_wooticket_event', '27'),
(1032, 82, '_tribe_wooticket_attendee_optout', ''),
(1033, 82, '_tribe_wooticket_security_code', '950582c6a1'),
(1034, 82, '_paid_price', '10'),
(1035, 82, '_price_currency_symbol', '$'),
(1036, 82, '_unique_id', 'A-5-K92U4H'),
(1037, 82, '_tribe_tickets_attendee_user_id', '0'),
(1103, 85, '_tribe_wooticket_product', '29'),
(1104, 85, '_tribe_wooticket_order', '84'),
(1105, 85, '_tribe_wooticket_order_item', '10'),
(1106, 85, '_tribe_wooticket_event', '27'),
(1107, 85, '_tribe_wooticket_attendee_optout', ''),
(1108, 85, '_tribe_wooticket_security_code', '732182cab2'),
(1109, 85, '_paid_price', '19.99'),
(1110, 85, '_price_currency_symbol', '$'),
(1111, 85, '_unique_id', 'A-6-I9O4BJ'),
(1112, 85, '_tribe_tickets_attendee_user_id', '0'),
(1113, 86, '_tribe_wooticket_product', '29'),
(1114, 86, '_tribe_wooticket_order', '84'),
(1115, 86, '_tribe_wooticket_order_item', '10'),
(1116, 86, '_tribe_wooticket_event', '27'),
(1117, 86, '_tribe_wooticket_attendee_optout', ''),
(1118, 86, '_tribe_wooticket_security_code', '0648914b95'),
(1119, 86, '_paid_price', '19.99'),
(1120, 86, '_price_currency_symbol', '$'),
(1121, 86, '_unique_id', 'A-7-XG7CQH'),
(1122, 86, '_tribe_tickets_attendee_user_id', '0'),
(1124, 87, '_tribe_wooticket_product', '70'),
(1125, 87, '_tribe_wooticket_order', '84'),
(1126, 87, '_tribe_wooticket_order_item', '11'),
(1127, 87, '_tribe_wooticket_event', '27'),
(1128, 87, '_tribe_wooticket_attendee_optout', ''),
(1129, 87, '_tribe_wooticket_security_code', 'cd1df10616'),
(1130, 87, '_paid_price', '10'),
(1131, 87, '_price_currency_symbol', '$'),
(1132, 87, '_unique_id', 'A-8-QLC426'),
(1133, 87, '_tribe_tickets_attendee_user_id', '0'),
(1145, 27, '_tribe_is_classic_editor', '1'),
(1148, 66, '_tribe_ticket_capacity', '0'),
(1149, 66, '_tribe_ticket_version', '4.10.2'),
(1150, 29, '_tribe_global_id', 'localhost?type=woo&id=29'),
(1152, 29, '_tribe_global_id_lineage', 'a:1:{i:0;s:24:\"localhost?type=woo&id=29\";}'),
(1154, 74, '_tribe_global_id', 'localhost?type=attendee&id=74'),
(1155, 78, '_tribe_global_id', 'localhost?type=attendee&id=78'),
(1156, 74, '_tribe_global_id_lineage', 'a:1:{i:0;s:29:\"localhost?type=attendee&id=74\";}'),
(1157, 78, '_tribe_global_id_lineage', 'a:1:{i:0;s:29:\"localhost?type=attendee&id=78\";}'),
(1158, 81, '_tribe_global_id', 'localhost?type=attendee&id=81'),
(1159, 79, '_tribe_global_id', 'localhost?type=attendee&id=79'),
(1160, 81, '_tribe_global_id_lineage', 'a:1:{i:0;s:29:\"localhost?type=attendee&id=81\";}'),
(1161, 79, '_tribe_global_id_lineage', 'a:1:{i:0;s:29:\"localhost?type=attendee&id=79\";}'),
(1162, 85, '_tribe_global_id', 'localhost?type=attendee&id=85'),
(1163, 82, '_tribe_global_id', 'localhost?type=attendee&id=82'),
(1164, 85, '_tribe_global_id_lineage', 'a:1:{i:0;s:29:\"localhost?type=attendee&id=85\";}'),
(1165, 82, '_tribe_global_id_lineage', 'a:1:{i:0;s:29:\"localhost?type=attendee&id=82\";}'),
(1166, 86, '_tribe_global_id', 'localhost?type=attendee&id=86'),
(1167, 86, '_tribe_global_id_lineage', 'a:1:{i:0;s:29:\"localhost?type=attendee&id=86\";}'),
(1168, 87, '_tribe_global_id', 'localhost?type=attendee&id=87'),
(1169, 87, '_tribe_global_id_lineage', 'a:1:{i:0;s:29:\"localhost?type=attendee&id=87\";}'),
(1170, 14, '_tribe_ticket_capacity', '0'),
(1171, 14, '_tribe_ticket_version', '4.10.2'),
(1172, 27, '_EventDateTimeSeparator', '@'),
(1173, 27, '_EventTimeRangeSeparator', '-'),
(1185, 27, '_tribe_modified_fields', 'a:15:{s:27:\"_tribe_tickets_flush_blocks\";i:1554905952;s:26:\"_transient_tribe_attendees\";i:1558329893;s:34:\"_transient_timeout_tribe_attendees\";i:1558329893;s:10:\"post_title\";i:1554923014;s:12:\"post_content\";i:1554929255;s:7:\"program\";i:1554923954;s:8:\"_program\";i:1554923014;s:13:\"_EventEndDate\";i:1554923014;s:10:\"_EventCost\";i:1554929255;s:12:\"post_excerpt\";i:1554923994;s:40:\"_tribe_progressive_ticket_current_number\";i:1554936627;s:13:\"_thumbnail_id\";i:1554937094;s:20:\"dpsp_networks_shares\";i:1559847060;s:26:\"dpsp_networks_shares_total\";i:1559847060;s:33:\"dpsp_networks_shares_last_updated\";i:1559847060;}'),
(1186, 27, '_tribe_tickets_flush_blocks', '1'),
(1187, 97, '_edit_last', '1'),
(1188, 97, '_edit_lock', '1558378334:1'),
(1195, 68, 'custom_text_field_title', 'emial'),
(1198, 3, '_tribe_ticket_capacity', '0'),
(1199, 3, '_tribe_ticket_version', '4.10.2'),
(1200, 3, '_edit_lock', '1554918895:1'),
(1203, 1, '_tribe_ticket_capacity', '0'),
(1204, 1, '_tribe_ticket_version', '4.10.2'),
(1205, 1, '_edit_lock', '1559846568:1'),
(1208, 102, '_tribe_ticket_capacity', '0'),
(1209, 102, '_tribe_ticket_version', '4.10.2'),
(1210, 102, '_edit_lock', '1559846579:1'),
(1217, 106, '_EventShowMapLink', ''),
(1218, 106, '_EventShowMap', ''),
(1219, 106, '_EventAllDay', 'yes'),
(1220, 106, '_EventStartDate', '2019-04-10 00:00:00'),
(1221, 106, '_EventEndDate', '2019-04-10 23:59:59'),
(1222, 106, '_EventStartDateUTC', '2019-04-10 00:00:00'),
(1223, 106, '_EventEndDateUTC', '2019-04-10 23:59:59'),
(1224, 106, '_EventURL', ''),
(1225, 106, '_EventTimezone', 'UTC+0'),
(1226, 106, '_EventTimezoneAbbr', 'UTC+0'),
(1227, 27, 'program', '<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Secrets of dating</strong>\r\n<ul>\r\n 	<li>Hoe aantrekking in realiteit werkt</li>\r\n 	<li>Waarom trek je altijd de verkeerde aan</li>\r\n 	<li>Waarom mislukt het altijd bij de persoon die je echt wilt</li>\r\n 	<li>Do’s en dont’s bij online dating</li>\r\n 	<li>Invloed van social media op dating</li>\r\n 	<li>Fouten op dates vermijden</li>\r\n 	<li>Hoe een persoon testen op karakter</li>\r\n 	<li>Hoe jij onbewust getest wordt</li>\r\n 	<li>“Push and pull” methode begrijpen</li>\r\n 	<li>Emotioneel in balans blijven bij afwijzing</li>\r\n 	<li>Waarom de friendzone een mythe is</li>\r\n 	<li>Hoe een ex weer aantrekken</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Foundation of a strong relationship</strong>\r\n<ul>\r\n 	<li>Hoe een sterke fundering creëren</li>\r\n 	<li>Hoe argumenteren met jouw partner</li>\r\n 	<li>Hoe jouw partner supporteren in hun doelen</li>\r\n 	<li>Vertrouwen en jaloezie</li>\r\n 	<li>De liefdestaal van uw partner ontdekken</li>\r\n 	<li>Waarom liefde vrij is</li>\r\n 	<li>Hoe jullie liefde weer aanwakkeren</li>\r\n 	<li>Hoe jouw partner “trainen”</li>\r\n 	<li>Hoe om te gaan met het bedrog van uw partner</li>\r\n 	<li>Hoe met een scheiding om te gaan</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Engineer your future self </strong>\r\n<ul>\r\n 	<li>Hoe jouw levensdoel vinden</li>\r\n 	<li>Hoe Law of attraction implementeren</li>\r\n 	<li>Jouw onderbewuste reprogrammeren</li>\r\n 	<li>Controle krijgen over je leven</li>\r\n 	<li>Gevaar en invloed van omgeving</li>\r\n 	<li>Kracht van affirmaties</li>\r\n 	<li>Geloof in jouw eigen vinden</li>\r\n 	<li>Hoe je negatieve energie omdraait in positieve energie</li>\r\n 	<li>Hoe angst overkomen</li>\r\n</ul>\r\n</div>\r\n</div>'),
(1228, 27, '_program', 'field_5cadfcd356735'),
(1229, 106, 'program', '<h3>Secrets of dating</h3>\r\n<ul>\r\n<li>Hoe aantrekking in realiteit werkt</li>\r\n<li>Waarom trek je altijd de verkeerde aan</li>\r\n<li>Waarom mislukt het altijd bij de persoon die je echt wilt</li>\r\n<li>Do’s en dont’s bij online dating</li>\r\n<li>Invloed van social media op dating</li>\r\n<li>Fouten op dates vermijden</li>\r\n<li>Hoe een persoon testen op karakter</li>\r\n<li>Hoe jij onbewust getest wordt</li>\r\n<li>“Push and pull” methode begrijpen</li>\r\n<li>Emotioneel in balans blijven bij afwijzing</li>\r\n<li>Waarom de friendzone een mythe is</li>\r\n<li>Hoe een ex weer aantrekken</li>\r\n</ul>'),
(1230, 106, '_program', 'field_5cadfcd356735'),
(1233, 107, '_EventShowMapLink', ''),
(1234, 107, '_EventShowMap', ''),
(1235, 107, '_EventAllDay', 'yes'),
(1236, 107, '_EventStartDate', '2019-04-10 00:00:00'),
(1237, 107, '_EventEndDate', '2019-04-10 23:59:59'),
(1238, 107, '_EventStartDateUTC', '2019-04-10 00:00:00'),
(1239, 107, '_EventEndDateUTC', '2019-04-10 23:59:59'),
(1240, 107, '_EventURL', ''),
(1241, 107, '_EventTimezone', 'UTC+0'),
(1242, 107, '_EventTimezoneAbbr', 'UTC+0'),
(1243, 107, 'program', '<h3>Secrets of dating</h3>\r\n<ul>\r\n<li>Hoe aantrekking in realiteit werkt</li>\r\n<li>Waarom trek je altijd de verkeerde aan</li>\r\n<li>Waarom mislukt het altijd bij de persoon die je echt wilt</li>\r\n<li>Do’s en dont’s bij online dating</li>\r\n<li>Invloed van social media op dating</li>\r\n<li>Fouten op dates vermijden</li>\r\n<li>Hoe een persoon testen op karakter</li>\r\n<li>Hoe jij onbewust getest wordt</li>\r\n<li>“Push and pull” methode begrijpen</li>\r\n<li>Emotioneel in balans blijven bij afwijzing</li>\r\n<li>Waarom de friendzone een mythe is</li>\r\n<li>Hoe een ex weer aantrekken</li>\r\n</ul>'),
(1244, 107, '_program', 'field_5cadfcd356735'),
(1247, 108, '_EventShowMapLink', ''),
(1248, 108, '_EventShowMap', ''),
(1249, 108, '_EventAllDay', 'yes'),
(1250, 108, '_EventStartDate', '2019-04-10 00:00:00'),
(1251, 108, '_EventEndDate', '2019-04-10 23:59:59'),
(1252, 108, '_EventStartDateUTC', '2019-04-10 00:00:00'),
(1253, 108, '_EventEndDateUTC', '2019-04-10 23:59:59'),
(1254, 108, '_EventURL', ''),
(1255, 108, '_EventTimezone', 'UTC+0'),
(1256, 108, '_EventTimezoneAbbr', 'UTC+0'),
(1257, 108, 'program', '<h4>Secrets of dating</h4>\r\n<ul>\r\n 	<li>Hoe aantrekking in realiteit werkt</li>\r\n 	<li>Waarom trek je altijd de verkeerde aan</li>\r\n 	<li>Waarom mislukt het altijd bij de persoon die je echt wilt</li>\r\n 	<li>Do’s en dont’s bij online dating</li>\r\n 	<li>Invloed van social media op dating</li>\r\n 	<li>Fouten op dates vermijden</li>\r\n 	<li>Hoe een persoon testen op karakter</li>\r\n 	<li>Hoe jij onbewust getest wordt</li>\r\n 	<li>“Push and pull” methode begrijpen</li>\r\n 	<li>Emotioneel in balans blijven bij afwijzing</li>\r\n 	<li>Waarom de friendzone een mythe is</li>\r\n 	<li>Hoe een ex weer aantrekken</li>\r\n</ul>'),
(1258, 108, '_program', 'field_5cadfcd356735'),
(1261, 109, '_EventShowMapLink', ''),
(1262, 109, '_EventShowMap', ''),
(1263, 109, '_EventAllDay', 'yes'),
(1264, 109, '_EventStartDate', '2019-04-10 00:00:00'),
(1265, 109, '_EventEndDate', '2019-04-10 23:59:59'),
(1266, 109, '_EventStartDateUTC', '2019-04-10 00:00:00'),
(1267, 109, '_EventEndDateUTC', '2019-04-10 23:59:59'),
(1268, 109, '_EventURL', ''),
(1269, 109, '_EventTimezone', 'UTC+0'),
(1270, 109, '_EventTimezoneAbbr', 'UTC+0'),
(1271, 109, 'program', '<strong>Secrets of dating</strong>\r\n<ul>\r\n 	<li>Hoe aantrekking in realiteit werkt</li>\r\n 	<li>Waarom trek je altijd de verkeerde aan</li>\r\n 	<li>Waarom mislukt het altijd bij de persoon die je echt wilt</li>\r\n 	<li>Do’s en dont’s bij online dating</li>\r\n 	<li>Invloed van social media op dating</li>\r\n 	<li>Fouten op dates vermijden</li>\r\n 	<li>Hoe een persoon testen op karakter</li>\r\n 	<li>Hoe jij onbewust getest wordt</li>\r\n 	<li>“Push and pull” methode begrijpen</li>\r\n 	<li>Emotioneel in balans blijven bij afwijzing</li>\r\n 	<li>Waarom de friendzone een mythe is</li>\r\n 	<li>Hoe een ex weer aantrekken</li>\r\n</ul>'),
(1272, 109, '_program', 'field_5cadfcd356735'),
(1275, 110, '_EventShowMapLink', ''),
(1276, 110, '_EventShowMap', ''),
(1277, 110, '_EventAllDay', 'yes'),
(1278, 110, '_EventStartDate', '2019-04-10 00:00:00'),
(1279, 110, '_EventEndDate', '2019-04-10 23:59:59'),
(1280, 110, '_EventStartDateUTC', '2019-04-10 00:00:00'),
(1281, 110, '_EventEndDateUTC', '2019-04-10 23:59:59'),
(1282, 110, '_EventURL', ''),
(1283, 110, '_EventTimezone', 'UTC+0'),
(1284, 110, '_EventTimezoneAbbr', 'UTC+0'),
(1285, 110, 'program', '<div class=\"row\">\r\n<div class=\"col\">\r\n\r\n<strong>Secrets of dating</strong>\r\n<ul>\r\n 	<li>Hoe aantrekking in realiteit werkt</li>\r\n 	<li>Waarom trek je altijd de verkeerde aan</li>\r\n 	<li>Waarom mislukt het altijd bij de persoon die je echt wilt</li>\r\n 	<li>Do’s en dont’s bij online dating</li>\r\n 	<li>Invloed van social media op dating</li>\r\n 	<li>Fouten op dates vermijden</li>\r\n 	<li>Hoe een persoon testen op karakter</li>\r\n 	<li>Hoe jij onbewust getest wordt</li>\r\n 	<li>“Push and pull” methode begrijpen</li>\r\n 	<li>Emotioneel in balans blijven bij afwijzing</li>\r\n 	<li>Waarom de friendzone een mythe is</li>\r\n 	<li>Hoe een ex weer aantrekken</li>\r\n</ul>\r\n</div>\r\n<div class=\"col\">\r\n\r\n<strong>Foundation of a strong relationship</strong>\r\n<ul>\r\n 	<li>Hoe een sterke fundering creëren</li>\r\n 	<li>Hoe argumenteren met jouw partner</li>\r\n 	<li>Hoe jouw partner supporteren in hun doelen</li>\r\n 	<li>Vertrouwen en jaloezie</li>\r\n 	<li>De liefdestaal van uw partner ontdekken</li>\r\n 	<li>Waarom liefde vrij is</li>\r\n 	<li>Hoe jullie liefde weer aanwakkeren</li>\r\n 	<li>Hoe jouw partner “trainen”</li>\r\n 	<li>Hoe om te gaan met het bedrog van uw partner</li>\r\n 	<li>Hoe met een scheiding om te gaan</li>\r\n</ul>\r\n</div>\r\n<div class=\"col\">\r\n\r\n<strong>Engineer your future self </strong>\r\n<ul>\r\n 	<li>Hoe jouw levensdoel vinden</li>\r\n 	<li>Hoe Law of attraction implementeren</li>\r\n 	<li>Jouw onderbewuste reprogrammeren</li>\r\n 	<li>Controle krijgen over je leven</li>\r\n 	<li>Gevaar en invloed van omgeving</li>\r\n 	<li>Kracht van affirmaties</li>\r\n 	<li>Geloof in jouw eigen vinden</li>\r\n 	<li>Hoe je negatieve energie omdraait in positieve energie</li>\r\n 	<li>Hoe angst overkomen</li>\r\n</ul>\r\n</div>\r\n</div>'),
(1286, 110, '_program', 'field_5cadfcd356735'),
(1289, 111, '_EventShowMapLink', ''),
(1290, 111, '_EventShowMap', ''),
(1291, 111, '_EventAllDay', 'yes'),
(1292, 111, '_EventStartDate', '2019-04-10 00:00:00'),
(1293, 111, '_EventEndDate', '2019-04-10 23:59:59'),
(1294, 111, '_EventStartDateUTC', '2019-04-10 00:00:00'),
(1295, 111, '_EventEndDateUTC', '2019-04-10 23:59:59'),
(1296, 111, '_EventURL', ''),
(1297, 111, '_EventTimezone', 'UTC+0'),
(1298, 111, '_EventTimezoneAbbr', 'UTC+0'),
(1299, 111, 'program', '<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Secrets of dating</strong>\r\n<ul>\r\n 	<li>Hoe aantrekking in realiteit werkt</li>\r\n 	<li>Waarom trek je altijd de verkeerde aan</li>\r\n 	<li>Waarom mislukt het altijd bij de persoon die je echt wilt</li>\r\n 	<li>Do’s en dont’s bij online dating</li>\r\n 	<li>Invloed van social media op dating</li>\r\n 	<li>Fouten op dates vermijden</li>\r\n 	<li>Hoe een persoon testen op karakter</li>\r\n 	<li>Hoe jij onbewust getest wordt</li>\r\n 	<li>“Push and pull” methode begrijpen</li>\r\n 	<li>Emotioneel in balans blijven bij afwijzing</li>\r\n 	<li>Waarom de friendzone een mythe is</li>\r\n 	<li>Hoe een ex weer aantrekken</li>\r\n</ul>\r\n</div>\r\n<div class=\"coll-md-4\">\r\n\r\n<strong>Foundation of a strong relationship</strong>\r\n<ul>\r\n 	<li>Hoe een sterke fundering creëren</li>\r\n 	<li>Hoe argumenteren met jouw partner</li>\r\n 	<li>Hoe jouw partner supporteren in hun doelen</li>\r\n 	<li>Vertrouwen en jaloezie</li>\r\n 	<li>De liefdestaal van uw partner ontdekken</li>\r\n 	<li>Waarom liefde vrij is</li>\r\n 	<li>Hoe jullie liefde weer aanwakkeren</li>\r\n 	<li>Hoe jouw partner “trainen”</li>\r\n 	<li>Hoe om te gaan met het bedrog van uw partner</li>\r\n 	<li>Hoe met een scheiding om te gaan</li>\r\n</ul>\r\n</div>\r\n<div class=\"coll-md-4\">\r\n\r\n<strong>Engineer your future self </strong>\r\n<ul>\r\n 	<li>Hoe jouw levensdoel vinden</li>\r\n 	<li>Hoe Law of attraction implementeren</li>\r\n 	<li>Jouw onderbewuste reprogrammeren</li>\r\n 	<li>Controle krijgen over je leven</li>\r\n 	<li>Gevaar en invloed van omgeving</li>\r\n 	<li>Kracht van affirmaties</li>\r\n 	<li>Geloof in jouw eigen vinden</li>\r\n 	<li>Hoe je negatieve energie omdraait in positieve energie</li>\r\n 	<li>Hoe angst overkomen</li>\r\n</ul>\r\n</div>\r\n</div>'),
(1300, 111, '_program', 'field_5cadfcd356735'),
(1303, 112, '_EventShowMapLink', ''),
(1304, 112, '_EventShowMap', ''),
(1305, 112, '_EventAllDay', 'yes'),
(1306, 112, '_EventStartDate', '2019-04-10 00:00:00'),
(1307, 112, '_EventEndDate', '2019-04-10 23:59:59'),
(1308, 112, '_EventStartDateUTC', '2019-04-10 00:00:00'),
(1309, 112, '_EventEndDateUTC', '2019-04-10 23:59:59'),
(1310, 112, '_EventURL', ''),
(1311, 112, '_EventTimezone', 'UTC+0'),
(1312, 112, '_EventTimezoneAbbr', 'UTC+0'),
(1313, 112, 'program', '<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Secrets of dating</strong>\r\n<ul>\r\n 	<li>Hoe aantrekking in realiteit werkt</li>\r\n 	<li>Waarom trek je altijd de verkeerde aan</li>\r\n 	<li>Waarom mislukt het altijd bij de persoon die je echt wilt</li>\r\n 	<li>Do’s en dont’s bij online dating</li>\r\n 	<li>Invloed van social media op dating</li>\r\n 	<li>Fouten op dates vermijden</li>\r\n 	<li>Hoe een persoon testen op karakter</li>\r\n 	<li>Hoe jij onbewust getest wordt</li>\r\n 	<li>“Push and pull” methode begrijpen</li>\r\n 	<li>Emotioneel in balans blijven bij afwijzing</li>\r\n 	<li>Waarom de friendzone een mythe is</li>\r\n 	<li>Hoe een ex weer aantrekken</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Foundation of a strong relationship</strong>\r\n<ul>\r\n 	<li>Hoe een sterke fundering creëren</li>\r\n 	<li>Hoe argumenteren met jouw partner</li>\r\n 	<li>Hoe jouw partner supporteren in hun doelen</li>\r\n 	<li>Vertrouwen en jaloezie</li>\r\n 	<li>De liefdestaal van uw partner ontdekken</li>\r\n 	<li>Waarom liefde vrij is</li>\r\n 	<li>Hoe jullie liefde weer aanwakkeren</li>\r\n 	<li>Hoe jouw partner “trainen”</li>\r\n 	<li>Hoe om te gaan met het bedrog van uw partner</li>\r\n 	<li>Hoe met een scheiding om te gaan</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Engineer your future self </strong>\r\n<ul>\r\n 	<li>Hoe jouw levensdoel vinden</li>\r\n 	<li>Hoe Law of attraction implementeren</li>\r\n 	<li>Jouw onderbewuste reprogrammeren</li>\r\n 	<li>Controle krijgen over je leven</li>\r\n 	<li>Gevaar en invloed van omgeving</li>\r\n 	<li>Kracht van affirmaties</li>\r\n 	<li>Geloof in jouw eigen vinden</li>\r\n 	<li>Hoe je negatieve energie omdraait in positieve energie</li>\r\n 	<li>Hoe angst overkomen</li>\r\n</ul>\r\n</div>\r\n</div>'),
(1314, 112, '_program', 'field_5cadfcd356735'),
(1317, 113, '_EventShowMapLink', ''),
(1318, 113, '_EventShowMap', ''),
(1319, 113, '_EventAllDay', 'yes'),
(1320, 113, '_EventStartDate', '2019-04-10 00:00:00'),
(1321, 113, '_EventEndDate', '2019-04-10 23:59:59'),
(1322, 113, '_EventStartDateUTC', '2019-04-10 00:00:00'),
(1323, 113, '_EventEndDateUTC', '2019-04-10 23:59:59'),
(1324, 113, '_EventURL', ''),
(1325, 113, '_EventTimezone', 'UTC+0'),
(1326, 113, '_EventTimezoneAbbr', 'UTC+0'),
(1327, 113, 'program', '<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Secrets of dating</strong>\r\n<ul>\r\n 	<li>Hoe aantrekking in realiteit werkt</li>\r\n 	<li>Waarom trek je altijd de verkeerde aan</li>\r\n 	<li>Waarom mislukt het altijd bij de persoon die je echt wilt</li>\r\n 	<li>Do’s en dont’s bij online dating</li>\r\n 	<li>Invloed van social media op dating</li>\r\n 	<li>Fouten op dates vermijden</li>\r\n 	<li>Hoe een persoon testen op karakter</li>\r\n 	<li>Hoe jij onbewust getest wordt</li>\r\n 	<li>“Push and pull” methode begrijpen</li>\r\n 	<li>Emotioneel in balans blijven bij afwijzing</li>\r\n 	<li>Waarom de friendzone een mythe is</li>\r\n 	<li>Hoe een ex weer aantrekken</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Foundation of a strong relationship</strong>\r\n<ul>\r\n 	<li>Hoe een sterke fundering creëren</li>\r\n 	<li>Hoe argumenteren met jouw partner</li>\r\n 	<li>Hoe jouw partner supporteren in hun doelen</li>\r\n 	<li>Vertrouwen en jaloezie</li>\r\n 	<li>De liefdestaal van uw partner ontdekken</li>\r\n 	<li>Waarom liefde vrij is</li>\r\n 	<li>Hoe jullie liefde weer aanwakkeren</li>\r\n 	<li>Hoe jouw partner “trainen”</li>\r\n 	<li>Hoe om te gaan met het bedrog van uw partner</li>\r\n 	<li>Hoe met een scheiding om te gaan</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Engineer your future self </strong>\r\n<ul>\r\n 	<li>Hoe jouw levensdoel vinden</li>\r\n 	<li>Hoe Law of attraction implementeren</li>\r\n 	<li>Jouw onderbewuste reprogrammeren</li>\r\n 	<li>Controle krijgen over je leven</li>\r\n 	<li>Gevaar en invloed van omgeving</li>\r\n 	<li>Kracht van affirmaties</li>\r\n 	<li>Geloof in jouw eigen vinden</li>\r\n 	<li>Hoe je negatieve energie omdraait in positieve energie</li>\r\n 	<li>Hoe angst overkomen</li>\r\n</ul>\r\n</div>\r\n</div>'),
(1328, 113, '_program', 'field_5cadfcd356735'),
(1331, 114, '_EventShowMapLink', ''),
(1332, 114, '_EventShowMap', ''),
(1333, 114, '_EventAllDay', 'yes'),
(1334, 114, '_EventStartDate', '2019-04-10 00:00:00'),
(1335, 114, '_EventEndDate', '2019-04-10 23:59:59'),
(1336, 114, '_EventStartDateUTC', '2019-04-10 00:00:00'),
(1337, 114, '_EventEndDateUTC', '2019-04-10 23:59:59'),
(1338, 114, '_EventURL', ''),
(1339, 114, '_EventTimezone', 'UTC+0'),
(1340, 114, '_EventTimezoneAbbr', 'UTC+0'),
(1341, 114, 'program', '<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Secrets of dating</strong>\r\n<ul>\r\n 	<li>Hoe aantrekking in realiteit werkt</li>\r\n 	<li>Waarom trek je altijd de verkeerde aan</li>\r\n 	<li>Waarom mislukt het altijd bij de persoon die je echt wilt</li>\r\n 	<li>Do’s en dont’s bij online dating</li>\r\n 	<li>Invloed van social media op dating</li>\r\n 	<li>Fouten op dates vermijden</li>\r\n 	<li>Hoe een persoon testen op karakter</li>\r\n 	<li>Hoe jij onbewust getest wordt</li>\r\n 	<li>“Push and pull” methode begrijpen</li>\r\n 	<li>Emotioneel in balans blijven bij afwijzing</li>\r\n 	<li>Waarom de friendzone een mythe is</li>\r\n 	<li>Hoe een ex weer aantrekken</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Foundation of a strong relationship</strong>\r\n<ul>\r\n 	<li>Hoe een sterke fundering creëren</li>\r\n 	<li>Hoe argumenteren met jouw partner</li>\r\n 	<li>Hoe jouw partner supporteren in hun doelen</li>\r\n 	<li>Vertrouwen en jaloezie</li>\r\n 	<li>De liefdestaal van uw partner ontdekken</li>\r\n 	<li>Waarom liefde vrij is</li>\r\n 	<li>Hoe jullie liefde weer aanwakkeren</li>\r\n 	<li>Hoe jouw partner “trainen”</li>\r\n 	<li>Hoe om te gaan met het bedrog van uw partner</li>\r\n 	<li>Hoe met een scheiding om te gaan</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Engineer your future self </strong>\r\n<ul>\r\n 	<li>Hoe jouw levensdoel vinden</li>\r\n 	<li>Hoe Law of attraction implementeren</li>\r\n 	<li>Jouw onderbewuste reprogrammeren</li>\r\n 	<li>Controle krijgen over je leven</li>\r\n 	<li>Gevaar en invloed van omgeving</li>\r\n 	<li>Kracht van affirmaties</li>\r\n 	<li>Geloof in jouw eigen vinden</li>\r\n 	<li>Hoe je negatieve energie omdraait in positieve energie</li>\r\n 	<li>Hoe angst overkomen</li>\r\n</ul>\r\n</div>\r\n</div>'),
(1342, 114, '_program', 'field_5cadfcd356735'),
(1345, 115, '_EventShowMapLink', ''),
(1346, 115, '_EventShowMap', ''),
(1347, 115, '_EventAllDay', 'yes'),
(1348, 115, '_EventStartDate', '2019-04-10 00:00:00'),
(1349, 115, '_EventEndDate', '2019-04-10 23:59:59'),
(1350, 115, '_EventStartDateUTC', '2019-04-10 00:00:00'),
(1351, 115, '_EventEndDateUTC', '2019-04-10 23:59:59'),
(1352, 115, '_EventURL', ''),
(1353, 115, '_EventTimezone', 'UTC+0'),
(1354, 115, '_EventTimezoneAbbr', 'UTC+0'),
(1355, 115, 'program', '<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Secrets of dating</strong>\r\n<ul>\r\n 	<li>Hoe aantrekking in realiteit werkt</li>\r\n 	<li>Waarom trek je altijd de verkeerde aan</li>\r\n 	<li>Waarom mislukt het altijd bij de persoon die je echt wilt</li>\r\n 	<li>Do’s en dont’s bij online dating</li>\r\n 	<li>Invloed van social media op dating</li>\r\n 	<li>Fouten op dates vermijden</li>\r\n 	<li>Hoe een persoon testen op karakter</li>\r\n 	<li>Hoe jij onbewust getest wordt</li>\r\n 	<li>“Push and pull” methode begrijpen</li>\r\n 	<li>Emotioneel in balans blijven bij afwijzing</li>\r\n 	<li>Waarom de friendzone een mythe is</li>\r\n 	<li>Hoe een ex weer aantrekken</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Foundation of a strong relationship</strong>\r\n<ul>\r\n 	<li>Hoe een sterke fundering creëren</li>\r\n 	<li>Hoe argumenteren met jouw partner</li>\r\n 	<li>Hoe jouw partner supporteren in hun doelen</li>\r\n 	<li>Vertrouwen en jaloezie</li>\r\n 	<li>De liefdestaal van uw partner ontdekken</li>\r\n 	<li>Waarom liefde vrij is</li>\r\n 	<li>Hoe jullie liefde weer aanwakkeren</li>\r\n 	<li>Hoe jouw partner “trainen”</li>\r\n 	<li>Hoe om te gaan met het bedrog van uw partner</li>\r\n 	<li>Hoe met een scheiding om te gaan</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Engineer your future self </strong>\r\n<ul>\r\n 	<li>Hoe jouw levensdoel vinden</li>\r\n 	<li>Hoe Law of attraction implementeren</li>\r\n 	<li>Jouw onderbewuste reprogrammeren</li>\r\n 	<li>Controle krijgen over je leven</li>\r\n 	<li>Gevaar en invloed van omgeving</li>\r\n 	<li>Kracht van affirmaties</li>\r\n 	<li>Geloof in jouw eigen vinden</li>\r\n 	<li>Hoe je negatieve energie omdraait in positieve energie</li>\r\n 	<li>Hoe angst overkomen</li>\r\n</ul>\r\n</div>\r\n</div>'),
(1356, 115, '_program', 'field_5cadfcd356735'),
(1359, 116, '_wp_attached_file', '2019/03/ak-1.jpg'),
(1360, 116, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1000;s:6:\"height\";i:1001;s:4:\"file\";s:16:\"2019/03/ak-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"ak-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"ak-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"ak-1-768x769.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:769;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:16:\"ak-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:16:\"ak-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"ak-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:16:\"ak-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:16:\"ak-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"ak-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1361, 117, '_wp_attached_file', '2019/03/as-2.jpg');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1362, 117, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1000;s:6:\"height\";i:1001;s:4:\"file\";s:16:\"2019/03/as-2.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"as-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"as-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"as-2-768x769.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:769;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:16:\"as-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:16:\"as-2-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"as-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:16:\"as-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:16:\"as-2-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"as-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1363, 118, '_EventShowMapLink', ''),
(1364, 118, '_EventShowMap', ''),
(1365, 118, '_EventAllDay', 'yes'),
(1366, 118, '_EventStartDate', '2019-04-10 00:00:00'),
(1367, 118, '_EventEndDate', '2019-04-10 23:59:59'),
(1368, 118, '_EventStartDateUTC', '2019-04-10 00:00:00'),
(1369, 118, '_EventEndDateUTC', '2019-04-10 23:59:59'),
(1370, 118, '_EventURL', ''),
(1371, 118, '_EventTimezone', 'UTC+0'),
(1372, 118, '_EventTimezoneAbbr', 'UTC+0'),
(1373, 118, 'program', '<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Secrets of dating</strong>\r\n<ul>\r\n 	<li>Hoe aantrekking in realiteit werkt</li>\r\n 	<li>Waarom trek je altijd de verkeerde aan</li>\r\n 	<li>Waarom mislukt het altijd bij de persoon die je echt wilt</li>\r\n 	<li>Do’s en dont’s bij online dating</li>\r\n 	<li>Invloed van social media op dating</li>\r\n 	<li>Fouten op dates vermijden</li>\r\n 	<li>Hoe een persoon testen op karakter</li>\r\n 	<li>Hoe jij onbewust getest wordt</li>\r\n 	<li>“Push and pull” methode begrijpen</li>\r\n 	<li>Emotioneel in balans blijven bij afwijzing</li>\r\n 	<li>Waarom de friendzone een mythe is</li>\r\n 	<li>Hoe een ex weer aantrekken</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Foundation of a strong relationship</strong>\r\n<ul>\r\n 	<li>Hoe een sterke fundering creëren</li>\r\n 	<li>Hoe argumenteren met jouw partner</li>\r\n 	<li>Hoe jouw partner supporteren in hun doelen</li>\r\n 	<li>Vertrouwen en jaloezie</li>\r\n 	<li>De liefdestaal van uw partner ontdekken</li>\r\n 	<li>Waarom liefde vrij is</li>\r\n 	<li>Hoe jullie liefde weer aanwakkeren</li>\r\n 	<li>Hoe jouw partner “trainen”</li>\r\n 	<li>Hoe om te gaan met het bedrog van uw partner</li>\r\n 	<li>Hoe met een scheiding om te gaan</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Engineer your future self </strong>\r\n<ul>\r\n 	<li>Hoe jouw levensdoel vinden</li>\r\n 	<li>Hoe Law of attraction implementeren</li>\r\n 	<li>Jouw onderbewuste reprogrammeren</li>\r\n 	<li>Controle krijgen over je leven</li>\r\n 	<li>Gevaar en invloed van omgeving</li>\r\n 	<li>Kracht van affirmaties</li>\r\n 	<li>Geloof in jouw eigen vinden</li>\r\n 	<li>Hoe je negatieve energie omdraait in positieve energie</li>\r\n 	<li>Hoe angst overkomen</li>\r\n</ul>\r\n</div>\r\n</div>'),
(1374, 118, '_program', 'field_5cadfcd356735'),
(1377, 119, '_EventShowMapLink', ''),
(1378, 119, '_EventShowMap', ''),
(1379, 119, '_EventAllDay', 'yes'),
(1380, 119, '_EventStartDate', '2019-04-10 00:00:00'),
(1381, 119, '_EventEndDate', '2019-04-10 23:59:59'),
(1382, 119, '_EventStartDateUTC', '2019-04-10 00:00:00'),
(1383, 119, '_EventEndDateUTC', '2019-04-10 23:59:59'),
(1384, 119, '_EventURL', ''),
(1385, 119, '_EventTimezone', 'UTC+0'),
(1386, 119, '_EventTimezoneAbbr', 'UTC+0'),
(1387, 119, 'program', '<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Secrets of dating</strong>\r\n<ul>\r\n 	<li>Hoe aantrekking in realiteit werkt</li>\r\n 	<li>Waarom trek je altijd de verkeerde aan</li>\r\n 	<li>Waarom mislukt het altijd bij de persoon die je echt wilt</li>\r\n 	<li>Do’s en dont’s bij online dating</li>\r\n 	<li>Invloed van social media op dating</li>\r\n 	<li>Fouten op dates vermijden</li>\r\n 	<li>Hoe een persoon testen op karakter</li>\r\n 	<li>Hoe jij onbewust getest wordt</li>\r\n 	<li>“Push and pull” methode begrijpen</li>\r\n 	<li>Emotioneel in balans blijven bij afwijzing</li>\r\n 	<li>Waarom de friendzone een mythe is</li>\r\n 	<li>Hoe een ex weer aantrekken</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Foundation of a strong relationship</strong>\r\n<ul>\r\n 	<li>Hoe een sterke fundering creëren</li>\r\n 	<li>Hoe argumenteren met jouw partner</li>\r\n 	<li>Hoe jouw partner supporteren in hun doelen</li>\r\n 	<li>Vertrouwen en jaloezie</li>\r\n 	<li>De liefdestaal van uw partner ontdekken</li>\r\n 	<li>Waarom liefde vrij is</li>\r\n 	<li>Hoe jullie liefde weer aanwakkeren</li>\r\n 	<li>Hoe jouw partner “trainen”</li>\r\n 	<li>Hoe om te gaan met het bedrog van uw partner</li>\r\n 	<li>Hoe met een scheiding om te gaan</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Engineer your future self </strong>\r\n<ul>\r\n 	<li>Hoe jouw levensdoel vinden</li>\r\n 	<li>Hoe Law of attraction implementeren</li>\r\n 	<li>Jouw onderbewuste reprogrammeren</li>\r\n 	<li>Controle krijgen over je leven</li>\r\n 	<li>Gevaar en invloed van omgeving</li>\r\n 	<li>Kracht van affirmaties</li>\r\n 	<li>Geloof in jouw eigen vinden</li>\r\n 	<li>Hoe je negatieve energie omdraait in positieve energie</li>\r\n 	<li>Hoe angst overkomen</li>\r\n</ul>\r\n</div>\r\n</div>'),
(1388, 119, '_program', 'field_5cadfcd356735'),
(1391, 120, '_EventShowMapLink', ''),
(1392, 120, '_EventShowMap', ''),
(1393, 120, '_EventAllDay', 'yes'),
(1394, 120, '_EventStartDate', '2019-04-10 00:00:00'),
(1395, 120, '_EventEndDate', '2019-04-10 23:59:59'),
(1396, 120, '_EventStartDateUTC', '2019-04-10 00:00:00'),
(1397, 120, '_EventEndDateUTC', '2019-04-10 23:59:59'),
(1398, 120, '_EventURL', ''),
(1399, 120, '_EventTimezone', 'UTC+0'),
(1400, 120, '_EventTimezoneAbbr', 'UTC+0'),
(1401, 120, 'program', '<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Secrets of dating</strong>\r\n<ul>\r\n 	<li>Hoe aantrekking in realiteit werkt</li>\r\n 	<li>Waarom trek je altijd de verkeerde aan</li>\r\n 	<li>Waarom mislukt het altijd bij de persoon die je echt wilt</li>\r\n 	<li>Do’s en dont’s bij online dating</li>\r\n 	<li>Invloed van social media op dating</li>\r\n 	<li>Fouten op dates vermijden</li>\r\n 	<li>Hoe een persoon testen op karakter</li>\r\n 	<li>Hoe jij onbewust getest wordt</li>\r\n 	<li>“Push and pull” methode begrijpen</li>\r\n 	<li>Emotioneel in balans blijven bij afwijzing</li>\r\n 	<li>Waarom de friendzone een mythe is</li>\r\n 	<li>Hoe een ex weer aantrekken</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Foundation of a strong relationship</strong>\r\n<ul>\r\n 	<li>Hoe een sterke fundering creëren</li>\r\n 	<li>Hoe argumenteren met jouw partner</li>\r\n 	<li>Hoe jouw partner supporteren in hun doelen</li>\r\n 	<li>Vertrouwen en jaloezie</li>\r\n 	<li>De liefdestaal van uw partner ontdekken</li>\r\n 	<li>Waarom liefde vrij is</li>\r\n 	<li>Hoe jullie liefde weer aanwakkeren</li>\r\n 	<li>Hoe jouw partner “trainen”</li>\r\n 	<li>Hoe om te gaan met het bedrog van uw partner</li>\r\n 	<li>Hoe met een scheiding om te gaan</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Engineer your future self </strong>\r\n<ul>\r\n 	<li>Hoe jouw levensdoel vinden</li>\r\n 	<li>Hoe Law of attraction implementeren</li>\r\n 	<li>Jouw onderbewuste reprogrammeren</li>\r\n 	<li>Controle krijgen over je leven</li>\r\n 	<li>Gevaar en invloed van omgeving</li>\r\n 	<li>Kracht van affirmaties</li>\r\n 	<li>Geloof in jouw eigen vinden</li>\r\n 	<li>Hoe je negatieve energie omdraait in positieve energie</li>\r\n 	<li>Hoe angst overkomen</li>\r\n</ul>\r\n</div>\r\n</div>'),
(1402, 120, '_program', 'field_5cadfcd356735'),
(1405, 121, '_EventShowMapLink', ''),
(1406, 121, '_EventShowMap', ''),
(1407, 121, '_EventAllDay', 'yes'),
(1408, 121, '_EventStartDate', '2019-04-10 00:00:00'),
(1409, 121, '_EventEndDate', '2019-04-10 23:59:59'),
(1410, 121, '_EventStartDateUTC', '2019-04-10 00:00:00'),
(1411, 121, '_EventEndDateUTC', '2019-04-10 23:59:59'),
(1412, 121, '_EventURL', ''),
(1413, 121, '_EventTimezone', 'UTC+0'),
(1414, 121, '_EventTimezoneAbbr', 'UTC+0'),
(1415, 121, 'program', '<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Secrets of dating</strong>\r\n<ul>\r\n 	<li>Hoe aantrekking in realiteit werkt</li>\r\n 	<li>Waarom trek je altijd de verkeerde aan</li>\r\n 	<li>Waarom mislukt het altijd bij de persoon die je echt wilt</li>\r\n 	<li>Do’s en dont’s bij online dating</li>\r\n 	<li>Invloed van social media op dating</li>\r\n 	<li>Fouten op dates vermijden</li>\r\n 	<li>Hoe een persoon testen op karakter</li>\r\n 	<li>Hoe jij onbewust getest wordt</li>\r\n 	<li>“Push and pull” methode begrijpen</li>\r\n 	<li>Emotioneel in balans blijven bij afwijzing</li>\r\n 	<li>Waarom de friendzone een mythe is</li>\r\n 	<li>Hoe een ex weer aantrekken</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Foundation of a strong relationship</strong>\r\n<ul>\r\n 	<li>Hoe een sterke fundering creëren</li>\r\n 	<li>Hoe argumenteren met jouw partner</li>\r\n 	<li>Hoe jouw partner supporteren in hun doelen</li>\r\n 	<li>Vertrouwen en jaloezie</li>\r\n 	<li>De liefdestaal van uw partner ontdekken</li>\r\n 	<li>Waarom liefde vrij is</li>\r\n 	<li>Hoe jullie liefde weer aanwakkeren</li>\r\n 	<li>Hoe jouw partner “trainen”</li>\r\n 	<li>Hoe om te gaan met het bedrog van uw partner</li>\r\n 	<li>Hoe met een scheiding om te gaan</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Engineer your future self </strong>\r\n<ul>\r\n 	<li>Hoe jouw levensdoel vinden</li>\r\n 	<li>Hoe Law of attraction implementeren</li>\r\n 	<li>Jouw onderbewuste reprogrammeren</li>\r\n 	<li>Controle krijgen over je leven</li>\r\n 	<li>Gevaar en invloed van omgeving</li>\r\n 	<li>Kracht van affirmaties</li>\r\n 	<li>Geloof in jouw eigen vinden</li>\r\n 	<li>Hoe je negatieve energie omdraait in positieve energie</li>\r\n 	<li>Hoe angst overkomen</li>\r\n</ul>\r\n</div>\r\n</div>'),
(1416, 121, '_program', 'field_5cadfcd356735'),
(1419, 122, '_EventShowMapLink', ''),
(1420, 122, '_EventShowMap', ''),
(1421, 122, '_EventAllDay', 'yes'),
(1422, 122, '_EventStartDate', '2019-04-10 00:00:00'),
(1423, 122, '_EventEndDate', '2019-04-10 23:59:59'),
(1424, 122, '_EventStartDateUTC', '2019-04-10 00:00:00'),
(1425, 122, '_EventEndDateUTC', '2019-04-10 23:59:59'),
(1426, 122, '_EventURL', ''),
(1427, 122, '_EventTimezone', 'UTC+0'),
(1428, 122, '_EventTimezoneAbbr', 'UTC+0'),
(1429, 122, 'program', '<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Secrets of dating</strong>\r\n<ul>\r\n 	<li>Hoe aantrekking in realiteit werkt</li>\r\n 	<li>Waarom trek je altijd de verkeerde aan</li>\r\n 	<li>Waarom mislukt het altijd bij de persoon die je echt wilt</li>\r\n 	<li>Do’s en dont’s bij online dating</li>\r\n 	<li>Invloed van social media op dating</li>\r\n 	<li>Fouten op dates vermijden</li>\r\n 	<li>Hoe een persoon testen op karakter</li>\r\n 	<li>Hoe jij onbewust getest wordt</li>\r\n 	<li>“Push and pull” methode begrijpen</li>\r\n 	<li>Emotioneel in balans blijven bij afwijzing</li>\r\n 	<li>Waarom de friendzone een mythe is</li>\r\n 	<li>Hoe een ex weer aantrekken</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Foundation of a strong relationship</strong>\r\n<ul>\r\n 	<li>Hoe een sterke fundering creëren</li>\r\n 	<li>Hoe argumenteren met jouw partner</li>\r\n 	<li>Hoe jouw partner supporteren in hun doelen</li>\r\n 	<li>Vertrouwen en jaloezie</li>\r\n 	<li>De liefdestaal van uw partner ontdekken</li>\r\n 	<li>Waarom liefde vrij is</li>\r\n 	<li>Hoe jullie liefde weer aanwakkeren</li>\r\n 	<li>Hoe jouw partner “trainen”</li>\r\n 	<li>Hoe om te gaan met het bedrog van uw partner</li>\r\n 	<li>Hoe met een scheiding om te gaan</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Engineer your future self </strong>\r\n<ul>\r\n 	<li>Hoe jouw levensdoel vinden</li>\r\n 	<li>Hoe Law of attraction implementeren</li>\r\n 	<li>Jouw onderbewuste reprogrammeren</li>\r\n 	<li>Controle krijgen over je leven</li>\r\n 	<li>Gevaar en invloed van omgeving</li>\r\n 	<li>Kracht van affirmaties</li>\r\n 	<li>Geloof in jouw eigen vinden</li>\r\n 	<li>Hoe je negatieve energie omdraait in positieve energie</li>\r\n 	<li>Hoe angst overkomen</li>\r\n</ul>\r\n</div>\r\n</div>'),
(1430, 122, '_program', 'field_5cadfcd356735'),
(1435, 123, '_EventShowMapLink', ''),
(1436, 123, '_EventShowMap', ''),
(1437, 123, '_EventAllDay', 'yes'),
(1438, 123, '_EventStartDate', '2019-04-10 00:00:00'),
(1439, 123, '_EventEndDate', '2019-04-10 23:59:59'),
(1440, 123, '_EventStartDateUTC', '2019-04-10 00:00:00'),
(1441, 123, '_EventEndDateUTC', '2019-04-10 23:59:59'),
(1442, 123, '_EventURL', ''),
(1443, 123, '_EventTimezone', 'UTC+0'),
(1444, 123, '_EventTimezoneAbbr', 'UTC+0'),
(1445, 123, 'program', '<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Secrets of dating</strong>\r\n<ul>\r\n 	<li>Hoe aantrekking in realiteit werkt</li>\r\n 	<li>Waarom trek je altijd de verkeerde aan</li>\r\n 	<li>Waarom mislukt het altijd bij de persoon die je echt wilt</li>\r\n 	<li>Do’s en dont’s bij online dating</li>\r\n 	<li>Invloed van social media op dating</li>\r\n 	<li>Fouten op dates vermijden</li>\r\n 	<li>Hoe een persoon testen op karakter</li>\r\n 	<li>Hoe jij onbewust getest wordt</li>\r\n 	<li>“Push and pull” methode begrijpen</li>\r\n 	<li>Emotioneel in balans blijven bij afwijzing</li>\r\n 	<li>Waarom de friendzone een mythe is</li>\r\n 	<li>Hoe een ex weer aantrekken</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Foundation of a strong relationship</strong>\r\n<ul>\r\n 	<li>Hoe een sterke fundering creëren</li>\r\n 	<li>Hoe argumenteren met jouw partner</li>\r\n 	<li>Hoe jouw partner supporteren in hun doelen</li>\r\n 	<li>Vertrouwen en jaloezie</li>\r\n 	<li>De liefdestaal van uw partner ontdekken</li>\r\n 	<li>Waarom liefde vrij is</li>\r\n 	<li>Hoe jullie liefde weer aanwakkeren</li>\r\n 	<li>Hoe jouw partner “trainen”</li>\r\n 	<li>Hoe om te gaan met het bedrog van uw partner</li>\r\n 	<li>Hoe met een scheiding om te gaan</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Engineer your future self </strong>\r\n<ul>\r\n 	<li>Hoe jouw levensdoel vinden</li>\r\n 	<li>Hoe Law of attraction implementeren</li>\r\n 	<li>Jouw onderbewuste reprogrammeren</li>\r\n 	<li>Controle krijgen over je leven</li>\r\n 	<li>Gevaar en invloed van omgeving</li>\r\n 	<li>Kracht van affirmaties</li>\r\n 	<li>Geloof in jouw eigen vinden</li>\r\n 	<li>Hoe je negatieve energie omdraait in positieve energie</li>\r\n 	<li>Hoe angst overkomen</li>\r\n</ul>\r\n</div>\r\n</div>'),
(1446, 123, '_program', 'field_5cadfcd356735'),
(1453, 124, '_EventShowMapLink', ''),
(1454, 124, '_EventShowMap', ''),
(1455, 124, '_EventAllDay', 'yes'),
(1456, 124, '_EventStartDate', '2019-04-10 00:00:00'),
(1457, 124, '_EventEndDate', '2019-04-10 23:59:59'),
(1458, 124, '_EventStartDateUTC', '2019-04-10 00:00:00'),
(1459, 124, '_EventEndDateUTC', '2019-04-10 23:59:59'),
(1460, 124, '_EventURL', ''),
(1461, 124, '_EventTimezone', 'UTC+0'),
(1462, 124, '_EventTimezoneAbbr', 'UTC+0'),
(1463, 124, 'program', '<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Secrets of dating</strong>\r\n<ul>\r\n 	<li>Hoe aantrekking in realiteit werkt</li>\r\n 	<li>Waarom trek je altijd de verkeerde aan</li>\r\n 	<li>Waarom mislukt het altijd bij de persoon die je echt wilt</li>\r\n 	<li>Do’s en dont’s bij online dating</li>\r\n 	<li>Invloed van social media op dating</li>\r\n 	<li>Fouten op dates vermijden</li>\r\n 	<li>Hoe een persoon testen op karakter</li>\r\n 	<li>Hoe jij onbewust getest wordt</li>\r\n 	<li>“Push and pull” methode begrijpen</li>\r\n 	<li>Emotioneel in balans blijven bij afwijzing</li>\r\n 	<li>Waarom de friendzone een mythe is</li>\r\n 	<li>Hoe een ex weer aantrekken</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Foundation of a strong relationship</strong>\r\n<ul>\r\n 	<li>Hoe een sterke fundering creëren</li>\r\n 	<li>Hoe argumenteren met jouw partner</li>\r\n 	<li>Hoe jouw partner supporteren in hun doelen</li>\r\n 	<li>Vertrouwen en jaloezie</li>\r\n 	<li>De liefdestaal van uw partner ontdekken</li>\r\n 	<li>Waarom liefde vrij is</li>\r\n 	<li>Hoe jullie liefde weer aanwakkeren</li>\r\n 	<li>Hoe jouw partner “trainen”</li>\r\n 	<li>Hoe om te gaan met het bedrog van uw partner</li>\r\n 	<li>Hoe met een scheiding om te gaan</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n\r\n<strong>Engineer your future self </strong>\r\n<ul>\r\n 	<li>Hoe jouw levensdoel vinden</li>\r\n 	<li>Hoe Law of attraction implementeren</li>\r\n 	<li>Jouw onderbewuste reprogrammeren</li>\r\n 	<li>Controle krijgen over je leven</li>\r\n 	<li>Gevaar en invloed van omgeving</li>\r\n 	<li>Kracht van affirmaties</li>\r\n 	<li>Geloof in jouw eigen vinden</li>\r\n 	<li>Hoe je negatieve energie omdraait in positieve energie</li>\r\n 	<li>Hoe angst overkomen</li>\r\n</ul>\r\n</div>\r\n</div>'),
(1464, 124, '_program', 'field_5cadfcd356735'),
(1465, 27, '_EventCost', '19.99'),
(1466, 27, '_EventCost', '19.99'),
(1470, 125, '_EventShowMapLink', '1'),
(1471, 125, '_EventShowMap', '1'),
(1472, 125, '_EventStartDate', '2019-03-28 08:00:00'),
(1473, 125, '_EventEndDate', '2019-03-30 17:00:00'),
(1474, 125, '_EventStartDateUTC', '2019-03-28 08:00:00'),
(1475, 125, '_EventEndDateUTC', '2019-03-30 17:00:00'),
(1476, 125, '_EventDuration', '205200'),
(1477, 125, '_EventURL', ''),
(1478, 125, '_EventTimezone', 'UTC+0'),
(1479, 125, '_EventTimezoneAbbr', 'UTC+0'),
(1480, 52, 'program', '<div class=\"row\">\r\n<div class=\"col-md-6\">\r\n\r\n<strong>Secrets of dating</strong>\r\n<ul>\r\n 	<li>Hoe aantrekking in realiteit werkt</li>\r\n 	<li>Waarom trek je altijd de verkeerde aan</li>\r\n 	<li>Waarom mislukt het altijd bij de persoon die je echt wilt</li>\r\n 	<li>Do’s en dont’s bij online dating</li>\r\n 	<li>Invloed van social media op dating</li>\r\n 	<li>Fouten op dates vermijden</li>\r\n 	<li>Hoe een persoon testen op karakter</li>\r\n 	<li>Hoe jij onbewust getest wordt</li>\r\n 	<li>“Push and pull” methode begrijpen</li>\r\n 	<li>Emotioneel in balans blijven bij afwijzing</li>\r\n 	<li>Waarom de friendzone een mythe is</li>\r\n 	<li>Hoe een ex weer aantrekken</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-6\">\r\n\r\n<strong>Foundation of a strong relationship</strong>\r\n<ul>\r\n 	<li>Hoe een sterke fundering creëren</li>\r\n 	<li>Hoe argumenteren met jouw partner</li>\r\n 	<li>Hoe jouw partner supporteren in hun doelen</li>\r\n 	<li>Vertrouwen en jaloezie</li>\r\n 	<li>De liefdestaal van uw partner ontdekken</li>\r\n 	<li>Waarom liefde vrij is</li>\r\n 	<li>Hoe jullie liefde weer aanwakkeren</li>\r\n 	<li>Hoe jouw partner “trainen”</li>\r\n 	<li>Hoe om te gaan met het bedrog van uw partner</li>\r\n 	<li>Hoe met een scheiding om te gaan</li>\r\n</ul>\r\n</div>\r\n\r\n</div>'),
(1481, 52, '_program', 'field_5cadfcd356735'),
(1482, 125, 'program', ''),
(1483, 125, '_program', 'field_5cadfcd356735'),
(1486, 126, '_EventShowMapLink', '1'),
(1487, 126, '_EventShowMap', '1'),
(1488, 126, '_EventAllDay', 'yes'),
(1489, 126, '_EventStartDate', '2019-03-20 00:00:00'),
(1490, 126, '_EventEndDate', '2019-03-20 23:59:59'),
(1491, 126, '_EventStartDateUTC', '2019-03-20 00:00:00'),
(1492, 126, '_EventEndDateUTC', '2019-03-20 23:59:59'),
(1493, 126, '_EventURL', ''),
(1494, 126, '_EventTimezone', 'UTC+0'),
(1495, 126, '_EventTimezoneAbbr', 'UTC+0'),
(1496, 34, 'program', ''),
(1497, 34, '_program', 'field_5cadfcd356735'),
(1498, 126, 'program', ''),
(1499, 126, '_program', 'field_5cadfcd356735'),
(1500, 127, '_EventShowMapLink', '1'),
(1501, 127, '_EventShowMap', '1'),
(1502, 127, '_EventAllDay', 'yes'),
(1503, 127, '_EventStartDate', '2019-03-20 00:00:00'),
(1504, 127, '_EventEndDate', '2019-03-20 23:59:59'),
(1505, 127, '_EventStartDateUTC', '2019-03-20 00:00:00'),
(1506, 127, '_EventEndDateUTC', '2019-03-20 23:59:59'),
(1507, 127, '_EventURL', ''),
(1508, 127, '_EventTimezone', 'UTC+0'),
(1509, 127, '_EventTimezoneAbbr', 'UTC+0'),
(1510, 127, 'program', ''),
(1511, 127, '_program', 'field_5cadfcd356735'),
(1512, 128, '_wc_review_count', '0'),
(1513, 128, '_wc_rating_count', 'a:0:{}'),
(1514, 128, '_wc_average_rating', '0'),
(1515, 128, '_sku', '128-1-ALIQUAM-SED-POSUERE-LIBERO-NORMAL-TICKET'),
(1516, 128, '_regular_price', '45'),
(1517, 128, '_sale_price', ''),
(1518, 128, '_sale_price_dates_from', ''),
(1519, 128, '_sale_price_dates_to', ''),
(1520, 128, 'total_sales', '0'),
(1521, 128, '_tax_status', 'taxable'),
(1522, 128, '_tax_class', ''),
(1523, 128, '_manage_stock', 'yes'),
(1524, 128, '_backorders', 'no'),
(1525, 128, '_low_stock_amount', ''),
(1526, 128, '_sold_individually', 'no'),
(1527, 128, '_weight', ''),
(1528, 128, '_length', ''),
(1529, 128, '_width', ''),
(1530, 128, '_height', ''),
(1531, 128, '_upsell_ids', 'a:0:{}'),
(1532, 128, '_crosssell_ids', 'a:0:{}'),
(1533, 128, '_purchase_note', ''),
(1534, 128, '_default_attributes', 'a:0:{}'),
(1535, 128, '_virtual', 'yes'),
(1536, 128, '_downloadable', 'no'),
(1537, 128, '_product_image_gallery', ''),
(1538, 128, '_download_limit', '-1'),
(1539, 128, '_download_expiry', '-1'),
(1540, 128, '_stock', '25'),
(1541, 128, '_stock_status', 'instock'),
(1542, 128, '_product_version', '3.5.7'),
(1543, 128, '_price', '45'),
(1544, 128, '_tribe_wooticket_for_event', '34'),
(1545, 128, '_tribe_ticket_show_description', 'yes'),
(1546, 34, '_tribe_ticket_use_global_stock', '1'),
(1547, 34, '_tribe_ticket_global_stock_level', '25'),
(1548, 128, '_global_stock_mode', 'global'),
(1549, 128, '_tribe_ticket_version', '4.10.2'),
(1550, 128, '_tribe_tickets_meta', 'a:0:{}'),
(1551, 128, '_ticket_start_date', '2019-04-01 00:00:00'),
(1552, 128, '_ticket_end_date', '2019-04-25 00:00:00'),
(1554, 128, '_tribe_ticket_capacity', '25'),
(1555, 128, '_tribe_ticket_manual_updated', '_ticket_start_date'),
(1556, 128, '_tribe_ticket_manual_updated', '_ticket_end_date'),
(1557, 34, '_EventCost', '45'),
(1560, 129, '_EventShowMapLink', '1'),
(1561, 129, '_EventShowMap', '1'),
(1562, 129, '_EventStartDate', '2019-04-25 08:00:00'),
(1563, 129, '_EventEndDate', '2019-04-28 17:00:00'),
(1564, 129, '_EventStartDateUTC', '2019-04-25 08:00:00'),
(1565, 129, '_EventEndDateUTC', '2019-04-28 17:00:00'),
(1566, 129, '_EventDuration', '291600'),
(1567, 129, '_EventURL', ''),
(1568, 129, '_EventTimezone', 'UTC+0'),
(1569, 129, '_EventTimezoneAbbr', 'UTC+0'),
(1570, 129, 'program', ''),
(1571, 129, '_program', 'field_5cadfcd356735'),
(1572, 130, '_wc_review_count', '0'),
(1573, 130, '_wc_rating_count', 'a:0:{}'),
(1574, 130, '_wc_average_rating', '0'),
(1575, 130, '_sku', '130-1-LOREM-IPSUM-DOLOR-SIT-AMET-NORMAL'),
(1576, 130, '_regular_price', '69.99'),
(1577, 130, '_sale_price', ''),
(1578, 130, '_sale_price_dates_from', ''),
(1579, 130, '_sale_price_dates_to', ''),
(1580, 130, 'total_sales', '0'),
(1581, 130, '_tax_status', 'taxable'),
(1582, 130, '_tax_class', ''),
(1583, 130, '_manage_stock', 'yes'),
(1584, 130, '_backorders', 'no'),
(1585, 130, '_low_stock_amount', ''),
(1586, 130, '_sold_individually', 'no'),
(1587, 130, '_weight', ''),
(1588, 130, '_length', ''),
(1589, 130, '_width', ''),
(1590, 130, '_height', ''),
(1591, 130, '_upsell_ids', 'a:0:{}'),
(1592, 130, '_crosssell_ids', 'a:0:{}'),
(1593, 130, '_purchase_note', ''),
(1594, 130, '_default_attributes', 'a:0:{}'),
(1595, 130, '_virtual', 'yes'),
(1596, 130, '_downloadable', 'no'),
(1597, 130, '_product_image_gallery', ''),
(1598, 130, '_download_limit', '-1'),
(1599, 130, '_download_expiry', '-1'),
(1600, 130, '_stock', '100'),
(1601, 130, '_stock_status', 'instock'),
(1602, 130, '_product_version', '3.5.7'),
(1603, 130, '_price', '69.99'),
(1604, 130, '_tribe_wooticket_for_event', '52'),
(1605, 130, '_tribe_ticket_show_description', 'yes'),
(1606, 52, '_tribe_ticket_use_global_stock', '1'),
(1607, 52, '_tribe_ticket_global_stock_level', '100'),
(1608, 130, '_global_stock_mode', 'global'),
(1609, 130, '_tribe_ticket_version', '4.10.2'),
(1610, 130, '_tribe_tickets_meta', 'a:0:{}'),
(1611, 130, '_ticket_start_date', '2019-03-28 00:00:00'),
(1612, 130, '_ticket_end_date', '2019-04-28 17:00:00'),
(1613, 52, '_EventCost', '69.99'),
(1614, 131, '_EventShowMapLink', '1'),
(1615, 131, '_EventShowMap', '1'),
(1616, 131, '_EventStartDate', '2019-04-25 08:00:00'),
(1617, 131, '_EventEndDate', '2019-04-28 17:00:00'),
(1618, 131, '_EventStartDateUTC', '2019-04-25 08:00:00'),
(1619, 131, '_EventEndDateUTC', '2019-04-28 17:00:00'),
(1620, 131, '_EventDuration', '291600'),
(1621, 131, '_EventURL', ''),
(1622, 131, '_EventTimezone', 'UTC+0'),
(1623, 131, '_EventTimezoneAbbr', 'UTC+0'),
(1624, 131, 'program', ''),
(1625, 131, '_program', 'field_5cadfcd356735'),
(1626, 52, '_tribe_tickets_flush_blocks', '1'),
(1627, 132, '_EventShowMapLink', '1'),
(1628, 132, '_EventShowMap', '1'),
(1629, 132, '_EventStartDate', '2019-04-25 08:00:00'),
(1630, 132, '_EventEndDate', '2019-04-28 17:00:00'),
(1631, 132, '_EventStartDateUTC', '2019-04-25 08:00:00'),
(1632, 132, '_EventEndDateUTC', '2019-04-28 17:00:00'),
(1633, 132, '_EventDuration', '291600'),
(1634, 132, '_EventURL', ''),
(1635, 132, '_EventTimezone', 'UTC+0'),
(1636, 132, '_EventTimezoneAbbr', 'UTC+0'),
(1637, 132, 'program', '<div class=\"row\">\r\n<div class=\"col-md-6\">\r\n\r\n<strong>Secrets of dating</strong>\r\n<ul>\r\n 	<li>Hoe aantrekking in realiteit werkt</li>\r\n 	<li>Waarom trek je altijd de verkeerde aan</li>\r\n 	<li>Waarom mislukt het altijd bij de persoon die je echt wilt</li>\r\n 	<li>Do’s en dont’s bij online dating</li>\r\n 	<li>Invloed van social media op dating</li>\r\n 	<li>Fouten op dates vermijden</li>\r\n 	<li>Hoe een persoon testen op karakter</li>\r\n 	<li>Hoe jij onbewust getest wordt</li>\r\n 	<li>“Push and pull” methode begrijpen</li>\r\n 	<li>Emotioneel in balans blijven bij afwijzing</li>\r\n 	<li>Waarom de friendzone een mythe is</li>\r\n 	<li>Hoe een ex weer aantrekken</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-6\">\r\n\r\n<strong>Foundation of a strong relationship</strong>\r\n<ul>\r\n 	<li>Hoe een sterke fundering creëren</li>\r\n 	<li>Hoe argumenteren met jouw partner</li>\r\n 	<li>Hoe jouw partner supporteren in hun doelen</li>\r\n 	<li>Vertrouwen en jaloezie</li>\r\n 	<li>De liefdestaal van uw partner ontdekken</li>\r\n 	<li>Waarom liefde vrij is</li>\r\n 	<li>Hoe jullie liefde weer aanwakkeren</li>\r\n 	<li>Hoe jouw partner “trainen”</li>\r\n 	<li>Hoe om te gaan met het bedrog van uw partner</li>\r\n 	<li>Hoe met een scheiding om te gaan</li>\r\n</ul>\r\n</div>\r\n\r\n</div>'),
(1638, 132, '_program', 'field_5cadfcd356735'),
(1691, 134, '_tribe_wooticket_product', '29'),
(1692, 134, '_tribe_wooticket_order', '133'),
(1693, 134, '_tribe_wooticket_order_item', '15'),
(1694, 134, '_tribe_wooticket_event', '27'),
(1695, 134, '_tribe_wooticket_attendee_optout', ''),
(1696, 134, '_tribe_wooticket_security_code', 'f41c96fc99'),
(1697, 134, '_paid_price', '19.99'),
(1698, 134, '_price_currency_symbol', '$'),
(1699, 134, '_unique_id', 'A-9-NDB6Y2'),
(1700, 134, '_tribe_tickets_attendee_user_id', '1'),
(1703, 135, '_wp_attached_file', '2019/03/placeholder.jpg'),
(1704, 135, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1000;s:6:\"height\";i:1000;s:4:\"file\";s:23:\"2019/03/placeholder.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"placeholder-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"placeholder-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"placeholder-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:23:\"placeholder-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:23:\"placeholder-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"placeholder-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:23:\"placeholder-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:23:\"placeholder-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"placeholder-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1705, 34, '_thumbnail_id', '135'),
(1708, 27, '_thumbnail_id', '135'),
(1709, 52, '_thumbnail_id', '135'),
(1712, 136, '_wp_attached_file', '2019/03/book.jpg'),
(1713, 136, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:734;s:6:\"height\";i:800;s:4:\"file\";s:16:\"2019/03/book.jpg\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"book-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"book-275x300.jpg\";s:5:\"width\";i:275;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:16:\"book-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:16:\"book-416x453.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:453;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"book-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:16:\"book-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:16:\"book-416x453.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:453;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"book-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1816, 139, '_menu_item_type', 'post_type'),
(1817, 139, '_menu_item_menu_item_parent', '0'),
(1818, 139, '_menu_item_object_id', '6'),
(1819, 139, '_menu_item_object', 'page'),
(1820, 139, '_menu_item_target', ''),
(1821, 139, '_menu_item_classes', 'a:2:{i:0;s:9:\"d-sm-none\";i:1;s:7:\"d-block\";}'),
(1822, 139, '_menu_item_xfn', ''),
(1823, 139, '_menu_item_url', ''),
(1825, 140, '_order_key', 'wc_order_DZBwphOUaoDAG'),
(1826, 140, '_customer_user', '1'),
(1827, 140, '_payment_method', 'ppec_paypal'),
(1828, 140, '_payment_method_title', 'PayPal'),
(1829, 140, '_transaction_id', ''),
(1830, 140, '_customer_ip_address', '172.26.0.1'),
(1831, 140, '_customer_user_agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36'),
(1832, 140, '_created_via', 'checkout'),
(1833, 140, '_date_completed', ''),
(1834, 140, '_completed_date', ''),
(1835, 140, '_date_paid', ''),
(1836, 140, '_paid_date', ''),
(1837, 140, '_cart_hash', '6ace98fadd831d2e24962a76c73435bb'),
(1838, 140, '_billing_first_name', 'Tobiasz'),
(1839, 140, '_billing_last_name', 'Kochański'),
(1840, 140, '_billing_company', ''),
(1841, 140, '_billing_address_1', ''),
(1842, 140, '_billing_address_2', ''),
(1843, 140, '_billing_city', ''),
(1844, 140, '_billing_state', ''),
(1845, 140, '_billing_postcode', ''),
(1846, 140, '_billing_country', 'NL'),
(1847, 140, '_billing_email', 'tobiaszkochanski@gmail.com'),
(1848, 140, '_billing_phone', ''),
(1849, 140, '_shipping_first_name', ''),
(1850, 140, '_shipping_last_name', ''),
(1851, 140, '_shipping_company', ''),
(1852, 140, '_shipping_address_1', ''),
(1853, 140, '_shipping_address_2', ''),
(1854, 140, '_shipping_city', ''),
(1855, 140, '_shipping_state', ''),
(1856, 140, '_shipping_postcode', ''),
(1857, 140, '_shipping_country', ''),
(1858, 140, '_order_currency', 'EUR'),
(1859, 140, '_cart_discount', '0'),
(1860, 140, '_cart_discount_tax', '0'),
(1861, 140, '_order_shipping', '0.00'),
(1862, 140, '_order_shipping_tax', '0'),
(1863, 140, '_order_tax', '0'),
(1864, 140, '_order_total', '100.00'),
(1865, 140, '_order_version', '3.5.7'),
(1866, 140, '_prices_include_tax', 'no'),
(1867, 140, '_billing_address_index', 'Tobiasz Kochański       NL tobiaszkochanski@gmail.com '),
(1868, 140, '_shipping_address_index', '        '),
(1870, 74, '_tribe_deleted_product_name', 'Lorem Ipsum Ticket Senior'),
(1871, 79, '_tribe_deleted_product_name', 'Lorem Ipsum Ticket Senior'),
(1872, 82, '_tribe_deleted_product_name', 'Lorem Ipsum Ticket Senior'),
(1873, 87, '_tribe_deleted_product_name', 'Lorem Ipsum Ticket Senior'),
(1874, 27, '_transient_timeout_tribe_attendees', '1558333493'),
(1875, 27, '_transient_tribe_attendees', 'a:9:{i:0;a:28:{s:8:\"order_id\";s:2:\"73\";s:16:\"order_id_display\";s:2:\"73\";s:13:\"order_id_link\";s:35:\"<a class=\"row-title\" href=\"\">73</a>\";s:17:\"order_id_link_src\";N;s:12:\"order_status\";b:0;s:18:\"order_status_label\";s:7:\"Deleted\";s:13:\"order_warning\";b:1;s:14:\"purchaser_name\";s:1:\" \";s:15:\"purchaser_email\";s:0:\"\";s:8:\"provider\";s:48:\"Tribe__Tickets_Plus__Commerce__WooCommerce__Main\";s:13:\"provider_slug\";s:3:\"woo\";s:13:\"purchase_time\";b:0;s:6:\"ticket\";s:35:\"Lorem Ipsum Ticket Senior (deleted)\";s:11:\"attendee_id\";i:74;s:13:\"order_item_id\";s:1:\"3\";s:8:\"security\";s:10:\"eb2300ed44\";s:10:\"product_id\";s:2:\"70\";s:8:\"check_in\";s:0:\"\";s:6:\"optout\";b:0;s:7:\"user_id\";s:1:\"1\";s:8:\"event_id\";s:2:\"27\";s:11:\"ticket_name\";b:0;s:11:\"holder_name\";s:1:\" \";s:9:\"ticket_id\";s:10:\"A-1-Z4A508\";s:12:\"qr_ticket_id\";i:74;s:13:\"security_code\";s:10:\"eb2300ed44\";s:13:\"attendee_meta\";s:0:\"\";s:13:\"ticket_exists\";b:0;}i:1;a:28:{s:8:\"order_id\";s:2:\"77\";s:16:\"order_id_display\";s:2:\"77\";s:13:\"order_id_link\";s:35:\"<a class=\"row-title\" href=\"\">77</a>\";s:17:\"order_id_link_src\";N;s:12:\"order_status\";b:0;s:18:\"order_status_label\";s:7:\"Deleted\";s:13:\"order_warning\";b:1;s:14:\"purchaser_name\";s:1:\" \";s:15:\"purchaser_email\";s:0:\"\";s:8:\"provider\";s:48:\"Tribe__Tickets_Plus__Commerce__WooCommerce__Main\";s:13:\"provider_slug\";s:3:\"woo\";s:13:\"purchase_time\";b:0;s:6:\"ticket\";s:25:\"Lorem Ipsum Ticket Normal\";s:11:\"attendee_id\";i:78;s:13:\"order_item_id\";s:1:\"4\";s:8:\"security\";s:10:\"029b5b8a1a\";s:10:\"product_id\";s:2:\"29\";s:8:\"check_in\";s:0:\"\";s:6:\"optout\";b:0;s:7:\"user_id\";s:1:\"1\";s:8:\"event_id\";s:2:\"27\";s:11:\"ticket_name\";s:25:\"Lorem Ipsum Ticket Normal\";s:11:\"holder_name\";s:1:\" \";s:9:\"ticket_id\";s:10:\"A-2-51T0YL\";s:12:\"qr_ticket_id\";i:78;s:13:\"security_code\";s:10:\"029b5b8a1a\";s:13:\"attendee_meta\";s:0:\"\";s:13:\"ticket_exists\";b:1;}i:2;a:28:{s:8:\"order_id\";s:2:\"77\";s:16:\"order_id_display\";s:2:\"77\";s:13:\"order_id_link\";s:35:\"<a class=\"row-title\" href=\"\">77</a>\";s:17:\"order_id_link_src\";N;s:12:\"order_status\";b:0;s:18:\"order_status_label\";s:7:\"Deleted\";s:13:\"order_warning\";b:1;s:14:\"purchaser_name\";s:1:\" \";s:15:\"purchaser_email\";s:0:\"\";s:8:\"provider\";s:48:\"Tribe__Tickets_Plus__Commerce__WooCommerce__Main\";s:13:\"provider_slug\";s:3:\"woo\";s:13:\"purchase_time\";b:0;s:6:\"ticket\";s:35:\"Lorem Ipsum Ticket Senior (deleted)\";s:11:\"attendee_id\";i:79;s:13:\"order_item_id\";s:1:\"5\";s:8:\"security\";s:10:\"70bdf65248\";s:10:\"product_id\";s:2:\"70\";s:8:\"check_in\";s:0:\"\";s:6:\"optout\";b:0;s:7:\"user_id\";s:1:\"1\";s:8:\"event_id\";s:2:\"27\";s:11:\"ticket_name\";b:0;s:11:\"holder_name\";s:1:\" \";s:9:\"ticket_id\";s:10:\"A-3-TUM5ZC\";s:12:\"qr_ticket_id\";i:79;s:13:\"security_code\";s:10:\"70bdf65248\";s:13:\"attendee_meta\";s:0:\"\";s:13:\"ticket_exists\";b:0;}i:3;a:28:{s:8:\"order_id\";s:2:\"80\";s:16:\"order_id_display\";s:2:\"80\";s:13:\"order_id_link\";s:35:\"<a class=\"row-title\" href=\"\">80</a>\";s:17:\"order_id_link_src\";N;s:12:\"order_status\";b:0;s:18:\"order_status_label\";s:7:\"Deleted\";s:13:\"order_warning\";b:1;s:14:\"purchaser_name\";s:1:\" \";s:15:\"purchaser_email\";s:0:\"\";s:8:\"provider\";s:48:\"Tribe__Tickets_Plus__Commerce__WooCommerce__Main\";s:13:\"provider_slug\";s:3:\"woo\";s:13:\"purchase_time\";b:0;s:6:\"ticket\";s:25:\"Lorem Ipsum Ticket Normal\";s:11:\"attendee_id\";i:81;s:13:\"order_item_id\";s:1:\"8\";s:8:\"security\";s:10:\"77e1fec230\";s:10:\"product_id\";s:2:\"29\";s:8:\"check_in\";s:0:\"\";s:6:\"optout\";b:0;s:7:\"user_id\";s:1:\"0\";s:8:\"event_id\";s:2:\"27\";s:11:\"ticket_name\";s:25:\"Lorem Ipsum Ticket Normal\";s:11:\"holder_name\";s:1:\" \";s:9:\"ticket_id\";s:10:\"A-4-YNRHZS\";s:12:\"qr_ticket_id\";i:81;s:13:\"security_code\";s:10:\"77e1fec230\";s:13:\"attendee_meta\";s:0:\"\";s:13:\"ticket_exists\";b:1;}i:4;a:28:{s:8:\"order_id\";s:2:\"80\";s:16:\"order_id_display\";s:2:\"80\";s:13:\"order_id_link\";s:35:\"<a class=\"row-title\" href=\"\">80</a>\";s:17:\"order_id_link_src\";N;s:12:\"order_status\";b:0;s:18:\"order_status_label\";s:7:\"Deleted\";s:13:\"order_warning\";b:1;s:14:\"purchaser_name\";s:1:\" \";s:15:\"purchaser_email\";s:0:\"\";s:8:\"provider\";s:48:\"Tribe__Tickets_Plus__Commerce__WooCommerce__Main\";s:13:\"provider_slug\";s:3:\"woo\";s:13:\"purchase_time\";b:0;s:6:\"ticket\";s:35:\"Lorem Ipsum Ticket Senior (deleted)\";s:11:\"attendee_id\";i:82;s:13:\"order_item_id\";s:1:\"9\";s:8:\"security\";s:10:\"950582c6a1\";s:10:\"product_id\";s:2:\"70\";s:8:\"check_in\";s:0:\"\";s:6:\"optout\";b:0;s:7:\"user_id\";s:1:\"0\";s:8:\"event_id\";s:2:\"27\";s:11:\"ticket_name\";b:0;s:11:\"holder_name\";s:1:\" \";s:9:\"ticket_id\";s:10:\"A-5-K92U4H\";s:12:\"qr_ticket_id\";i:82;s:13:\"security_code\";s:10:\"950582c6a1\";s:13:\"attendee_meta\";s:0:\"\";s:13:\"ticket_exists\";b:0;}i:5;a:28:{s:8:\"order_id\";s:2:\"84\";s:16:\"order_id_display\";s:2:\"84\";s:13:\"order_id_link\";s:35:\"<a class=\"row-title\" href=\"\">84</a>\";s:17:\"order_id_link_src\";N;s:12:\"order_status\";b:0;s:18:\"order_status_label\";s:7:\"Deleted\";s:13:\"order_warning\";b:1;s:14:\"purchaser_name\";s:1:\" \";s:15:\"purchaser_email\";s:0:\"\";s:8:\"provider\";s:48:\"Tribe__Tickets_Plus__Commerce__WooCommerce__Main\";s:13:\"provider_slug\";s:3:\"woo\";s:13:\"purchase_time\";b:0;s:6:\"ticket\";s:25:\"Lorem Ipsum Ticket Normal\";s:11:\"attendee_id\";i:85;s:13:\"order_item_id\";s:2:\"10\";s:8:\"security\";s:10:\"732182cab2\";s:10:\"product_id\";s:2:\"29\";s:8:\"check_in\";s:0:\"\";s:6:\"optout\";b:0;s:7:\"user_id\";s:1:\"0\";s:8:\"event_id\";s:2:\"27\";s:11:\"ticket_name\";s:25:\"Lorem Ipsum Ticket Normal\";s:11:\"holder_name\";s:1:\" \";s:9:\"ticket_id\";s:10:\"A-6-I9O4BJ\";s:12:\"qr_ticket_id\";i:85;s:13:\"security_code\";s:10:\"732182cab2\";s:13:\"attendee_meta\";s:0:\"\";s:13:\"ticket_exists\";b:1;}i:6;a:28:{s:8:\"order_id\";s:2:\"84\";s:16:\"order_id_display\";s:2:\"84\";s:13:\"order_id_link\";s:35:\"<a class=\"row-title\" href=\"\">84</a>\";s:17:\"order_id_link_src\";N;s:12:\"order_status\";b:0;s:18:\"order_status_label\";s:7:\"Deleted\";s:13:\"order_warning\";b:1;s:14:\"purchaser_name\";s:1:\" \";s:15:\"purchaser_email\";s:0:\"\";s:8:\"provider\";s:48:\"Tribe__Tickets_Plus__Commerce__WooCommerce__Main\";s:13:\"provider_slug\";s:3:\"woo\";s:13:\"purchase_time\";b:0;s:6:\"ticket\";s:25:\"Lorem Ipsum Ticket Normal\";s:11:\"attendee_id\";i:86;s:13:\"order_item_id\";s:2:\"10\";s:8:\"security\";s:10:\"0648914b95\";s:10:\"product_id\";s:2:\"29\";s:8:\"check_in\";s:0:\"\";s:6:\"optout\";b:0;s:7:\"user_id\";s:1:\"0\";s:8:\"event_id\";s:2:\"27\";s:11:\"ticket_name\";s:25:\"Lorem Ipsum Ticket Normal\";s:11:\"holder_name\";s:1:\" \";s:9:\"ticket_id\";s:10:\"A-7-XG7CQH\";s:12:\"qr_ticket_id\";i:86;s:13:\"security_code\";s:10:\"0648914b95\";s:13:\"attendee_meta\";s:0:\"\";s:13:\"ticket_exists\";b:1;}i:7;a:28:{s:8:\"order_id\";s:2:\"84\";s:16:\"order_id_display\";s:2:\"84\";s:13:\"order_id_link\";s:35:\"<a class=\"row-title\" href=\"\">84</a>\";s:17:\"order_id_link_src\";N;s:12:\"order_status\";b:0;s:18:\"order_status_label\";s:7:\"Deleted\";s:13:\"order_warning\";b:1;s:14:\"purchaser_name\";s:1:\" \";s:15:\"purchaser_email\";s:0:\"\";s:8:\"provider\";s:48:\"Tribe__Tickets_Plus__Commerce__WooCommerce__Main\";s:13:\"provider_slug\";s:3:\"woo\";s:13:\"purchase_time\";b:0;s:6:\"ticket\";s:35:\"Lorem Ipsum Ticket Senior (deleted)\";s:11:\"attendee_id\";i:87;s:13:\"order_item_id\";s:2:\"11\";s:8:\"security\";s:10:\"cd1df10616\";s:10:\"product_id\";s:2:\"70\";s:8:\"check_in\";s:0:\"\";s:6:\"optout\";b:0;s:7:\"user_id\";s:1:\"0\";s:8:\"event_id\";s:2:\"27\";s:11:\"ticket_name\";b:0;s:11:\"holder_name\";s:1:\" \";s:9:\"ticket_id\";s:10:\"A-8-QLC426\";s:12:\"qr_ticket_id\";i:87;s:13:\"security_code\";s:10:\"cd1df10616\";s:13:\"attendee_meta\";s:0:\"\";s:13:\"ticket_exists\";b:0;}i:8;a:28:{s:8:\"order_id\";s:3:\"133\";s:16:\"order_id_display\";s:3:\"133\";s:13:\"order_id_link\";s:36:\"<a class=\"row-title\" href=\"\">133</a>\";s:17:\"order_id_link_src\";N;s:12:\"order_status\";b:0;s:18:\"order_status_label\";s:7:\"Deleted\";s:13:\"order_warning\";b:1;s:14:\"purchaser_name\";s:1:\" \";s:15:\"purchaser_email\";s:0:\"\";s:8:\"provider\";s:48:\"Tribe__Tickets_Plus__Commerce__WooCommerce__Main\";s:13:\"provider_slug\";s:3:\"woo\";s:13:\"purchase_time\";b:0;s:6:\"ticket\";s:25:\"Lorem Ipsum Ticket Normal\";s:11:\"attendee_id\";i:134;s:13:\"order_item_id\";s:2:\"15\";s:8:\"security\";s:10:\"f41c96fc99\";s:10:\"product_id\";s:2:\"29\";s:8:\"check_in\";s:0:\"\";s:6:\"optout\";b:0;s:7:\"user_id\";s:1:\"1\";s:8:\"event_id\";s:2:\"27\";s:11:\"ticket_name\";s:25:\"Lorem Ipsum Ticket Normal\";s:11:\"holder_name\";s:1:\" \";s:9:\"ticket_id\";s:10:\"A-9-NDB6Y2\";s:12:\"qr_ticket_id\";i:134;s:13:\"security_code\";s:10:\"f41c96fc99\";s:13:\"attendee_meta\";s:0:\"\";s:13:\"ticket_exists\";b:1;}}'),
(1881, 143, '_edit_last', '1'),
(1882, 143, '_edit_lock', '1558378834:1'),
(1883, 15, '_edit_last', '1'),
(1884, 15, 'facebook', 'https://facebook.com'),
(1885, 15, '_facebook', 'field_5ce2e990dff16'),
(1886, 15, 'twitter', 'https://facebook.com'),
(1887, 15, '_twitter', 'field_5ce2e9be05643'),
(1888, 15, 'pinterest', 'https://facebook.com'),
(1889, 15, '_pinterest', 'field_5ce2e9cd05644'),
(1890, 15, 'instagram', 'https://facebook.com'),
(1891, 15, '_instagram', 'field_5ce2e9d905645'),
(1892, 148, 'facebook', 'https://facebook.com'),
(1893, 148, '_facebook', 'field_5ce2e990dff16'),
(1894, 148, 'twitter', 'https://facebook.com'),
(1895, 148, '_twitter', 'field_5ce2e9be05643'),
(1896, 148, 'pinterest', 'https://facebook.com'),
(1897, 148, '_pinterest', 'field_5ce2e9cd05644'),
(1898, 148, 'instagram', 'https://facebook.com'),
(1899, 148, '_instagram', 'field_5ce2e9d905645'),
(1900, 116, '_tribe_ticket_capacity', '0'),
(1901, 116, '_tribe_ticket_version', '4.10.2'),
(1904, 102, '_thumbnail_id', '116'),
(1905, 102, '_edit_last', '1'),
(1907, 102, 'facebook', ''),
(1908, 102, '_facebook', 'field_5ce2e990dff16'),
(1909, 102, 'twitter', ''),
(1910, 102, '_twitter', 'field_5ce2e9be05643'),
(1911, 102, 'pinterest', ''),
(1912, 102, '_pinterest', 'field_5ce2e9cd05644'),
(1913, 102, 'instagram', ''),
(1914, 102, '_instagram', 'field_5ce2e9d905645'),
(1915, 104, 'facebook', ''),
(1916, 104, '_facebook', 'field_5ce2e990dff16'),
(1917, 104, 'twitter', ''),
(1918, 104, '_twitter', 'field_5ce2e9be05643'),
(1919, 104, 'pinterest', ''),
(1920, 104, '_pinterest', 'field_5ce2e9cd05644'),
(1921, 104, 'instagram', ''),
(1922, 104, '_instagram', 'field_5ce2e9d905645'),
(1923, 140, '_edit_lock', '1563906220:1'),
(1924, 68, '_wp_old_slug', 'emergency-contact'),
(1925, 102, 'dpsp_networks_shares', 'a:1:{s:9:\"pinterest\";i:0;}'),
(1926, 102, 'dpsp_networks_shares_total', '0'),
(1927, 102, 'dpsp_networks_shares_last_updated', '1559847037'),
(1929, 30, '_edit_last', '1'),
(1931, 1, '_edit_last', '1'),
(1936, 15, 'dpsp_networks_shares', 'a:1:{s:9:\"pinterest\";i:19;}'),
(1937, 15, 'dpsp_networks_shares_total', '19'),
(1938, 15, 'dpsp_networks_shares_last_updated', '1564315477'),
(1939, 62, 'dpsp_networks_shares', 'a:1:{s:9:\"pinterest\";i:0;}'),
(1940, 62, 'dpsp_networks_shares_total', '0'),
(1941, 62, 'dpsp_networks_shares_last_updated', '1559846606'),
(1942, 30, 'dpsp_networks_shares', 'a:1:{s:9:\"pinterest\";i:0;}'),
(1943, 30, 'dpsp_networks_shares_total', '0'),
(1944, 30, 'dpsp_networks_shares_last_updated', '1559846623'),
(1945, 1, 'dpsp_networks_shares', 'a:1:{s:9:\"pinterest\";i:0;}'),
(1946, 1, 'dpsp_networks_shares_total', '0'),
(1947, 1, 'dpsp_networks_shares_last_updated', '1559846630'),
(1948, 27, 'dpsp_networks_shares', 'a:1:{s:9:\"pinterest\";i:0;}'),
(1949, 27, 'dpsp_networks_shares_total', '0'),
(1950, 27, 'dpsp_networks_shares_last_updated', '1559847060'),
(1951, 52, 'dpsp_networks_shares', 'a:1:{s:9:\"pinterest\";i:0;}'),
(1952, 52, 'dpsp_networks_shares_total', '0'),
(1953, 52, 'dpsp_networks_shares_last_updated', '1563986803'),
(1954, 34, 'dpsp_networks_shares', 'a:1:{s:9:\"pinterest\";i:0;}'),
(1955, 34, 'dpsp_networks_shares_total', '0'),
(1956, 34, 'dpsp_networks_shares_last_updated', '1563986818'),
(1957, 6, '_tribe_ticket_capacity', '0'),
(1958, 6, '_tribe_ticket_version', '4.10.6'),
(1959, 6, '_edit_lock', '1563906257:1'),
(1960, 151, '_edit_last', '1'),
(1961, 151, '_edit_lock', '1563986772:1'),
(1962, 167, '_wp_attached_file', '2019/07/Layer-1.png'),
(1963, 167, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:110;s:6:\"height\";i:111;s:4:\"file\";s:19:\"2019/07/Layer-1.png\";s:5:\"sizes\";a:2:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"Layer-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:19:\"Layer-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1964, 15, 'testimonial_01_testimonial_01_image', '167'),
(1965, 15, '_testimonial_01_testimonial_01_image', 'field_5d3752ba9039a'),
(1966, 15, 'testimonial_01_testimonial_01_comment', 'Cras quis libero nec augue rhoncus aliquam. Nunc eros quam, imperdiet a aliquet vitae, gravida non tortor. Fusce ac accumsan nisi. Donec vitae dictum est. Curabitur scelerisque quam non tortor rhoncus tempor.'),
(1967, 15, '_testimonial_01_testimonial_01_comment', 'field_5d3752d39039b'),
(1968, 15, 'testimonial_01_testimonial_01_name', 'Jessica Jones'),
(1969, 15, '_testimonial_01_testimonial_01_name', 'field_5d3752f49039c'),
(1970, 15, 'testimonial_01_testimonial_01_title', 'Happy customer'),
(1971, 15, '_testimonial_01_testimonial_01_title', 'field_5d3752fd9039d'),
(1972, 15, 'testimonial_01', ''),
(1973, 15, '_testimonial_01', 'field_5d37518590399'),
(1974, 15, 'testimonial_02_testimonial_02_image', '167'),
(1975, 15, '_testimonial_02_testimonial_02_image', 'field_5d375310bd396'),
(1976, 15, 'testimonial_02_testimonial_02_comment', 'Cras quis libero nec augue rhoncus aliquam. Nunc eros quam, imperdiet a aliquet vitae, gravida non tortor. Fusce ac accumsan nisi. Donec vitae dictum est. Curabitur scelerisque quam non tortor rhoncus tempor.Cras quis libero nec augue rhoncus aliquam. Nunc eros quam, imperdiet a aliquet vitae, gravida non tortor. Fusce ac accumsan nisi. Donec vitae dictum est. Curabitur scelerisque quam non tortor rhoncus tempor.'),
(1977, 15, '_testimonial_02_testimonial_02_comment', 'field_5d375310bd397'),
(1978, 15, 'testimonial_02_testimonial_02_name', 'Jessica Jones'),
(1979, 15, '_testimonial_02_testimonial_02_name', 'field_5d375310bd398'),
(1980, 15, 'testimonial_02_testimonial_02_title', 'Happy customer'),
(1981, 15, '_testimonial_02_testimonial_02_title', 'field_5d375310bd399'),
(1982, 15, 'testimonial_02', ''),
(1983, 15, '_testimonial_02', 'field_5d375310bd395'),
(1984, 15, 'testimonial_03_testimonial_03_image', '167'),
(1985, 15, '_testimonial_03_testimonial_03_image', 'field_5d37530dbd391');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1986, 15, 'testimonial_03_testimonial_03_comment', 'Cras quis libero nec augue rhoncus aliquam. Nunc eros quam, imperdiet a aliquet vitae, gravida non tortor. Fusce ac accumsan nisi. Donec vitae dictum est. Curabitur scelerisque quam non tortor rhoncus tempor.Cras quis libero nec augue rhoncus aliquam. Nunc eros quam, imperdiet a aliquet vitae, gravida non tortor. Fusce ac accumsan nisi. Donec vitae dictum est. Curabitur scelerisque quam non tortor rhoncus tempor.'),
(1987, 15, '_testimonial_03_testimonial_03_comment', 'field_5d37530dbd392'),
(1988, 15, 'testimonial_03_testimonial_03_name', 'Jessica Jones'),
(1989, 15, '_testimonial_03_testimonial_03_name', 'field_5d37530dbd393'),
(1990, 15, 'testimonial_03_testimonial_03_title', 'Happy customer'),
(1991, 15, '_testimonial_03_testimonial_03_title', 'field_5d37530dbd394'),
(1992, 15, 'testimonial_03', ''),
(1993, 15, '_testimonial_03', 'field_5d37530dbd390'),
(1994, 168, 'facebook', 'https://facebook.com'),
(1995, 168, '_facebook', 'field_5ce2e990dff16'),
(1996, 168, 'twitter', 'https://facebook.com'),
(1997, 168, '_twitter', 'field_5ce2e9be05643'),
(1998, 168, 'pinterest', 'https://facebook.com'),
(1999, 168, '_pinterest', 'field_5ce2e9cd05644'),
(2000, 168, 'instagram', 'https://facebook.com'),
(2001, 168, '_instagram', 'field_5ce2e9d905645'),
(2002, 168, 'testimonial_01_testimonial_01_image', ''),
(2003, 168, '_testimonial_01_testimonial_01_image', 'field_5d3752ba9039a'),
(2004, 168, 'testimonial_01_testimonial_01_comment', ''),
(2005, 168, '_testimonial_01_testimonial_01_comment', 'field_5d3752d39039b'),
(2006, 168, 'testimonial_01_testimonial_01_name', ''),
(2007, 168, '_testimonial_01_testimonial_01_name', 'field_5d3752f49039c'),
(2008, 168, 'testimonial_01_testimonial_01_title', ''),
(2009, 168, '_testimonial_01_testimonial_01_title', 'field_5d3752fd9039d'),
(2010, 168, 'testimonial_01', ''),
(2011, 168, '_testimonial_01', 'field_5d37518590399'),
(2012, 168, 'testimonial_02_testimonial_02_image', '167'),
(2013, 168, '_testimonial_02_testimonial_02_image', 'field_5d375310bd396'),
(2014, 168, 'testimonial_02_testimonial_02_comment', 'Cras quis libero nec augue rhoncus aliquam. Nunc eros quam, imperdiet a aliquet vitae, gravida non tortor. Fusce ac accumsan nisi. Donec vitae dictum est. Curabitur scelerisque quam non tortor rhoncus tempor.'),
(2015, 168, '_testimonial_02_testimonial_02_comment', 'field_5d375310bd397'),
(2016, 168, 'testimonial_02_testimonial_02_name', 'Jessica Jones'),
(2017, 168, '_testimonial_02_testimonial_02_name', 'field_5d375310bd398'),
(2018, 168, 'testimonial_02_testimonial_02_title', 'Happy customer'),
(2019, 168, '_testimonial_02_testimonial_02_title', 'field_5d375310bd399'),
(2020, 168, 'testimonial_02', ''),
(2021, 168, '_testimonial_02', 'field_5d375310bd395'),
(2022, 168, 'testimonial_03_testimonial_03_image', ''),
(2023, 168, '_testimonial_03_testimonial_03_image', 'field_5d37530dbd391'),
(2024, 168, 'testimonial_03_testimonial_03_comment', ''),
(2025, 168, '_testimonial_03_testimonial_03_comment', 'field_5d37530dbd392'),
(2026, 168, 'testimonial_03_testimonial_03_name', ''),
(2027, 168, '_testimonial_03_testimonial_03_name', 'field_5d37530dbd393'),
(2028, 168, 'testimonial_03_testimonial_03_title', ''),
(2029, 168, '_testimonial_03_testimonial_03_title', 'field_5d37530dbd394'),
(2030, 168, 'testimonial_03', ''),
(2031, 168, '_testimonial_03', 'field_5d37530dbd390'),
(2032, 169, 'facebook', 'https://facebook.com'),
(2033, 169, '_facebook', 'field_5ce2e990dff16'),
(2034, 169, 'twitter', 'https://facebook.com'),
(2035, 169, '_twitter', 'field_5ce2e9be05643'),
(2036, 169, 'pinterest', 'https://facebook.com'),
(2037, 169, '_pinterest', 'field_5ce2e9cd05644'),
(2038, 169, 'instagram', 'https://facebook.com'),
(2039, 169, '_instagram', 'field_5ce2e9d905645'),
(2040, 169, 'testimonial_01_testimonial_01_image', '167'),
(2041, 169, '_testimonial_01_testimonial_01_image', 'field_5d3752ba9039a'),
(2042, 169, 'testimonial_01_testimonial_01_comment', 'Cras quis libero nec augue rhoncus aliquam. Nunc eros quam, imperdiet a aliquet vitae, gravida non tortor. Fusce ac accumsan nisi. Donec vitae dictum est. Curabitur scelerisque quam non tortor rhoncus tempor.'),
(2043, 169, '_testimonial_01_testimonial_01_comment', 'field_5d3752d39039b'),
(2044, 169, 'testimonial_01_testimonial_01_name', 'Jessica Jones'),
(2045, 169, '_testimonial_01_testimonial_01_name', 'field_5d3752f49039c'),
(2046, 169, 'testimonial_01_testimonial_01_title', 'Happy customer'),
(2047, 169, '_testimonial_01_testimonial_01_title', 'field_5d3752fd9039d'),
(2048, 169, 'testimonial_01', ''),
(2049, 169, '_testimonial_01', 'field_5d37518590399'),
(2050, 169, 'testimonial_02_testimonial_02_image', '167'),
(2051, 169, '_testimonial_02_testimonial_02_image', 'field_5d375310bd396'),
(2052, 169, 'testimonial_02_testimonial_02_comment', 'Cras quis libero nec augue rhoncus aliquam. Nunc eros quam, imperdiet a aliquet vitae, gravida non tortor. Fusce ac accumsan nisi. Donec vitae dictum est. Curabitur scelerisque quam non tortor rhoncus tempor.'),
(2053, 169, '_testimonial_02_testimonial_02_comment', 'field_5d375310bd397'),
(2054, 169, 'testimonial_02_testimonial_02_name', 'Jessica Jones'),
(2055, 169, '_testimonial_02_testimonial_02_name', 'field_5d375310bd398'),
(2056, 169, 'testimonial_02_testimonial_02_title', 'Happy customer'),
(2057, 169, '_testimonial_02_testimonial_02_title', 'field_5d375310bd399'),
(2058, 169, 'testimonial_02', ''),
(2059, 169, '_testimonial_02', 'field_5d375310bd395'),
(2060, 169, 'testimonial_03_testimonial_03_image', ''),
(2061, 169, '_testimonial_03_testimonial_03_image', 'field_5d37530dbd391'),
(2062, 169, 'testimonial_03_testimonial_03_comment', ''),
(2063, 169, '_testimonial_03_testimonial_03_comment', 'field_5d37530dbd392'),
(2064, 169, 'testimonial_03_testimonial_03_name', ''),
(2065, 169, '_testimonial_03_testimonial_03_name', 'field_5d37530dbd393'),
(2066, 169, 'testimonial_03_testimonial_03_title', ''),
(2067, 169, '_testimonial_03_testimonial_03_title', 'field_5d37530dbd394'),
(2068, 169, 'testimonial_03', ''),
(2069, 169, '_testimonial_03', 'field_5d37530dbd390'),
(2070, 170, 'facebook', 'https://facebook.com'),
(2071, 170, '_facebook', 'field_5ce2e990dff16'),
(2072, 170, 'twitter', 'https://facebook.com'),
(2073, 170, '_twitter', 'field_5ce2e9be05643'),
(2074, 170, 'pinterest', 'https://facebook.com'),
(2075, 170, '_pinterest', 'field_5ce2e9cd05644'),
(2076, 170, 'instagram', 'https://facebook.com'),
(2077, 170, '_instagram', 'field_5ce2e9d905645'),
(2078, 170, 'testimonial_01_testimonial_01_image', '167'),
(2079, 170, '_testimonial_01_testimonial_01_image', 'field_5d3752ba9039a'),
(2080, 170, 'testimonial_01_testimonial_01_comment', 'Cras quis libero nec augue rhoncus aliquam. Nunc eros quam, imperdiet a aliquet vitae, gravida non tortor. Fusce ac accumsan nisi. Donec vitae dictum est. Curabitur scelerisque quam non tortor rhoncus tempor.'),
(2081, 170, '_testimonial_01_testimonial_01_comment', 'field_5d3752d39039b'),
(2082, 170, 'testimonial_01_testimonial_01_name', 'Jessica Jones'),
(2083, 170, '_testimonial_01_testimonial_01_name', 'field_5d3752f49039c'),
(2084, 170, 'testimonial_01_testimonial_01_title', 'Happy customer'),
(2085, 170, '_testimonial_01_testimonial_01_title', 'field_5d3752fd9039d'),
(2086, 170, 'testimonial_01', ''),
(2087, 170, '_testimonial_01', 'field_5d37518590399'),
(2088, 170, 'testimonial_02_testimonial_02_image', '167'),
(2089, 170, '_testimonial_02_testimonial_02_image', 'field_5d375310bd396'),
(2090, 170, 'testimonial_02_testimonial_02_comment', 'Cras quis libero nec augue rhoncus aliquam. Nunc eros quam, imperdiet a aliquet vitae, gravida non tortor. Fusce ac accumsan nisi. Donec vitae dictum est. Curabitur scelerisque quam non tortor rhoncus tempor.Cras quis libero nec augue rhoncus aliquam. Nunc eros quam, imperdiet a aliquet vitae, gravida non tortor. Fusce ac accumsan nisi. Donec vitae dictum est. Curabitur scelerisque quam non tortor rhoncus tempor.'),
(2091, 170, '_testimonial_02_testimonial_02_comment', 'field_5d375310bd397'),
(2092, 170, 'testimonial_02_testimonial_02_name', 'Jessica Jones'),
(2093, 170, '_testimonial_02_testimonial_02_name', 'field_5d375310bd398'),
(2094, 170, 'testimonial_02_testimonial_02_title', 'Happy customer'),
(2095, 170, '_testimonial_02_testimonial_02_title', 'field_5d375310bd399'),
(2096, 170, 'testimonial_02', ''),
(2097, 170, '_testimonial_02', 'field_5d375310bd395'),
(2098, 170, 'testimonial_03_testimonial_03_image', ''),
(2099, 170, '_testimonial_03_testimonial_03_image', 'field_5d37530dbd391'),
(2100, 170, 'testimonial_03_testimonial_03_comment', ''),
(2101, 170, '_testimonial_03_testimonial_03_comment', 'field_5d37530dbd392'),
(2102, 170, 'testimonial_03_testimonial_03_name', ''),
(2103, 170, '_testimonial_03_testimonial_03_name', 'field_5d37530dbd393'),
(2104, 170, 'testimonial_03_testimonial_03_title', ''),
(2105, 170, '_testimonial_03_testimonial_03_title', 'field_5d37530dbd394'),
(2106, 170, 'testimonial_03', ''),
(2107, 170, '_testimonial_03', 'field_5d37530dbd390'),
(2108, 171, 'facebook', 'https://facebook.com'),
(2109, 171, '_facebook', 'field_5ce2e990dff16'),
(2110, 171, 'twitter', 'https://facebook.com'),
(2111, 171, '_twitter', 'field_5ce2e9be05643'),
(2112, 171, 'pinterest', 'https://facebook.com'),
(2113, 171, '_pinterest', 'field_5ce2e9cd05644'),
(2114, 171, 'instagram', 'https://facebook.com'),
(2115, 171, '_instagram', 'field_5ce2e9d905645'),
(2116, 171, 'testimonial_01_testimonial_01_image', '167'),
(2117, 171, '_testimonial_01_testimonial_01_image', 'field_5d3752ba9039a'),
(2118, 171, 'testimonial_01_testimonial_01_comment', 'Cras quis libero nec augue rhoncus aliquam. Nunc eros quam, imperdiet a aliquet vitae, gravida non tortor. Fusce ac accumsan nisi. Donec vitae dictum est. Curabitur scelerisque quam non tortor rhoncus tempor.'),
(2119, 171, '_testimonial_01_testimonial_01_comment', 'field_5d3752d39039b'),
(2120, 171, 'testimonial_01_testimonial_01_name', 'Jessica Jones'),
(2121, 171, '_testimonial_01_testimonial_01_name', 'field_5d3752f49039c'),
(2122, 171, 'testimonial_01_testimonial_01_title', 'Happy customer'),
(2123, 171, '_testimonial_01_testimonial_01_title', 'field_5d3752fd9039d'),
(2124, 171, 'testimonial_01', ''),
(2125, 171, '_testimonial_01', 'field_5d37518590399'),
(2126, 171, 'testimonial_02_testimonial_02_image', '167'),
(2127, 171, '_testimonial_02_testimonial_02_image', 'field_5d375310bd396'),
(2128, 171, 'testimonial_02_testimonial_02_comment', 'Cras quis libero nec augue rhoncus aliquam. Nunc eros quam, imperdiet a aliquet vitae, gravida non tortor. Fusce ac accumsan nisi. Donec vitae dictum est. Curabitur scelerisque quam non tortor rhoncus tempor.Cras quis libero nec augue rhoncus aliquam. Nunc eros quam, imperdiet a aliquet vitae, gravida non tortor. Fusce ac accumsan nisi. Donec vitae dictum est. Curabitur scelerisque quam non tortor rhoncus tempor.'),
(2129, 171, '_testimonial_02_testimonial_02_comment', 'field_5d375310bd397'),
(2130, 171, 'testimonial_02_testimonial_02_name', 'Jessica Jones'),
(2131, 171, '_testimonial_02_testimonial_02_name', 'field_5d375310bd398'),
(2132, 171, 'testimonial_02_testimonial_02_title', 'Happy customer'),
(2133, 171, '_testimonial_02_testimonial_02_title', 'field_5d375310bd399'),
(2134, 171, 'testimonial_02', ''),
(2135, 171, '_testimonial_02', 'field_5d375310bd395'),
(2136, 171, 'testimonial_03_testimonial_03_image', '167'),
(2137, 171, '_testimonial_03_testimonial_03_image', 'field_5d37530dbd391'),
(2138, 171, 'testimonial_03_testimonial_03_comment', 'Cras quis libero nec augue rhoncus aliquam. Nunc eros quam, imperdiet a aliquet vitae, gravida non tortor. Fusce ac accumsan nisi. Donec vitae dictum est. Curabitur scelerisque quam non tortor rhoncus tempor.Cras quis libero nec augue rhoncus aliquam. Nunc eros quam, imperdiet a aliquet vitae, gravida non tortor. Fusce ac accumsan nisi. Donec vitae dictum est. Curabitur scelerisque quam non tortor rhoncus tempor.'),
(2139, 171, '_testimonial_03_testimonial_03_comment', 'field_5d37530dbd392'),
(2140, 171, 'testimonial_03_testimonial_03_name', 'Jessica Jones'),
(2141, 171, '_testimonial_03_testimonial_03_name', 'field_5d37530dbd393'),
(2142, 171, 'testimonial_03_testimonial_03_title', 'Happy customer'),
(2143, 171, '_testimonial_03_testimonial_03_title', 'field_5d37530dbd394'),
(2144, 171, 'testimonial_03', ''),
(2145, 171, '_testimonial_03', 'field_5d37530dbd390'),
(2146, 172, '_form', '<div class=\"free-form\">\n  <div class=\"free-form__input\">\n    <label class=\"labels-tag\">Naam</label>\n    [text* text-181 class:inputs-text placeholder \"naam\"]\n  </div>\n  <div class=\"free-form__input\">\n    <label class=\"labels-tag\">E-mailadres</label>\n    [email* email-548 class:inputs-text placeholder \"e-mailadres\"]\n  </div>\n  <div class=\"free-form__input\">\n    <label class=\"labels-tag\">Telefoonnummer</label>\n    [tel* tel-456 class:inputs-text placeholder \"telefoonnummer\"]\n  </div>\n  <div class=\"free-form__input\">\n    <label class=\"labels-tag\"> Bericht</label>\n    [textarea* textarea-404 class:inputs-text placeholder \"bericht\"]\n  </div>\n  <div class=\"free-form__input\">\n    <label class=\"labels-tag\">Website</label>\n    [text text-182 class:inputs-text placeholder \"website\"]\n  </div>\n  <div class=\"free-form__input\">\n    <label class=\"labels-tag\">Event</label>\n    [text text-183 class:inputs-text placeholder \"event\"]\n  </div>\n  <div class=\"free-form__input\">\n    <label class=\"labels-tag\">Event address</label>\n    [text* text-184 class:inputs-text placeholder \"event address\"]\n  </div>\n  <div class=\"free-form__input\">\n    <label class=\"labels-tag\">Date of event</label>\n    [date date-824 class:inputs-text placeholder \"date of event\"]\n  </div>\n  <div class=\"free-form__input\">\n    <label class=\"labels-tag\">Duration of speach</label>\n    [text* text-185 class:inputs-text placeholder \"duration of speach\"]\n  </div>\n  <div class=\"free-form__input\">\n    <label class=\"labels-tag\">Company name</label>\n    [text* text-186 class:inputs-text placeholder \"company name\"]\n  </div>\n  <div class=\"free-form__input\">\n    <label class=\"labels-tag\">VAT</label>\n    [text* text-187 class:inputs-text placeholder \"vat\"]\n  </div>\n[submit class:btn class:btn-price class:btn-primary class:mr-2 class:mb-2 class:mt-4 class:free-form__button \"Send\"]\n</div>'),
(2147, 172, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:25:\"MindEngineer \"[text-181]\"\";s:6:\"sender\";s:43:\"MindEngineer <dzianis.makeichyk@muulabs.pl>\";s:9:\"recipient\";s:28:\"dzianis.makeichyk@muulabs.pl\";s:4:\"body\";s:328:\"From: [text-181] <[email-548]>\nTelefoonnummer: [tel-456]\nBericht: [textarea-404]\nWebsite: [text-182]\nEvent: [text-183]\nEvent address: [text-184]\nDate of event: [date-824]\nDuration of speach: [text-185]\nCompany name: [text-186]\nVAT: [text-187]\n\n-- \nThis e-mail was sent from a contact form on MindEngineer (https://wp2.dev-c01.muulabs.pl)\";s:18:\"additional_headers\";s:0:\"\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(2148, 172, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:29:\"MindEngineer \"[your-subject]\"\";s:6:\"sender\";s:41:\"MindEngineer <tobiaszkochanski@gmail.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:114:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on MindEngineer (https://wp2.dev-c01.muulabs.pl)\";s:18:\"additional_headers\";s:36:\"Reply-To: tobiaszkochanski@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(2149, 172, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(2150, 172, '_additional_settings', ''),
(2151, 172, '_locale', 'en_US');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-03-18 22:30:56', '2019-03-18 22:30:56', '<!-- wp:paragraph -->\n<p>Praesent id condimentum ex. Maecenas tempus in lectus nec lacinia. Ut porta ut purus et consequat. Phasellus ornare gravida massa, sit amet suscipit mi maximus vitae. Curabitur ipsum lacus, viverra id fringilla vitae, facilisis vel nunc. Integer dignissim tincidunt malesuada. Mauris eu orci id purus aliquet ullamcorper ac quis neque. Pellentesque eu nulla nulla. Integer aliquet, erat sed luctus malesuada, mauris sapien tincidunt nisi, sed condimentum magna lectus a ante. In libero metus, luctus eu fringilla fermentum, ultrices ut ante. Maecenas quis aliquet velit, nec faucibus tellus. Donec feugiat massa diam, et maximus mi dapibus non. Mauris auctor mattis quam eu vestibulum. Pellentesque leo nunc, pharetra et interdum sit amet, finibus a erat. Phasellus dapibus congue nisl eu cursus.</p>\n<!-- /wp:paragraph -->', 'Cras euismod vitae dolor id sagittis', '', 'publish', 'closed', 'closed', '', 'hello-world', '', '', '2019-06-06 18:42:48', '2019-06-06 18:42:48', '', 0, 'https://wp2.dev-c01.muulabs.pl/?p=1', 0, 'post', '', 0),
(2, 1, '2019-03-18 22:30:56', '2019-03-18 22:30:56', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"https://wp2.dev-c01.muulabs.pl/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2019-03-18 22:30:56', '2019-03-18 22:30:56', '', 0, 'https://wp2.dev-c01.muulabs.pl/?page_id=2', 0, 'page', '', 0),
(3, 1, '2019-03-18 22:30:56', '2019-03-18 22:30:56', '<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eu accumsan odio, et aliquet odio. Pellentesque ut nibh sit amet neque ullamcorper tincidunt. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus cursus auctor accumsan. Phasellus varius urna et ipsum vulputate, nec finibus libero tincidunt. Aenean eu nulla neque. Aliquam erat volutpat. Suspendisse potenti.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Nulla dapibus ligula non nibh malesuada eleifend. Morbi mollis velit nec erat malesuada, eget vestibulum felis aliquet. Nam id blandit sem. Vivamus eleifend pretium semper. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras faucibus felis odio, vitae commodo nisl porta ac. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut odio tortor, sagittis eu velit in, convallis placerat purus. Fusce ultrices lorem sit amet pharetra facilisis. Maecenas dui libero, aliquet vel varius et, finibus faucibus augue.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Proin nec mauris eu libero semper feugiat. Donec dui elit, lobortis sed augue eu, dictum porta diam. Vivamus ac sem id turpis vehicula congue id consectetur diam. Duis libero quam, rhoncus eu porta pellentesque, feugiat sagittis nisi. Donec porta ullamcorper cursus. Nunc pretium, eros nec scelerisque suscipit, risus sem varius nunc, eget iaculis risus ex ac felis. Quisque mollis iaculis leo ac sodales. Aenean augue ex, blandit ut velit ut, porta aliquam odio. Nulla sit amet purus lacus. Proin semper, ipsum in finibus dapibus, nisi ante ultrices dui, et rutrum est est nec felis. In mi orci, condimentum a odio et, lacinia dictum lectus. Aenean elit dolor, dapibus ac mauris et, dapibus scelerisque odio.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Cras nec tempus ligula. Donec tempor commodo mi, in mattis lectus. Nam aliquam efficitur mollis. Donec mattis purus in odio dapibus, vel porta diam condimentum. Integer aliquam, neque quis iaculis blandit, nisi diam auctor elit, et pretium erat justo non odio. Suspendisse ut nunc quis nulla sollicitudin commodo. Quisque enim ante, sodales vitae felis vel, vulputate ultrices tellus. Sed rhoncus, mi at accumsan volutpat, odio lectus aliquam purus, a varius enim odio ut libero. Integer commodo at mauris ut tempus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque ut dui sed mauris maximus accumsan et in ante. Curabitur enim magna, luctus vel scelerisque in, ultrices in tellus.</p>\n<!-- /wp:paragraph -->', 'Privacy Policy', '', 'publish', 'closed', 'open', '', 'privacy-policy', '', '', '2019-04-10 17:43:58', '2019-04-10 17:43:58', '', 0, 'https://wp2.dev-c01.muulabs.pl/?page_id=3', 0, 'page', '', 0),
(5, 1, '2019-03-18 22:36:58', '2019-03-18 22:36:58', '', 'Shop', '', 'publish', 'closed', 'closed', '', 'shop', '', '', '2019-03-18 22:36:58', '2019-03-18 22:36:58', '', 0, 'https://wp2.dev-c01.muulabs.pl/?page_id=5', 0, 'page', '', 0),
(6, 1, '2019-03-18 22:36:58', '2019-03-18 22:36:58', '[woocommerce_cart]', 'Cart', '', 'publish', 'closed', 'closed', '', 'cart', '', '', '2019-03-18 22:36:58', '2019-03-18 22:36:58', '', 0, 'https://wp2.dev-c01.muulabs.pl/?page_id=6', 0, 'page', '', 0),
(7, 1, '2019-03-18 22:36:58', '2019-03-18 22:36:58', '[woocommerce_checkout]', 'Checkout', '', 'publish', 'closed', 'closed', '', 'checkout', '', '', '2019-03-18 22:36:58', '2019-03-18 22:36:58', '', 0, 'https://wp2.dev-c01.muulabs.pl/?page_id=7', 0, 'page', '', 0),
(8, 1, '2019-03-18 22:36:58', '2019-03-18 22:36:58', '[woocommerce_my_account]', 'My account', '', 'publish', 'closed', 'closed', '', 'my-account', '', '', '2019-03-18 22:36:58', '2019-03-18 22:36:58', '', 0, 'https://wp2.dev-c01.muulabs.pl/?page_id=8', 0, 'page', '', 0),
(14, 1, '2019-03-18 23:07:18', '2019-03-18 23:07:18', '', 'Hotel Arena, Amsterdam, Netherlands', '', 'publish', 'closed', 'closed', '', 'fgjgghfj', '', '', '2019-03-28 03:34:29', '2019-03-28 03:34:29', '', 0, 'https://wp2.dev-c01.muulabs.pl/venue/fgjgghfj/', 0, 'tribe_venue', '', 0),
(15, 1, '2019-03-18 23:20:20', '2019-03-18 23:20:20', 'Vestibulum mauris sapien, maximus non porttitor blandit, feugiat eget nisi. Nunc nisi leo, blandit ac ipsum eget, bibendum pulvinar lacus. Maecenas aliquet vel elit quis malesuada. Sed et erat ligula. Etiam in diam ac sapien mattis scelerisque.\n\nFusce rhoncus, odio pulvinar consequat blandit, magna nisl venenatis massa, id varius erat libero sollicitudin velit.', 'Angelo Kostakis', '', 'publish', 'closed', 'closed', '', 'welcome', '', '', '2019-07-23 19:21:23', '2019-07-23 19:21:23', '', 0, 'https://wp2.dev-c01.muulabs.pl/?page_id=15', 0, 'page', '', 0),
(16, 1, '2019-03-18 23:20:20', '2019-03-18 23:20:20', '', 'Blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2019-03-18 23:20:20', '2019-03-18 23:20:20', '', 0, 'https://wp2.dev-c01.muulabs.pl/?page_id=16', 0, 'page', '', 0),
(18, 1, '2019-03-18 23:20:20', '2019-03-18 23:20:20', 'This is your homepage which is what most visitors will see when they first visit your shop.\n\nYou can change this text by editing the &quot;Welcome&quot; page via the &quot;Pages&quot; menu in your dashboard.', 'Welcome', '', 'inherit', 'closed', 'closed', '', '15-revision-v1', '', '', '2019-03-18 23:20:20', '2019-03-18 23:20:20', '', 15, 'https://wp2.dev-c01.muulabs.pl/15-revision-v1/', 0, 'revision', '', 0),
(19, 1, '2019-03-18 23:20:20', '2019-03-18 23:20:20', '', 'Blog', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2019-03-18 23:20:20', '2019-03-18 23:20:20', '', 16, 'https://wp2.dev-c01.muulabs.pl/16-revision-v1/', 0, 'revision', '', 0),
(27, 1, '2019-03-19 02:07:21', '2019-03-19 02:07:21', '<div class=\"row\">\r\n<div class=\"col-md-6 p-0\"><img class=\"alignnone wp-image-117 size-full\" src=\"https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/as-2.jpg\" alt=\"\" width=\"1000\" height=\"1001\" /></div>\r\n<div class=\"col-md-6 p-0\">\r\n<div class=\"p-5\">\r\n<h5>Waarom kiezen voor dit seminarie?</h5>\r\nAngelo zijn seminaries staan vol met psychologische trucjes, veel metaforen waarbij mensen zich onmiddellijk in herkennen en sluit af met een empowerment gevoel. Een mind engineer pur sang en creëert een reis doorheen het bewustzijn als het ware.\r\n\r\nZijn favoriete onderwerpen zijn:\r\n<ul>\r\n 	<li>Law of attraction</li>\r\n 	<li>Dating is a mirror</li>\r\n 	<li>Engineer your future self</li>\r\n 	<li>Programming your mind</li>\r\n 	<li>Self love</li>\r\n 	<li>Smashing through limiting beliefs</li>\r\n 	<li>Mission in life</li>\r\n</ul>\r\n</div>\r\n</div>\r\n<div class=\"col-md-6 p-0\">\r\n<div class=\"p-5\">\r\n<h5>Wie is de organisator van het seminarie?</h5>\r\nIk ben Angelo Kostakis en merkte reeds van jongs af aan dat ik zeer geïnteresseerd ben in hoe mensen denken en hoe problemen opgelost kunnen worden wanneer je objectief weet te blijven in moeilijke situaties.\r\n\r\nIk heb mijn studie bouwkundig ingenieur afgerond maar merkte gaandeweg mijn job dat het mij meer voldoening gaf om mensen hun problemen op te lossen dan bouwkundige berekeningen te maken.\r\n\r\nIk ben begonnen om over liefde – in de vorm van poëzie – te schrijven op mijn Instagram account. Dit om mensen te raken en weer te doen geloven in het goede van de liefde, maar ook om te laten zien dat het vaak gepaard gaat met pijn.\r\n\r\n</div>\r\n</div>\r\n<div class=\"col-md-6 p-0\"><img class=\"alignnone wp-image-116 size-full\" src=\"https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/ak-1.jpg\" alt=\"\" width=\"1000\" height=\"1001\" /></div>\r\n</div>', 'The secrets of dating event', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.\r\n\r\nHet antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'publish', 'open', 'closed', '', 'asdasdasdsa', '', '', '2019-04-10 22:58:14', '2019-04-10 22:58:14', '', 0, 'https://wp2.dev-c01.muulabs.pl/?post_type=tribe_events&#038;p=27', 0, 'tribe_events', '', 0),
(28, 1, '2019-03-19 02:07:21', '2019-03-19 02:07:21', 'asdasdsd', 'asdasdasdsa', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-03-19 02:07:21', '2019-03-19 02:07:21', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(29, 1, '2019-03-19 02:12:28', '2019-03-19 02:12:28', '', 'Lorem Ipsum Ticket Normal', 'Een mind engineer pur sang en creëert een reis doorheen het bewustzijn als het ware.', 'publish', 'closed', 'closed', '', 'normal', '', '', '2019-04-10 22:58:14', '2019-04-10 22:58:14', '', 0, 'https://wp2.dev-c01.muulabs.pl/product/normal/', 0, 'product', '', 0),
(30, 1, '2019-03-19 02:18:30', '2019-03-19 02:18:30', '<!-- wp:paragraph -->\n<p>Sed elementum risus sit amet ligula accumsan consequat. Aliquam ut quam consequat, vehicula metus vitae, faucibus elit. Nulla magna justo, viverra ut posuere vel, egestas non ex. Morbi sit amet magna sed risus auctor tincidunt et imperdiet mi. Praesent ac facilisis urna, id feugiat lorem. Vestibulum risus lacus, dictum sit amet tincidunt ac, aliquet vel nisl. Donec ex magna, dignissim eget tempus eget, ultricies sit amet leo. Etiam et nisi rutrum, molestie magna non, dignissim lacus. Cras euismod vitae dolor id sagittis. Duis rutrum felis ut augue congue lobortis. Vivamus vehicula et tortor sit amet efficitur. Morbi luctus sem sed ipsum accumsan, et finibus purus facilisis. Sed ut malesuada dolor. Nulla sed viverra velit. Nullam ac sagittis tellus. Integer ut nibh rutrum, aliquet ligula at, maximus lectus.</p>\n<!-- /wp:paragraph -->', 'Morbi sit amet magna sed risus auctor', '', 'publish', 'closed', 'closed', '', 'fsgfdsg', '', '', '2019-06-06 18:42:54', '2019-06-06 18:42:54', '', 0, 'https://wp2.dev-c01.muulabs.pl/?p=30', 0, 'post', '', 0),
(31, 1, '2019-03-19 02:18:30', '2019-03-19 02:18:30', '<!-- wp:paragraph -->\n<p>sgfdgsdfgdsfg</p>\n<!-- /wp:paragraph -->', 'fsgfdsg', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2019-03-19 02:18:30', '2019-03-19 02:18:30', '', 30, 'https://wp2.dev-c01.muulabs.pl/30-revision-v1/', 0, 'revision', '', 0),
(34, 1, '2019-03-19 02:24:48', '2019-03-19 02:24:48', 'Maecenas eu hendrerit sem, in interdum lacus. Curabitur diam dolor, ullamcorper in dolor vel, placerat interdum massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas in justo commodo quam molestie vehicula. Curabitur vehicula dictum tortor, ac sollicitudin neque vestibulum quis. Aliquam quis maximus neque, eget semper neque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed sem hendrerit, fringilla augue eu, luctus purus. Integer tempus arcu in tortor placerat, non imperdiet nisi sollicitudin.', 'Aliquam sed posuere libero', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.\r\n\r\nHet antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'publish', 'open', 'closed', '', 'fsdfsf', '', '', '2019-04-10 22:57:27', '2019-04-10 22:57:27', '', 0, 'https://wp2.dev-c01.muulabs.pl/?post_type=tribe_events&#038;p=34', 0, 'tribe_events', '', 0),
(36, 1, '2019-03-19 02:24:48', '2019-03-19 02:24:48', 'sfsdf', 'fsdfsf', '', 'inherit', 'closed', 'closed', '', '34-revision-v1', '', '', '2019-03-19 02:24:48', '2019-03-19 02:24:48', '', 34, 'https://wp2.dev-c01.muulabs.pl/34-revision-v1/', 0, 'revision', '', 0),
(39, 1, '2019-03-27 22:29:27', '2019-03-27 22:29:27', '', 'Events', '', 'publish', 'closed', 'closed', '', 'events', '', '', '2019-06-05 20:01:23', '2019-06-05 20:01:23', '', 0, 'https://wp2.dev-c01.muulabs.pl/events/', 2, 'nav_menu_item', '', 0),
(40, 1, '2019-03-27 22:29:27', '2019-03-27 22:29:27', '', 'Books', '', 'publish', 'closed', 'closed', '', 'books', '', '', '2019-06-05 20:01:23', '2019-06-05 20:01:23', '', 0, 'https://wp2.dev-c01.muulabs.pl/books/', 3, 'nav_menu_item', '', 0),
(42, 1, '2019-03-27 23:13:13', '2019-03-27 23:13:13', '', 'Mentoring Program', '', 'publish', 'closed', 'closed', '', 'emergency-contact', '', '', '2019-06-05 20:01:23', '2019-06-05 20:01:23', '', 0, 'https://wp2.dev-c01.muulabs.pl/?p=42', 5, 'nav_menu_item', '', 0),
(44, 1, '2019-03-27 23:23:41', '2019-03-27 23:23:41', ' ', '', '', 'publish', 'closed', 'closed', '', '44', '', '', '2019-06-05 20:01:23', '2019-06-05 20:01:23', '', 0, 'https://wp2.dev-c01.muulabs.pl/?p=44', 4, 'nav_menu_item', '', 0),
(51, 1, '2019-03-28 01:35:34', '2019-03-28 01:35:34', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:woocommerce/product-best-sellers {\"columns\":1,\"rows\":2,\"categories\":[15]} -->\n<div class=\"wp-block-woocommerce-product-best-sellers\">[products limit=\"2\" columns=\"1\" category=\"15\" best_selling=\"1\"]</div>\n<!-- /wp:woocommerce/product-best-sellers -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like piña coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"https://wp2.dev-c01.muulabs.pl/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-autosave-v1', '', '', '2019-03-28 01:35:34', '2019-03-28 01:35:34', '', 2, 'https://wp2.dev-c01.muulabs.pl/2-autosave-v1/', 0, 'revision', '', 0),
(52, 1, '2019-03-28 02:42:16', '2019-03-28 02:42:16', 'Mauris hendrerit, odio sit amet suscipit pellentesque, risus enim maximus metus, a dictum ex lorem quis tellus. Nunc et orci at justo auctor tincidunt. Vestibulum elementum felis ut libero sagittis, id accumsan est rutrum. Sed non nisl nulla. Nulla mollis lorem et ex facilisis, vel vulputate turpis pulvinar. Donec at dignissim ex. Maecenas efficitur, odio vitae congue elementum, eros tellus sagittis erat, eget tristique nisl tellus vel lacus. In hac habitasse platea dictumst. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris sit amet mattis diam. Proin massa enim, scelerisque et dolor eu, consequat scelerisque urna. Phasellus sit amet leo bibendum, accumsan urna vel, eleifend lectus. Proin ipsum orci, blandit vel tortor a, varius congue purus.\r\n\r\nMaecenas eu hendrerit sem, in interdum lacus. Curabitur diam dolor, ullamcorper in dolor vel, placerat interdum massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas in justo commodo quam molestie vehicula. Curabitur vehicula dictum tortor, ac sollicitudin neque vestibulum quis. Aliquam quis maximus neque, eget semper neque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed sem hendrerit, fringilla augue eu, luctus purus. Integer tempus arcu in tortor placerat, non imperdiet nisi sollicitudin.\r\n\r\nAliquam sed posuere libero, interdum consequat orci. Nunc egestas velit sed diam aliquam ultrices. Fusce mauris erat, accumsan et tempor et, lobortis vel sapien. Praesent rutrum ut est dictum imperdiet. Etiam tempus orci turpis, eu fermentum est finibus et. In interdum ipsum et nunc tincidunt posuere. Maecenas eros purus, euismod id ex nec, ultricies tempus justo.', 'Lorem ipsum dolor sit amet', 'Nunc sed sem hendrerit, fringilla augue eu, luctus purus. Integer tempus arcu in tortor placerat, non imperdiet nisi sollicitudin.', 'publish', 'open', 'closed', '', 'asdasdasd', '', '', '2019-04-10 22:59:08', '2019-04-10 22:59:08', '', 0, 'https://wp2.dev-c01.muulabs.pl/?post_type=tribe_events&#038;p=52', 0, 'tribe_events', '', 0),
(53, 1, '2019-03-28 02:42:16', '2019-03-28 02:42:16', 'adadasd', 'asdasdasd', '', 'inherit', 'closed', 'closed', '', '52-revision-v1', '', '', '2019-03-28 02:42:16', '2019-03-28 02:42:16', '', 52, 'https://wp2.dev-c01.muulabs.pl/52-revision-v1/', 0, 'revision', '', 0),
(54, 1, '2019-03-28 02:48:20', '2019-03-28 02:48:20', 'This is your homepage which is what most visitors will see when they first visit your shop.\n\nYou can change this text by editing the \"Welcome\" page via the \"Pages\" menu in your dashboard.', 'Welcome', '', 'inherit', 'closed', 'closed', '', '15-revision-v1', '', '', '2019-03-28 02:48:20', '2019-03-28 02:48:20', '', 15, 'https://wp2.dev-c01.muulabs.pl/15-revision-v1/', 0, 'revision', '', 0),
(55, 1, '2019-03-28 03:30:25', '2019-03-28 03:30:25', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.\r\n\r\nHet antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'Simple event', '', 'inherit', 'closed', 'closed', '', '34-revision-v1', '', '', '2019-03-28 03:30:25', '2019-03-28 03:30:25', '', 34, 'https://wp2.dev-c01.muulabs.pl/34-revision-v1/', 0, 'revision', '', 0),
(56, 1, '2019-03-28 03:36:27', '2019-03-28 03:36:27', 'adadasd', 'Simple event 2....', '', 'inherit', 'closed', 'closed', '', '52-revision-v1', '', '', '2019-03-28 03:36:27', '2019-03-28 03:36:27', '', 52, 'https://wp2.dev-c01.muulabs.pl/52-revision-v1/', 0, 'revision', '', 0),
(57, 1, '2019-03-28 03:37:03', '2019-03-28 03:37:03', 'asdasdsd', 'The secrets of dating event', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-03-28 03:37:03', '2019-03-28 03:37:03', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(58, 1, '2019-03-28 03:37:14', '2019-03-28 03:37:14', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.\r\n\r\nHet antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'The secrets of dating event', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-03-28 03:37:14', '2019-03-28 03:37:14', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(59, 1, '2019-03-28 03:57:09', '2019-03-28 03:57:09', 'Vestibulum mauris sapien, maximus non porttitor blandit, feugiat eget nisi. Nunc nisi leo, blandit ac ipsum eget, bibendum pulvinar lacus. Maecenas aliquet vel elit quis malesuada. Sed et erat ligula. Etiam in diam ac sapien mattis scelerisque.\n\nFusce rhoncus, odio pulvinar consequat blandit, magna nisl venenatis massa, id varius erat libero sollicitudin velit.', 'Angelo Kostakis', '', 'inherit', 'closed', 'closed', '', '15-revision-v1', '', '', '2019-03-28 03:57:09', '2019-03-28 03:57:09', '', 15, 'https://wp2.dev-c01.muulabs.pl/15-revision-v1/', 0, 'revision', '', 0),
(60, 1, '2019-03-28 04:27:53', '2019-03-28 04:27:53', '', 'book', '', 'inherit', 'open', 'closed', '', 'book', '', '', '2019-03-28 04:27:53', '2019-03-28 04:27:53', '', 0, 'https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/book.png', 0, 'attachment', 'image/png', 0),
(61, 1, '2019-03-28 04:29:25', '2019-03-28 04:29:25', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer enim velit, ultricies a placerat vel, posuere id enim. Ut vitae enim ac sem tempor venenatis eget sit amet quam. Morbi tincidunt pretium nisi sit amet hendrerit. Donec consequat turpis metus, ac tincidunt erat rhoncus ac. Proin vitae tortor nec sapien fermentum malesuada in vel purus. Etiam vestibulum tincidunt rhoncus. Integer vitae sapien tincidunt, rhoncus urna a, blandit est. Aliquam erat volutpat. Integer eget eleifend justo. Praesent erat leo, tincidunt vitae ornare sed, vehicula vitae neque. Morbi at bibendum risus. Duis sit amet ante vel est lacinia varius.\r\n\r\nNullam faucibus orci eu augue semper, eget eleifend leo rhoncus. Praesent ornare mauris ante, id congue lectus luctus nec. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec dignissim varius quam ac venenatis. Curabitur volutpat ante semper arcu vulputate pellentesque. Maecenas congue non purus non maximus. Curabitur quis risus nec ex porta cursus.', 'Vestibulum ullamcorper gravida libero', '', 'publish', 'closed', 'closed', '', 'vestibulum-ullamcorper-gravida-libero', '', '', '2019-04-10 23:01:35', '2019-04-10 23:01:35', '', 0, 'https://wp2.dev-c01.muulabs.pl/?post_type=product&#038;p=61', 0, 'product', '', 0),
(62, 1, '2019-03-28 04:29:49', '2019-03-28 04:29:49', 'Nam pharetra nisi a tincidunt porttitor. Nullam at lectus magna. Donec sodales nisi accumsan quam euismod pulvinar. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse viverra ac est eu finibus. Sed sodales ultricies est at hendrerit. Maecenas aliquet porttitor tellus, et finibus nisl convallis.\r\n\r\nSuspendisse fermentum viverra turpis, molestie faucibus metus pharetra eget. Quisque rutrum et metus quis pharetra. Cras id aliquet urna. Etiam pellentesque metus euismod, iaculis justo quis, ultricies nisi.', 'The secrets of dating', '', 'publish', 'closed', 'closed', '', 'the-secrets-of-dating', '', '', '2019-04-10 23:02:03', '2019-04-10 23:02:03', '', 0, 'https://wp2.dev-c01.muulabs.pl/?post_type=product&#038;p=62', 0, 'product', '', 0),
(63, 1, '2019-03-28 04:32:39', '2019-03-28 04:32:39', '', 'About', '', 'publish', 'closed', 'closed', '', 'about-2', '', '', '2019-06-05 20:01:23', '2019-06-05 20:01:23', '', 0, 'https://wp2.dev-c01.muulabs.pl/?p=63', 1, 'nav_menu_item', '', 0),
(66, 1, '2019-04-02 06:40:15', '2019-04-02 06:40:15', '', 'dsf', '', 'publish', 'closed', 'closed', '', 'dsf', '', '', '2019-04-02 06:40:15', '2019-04-02 06:40:15', '', 0, 'https://wp2.dev-c01.muulabs.pl/organizer/dsf/', 0, 'tribe_organizer', '', 0),
(68, 1, '2019-04-02 09:51:54', '2019-04-02 09:51:54', '', 'Mentoring Program', '<p class=\"pb-5 lead text-muted\">Nam pharetra nisi a tincidunt porttitor. Nullam at lectus magna. Donec sodales nisi accumsan quam euismod pulvinar. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse viverra ac est eu finibus.</p>', 'publish', 'closed', 'closed', '', 'mentoring-program', '', '', '2019-06-05 19:59:57', '2019-06-05 19:59:57', '', 0, 'https://wp2.dev-c01.muulabs.pl/?post_type=product&#038;p=68', 0, 'product', '', 0),
(71, 1, '2019-04-08 17:42:49', '2019-04-08 17:42:49', 'dfasfsadfs', 'Emergency Contact', '', 'inherit', 'closed', 'closed', '', '68-autosave-v1', '', '', '2019-04-08 17:42:49', '2019-04-08 17:42:49', '', 68, 'https://wp2.dev-c01.muulabs.pl/68-autosave-v1/', 0, 'revision', '', 0),
(74, 1, '2019-04-08 18:31:20', '2019-04-08 18:31:20', '', '73 | dfasd | 1', '', 'publish', 'closed', 'closed', '', '73-dfasd-1', '', '', '2019-04-08 18:31:20', '2019-04-08 18:31:20', '', 0, 'https://wp2.dev-c01.muulabs.pl/?post_type=tribe_wooticket&p=74', 0, 'tribe_wooticket', '', 0),
(76, 1, '2019-04-08 18:46:41', '2019-04-08 18:46:41', '', 'book', '', 'inherit', 'open', 'closed', '', 'book-3', '', '', '2019-04-08 18:46:41', '2019-04-08 18:46:41', '', 61, 'https://wp2.dev-c01.muulabs.pl/wp-content/uploads/woocommerce_uploads/2019/03/book.pdf', 0, 'attachment', 'application/pdf', 0),
(78, 1, '2019-04-08 18:58:25', '2019-04-08 18:58:25', '', '77 | Lorem Ipsum Ticket Normal | 1', '', 'publish', 'closed', 'closed', '', '77-lorem-ipsum-ticket-normal-1', '', '', '2019-04-08 18:58:25', '2019-04-08 18:58:25', '', 0, 'https://wp2.dev-c01.muulabs.pl/?post_type=tribe_wooticket&p=78', 0, 'tribe_wooticket', '', 0),
(79, 1, '2019-04-08 18:58:25', '2019-04-08 18:58:25', '', '77 | Lorem Ipsum Ticket Senior | 1', '', 'publish', 'closed', 'closed', '', '77-lorem-ipsum-ticket-senior-1', '', '', '2019-04-08 18:58:25', '2019-04-08 18:58:25', '', 0, 'https://wp2.dev-c01.muulabs.pl/?post_type=tribe_wooticket&p=79', 0, 'tribe_wooticket', '', 0),
(81, 0, '2019-04-08 19:27:58', '2019-04-08 19:27:58', '', '80 | Lorem Ipsum Ticket Normal | 1', '', 'publish', 'closed', 'closed', '', '80-lorem-ipsum-ticket-normal-1', '', '', '2019-04-08 19:27:58', '2019-04-08 19:27:58', '', 0, 'https://wp2.dev-c01.muulabs.pl/?post_type=tribe_wooticket&p=81', 0, 'tribe_wooticket', '', 0),
(82, 0, '2019-04-08 19:27:58', '2019-04-08 19:27:58', '', '80 | Lorem Ipsum Ticket Senior | 1', '', 'publish', 'closed', 'closed', '', '80-lorem-ipsum-ticket-senior-1', '', '', '2019-04-08 19:27:58', '2019-04-08 19:27:58', '', 0, 'https://wp2.dev-c01.muulabs.pl/?post_type=tribe_wooticket&p=82', 0, 'tribe_wooticket', '', 0),
(85, 0, '2019-04-08 22:46:49', '2019-04-08 22:46:49', '', '84 | Lorem Ipsum Ticket Normal | 1', '', 'publish', 'closed', 'closed', '', '84-lorem-ipsum-ticket-normal-1', '', '', '2019-04-08 22:46:49', '2019-04-08 22:46:49', '', 0, 'https://wp2.dev-c01.muulabs.pl/?post_type=tribe_wooticket&p=85', 0, 'tribe_wooticket', '', 0),
(86, 0, '2019-04-08 22:46:49', '2019-04-08 22:46:49', '', '84 | Lorem Ipsum Ticket Normal | 2', '', 'publish', 'closed', 'closed', '', '84-lorem-ipsum-ticket-normal-2', '', '', '2019-04-08 22:46:49', '2019-04-08 22:46:49', '', 0, 'https://wp2.dev-c01.muulabs.pl/?post_type=tribe_wooticket&p=86', 0, 'tribe_wooticket', '', 0),
(87, 0, '2019-04-08 22:46:49', '2019-04-08 22:46:49', '', '84 | Lorem Ipsum Ticket Senior | 1', '', 'publish', 'closed', 'closed', '', '84-lorem-ipsum-ticket-senior-1', '', '', '2019-04-08 22:46:49', '2019-04-08 22:46:49', '', 0, 'https://wp2.dev-c01.muulabs.pl/?post_type=tribe_wooticket&p=87', 0, 'tribe_wooticket', '', 0),
(89, 1, '2019-04-09 13:56:28', '2019-04-09 13:56:28', '<!-- wp:tribe/event-datetime  /-->\n\r<!-- wp:tribe/featured-image  /-->\n\r<!-- wp:freeform -->\n\rHeb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.\r\n\r\nHet antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.\n\r<!-- /wp:freeform -->\n\r<!-- wp:tribe/event-links  /-->\n\r<!-- wp:tribe/classic-event-details  /-->\n\r<!-- wp:tribe/event-venue  /-->\n\r<!-- wp:tribe/tickets --><div class=\"wp-block-tribe-tickets\">\n\r<!-- wp:tribe/tickets-item {\"hasBeenCreated\":true,\"ticketId\":29} --><div class=\"wp-block-tribe-tickets-item\"></div><!-- /wp:tribe/tickets-item -->\n\r<!-- wp:tribe/tickets-item {\"hasBeenCreated\":true,\"ticketId\":70} --><div class=\"wp-block-tribe-tickets-item\"></div><!-- /wp:tribe/tickets-item -->\n\r</div><!-- /wp:tribe/tickets -->\n\r<!-- wp:tribe/rsvp  /-->\n\r<!-- wp:tribe/attendees  /-->', 'The secrets of dating event', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-09 13:56:28', '2019-04-09 13:56:28', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(90, 1, '2019-04-09 13:57:07', '2019-04-09 13:57:07', '<!-- wp:tribe/event-datetime /-->\n\n<!-- wp:tribe/featured-image /-->\n\n<p>Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.</p>\n<p>Het antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.</p>\n\n<!-- wp:tribe/event-links /-->\n\n<!-- wp:tribe/classic-event-details /-->\n\n<!-- wp:tribe/event-venue /-->\n\n<!-- wp:tribe/tickets -->\n<div class=\"wp-block-tribe-tickets\"><!-- wp:tribe/tickets-item {\"hasBeenCreated\":true,\"ticketId\":29} -->\n<div class=\"wp-block-tribe-tickets-item\"></div>\n<!-- /wp:tribe/tickets-item -->\n\n<!-- wp:tribe/tickets-item {\"hasBeenCreated\":true,\"ticketId\":70} -->\n<div class=\"wp-block-tribe-tickets-item\"></div>\n<!-- /wp:tribe/tickets-item --></div>\n<!-- /wp:tribe/tickets -->\n\n<!-- wp:tribe/rsvp /-->\n\n<!-- wp:tribe/attendees /-->', 'The secrets of dating event 1', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-09 13:57:07', '2019-04-09 13:57:07', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(91, 1, '2019-04-09 13:57:10', '2019-04-09 13:57:10', '<!-- wp:tribe/event-datetime /-->\n\n<!-- wp:tribe/featured-image /-->\n\n<p>Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.</p>\n<p>Het antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.</p>\n\n<!-- wp:tribe/event-links /-->\n\n<!-- wp:tribe/classic-event-details /-->\n\n<!-- wp:tribe/event-venue /-->\n<!-- wp:tribe/tickets  /-->\n\n<!-- wp:tribe/rsvp /-->\n\n<!-- wp:tribe/attendees /-->', 'The secrets of dating event 1', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-09 13:57:10', '2019-04-09 13:57:10', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(92, 1, '2019-04-09 13:57:10', '2019-04-09 13:57:10', '<!-- wp:tribe/event-datetime /-->\n\n<!-- wp:tribe/featured-image /-->\n\n<p>Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.</p>\n<p>Het antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.</p>\n\n<!-- wp:tribe/event-links /-->\n\n<!-- wp:tribe/classic-event-details /-->\n\n<!-- wp:tribe/event-venue /-->\n<!-- wp:tribe/tickets --><div class=\"wp-block-tribe-tickets\">\n\r<!-- wp:tribe/tickets-item {\"hasBeenCreated\":true,\"ticketId\":29} --><div class=\"wp-block-tribe-tickets-item\"></div><!-- /wp:tribe/tickets-item -->\n\r<!-- wp:tribe/tickets-item {\"hasBeenCreated\":true,\"ticketId\":70} --><div class=\"wp-block-tribe-tickets-item\"></div><!-- /wp:tribe/tickets-item -->\n\r</div><!-- /wp:tribe/tickets -->\n\n<!-- wp:tribe/rsvp /-->\n\n<!-- wp:tribe/attendees /-->', 'The secrets of dating event 1', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-09 13:57:10', '2019-04-09 13:57:10', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(94, 1, '2019-04-10 13:32:59', '2019-04-10 13:32:59', '<!-- wp:tribe/event-datetime /-->\n\n<!-- wp:tribe/featured-image /-->\n\n<p>Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.</p>\n<p>Het antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.</p>\n\n<!-- wp:tribe/event-links /-->\n\n<!-- wp:tribe/classic-event-details /-->\n\n<!-- wp:tribe/event-venue /-->\n<!-- wp:tribe/tickets  /-->\n\n<!-- wp:tribe/rsvp /-->\n\n<!-- wp:tribe/attendees /-->', 'The secrets of dating event 1', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-10 13:32:59', '2019-04-10 13:32:59', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(95, 1, '2019-04-10 13:32:59', '2019-04-10 13:32:59', '<!-- wp:tribe/event-datetime /-->\n\n<!-- wp:tribe/featured-image /-->\n\n<p>Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.</p>\n<p>Het antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.</p>\n\n<!-- wp:tribe/event-links /-->\n\n<!-- wp:tribe/classic-event-details /-->\n\n<!-- wp:tribe/event-venue /-->\n<!-- wp:tribe/tickets --><div class=\"wp-block-tribe-tickets\">\n\r<!-- wp:tribe/tickets-item {\"hasBeenCreated\":true,\"ticketId\":29} --><div class=\"wp-block-tribe-tickets-item\"></div><!-- /wp:tribe/tickets-item -->\n\r<!-- wp:tribe/tickets-item {\"hasBeenCreated\":true,\"ticketId\":70} --><div class=\"wp-block-tribe-tickets-item\"></div><!-- /wp:tribe/tickets-item -->\n\r</div><!-- /wp:tribe/tickets -->\n\n<!-- wp:tribe/rsvp /-->\n\n<!-- wp:tribe/attendees /-->', 'The secrets of dating event 1', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-10 13:32:59', '2019-04-10 13:32:59', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(96, 1, '2019-04-10 19:35:56', '2019-04-10 19:35:56', '<div class=\'row\'>\n<div class=\'col-md-6\'></div>\n<div class=\'col-md-6\'></div>\n</div>\n<img class=\"alignnone size-medium wp-image-117\" src=\"https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/as-2-300x300.jpg\" alt=\"\" width=\"300\" height=\"300\" />\n<h5>Waarom kiezen voor dit seminarie?</h5>\nAngelo zijn seminaries staan vol met psychologische trucjes, veel metaforen waarbij mensen zich onmiddellijk in herkennen en sluit af met een empowerment gevoel. Een mind engineer pur sang en creëert een reis doorheen het bewustzijn als het ware.\n\nZijn favoriete onderwerpen zijn:\n<ul>\n 	<li>Law of attraction</li>\n 	<li>Dating is a mirror</li>\n 	<li>Engineer your future self</li>\n 	<li>Programming your mind</li>\n 	<li>Self love</li>\n 	<li>Smashing through limiting beliefs</li>\n 	<li>Mission in life</li>\n</ul>\n<img class=\"alignnone size-medium wp-image-116\" src=\"https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/ak-1-300x300.jpg\" alt=\"\" width=\"300\" height=\"300\" />\n<h5>Wie is de organisator van het seminarie?</h5>\nIk ben Angelo Kostakis en merkte reeds van jongs af aan dat ik zeer geïnteresseerd ben in hoe mensen denken en hoe problemen opgelost kunnen worden wanneer je objectief weet te blijven in moeilijke situaties.\n\nIk heb mijn studie bouwkundig ingenieur afgerond maar merkte gaandeweg mijn job dat het mij meer voldoening gaf om mensen hun problemen op te lossen dan bouwkundige berekeningen te maken.\n\nIk ben begonnen om over liefde – in de vorm van poëzie – te schrijven op mijn Instagram account. Dit om mensen te raken en weer te doen geloven in het goede van de liefde, maar ook om te laten zien dat het vaak gepaard gaat met pijn.', 'The secrets of dating event', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.\n\nHet antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'inherit', 'closed', 'closed', '', '27-autosave-v1', '', '', '2019-04-10 19:35:56', '2019-04-10 19:35:56', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-autosave-v1/', 0, 'revision', '', 0),
(97, 1, '2019-04-10 14:26:31', '2019-04-10 14:26:31', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:12:\"tribe_events\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Events', 'events', 'publish', 'closed', 'closed', '', 'group_5cadfca7a2120', '', '', '2019-04-10 19:02:09', '2019-04-10 19:02:09', '', 0, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field-group&#038;p=97', 0, 'acf-field-group', '', 0),
(98, 1, '2019-04-10 14:26:31', '2019-04-10 14:26:31', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Programma', 'program', 'publish', 'closed', 'closed', '', 'field_5cadfcd356735', '', '', '2019-04-10 19:02:09', '2019-04-10 19:02:09', '', 97, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field&#038;p=98', 0, 'acf-field', '', 0),
(99, 1, '2019-04-10 17:03:41', '2019-04-10 17:03:41', '<!-- wp:heading -->\n<h2>Who we are</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Our website address is: https://wp2.dev-c01.muulabs.pl.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading -->\n<h2>What personal data we collect and why we collect it</h2>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Comments</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor’s IP address and browser user agent string to help spam detection.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Media</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Contact forms</h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Cookies</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select \"Remember Me\", your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Embedded content from other websites</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Analytics</h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading -->\n<h2>Who we share your data with</h2>\n<!-- /wp:heading -->\n\n<!-- wp:heading -->\n<h2>How long we retain your data</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading -->\n<h2>What rights you have over your data</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading -->\n<h2>Where we send your data</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Visitor comments may be checked through an automated spam detection service.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading -->\n<h2>Your contact information</h2>\n<!-- /wp:heading -->\n\n<!-- wp:heading -->\n<h2>Additional information</h2>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>How we protect your data</h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>What data breach procedures we have in place</h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>What third parties we receive data from</h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>What automated decision making and/or profiling we do with user data</h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Industry regulatory disclosure requirements</h3>\n<!-- /wp:heading -->', 'Privacy Policy', '', 'inherit', 'closed', 'closed', '', '3-revision-v1', '', '', '2019-04-10 17:03:41', '2019-04-10 17:03:41', '', 3, 'https://wp2.dev-c01.muulabs.pl/3-revision-v1/', 0, 'revision', '', 0),
(100, 1, '2019-04-10 17:38:49', '2019-04-10 17:38:49', '<!-- wp:paragraph -->\n<p>Sed elementum risus sit amet ligula accumsan consequat. Aliquam ut quam consequat, vehicula metus vitae, faucibus elit. Nulla magna justo, viverra ut posuere vel, egestas non ex. Morbi sit amet magna sed risus auctor tincidunt et imperdiet mi. Praesent ac facilisis urna, id feugiat lorem. Vestibulum risus lacus, dictum sit amet tincidunt ac, aliquet vel nisl. Donec ex magna, dignissim eget tempus eget, ultricies sit amet leo. Etiam et nisi rutrum, molestie magna non, dignissim lacus. Cras euismod vitae dolor id sagittis. Duis rutrum felis ut augue congue lobortis. Vivamus vehicula et tortor sit amet efficitur. Morbi luctus sem sed ipsum accumsan, et finibus purus facilisis. Sed ut malesuada dolor. Nulla sed viverra velit. Nullam ac sagittis tellus. Integer ut nibh rutrum, aliquet ligula at, maximus lectus.</p>\n<!-- /wp:paragraph -->', 'Morbi sit amet magna sed risus auctor', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2019-04-10 17:38:49', '2019-04-10 17:38:49', '', 30, 'https://wp2.dev-c01.muulabs.pl/30-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(101, 1, '2019-04-10 17:39:18', '2019-04-10 17:39:18', '<!-- wp:paragraph -->\n<p>Praesent id condimentum ex. Maecenas tempus in lectus nec lacinia. Ut porta ut purus et consequat. Phasellus ornare gravida massa, sit amet suscipit mi maximus vitae. Curabitur ipsum lacus, viverra id fringilla vitae, facilisis vel nunc. Integer dignissim tincidunt malesuada. Mauris eu orci id purus aliquet ullamcorper ac quis neque. Pellentesque eu nulla nulla. Integer aliquet, erat sed luctus malesuada, mauris sapien tincidunt nisi, sed condimentum magna lectus a ante. In libero metus, luctus eu fringilla fermentum, ultrices ut ante. Maecenas quis aliquet velit, nec faucibus tellus. Donec feugiat massa diam, et maximus mi dapibus non. Mauris auctor mattis quam eu vestibulum. Pellentesque leo nunc, pharetra et interdum sit amet, finibus a erat. Phasellus dapibus congue nisl eu cursus.</p>\n<!-- /wp:paragraph -->', 'Cras euismod vitae dolor id sagittis', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2019-04-10 17:39:18', '2019-04-10 17:39:18', '', 1, 'https://wp2.dev-c01.muulabs.pl/1-revision-v1/', 0, 'revision', '', 0),
(102, 1, '2019-04-10 17:39:37', '2019-04-10 17:39:37', '<!-- wp:paragraph -->\n<p>Nunc a imperdiet metus. Nunc scelerisque hendrerit arcu, eu mattis risus faucibus id. Nullam bibendum est ac finibus aliquet. Mauris finibus purus ut lectus vehicula elementum. Fusce congue, augue finibus maximus molestie, odio libero luctus dui, sit amet aliquet est nulla id turpis. Duis sed lectus est. Cras maximus lobortis nunc quis pretium. In consequat eleifend odio in varius. Curabitur a sapien iaculis, gravida justo sed, dignissim diam. Sed congue a odio ut commodo. Suspendisse nisi libero, consectetur et malesuada a, elementum eget massa. Ut vitae viverra arcu, nec luctus lorem. Duis interdum condimentum est quis consequat. Pellentesque eu scelerisque lectus. Phasellus dapibus congue nisl eu cursus. Quisque turpis ipsum, porta at auctor nec, cursus nec ligula.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Nunc a imperdiet metus. Nunc scelerisque hendrerit arcu, eu mattis risus faucibus id. Nullam bibendum est ac finibus aliquet. Mauris finibus purus ut lectus vehicula elementum. Fusce congue, augue finibus maximus molestie, odio libero luctus dui, sit amet aliquet est nulla id turpis. Duis sed lectus est. Cras maximus lobortis nunc quis pretium. In consequat eleifend odio in varius. Curabitur a sapien iaculis, gravida justo sed, dignissim diam. Sed congue a odio ut commodo. Suspendisse nisi libero, consectetur et malesuada a, elementum eget massa. Ut vitae viverra arcu, nec luctus lorem. Duis interdum condimentum est quis consequat. Pellentesque eu scelerisque lectus. Phasellus dapibus congue nisl eu cursus. Quisque turpis ipsum, porta at auctor nec, cursus nec ligula.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Nunc a imperdiet metus. Nunc scelerisque hendrerit arcu, eu mattis risus faucibus id. Nullam bibendum est ac finibus aliquet. Mauris finibus purus ut lectus vehicula elementum. Fusce congue, augue finibus maximus molestie, odio libero luctus dui, sit amet aliquet est nulla id turpis. Duis sed lectus est. Cras maximus lobortis nunc quis pretium. In consequat eleifend odio in varius. Curabitur a sapien iaculis, gravida justo sed, dignissim diam. Sed congue a odio ut commodo. Suspendisse nisi libero, consectetur et malesuada a, elementum eget massa. Ut vitae viverra arcu, nec luctus lorem. Duis interdum condimentum est quis consequat. Pellentesque eu scelerisque lectus. Phasellus dapibus congue nisl eu cursus. Quisque turpis ipsum, porta at auctor nec, cursus nec ligula.</p>\n<!-- /wp:paragraph -->', 'Donec tincidunt felis eget pretium rhoncus', '', 'publish', 'closed', 'closed', '', 'donec-tincidunt-felis-eget-pretium-rhoncus', '', '', '2019-06-06 18:42:59', '2019-06-06 18:42:59', '', 0, 'https://wp2.dev-c01.muulabs.pl/?p=102', 0, 'post', '', 0),
(103, 1, '2019-04-10 17:39:37', '2019-04-10 17:39:37', '<!-- wp:paragraph -->\n<p>Nunc a imperdiet metus. Nunc scelerisque hendrerit arcu, eu mattis risus faucibus id. Nullam bibendum est ac finibus aliquet. Mauris finibus purus ut lectus vehicula elementum. Fusce congue, augue finibus maximus molestie, odio libero luctus dui, sit amet aliquet est nulla id turpis. Duis sed lectus est. Cras maximus lobortis nunc quis pretium. In consequat eleifend odio in varius. Curabitur a sapien iaculis, gravida justo sed, dignissim diam. Sed congue a odio ut commodo. Suspendisse nisi libero, consectetur et malesuada a, elementum eget massa. Ut vitae viverra arcu, nec luctus lorem. Duis interdum condimentum est quis consequat. Pellentesque eu scelerisque lectus. Phasellus dapibus congue nisl eu cursus. Quisque turpis ipsum, porta at auctor nec, cursus nec ligula.</p>\n<!-- /wp:paragraph -->', 'Donec tincidunt felis eget pretium rhoncus', '', 'inherit', 'closed', 'closed', '', '102-revision-v1', '', '', '2019-04-10 17:39:37', '2019-04-10 17:39:37', '', 102, 'https://wp2.dev-c01.muulabs.pl/102-revision-v1/', 0, 'revision', '', 0),
(104, 1, '2019-04-10 17:40:23', '2019-04-10 17:40:23', '<!-- wp:paragraph -->\n<p>Nunc a imperdiet metus. Nunc scelerisque hendrerit arcu, eu mattis risus faucibus id. Nullam bibendum est ac finibus aliquet. Mauris finibus purus ut lectus vehicula elementum. Fusce congue, augue finibus maximus molestie, odio libero luctus dui, sit amet aliquet est nulla id turpis. Duis sed lectus est. Cras maximus lobortis nunc quis pretium. In consequat eleifend odio in varius. Curabitur a sapien iaculis, gravida justo sed, dignissim diam. Sed congue a odio ut commodo. Suspendisse nisi libero, consectetur et malesuada a, elementum eget massa. Ut vitae viverra arcu, nec luctus lorem. Duis interdum condimentum est quis consequat. Pellentesque eu scelerisque lectus. Phasellus dapibus congue nisl eu cursus. Quisque turpis ipsum, porta at auctor nec, cursus nec ligula.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Nunc a imperdiet metus. Nunc scelerisque hendrerit arcu, eu mattis risus faucibus id. Nullam bibendum est ac finibus aliquet. Mauris finibus purus ut lectus vehicula elementum. Fusce congue, augue finibus maximus molestie, odio libero luctus dui, sit amet aliquet est nulla id turpis. Duis sed lectus est. Cras maximus lobortis nunc quis pretium. In consequat eleifend odio in varius. Curabitur a sapien iaculis, gravida justo sed, dignissim diam. Sed congue a odio ut commodo. Suspendisse nisi libero, consectetur et malesuada a, elementum eget massa. Ut vitae viverra arcu, nec luctus lorem. Duis interdum condimentum est quis consequat. Pellentesque eu scelerisque lectus. Phasellus dapibus congue nisl eu cursus. Quisque turpis ipsum, porta at auctor nec, cursus nec ligula.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Nunc a imperdiet metus. Nunc scelerisque hendrerit arcu, eu mattis risus faucibus id. Nullam bibendum est ac finibus aliquet. Mauris finibus purus ut lectus vehicula elementum. Fusce congue, augue finibus maximus molestie, odio libero luctus dui, sit amet aliquet est nulla id turpis. Duis sed lectus est. Cras maximus lobortis nunc quis pretium. In consequat eleifend odio in varius. Curabitur a sapien iaculis, gravida justo sed, dignissim diam. Sed congue a odio ut commodo. Suspendisse nisi libero, consectetur et malesuada a, elementum eget massa. Ut vitae viverra arcu, nec luctus lorem. Duis interdum condimentum est quis consequat. Pellentesque eu scelerisque lectus. Phasellus dapibus congue nisl eu cursus. Quisque turpis ipsum, porta at auctor nec, cursus nec ligula.</p>\n<!-- /wp:paragraph -->', 'Donec tincidunt felis eget pretium rhoncus', '', 'inherit', 'closed', 'closed', '', '102-revision-v1', '', '', '2019-04-10 17:40:23', '2019-04-10 17:40:23', '', 102, 'https://wp2.dev-c01.muulabs.pl/102-revision-v1/', 0, 'revision', '', 0),
(105, 1, '2019-04-10 17:43:58', '2019-04-10 17:43:58', '<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eu accumsan odio, et aliquet odio. Pellentesque ut nibh sit amet neque ullamcorper tincidunt. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus cursus auctor accumsan. Phasellus varius urna et ipsum vulputate, nec finibus libero tincidunt. Aenean eu nulla neque. Aliquam erat volutpat. Suspendisse potenti.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Nulla dapibus ligula non nibh malesuada eleifend. Morbi mollis velit nec erat malesuada, eget vestibulum felis aliquet. Nam id blandit sem. Vivamus eleifend pretium semper. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras faucibus felis odio, vitae commodo nisl porta ac. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut odio tortor, sagittis eu velit in, convallis placerat purus. Fusce ultrices lorem sit amet pharetra facilisis. Maecenas dui libero, aliquet vel varius et, finibus faucibus augue.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Proin nec mauris eu libero semper feugiat. Donec dui elit, lobortis sed augue eu, dictum porta diam. Vivamus ac sem id turpis vehicula congue id consectetur diam. Duis libero quam, rhoncus eu porta pellentesque, feugiat sagittis nisi. Donec porta ullamcorper cursus. Nunc pretium, eros nec scelerisque suscipit, risus sem varius nunc, eget iaculis risus ex ac felis. Quisque mollis iaculis leo ac sodales. Aenean augue ex, blandit ut velit ut, porta aliquam odio. Nulla sit amet purus lacus. Proin semper, ipsum in finibus dapibus, nisi ante ultrices dui, et rutrum est est nec felis. In mi orci, condimentum a odio et, lacinia dictum lectus. Aenean elit dolor, dapibus ac mauris et, dapibus scelerisque odio.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Cras nec tempus ligula. Donec tempor commodo mi, in mattis lectus. Nam aliquam efficitur mollis. Donec mattis purus in odio dapibus, vel porta diam condimentum. Integer aliquam, neque quis iaculis blandit, nisi diam auctor elit, et pretium erat justo non odio. Suspendisse ut nunc quis nulla sollicitudin commodo. Quisque enim ante, sodales vitae felis vel, vulputate ultrices tellus. Sed rhoncus, mi at accumsan volutpat, odio lectus aliquam purus, a varius enim odio ut libero. Integer commodo at mauris ut tempus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque ut dui sed mauris maximus accumsan et in ante. Curabitur enim magna, luctus vel scelerisque in, ultrices in tellus.</p>\n<!-- /wp:paragraph -->', 'Privacy Policy', '', 'inherit', 'closed', 'closed', '', '3-revision-v1', '', '', '2019-04-10 17:43:58', '2019-04-10 17:43:58', '', 3, 'https://wp2.dev-c01.muulabs.pl/3-revision-v1/', 0, 'revision', '', 0),
(106, 1, '2019-04-10 19:03:33', '2019-04-10 19:03:33', '<!-- wp:tribe/event-datetime /-->\r\n\r\n<!-- wp:tribe/featured-image /-->\r\n<p>Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.</p>\r\n<p>Het antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.</p>\r\n<!-- wp:tribe/event-links /-->\r\n\r\n<!-- wp:tribe/classic-event-details /-->\r\n\r\n<!-- wp:tribe/event-venue /-->\r\n\r\n<!-- wp:tribe/tickets -->\r\n<div class=\"wp-block-tribe-tickets\"><!-- wp:tribe/tickets-item {\"hasBeenCreated\":true,\"ticketId\":29} -->\r\n<div class=\"wp-block-tribe-tickets-item\"> </div>\r\n<!-- /wp:tribe/tickets-item -->\r\n\r\n<!-- wp:tribe/tickets-item {\"hasBeenCreated\":true,\"ticketId\":70} -->\r\n<div class=\"wp-block-tribe-tickets-item\"> </div>\r\n<!-- /wp:tribe/tickets-item --></div>\r\n<!-- /wp:tribe/tickets -->\r\n\r\n<!-- wp:tribe/rsvp /-->\r\n\r\n<!-- wp:tribe/attendees /-->', 'The secrets of dating event', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-10 19:03:33', '2019-04-10 19:03:33', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(107, 1, '2019-04-10 19:03:51', '2019-04-10 19:03:51', '', 'The secrets of dating event', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.Het antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-10 19:03:51', '2019-04-10 19:03:51', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(108, 1, '2019-04-10 19:13:23', '2019-04-10 19:13:23', '', 'The secrets of dating event', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.Het antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-10 19:13:23', '2019-04-10 19:13:23', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(109, 1, '2019-04-10 19:13:55', '2019-04-10 19:13:55', '', 'The secrets of dating event', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.Het antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-10 19:13:55', '2019-04-10 19:13:55', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(110, 1, '2019-04-10 19:17:49', '2019-04-10 19:17:49', '', 'The secrets of dating event', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.Het antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-10 19:17:49', '2019-04-10 19:17:49', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(111, 1, '2019-04-10 19:18:48', '2019-04-10 19:18:48', '', 'The secrets of dating event', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.Het antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-10 19:18:48', '2019-04-10 19:18:48', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(112, 1, '2019-04-10 19:19:14', '2019-04-10 19:19:14', '', 'The secrets of dating event', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.Het antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-10 19:19:14', '2019-04-10 19:19:14', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(113, 1, '2019-04-10 19:19:54', '2019-04-10 19:19:54', '', 'The secrets of dating event', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.\r\n\r\nHet antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-10 19:19:54', '2019-04-10 19:19:54', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(114, 1, '2019-04-10 19:30:34', '2019-04-10 19:30:34', '<h5>Waarom kiezen voor dit seminarie?</h5>\r\nAngelo zijn seminaries staan vol met psychologische trucjes, veel metaforen waarbij mensen zich onmiddellijk in herkennen en sluit af met een empowerment gevoel. Een mind engineer pur sang en creëert een reis doorheen het bewustzijn als het ware.\r\n\r\nZijn favoriete onderwerpen zijn:\r\n<ul>\r\n 	<li>Law of attraction</li>\r\n 	<li>Dating is a mirror</li>\r\n 	<li>Engineer your future self</li>\r\n 	<li>Programming your mind</li>\r\n 	<li>Self love</li>\r\n 	<li>Smashing through limiting beliefs</li>\r\n 	<li>Mission in life</li>\r\n</ul>', 'The secrets of dating event', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.\r\n\r\nHet antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-10 19:30:34', '2019-04-10 19:30:34', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(115, 1, '2019-04-10 19:31:52', '2019-04-10 19:31:52', '<h5>Waarom kiezen voor dit seminarie?</h5>\r\nAngelo zijn seminaries staan vol met psychologische trucjes, veel metaforen waarbij mensen zich onmiddellijk in herkennen en sluit af met een empowerment gevoel. Een mind engineer pur sang en creëert een reis doorheen het bewustzijn als het ware.\r\n\r\nZijn favoriete onderwerpen zijn:\r\n<ul>\r\n 	<li>Law of attraction</li>\r\n 	<li>Dating is a mirror</li>\r\n 	<li>Engineer your future self</li>\r\n 	<li>Programming your mind</li>\r\n 	<li>Self love</li>\r\n 	<li>Smashing through limiting beliefs</li>\r\n 	<li>Mission in life</li>\r\n</ul>\r\n&nbsp;\r\n<h5>Wie is de organisator van het seminarie?</h5>\r\nIk ben Angelo Kostakis en merkte reeds van jongs af aan dat ik zeer geïnteresseerd ben in hoe mensen denken en hoe problemen opgelost kunnen worden wanneer je objectief weet te blijven in moeilijke situaties.\r\n\r\nIk heb mijn studie bouwkundig ingenieur afgerond maar merkte gaandeweg mijn job dat het mij meer voldoening gaf om mensen hun problemen op te lossen dan bouwkundige berekeningen te maken.\r\n\r\nIk ben begonnen om over liefde – in de vorm van poëzie – te schrijven op mijn Instagram account. Dit om mensen te raken en weer te doen geloven in het goede van de liefde, maar ook om te laten zien dat het vaak gepaard gaat met pijn.', 'The secrets of dating event', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.\r\n\r\nHet antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-10 19:31:52', '2019-04-10 19:31:52', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(116, 1, '2019-04-10 19:34:20', '2019-04-10 19:34:20', '', 'ak-1', '', 'inherit', 'open', 'closed', '', 'ak-1', '', '', '2019-04-10 19:34:20', '2019-04-10 19:34:20', '', 27, 'https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/ak-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(117, 1, '2019-04-10 19:34:23', '2019-04-10 19:34:23', '', 'as-2', '', 'inherit', 'open', 'closed', '', 'as-2', '', '', '2019-04-10 19:34:23', '2019-04-10 19:34:23', '', 27, 'https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/as-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(118, 1, '2019-04-10 19:34:51', '2019-04-10 19:34:51', '<h5>Waarom kiezen voor dit seminarie?</h5>\r\nAngelo zijn seminaries staan vol met psychologische trucjes, veel metaforen waarbij mensen zich onmiddellijk in herkennen en sluit af met een empowerment gevoel. Een mind engineer pur sang en creëert een reis doorheen het bewustzijn als het ware.\r\n\r\n<img class=\"alignnone size-medium wp-image-117\" src=\"https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/as-2-300x300.jpg\" alt=\"\" width=\"300\" height=\"300\" />\r\n\r\nZijn favoriete onderwerpen zijn:\r\n<ul>\r\n 	<li>Law of attraction</li>\r\n 	<li>Dating is a mirror</li>\r\n 	<li>Engineer your future self</li>\r\n 	<li>Programming your mind</li>\r\n 	<li>Self love</li>\r\n 	<li>Smashing through limiting beliefs</li>\r\n 	<li>Mission in life</li>\r\n</ul>\r\n<img class=\"alignnone size-medium wp-image-116\" src=\"https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/ak-1-300x300.jpg\" alt=\"\" width=\"300\" height=\"300\" />\r\n<h5>Wie is de organisator van het seminarie?</h5>\r\nIk ben Angelo Kostakis en merkte reeds van jongs af aan dat ik zeer geïnteresseerd ben in hoe mensen denken en hoe problemen opgelost kunnen worden wanneer je objectief weet te blijven in moeilijke situaties.\r\n\r\nIk heb mijn studie bouwkundig ingenieur afgerond maar merkte gaandeweg mijn job dat het mij meer voldoening gaf om mensen hun problemen op te lossen dan bouwkundige berekeningen te maken.\r\n\r\nIk ben begonnen om over liefde – in de vorm van poëzie – te schrijven op mijn Instagram account. Dit om mensen te raken en weer te doen geloven in het goede van de liefde, maar ook om te laten zien dat het vaak gepaard gaat met pijn.', 'The secrets of dating event', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.\r\n\r\nHet antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-10 19:34:51', '2019-04-10 19:34:51', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(119, 1, '2019-04-10 19:36:59', '2019-04-10 19:36:59', '<div class=\"row\">\r\n<div class=\"col-md-6\"><img class=\"alignnone size-medium wp-image-117\" src=\"https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/as-2-300x300.jpg\" alt=\"\" width=\"300\" height=\"300\" /></div>\r\n<div class=\"col-md-6\">\r\n<h5>Waarom kiezen voor dit seminarie?</h5>\r\nAngelo zijn seminaries staan vol met psychologische trucjes, veel metaforen waarbij mensen zich onmiddellijk in herkennen en sluit af met een empowerment gevoel. Een mind engineer pur sang en creëert een reis doorheen het bewustzijn als het ware.\r\n\r\nZijn favoriete onderwerpen zijn:\r\n<ul>\r\n 	<li>Law of attraction</li>\r\n 	<li>Dating is a mirror</li>\r\n 	<li>Engineer your future self</li>\r\n 	<li>Programming your mind</li>\r\n 	<li>Self love</li>\r\n 	<li>Smashing through limiting beliefs</li>\r\n 	<li>Mission in life</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-6\">\r\n<h5>Wie is de organisator van het seminarie?</h5>\r\nIk ben Angelo Kostakis en merkte reeds van jongs af aan dat ik zeer geïnteresseerd ben in hoe mensen denken en hoe problemen opgelost kunnen worden wanneer je objectief weet te blijven in moeilijke situaties.\r\n\r\nIk heb mijn studie bouwkundig ingenieur afgerond maar merkte gaandeweg mijn job dat het mij meer voldoening gaf om mensen hun problemen op te lossen dan bouwkundige berekeningen te maken.\r\n\r\nIk ben begonnen om over liefde – in de vorm van poëzie – te schrijven op mijn Instagram account. Dit om mensen te raken en weer te doen geloven in het goede van de liefde, maar ook om te laten zien dat het vaak gepaard gaat met pijn.\r\n\r\n</div>\r\n<div class=\"col-md-6\"><img class=\"alignnone size-medium wp-image-116\" src=\"https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/ak-1-300x300.jpg\" alt=\"\" width=\"300\" height=\"300\" /></div>\r\n</div>', 'The secrets of dating event', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.\r\n\r\nHet antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-10 19:36:59', '2019-04-10 19:36:59', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(120, 1, '2019-04-10 19:37:48', '2019-04-10 19:37:48', '<div class=\"row\">\r\n<div class=\"col-md-6\"><img class=\"alignnone wp-image-117 size-full\" src=\"https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/as-2.jpg\" alt=\"\" width=\"1000\" height=\"1001\" /></div>\r\n<div class=\"col-md-6\">\r\n<h5>Waarom kiezen voor dit seminarie?</h5>\r\nAngelo zijn seminaries staan vol met psychologische trucjes, veel metaforen waarbij mensen zich onmiddellijk in herkennen en sluit af met een empowerment gevoel. Een mind engineer pur sang en creëert een reis doorheen het bewustzijn als het ware.\r\n\r\nZijn favoriete onderwerpen zijn:\r\n<ul>\r\n 	<li>Law of attraction</li>\r\n 	<li>Dating is a mirror</li>\r\n 	<li>Engineer your future self</li>\r\n 	<li>Programming your mind</li>\r\n 	<li>Self love</li>\r\n 	<li>Smashing through limiting beliefs</li>\r\n 	<li>Mission in life</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-6\">\r\n<h5>Wie is de organisator van het seminarie?</h5>\r\nIk ben Angelo Kostakis en merkte reeds van jongs af aan dat ik zeer geïnteresseerd ben in hoe mensen denken en hoe problemen opgelost kunnen worden wanneer je objectief weet te blijven in moeilijke situaties.\r\n\r\nIk heb mijn studie bouwkundig ingenieur afgerond maar merkte gaandeweg mijn job dat het mij meer voldoening gaf om mensen hun problemen op te lossen dan bouwkundige berekeningen te maken.\r\n\r\nIk ben begonnen om over liefde – in de vorm van poëzie – te schrijven op mijn Instagram account. Dit om mensen te raken en weer te doen geloven in het goede van de liefde, maar ook om te laten zien dat het vaak gepaard gaat met pijn.\r\n\r\n</div>\r\n<div class=\"col-md-6\"><img class=\"alignnone wp-image-116 size-full\" src=\"https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/ak-1.jpg\" alt=\"\" width=\"1000\" height=\"1001\" /></div>\r\n</div>', 'The secrets of dating event', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.\r\n\r\nHet antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-10 19:37:48', '2019-04-10 19:37:48', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(121, 1, '2019-04-10 19:38:43', '2019-04-10 19:38:43', '<div class=\"row\">\r\n<div class=\"col-md-6\"><img class=\"alignnone wp-image-117 size-full\" src=\"https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/as-2.jpg\" alt=\"\" width=\"1000\" height=\"1001\" /></div>\r\n<div class=\"col-md-6\">\r\n<div class=\"p-3\">\r\n<h5>Waarom kiezen voor dit seminarie?</h5>\r\nAngelo zijn seminaries staan vol met psychologische trucjes, veel metaforen waarbij mensen zich onmiddellijk in herkennen en sluit af met een empowerment gevoel. Een mind engineer pur sang en creëert een reis doorheen het bewustzijn als het ware.\r\n\r\nZijn favoriete onderwerpen zijn:\r\n<ul>\r\n 	<li>Law of attraction</li>\r\n 	<li>Dating is a mirror</li>\r\n 	<li>Engineer your future self</li>\r\n 	<li>Programming your mind</li>\r\n 	<li>Self love</li>\r\n 	<li>Smashing through limiting beliefs</li>\r\n 	<li>Mission in life</li>\r\n</ul>\r\n</div>\r\n</div>\r\n<div class=\"col-md-6\">\r\n<div class=\"p-3\">\r\n<h5>Wie is de organisator van het seminarie?</h5>\r\nIk ben Angelo Kostakis en merkte reeds van jongs af aan dat ik zeer geïnteresseerd ben in hoe mensen denken en hoe problemen opgelost kunnen worden wanneer je objectief weet te blijven in moeilijke situaties.\r\n\r\nIk heb mijn studie bouwkundig ingenieur afgerond maar merkte gaandeweg mijn job dat het mij meer voldoening gaf om mensen hun problemen op te lossen dan bouwkundige berekeningen te maken.\r\n\r\nIk ben begonnen om over liefde – in de vorm van poëzie – te schrijven op mijn Instagram account. Dit om mensen te raken en weer te doen geloven in het goede van de liefde, maar ook om te laten zien dat het vaak gepaard gaat met pijn.\r\n\r\n</div>\r\n</div>\r\n<div class=\"col-md-6\"><img class=\"alignnone wp-image-116 size-full\" src=\"https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/ak-1.jpg\" alt=\"\" width=\"1000\" height=\"1001\" /></div>\r\n</div>', 'The secrets of dating event', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.\r\n\r\nHet antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-10 19:38:43', '2019-04-10 19:38:43', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(122, 1, '2019-04-10 19:42:05', '2019-04-10 19:42:05', '<div class=\"row\">\r\n<div class=\"col-md-6 p-0\"><img class=\"alignnone wp-image-117 size-full\" src=\"https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/as-2.jpg\" alt=\"\" width=\"1000\" height=\"1001\" /></div>\r\n<div class=\"col-md-6 p-0\">\r\n<div class=\"p-3\">\r\n<h5>Waarom kiezen voor dit seminarie?</h5>\r\nAngelo zijn seminaries staan vol met psychologische trucjes, veel metaforen waarbij mensen zich onmiddellijk in herkennen en sluit af met een empowerment gevoel. Een mind engineer pur sang en creëert een reis doorheen het bewustzijn als het ware.\r\n\r\nZijn favoriete onderwerpen zijn:\r\n<ul>\r\n 	<li>Law of attraction</li>\r\n 	<li>Dating is a mirror</li>\r\n 	<li>Engineer your future self</li>\r\n 	<li>Programming your mind</li>\r\n 	<li>Self love</li>\r\n 	<li>Smashing through limiting beliefs</li>\r\n 	<li>Mission in life</li>\r\n</ul>\r\n</div>\r\n</div>\r\n<div class=\"col-md-6 p-0\">\r\n<div class=\"p-3\">\r\n<h5>Wie is de organisator van het seminarie?</h5>\r\nIk ben Angelo Kostakis en merkte reeds van jongs af aan dat ik zeer geïnteresseerd ben in hoe mensen denken en hoe problemen opgelost kunnen worden wanneer je objectief weet te blijven in moeilijke situaties.\r\n\r\nIk heb mijn studie bouwkundig ingenieur afgerond maar merkte gaandeweg mijn job dat het mij meer voldoening gaf om mensen hun problemen op te lossen dan bouwkundige berekeningen te maken.\r\n\r\nIk ben begonnen om over liefde – in de vorm van poëzie – te schrijven op mijn Instagram account. Dit om mensen te raken en weer te doen geloven in het goede van de liefde, maar ook om te laten zien dat het vaak gepaard gaat met pijn.\r\n\r\n</div>\r\n</div>\r\n<div class=\"col-md-6 p-0\"><img class=\"alignnone wp-image-116 size-full\" src=\"https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/ak-1.jpg\" alt=\"\" width=\"1000\" height=\"1001\" /></div>\r\n</div>', 'The secrets of dating event', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.\r\n\r\nHet antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-10 19:42:05', '2019-04-10 19:42:05', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(123, 1, '2019-04-10 19:56:46', '2019-04-10 19:56:46', '<div class=\"row\">\r\n<div class=\"col-md-6 p-0\"><img class=\"alignnone wp-image-117 size-full\" src=\"https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/as-2.jpg\" alt=\"\" width=\"1000\" height=\"1001\" /></div>\r\n<div class=\"col-md-6 p-0\">\r\n<div class=\"px-4 py-3\">\r\n<h5>Waarom kiezen voor dit seminarie?</h5>\r\nAngelo zijn seminaries staan vol met psychologische trucjes, veel metaforen waarbij mensen zich onmiddellijk in herkennen en sluit af met een empowerment gevoel. Een mind engineer pur sang en creëert een reis doorheen het bewustzijn als het ware.\r\n\r\nZijn favoriete onderwerpen zijn:\r\n<ul>\r\n 	<li>Law of attraction</li>\r\n 	<li>Dating is a mirror</li>\r\n 	<li>Engineer your future self</li>\r\n 	<li>Programming your mind</li>\r\n 	<li>Self love</li>\r\n 	<li>Smashing through limiting beliefs</li>\r\n 	<li>Mission in life</li>\r\n</ul>\r\n</div>\r\n</div>\r\n<div class=\"col-md-6 p-0\">\r\n<div class=\"px-4 py-3\">\r\n<h5>Wie is de organisator van het seminarie?</h5>\r\nIk ben Angelo Kostakis en merkte reeds van jongs af aan dat ik zeer geïnteresseerd ben in hoe mensen denken en hoe problemen opgelost kunnen worden wanneer je objectief weet te blijven in moeilijke situaties.\r\n\r\nIk heb mijn studie bouwkundig ingenieur afgerond maar merkte gaandeweg mijn job dat het mij meer voldoening gaf om mensen hun problemen op te lossen dan bouwkundige berekeningen te maken.\r\n\r\nIk ben begonnen om over liefde – in de vorm van poëzie – te schrijven op mijn Instagram account. Dit om mensen te raken en weer te doen geloven in het goede van de liefde, maar ook om te laten zien dat het vaak gepaard gaat met pijn.\r\n\r\n</div>\r\n</div>\r\n<div class=\"col-md-6 p-0\"><img class=\"alignnone wp-image-116 size-full\" src=\"https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/ak-1.jpg\" alt=\"\" width=\"1000\" height=\"1001\" /></div>\r\n</div>', 'The secrets of dating event', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.\r\n\r\nHet antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-10 19:56:46', '2019-04-10 19:56:46', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(124, 1, '2019-04-10 20:47:35', '2019-04-10 20:47:35', '<div class=\"row\">\r\n<div class=\"col-md-6 p-0\"><img class=\"alignnone wp-image-117 size-full\" src=\"https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/as-2.jpg\" alt=\"\" width=\"1000\" height=\"1001\" /></div>\r\n<div class=\"col-md-6 p-0\">\r\n<div class=\"p-5\">\r\n<h5>Waarom kiezen voor dit seminarie?</h5>\r\nAngelo zijn seminaries staan vol met psychologische trucjes, veel metaforen waarbij mensen zich onmiddellijk in herkennen en sluit af met een empowerment gevoel. Een mind engineer pur sang en creëert een reis doorheen het bewustzijn als het ware.\r\n\r\nZijn favoriete onderwerpen zijn:\r\n<ul>\r\n 	<li>Law of attraction</li>\r\n 	<li>Dating is a mirror</li>\r\n 	<li>Engineer your future self</li>\r\n 	<li>Programming your mind</li>\r\n 	<li>Self love</li>\r\n 	<li>Smashing through limiting beliefs</li>\r\n 	<li>Mission in life</li>\r\n</ul>\r\n</div>\r\n</div>\r\n<div class=\"col-md-6 p-0\">\r\n<div class=\"p-5\">\r\n<h5>Wie is de organisator van het seminarie?</h5>\r\nIk ben Angelo Kostakis en merkte reeds van jongs af aan dat ik zeer geïnteresseerd ben in hoe mensen denken en hoe problemen opgelost kunnen worden wanneer je objectief weet te blijven in moeilijke situaties.\r\n\r\nIk heb mijn studie bouwkundig ingenieur afgerond maar merkte gaandeweg mijn job dat het mij meer voldoening gaf om mensen hun problemen op te lossen dan bouwkundige berekeningen te maken.\r\n\r\nIk ben begonnen om over liefde – in de vorm van poëzie – te schrijven op mijn Instagram account. Dit om mensen te raken en weer te doen geloven in het goede van de liefde, maar ook om te laten zien dat het vaak gepaard gaat met pijn.\r\n\r\n</div>\r\n</div>\r\n<div class=\"col-md-6 p-0\"><img class=\"alignnone wp-image-116 size-full\" src=\"https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/ak-1.jpg\" alt=\"\" width=\"1000\" height=\"1001\" /></div>\r\n</div>', 'The secrets of dating event', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.\r\n\r\nHet antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2019-04-10 20:47:35', '2019-04-10 20:47:35', '', 27, 'https://wp2.dev-c01.muulabs.pl/27-revision-v1/', 0, 'revision', '', 0),
(125, 1, '2019-04-10 21:43:10', '2019-04-10 21:43:10', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', '', 'inherit', 'closed', 'closed', '', '52-revision-v1', '', '', '2019-04-10 21:43:10', '2019-04-10 21:43:10', '', 52, 'https://wp2.dev-c01.muulabs.pl/52-revision-v1/', 0, 'revision', '', 0),
(126, 1, '2019-04-10 21:44:51', '2019-04-10 21:44:51', 'Maecenas eu hendrerit sem, in interdum lacus. Curabitur diam dolor, ullamcorper in dolor vel, placerat interdum massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas in justo commodo quam molestie vehicula. Curabitur vehicula dictum tortor, ac sollicitudin neque vestibulum quis. Aliquam quis maximus neque, eget semper neque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed sem hendrerit, fringilla augue eu, luctus purus. Integer tempus arcu in tortor placerat, non imperdiet nisi sollicitudin.', 'Simple event', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.\r\n\r\nHet antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'inherit', 'closed', 'closed', '', '34-revision-v1', '', '', '2019-04-10 21:44:51', '2019-04-10 21:44:51', '', 34, 'https://wp2.dev-c01.muulabs.pl/34-revision-v1/', 0, 'revision', '', 0),
(127, 1, '2019-04-10 21:45:05', '2019-04-10 21:45:05', 'Maecenas eu hendrerit sem, in interdum lacus. Curabitur diam dolor, ullamcorper in dolor vel, placerat interdum massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas in justo commodo quam molestie vehicula. Curabitur vehicula dictum tortor, ac sollicitudin neque vestibulum quis. Aliquam quis maximus neque, eget semper neque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed sem hendrerit, fringilla augue eu, luctus purus. Integer tempus arcu in tortor placerat, non imperdiet nisi sollicitudin.', 'Aliquam sed posuere libero', 'Heb je jezelf ooit afgevraagd waarom we altijd de persoon wegduwen die we juist graag zien terwijl de rest naar ons toekomt. Waarom we altijd dezelfde type mensen aantrekken waardoor het lijkt alsof iedereen hetzelfde is? Waarom het zo moeilijk is om verder te gaan in het leven na een breuk.\r\n\r\nHet antwoord ligt eigenlijk bij onszelf, want daten is een spiegel die we ons niet graag voorhouden. Met de wet van aantrekking leer je niet alleen jouw leven naar een hoger niveau te tillen op zowel persoonlijk als zakelijk vlak, maar ook om een levenspartner aan te trekken die ons complementeert en geen energie kost.', 'inherit', 'closed', 'closed', '', '34-revision-v1', '', '', '2019-04-10 21:45:05', '2019-04-10 21:45:05', '', 34, 'https://wp2.dev-c01.muulabs.pl/34-revision-v1/', 0, 'revision', '', 0),
(128, 1, '2019-04-10 21:46:03', '2019-04-10 21:46:03', '', 'Aliquam sed posuere libero Normal Ticket', 'Maecenas in justo commodo quam molestie vehicula.', 'publish', 'closed', 'closed', '', 'aliquam-sed-posuere-libero-normal-ticket', '', '', '2019-04-10 22:57:27', '2019-04-10 22:57:27', '', 0, 'https://wp2.dev-c01.muulabs.pl/product/aliquam-sed-posuere-libero-normal-ticket/', 0, 'product', '', 0),
(129, 1, '2019-04-10 21:47:52', '2019-04-10 21:47:52', '', 'Lorem ipsum dolor sit amet', 'Nunc sed sem hendrerit, fringilla augue eu, luctus purus. Integer tempus arcu in tortor placerat, non imperdiet nisi sollicitudin.', 'inherit', 'closed', 'closed', '', '52-revision-v1', '', '', '2019-04-10 21:47:52', '2019-04-10 21:47:52', '', 52, 'https://wp2.dev-c01.muulabs.pl/52-revision-v1/', 0, 'revision', '', 0),
(130, 1, '2019-04-10 21:48:25', '2019-04-10 21:48:25', '', 'Lorem ipsum dolor sit amet Normal', '', 'publish', 'closed', 'closed', '', 'lorem-ipsum-dolor-sit-amet-normal', '', '', '2019-04-10 22:59:08', '2019-04-10 22:59:08', '', 0, 'https://wp2.dev-c01.muulabs.pl/product/lorem-ipsum-dolor-sit-amet-normal/', 0, 'product', '', 0),
(131, 1, '2019-04-10 21:48:55', '2019-04-10 21:48:55', 'Mauris hendrerit, odio sit amet suscipit pellentesque, risus enim maximus metus, a dictum ex lorem quis tellus. Nunc et orci at justo auctor tincidunt. Vestibulum elementum felis ut libero sagittis, id accumsan est rutrum. Sed non nisl nulla. Nulla mollis lorem et ex facilisis, vel vulputate turpis pulvinar. Donec at dignissim ex. Maecenas efficitur, odio vitae congue elementum, eros tellus sagittis erat, eget tristique nisl tellus vel lacus. In hac habitasse platea dictumst. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris sit amet mattis diam. Proin massa enim, scelerisque et dolor eu, consequat scelerisque urna. Phasellus sit amet leo bibendum, accumsan urna vel, eleifend lectus. Proin ipsum orci, blandit vel tortor a, varius congue purus.\r\n\r\nMaecenas eu hendrerit sem, in interdum lacus. Curabitur diam dolor, ullamcorper in dolor vel, placerat interdum massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas in justo commodo quam molestie vehicula. Curabitur vehicula dictum tortor, ac sollicitudin neque vestibulum quis. Aliquam quis maximus neque, eget semper neque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed sem hendrerit, fringilla augue eu, luctus purus. Integer tempus arcu in tortor placerat, non imperdiet nisi sollicitudin.\r\n\r\nAliquam sed posuere libero, interdum consequat orci. Nunc egestas velit sed diam aliquam ultrices. Fusce mauris erat, accumsan et tempor et, lobortis vel sapien. Praesent rutrum ut est dictum imperdiet. Etiam tempus orci turpis, eu fermentum est finibus et. In interdum ipsum et nunc tincidunt posuere. Maecenas eros purus, euismod id ex nec, ultricies tempus justo.', 'Lorem ipsum dolor sit amet', 'Nunc sed sem hendrerit, fringilla augue eu, luctus purus. Integer tempus arcu in tortor placerat, non imperdiet nisi sollicitudin.', 'inherit', 'closed', 'closed', '', '52-revision-v1', '', '', '2019-04-10 21:48:55', '2019-04-10 21:48:55', '', 52, 'https://wp2.dev-c01.muulabs.pl/52-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(132, 1, '2019-04-10 21:49:50', '2019-04-10 21:49:50', 'Mauris hendrerit, odio sit amet suscipit pellentesque, risus enim maximus metus, a dictum ex lorem quis tellus. Nunc et orci at justo auctor tincidunt. Vestibulum elementum felis ut libero sagittis, id accumsan est rutrum. Sed non nisl nulla. Nulla mollis lorem et ex facilisis, vel vulputate turpis pulvinar. Donec at dignissim ex. Maecenas efficitur, odio vitae congue elementum, eros tellus sagittis erat, eget tristique nisl tellus vel lacus. In hac habitasse platea dictumst. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris sit amet mattis diam. Proin massa enim, scelerisque et dolor eu, consequat scelerisque urna. Phasellus sit amet leo bibendum, accumsan urna vel, eleifend lectus. Proin ipsum orci, blandit vel tortor a, varius congue purus.\r\n\r\nMaecenas eu hendrerit sem, in interdum lacus. Curabitur diam dolor, ullamcorper in dolor vel, placerat interdum massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas in justo commodo quam molestie vehicula. Curabitur vehicula dictum tortor, ac sollicitudin neque vestibulum quis. Aliquam quis maximus neque, eget semper neque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed sem hendrerit, fringilla augue eu, luctus purus. Integer tempus arcu in tortor placerat, non imperdiet nisi sollicitudin.\r\n\r\nAliquam sed posuere libero, interdum consequat orci. Nunc egestas velit sed diam aliquam ultrices. Fusce mauris erat, accumsan et tempor et, lobortis vel sapien. Praesent rutrum ut est dictum imperdiet. Etiam tempus orci turpis, eu fermentum est finibus et. In interdum ipsum et nunc tincidunt posuere. Maecenas eros purus, euismod id ex nec, ultricies tempus justo.', 'Lorem ipsum dolor sit amet', 'Nunc sed sem hendrerit, fringilla augue eu, luctus purus. Integer tempus arcu in tortor placerat, non imperdiet nisi sollicitudin.', 'inherit', 'closed', 'closed', '', '52-revision-v1', '', '', '2019-04-10 21:49:50', '2019-04-10 21:49:50', '', 52, 'https://wp2.dev-c01.muulabs.pl/52-revision-v1/', 0, 'revision', '', 0),
(134, 1, '2019-04-10 22:50:26', '2019-04-10 22:50:26', '', '133 | Lorem Ipsum Ticket Normal | 1', '', 'publish', 'closed', 'closed', '', '133-lorem-ipsum-ticket-normal-1', '', '', '2019-04-10 22:50:26', '2019-04-10 22:50:26', '', 0, 'https://wp2.dev-c01.muulabs.pl/?post_type=tribe_wooticket&p=134', 0, 'tribe_wooticket', '', 0),
(135, 1, '2019-04-10 22:57:01', '2019-04-10 22:57:01', '', 'placeholder', '', 'inherit', 'open', 'closed', '', 'placeholder', '', '', '2019-04-10 22:57:01', '2019-04-10 22:57:01', '', 34, 'https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/placeholder.jpg', 0, 'attachment', 'image/jpeg', 0),
(136, 1, '2019-04-10 23:01:32', '2019-04-10 23:01:32', '', 'book', '', 'inherit', 'open', 'closed', '', 'book-2', '', '', '2019-04-10 23:01:32', '2019-04-10 23:01:32', '', 61, 'https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/03/book.jpg', 0, 'attachment', 'image/jpeg', 0),
(139, 1, '2019-04-11 01:01:40', '2019-04-11 01:01:40', ' ', '', '', 'publish', 'closed', 'closed', '', '139', '', '', '2019-06-05 20:01:23', '2019-06-05 20:01:23', '', 0, 'https://wp2.dev-c01.muulabs.pl/?p=139', 6, 'nav_menu_item', '', 0),
(140, 1, '2019-04-11 01:10:17', '2019-04-11 01:10:17', '', 'Order &ndash; April 11, 2019 @ 01:10 AM', '', 'wc-cancelled', 'open', 'closed', 'wc_order_qUj0KlqS4dt8e', 'order-apr-11-2019-0110-am', '', '', '2019-05-20 05:01:32', '2019-05-20 05:01:32', '', 0, 'https://wp2.dev-c01.muulabs.pl/?post_type=shop_order&#038;p=140', 0, 'shop_order', '', 1),
(143, 1, '2019-05-20 17:53:50', '2019-05-20 17:53:50', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"page_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:10:\"front_page\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Social links', 'social-links', 'publish', 'closed', 'closed', '', 'group_5ce2e98b1d27a', '', '', '2019-05-20 18:55:41', '2019-05-20 18:55:41', '', 0, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field-group&#038;p=143', 0, 'acf-field-group', '', 0),
(144, 1, '2019-05-20 17:53:50', '2019-05-20 17:53:50', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'facebook', 'facebook', 'publish', 'closed', 'closed', '', 'field_5ce2e990dff16', '', '', '2019-05-20 17:53:50', '2019-05-20 17:53:50', '', 143, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field&p=144', 0, 'acf-field', '', 0),
(145, 1, '2019-05-20 17:54:39', '2019-05-20 17:54:39', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'twitter', 'twitter', 'publish', 'closed', 'closed', '', 'field_5ce2e9be05643', '', '', '2019-05-20 17:54:39', '2019-05-20 17:54:39', '', 143, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field&p=145', 1, 'acf-field', '', 0),
(146, 1, '2019-05-20 17:54:39', '2019-05-20 17:54:39', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'pinterest', 'pinterest', 'publish', 'closed', 'closed', '', 'field_5ce2e9cd05644', '', '', '2019-05-20 17:54:39', '2019-05-20 17:54:39', '', 143, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field&p=146', 2, 'acf-field', '', 0),
(147, 1, '2019-05-20 17:54:39', '2019-05-20 17:54:39', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'instagram', 'instagram', 'publish', 'closed', 'closed', '', 'field_5ce2e9d905645', '', '', '2019-05-20 17:54:39', '2019-05-20 17:54:39', '', 143, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field&p=147', 3, 'acf-field', '', 0),
(148, 1, '2019-05-20 18:57:25', '2019-05-20 18:57:25', 'Vestibulum mauris sapien, maximus non porttitor blandit, feugiat eget nisi. Nunc nisi leo, blandit ac ipsum eget, bibendum pulvinar lacus. Maecenas aliquet vel elit quis malesuada. Sed et erat ligula. Etiam in diam ac sapien mattis scelerisque.\n\nFusce rhoncus, odio pulvinar consequat blandit, magna nisl venenatis massa, id varius erat libero sollicitudin velit.', 'Angelo Kostakis', '', 'inherit', 'closed', 'closed', '', '15-revision-v1', '', '', '2019-05-20 18:57:25', '2019-05-20 18:57:25', '', 15, 'https://wp2.dev-c01.muulabs.pl/15-revision-v1/', 0, 'revision', '', 0),
(149, 1, '2019-07-23 17:49:10', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-07-23 17:49:10', '0000-00-00 00:00:00', '', 0, 'https://wp2.dev-c01.muulabs.pl/?p=149', 0, 'post', '', 0),
(150, 1, '2019-07-23 17:49:11', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-07-23 17:49:11', '0000-00-00 00:00:00', '', 0, 'https://wp2.dev-c01.muulabs.pl/?p=150', 0, 'post', '', 0),
(151, 1, '2019-07-23 18:33:45', '2019-07-23 18:33:45', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"page_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:10:\"front_page\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Testimonials', 'testimonials', 'publish', 'closed', 'closed', '', 'group_5d37518312bf2', '', '', '2019-07-23 18:34:57', '2019-07-23 18:34:57', '', 0, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field-group&#038;p=151', 0, 'acf-field-group', '', 0),
(152, 1, '2019-07-23 18:33:45', '2019-07-23 18:33:45', 'a:7:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:6:\"layout\";s:5:\"block\";s:10:\"sub_fields\";a:0:{}}', 'Testimonial 1', 'testimonial_01', 'publish', 'closed', 'closed', '', 'field_5d37518590399', '', '', '2019-07-23 18:33:45', '2019-07-23 18:33:45', '', 151, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field&p=152', 0, 'acf-field', '', 0),
(153, 1, '2019-07-23 18:33:45', '2019-07-23 18:33:45', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image', 'testimonial_01_image', 'publish', 'closed', 'closed', '', 'field_5d3752ba9039a', '', '', '2019-07-23 18:33:45', '2019-07-23 18:33:45', '', 152, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field&p=153', 0, 'acf-field', '', 0),
(154, 1, '2019-07-23 18:33:45', '2019-07-23 18:33:45', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Testimonial', 'testimonial_01_comment', 'publish', 'closed', 'closed', '', 'field_5d3752d39039b', '', '', '2019-07-23 18:33:45', '2019-07-23 18:33:45', '', 152, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field&p=154', 1, 'acf-field', '', 0),
(155, 1, '2019-07-23 18:33:45', '2019-07-23 18:33:45', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Name', 'testimonial_01_name', 'publish', 'closed', 'closed', '', 'field_5d3752f49039c', '', '', '2019-07-23 18:33:45', '2019-07-23 18:33:45', '', 152, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field&p=155', 2, 'acf-field', '', 0),
(156, 1, '2019-07-23 18:33:45', '2019-07-23 18:33:45', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'testimonial_01_title', 'publish', 'closed', 'closed', '', 'field_5d3752fd9039d', '', '', '2019-07-23 18:33:45', '2019-07-23 18:33:45', '', 152, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field&p=156', 3, 'acf-field', '', 0),
(157, 1, '2019-07-23 18:34:42', '2019-07-23 18:34:42', 'a:7:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:6:\"layout\";s:5:\"block\";s:10:\"sub_fields\";a:0:{}}', 'Testimonial 2', 'testimonial_02', 'publish', 'closed', 'closed', '', 'field_5d375310bd395', '', '', '2019-07-23 18:34:57', '2019-07-23 18:34:57', '', 151, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field&#038;p=157', 1, 'acf-field', '', 0),
(158, 1, '2019-07-23 18:34:42', '2019-07-23 18:34:42', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:0:\"\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:0:\"\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image', 'testimonial_02_image', 'publish', 'closed', 'closed', '', 'field_5d375310bd396', '', '', '2019-07-23 18:34:42', '2019-07-23 18:34:42', '', 157, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field&p=158', 0, 'acf-field', '', 0),
(159, 1, '2019-07-23 18:34:42', '2019-07-23 18:34:42', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Testimonial', 'testimonial_02_comment', 'publish', 'closed', 'closed', '', 'field_5d375310bd397', '', '', '2019-07-23 18:34:42', '2019-07-23 18:34:42', '', 157, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field&p=159', 1, 'acf-field', '', 0),
(160, 1, '2019-07-23 18:34:42', '2019-07-23 18:34:42', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Name', 'testimonial_02_name', 'publish', 'closed', 'closed', '', 'field_5d375310bd398', '', '', '2019-07-23 18:34:42', '2019-07-23 18:34:42', '', 157, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field&p=160', 2, 'acf-field', '', 0),
(161, 1, '2019-07-23 18:34:42', '2019-07-23 18:34:42', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'testimonial_02_title', 'publish', 'closed', 'closed', '', 'field_5d375310bd399', '', '', '2019-07-23 18:34:42', '2019-07-23 18:34:42', '', 157, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field&p=161', 3, 'acf-field', '', 0),
(162, 1, '2019-07-23 18:34:42', '2019-07-23 18:34:42', 'a:7:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:6:\"layout\";s:5:\"block\";s:10:\"sub_fields\";a:0:{}}', 'Testimonial 3', 'testimonial_03', 'publish', 'closed', 'closed', '', 'field_5d37530dbd390', '', '', '2019-07-23 18:34:57', '2019-07-23 18:34:57', '', 151, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field&#038;p=162', 2, 'acf-field', '', 0),
(163, 1, '2019-07-23 18:34:42', '2019-07-23 18:34:42', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image', 'testimonial_03_image', 'publish', 'closed', 'closed', '', 'field_5d37530dbd391', '', '', '2019-07-23 18:34:42', '2019-07-23 18:34:42', '', 162, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field&p=163', 0, 'acf-field', '', 0),
(164, 1, '2019-07-23 18:34:42', '2019-07-23 18:34:42', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Testimonial', 'testimonial_03_comment', 'publish', 'closed', 'closed', '', 'field_5d37530dbd392', '', '', '2019-07-23 18:34:42', '2019-07-23 18:34:42', '', 162, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field&p=164', 1, 'acf-field', '', 0),
(165, 1, '2019-07-23 18:34:42', '2019-07-23 18:34:42', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Name', 'testimonial_03_name', 'publish', 'closed', 'closed', '', 'field_5d37530dbd393', '', '', '2019-07-23 18:34:42', '2019-07-23 18:34:42', '', 162, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field&p=165', 2, 'acf-field', '', 0),
(166, 1, '2019-07-23 18:34:42', '2019-07-23 18:34:42', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'testimonial_03_title', 'publish', 'closed', 'closed', '', 'field_5d37530dbd394', '', '', '2019-07-23 18:34:42', '2019-07-23 18:34:42', '', 162, 'https://wp2.dev-c01.muulabs.pl/?post_type=acf-field&p=166', 3, 'acf-field', '', 0),
(167, 1, '2019-07-23 18:41:55', '2019-07-23 18:41:55', '', 'Layer 1', '', 'inherit', 'closed', 'closed', '', 'layer-1', '', '', '2019-07-23 18:41:55', '2019-07-23 18:41:55', '', 15, 'https://wp2.dev-c01.muulabs.pl/wp-content/uploads/2019/07/Layer-1.png', 0, 'attachment', 'image/png', 0),
(168, 1, '2019-07-23 18:42:48', '2019-07-23 18:42:48', 'Vestibulum mauris sapien, maximus non porttitor blandit, feugiat eget nisi. Nunc nisi leo, blandit ac ipsum eget, bibendum pulvinar lacus. Maecenas aliquet vel elit quis malesuada. Sed et erat ligula. Etiam in diam ac sapien mattis scelerisque.\n\nFusce rhoncus, odio pulvinar consequat blandit, magna nisl venenatis massa, id varius erat libero sollicitudin velit.', 'Angelo Kostakis', '', 'inherit', 'closed', 'closed', '', '15-revision-v1', '', '', '2019-07-23 18:42:48', '2019-07-23 18:42:48', '', 15, 'https://wp2.dev-c01.muulabs.pl/15-revision-v1/', 0, 'revision', '', 0),
(169, 1, '2019-07-23 19:02:46', '2019-07-23 19:02:46', 'Vestibulum mauris sapien, maximus non porttitor blandit, feugiat eget nisi. Nunc nisi leo, blandit ac ipsum eget, bibendum pulvinar lacus. Maecenas aliquet vel elit quis malesuada. Sed et erat ligula. Etiam in diam ac sapien mattis scelerisque.\n\nFusce rhoncus, odio pulvinar consequat blandit, magna nisl venenatis massa, id varius erat libero sollicitudin velit.', 'Angelo Kostakis', '', 'inherit', 'closed', 'closed', '', '15-revision-v1', '', '', '2019-07-23 19:02:46', '2019-07-23 19:02:46', '', 15, 'https://wp2.dev-c01.muulabs.pl/15-revision-v1/', 0, 'revision', '', 0),
(170, 1, '2019-07-23 19:17:39', '2019-07-23 19:17:39', 'Vestibulum mauris sapien, maximus non porttitor blandit, feugiat eget nisi. Nunc nisi leo, blandit ac ipsum eget, bibendum pulvinar lacus. Maecenas aliquet vel elit quis malesuada. Sed et erat ligula. Etiam in diam ac sapien mattis scelerisque.\n\nFusce rhoncus, odio pulvinar consequat blandit, magna nisl venenatis massa, id varius erat libero sollicitudin velit.', 'Angelo Kostakis', '', 'inherit', 'closed', 'closed', '', '15-revision-v1', '', '', '2019-07-23 19:17:39', '2019-07-23 19:17:39', '', 15, 'https://wp2.dev-c01.muulabs.pl/15-revision-v1/', 0, 'revision', '', 0),
(171, 1, '2019-07-23 19:21:23', '2019-07-23 19:21:23', 'Vestibulum mauris sapien, maximus non porttitor blandit, feugiat eget nisi. Nunc nisi leo, blandit ac ipsum eget, bibendum pulvinar lacus. Maecenas aliquet vel elit quis malesuada. Sed et erat ligula. Etiam in diam ac sapien mattis scelerisque.\n\nFusce rhoncus, odio pulvinar consequat blandit, magna nisl venenatis massa, id varius erat libero sollicitudin velit.', 'Angelo Kostakis', '', 'inherit', 'closed', 'closed', '', '15-revision-v1', '', '', '2019-07-23 19:21:23', '2019-07-23 19:21:23', '', 15, 'https://wp2.dev-c01.muulabs.pl/15-revision-v1/', 0, 'revision', '', 0),
(172, 1, '2019-07-28 10:11:41', '2019-07-28 10:11:41', '<div class=\"free-form\">\r\n  <div class=\"free-form__input\">\r\n    <label class=\"labels-tag\">Naam</label>\r\n    [text* text-181 class:inputs-text placeholder \"naam\"]\r\n  </div>\r\n  <div class=\"free-form__input\">\r\n    <label class=\"labels-tag\">E-mailadres</label>\r\n    [email* email-548 class:inputs-text placeholder \"e-mailadres\"]\r\n  </div>\r\n  <div class=\"free-form__input\">\r\n    <label class=\"labels-tag\">Telefoonnummer</label>\r\n    [tel* tel-456 class:inputs-text placeholder \"telefoonnummer\"]\r\n  </div>\r\n  <div class=\"free-form__input\">\r\n    <label class=\"labels-tag\"> Bericht</label>\r\n    [textarea* textarea-404 class:inputs-text placeholder \"bericht\"]\r\n  </div>\r\n  <div class=\"free-form__input\">\r\n    <label class=\"labels-tag\">Website</label>\r\n    [text text-182 class:inputs-text placeholder \"website\"]\r\n  </div>\r\n  <div class=\"free-form__input\">\r\n    <label class=\"labels-tag\">Event</label>\r\n    [text text-183 class:inputs-text placeholder \"event\"]\r\n  </div>\r\n  <div class=\"free-form__input\">\r\n    <label class=\"labels-tag\">Event address</label>\r\n    [text* text-184 class:inputs-text placeholder \"event address\"]\r\n  </div>\r\n  <div class=\"free-form__input\">\r\n    <label class=\"labels-tag\">Date of event</label>\r\n    [date date-824 class:inputs-text placeholder \"date of event\"]\r\n  </div>\r\n  <div class=\"free-form__input\">\r\n    <label class=\"labels-tag\">Duration of speach</label>\r\n    [text* text-185 class:inputs-text placeholder \"duration of speach\"]\r\n  </div>\r\n  <div class=\"free-form__input\">\r\n    <label class=\"labels-tag\">Company name</label>\r\n    [text* text-186 class:inputs-text placeholder \"company name\"]\r\n  </div>\r\n  <div class=\"free-form__input\">\r\n    <label class=\"labels-tag\">VAT</label>\r\n    [text* text-187 class:inputs-text placeholder \"vat\"]\r\n  </div>\r\n[submit class:btn class:btn-price class:btn-primary class:mr-2 class:mb-2 class:mt-4 class:free-form__button \"Send\"]\r\n</div>\n1\nMindEngineer \"[text-181]\"\nMindEngineer <dzianis.makeichyk@muulabs.pl>\ndzianis.makeichyk@muulabs.pl\nFrom: [text-181] <[email-548]>\r\nTelefoonnummer: [tel-456]\r\nBericht: [textarea-404]\r\nWebsite: [text-182]\r\nEvent: [text-183]\r\nEvent address: [text-184]\r\nDate of event: [date-824]\r\nDuration of speach: [text-185]\r\nCompany name: [text-186]\r\nVAT: [text-187]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on MindEngineer (https://wp2.dev-c01.muulabs.pl)\n\n\n\n\n\nMindEngineer \"[your-subject]\"\nMindEngineer <tobiaszkochanski@gmail.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on MindEngineer (https://wp2.dev-c01.muulabs.pl)\nReply-To: tobiaszkochanski@gmail.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Hur mij als spreker', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2019-07-28 12:04:34', '2019-07-28 12:04:34', '', 0, 'https://wp2.dev-c01.muulabs.pl/?post_type=wpcf7_contact_form&#038;p=172', 0, 'wpcf7_contact_form', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_queue`
--

CREATE TABLE `wp_queue` (
  `id` bigint(20) NOT NULL,
  `job` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `locked_at` datetime DEFAULT NULL,
  `available_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_termmeta`
--

INSERT INTO `wp_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(1, 16, 'order', '0'),
(2, 16, 'product_count_product_cat', '2'),
(3, 15, 'product_count_product_cat', '1'),
(4, 17, 'order', '0'),
(5, 17, 'product_count_product_cat', '0'),
(6, 17, 'display_type', ''),
(7, 17, 'thumbnail_id', '0');

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'exclude-from-search', 'exclude-from-search', 0),
(7, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(8, 'featured', 'featured', 0),
(9, 'outofstock', 'outofstock', 0),
(10, 'rated-1', 'rated-1', 0),
(11, 'rated-2', 'rated-2', 0),
(12, 'rated-3', 'rated-3', 0),
(13, 'rated-4', 'rated-4', 0),
(14, 'rated-5', 'rated-5', 0),
(15, 'Uncategorized', 'uncategorized', 0),
(16, 'Ebooks', 'ebooks', 0),
(17, 'Events', 'events', 0),
(18, 'Main', 'main', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(29, 2, 0),
(29, 6, 0),
(29, 7, 0),
(29, 17, 0),
(30, 1, 0),
(39, 18, 0),
(40, 18, 0),
(42, 18, 0),
(44, 18, 0),
(61, 2, 0),
(61, 16, 0),
(62, 2, 0),
(62, 16, 0),
(63, 18, 0),
(68, 2, 0),
(68, 15, 0),
(102, 1, 0),
(128, 2, 0),
(128, 6, 0),
(128, 7, 0),
(130, 2, 0),
(130, 6, 0),
(130, 7, 0),
(139, 18, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 3),
(2, 2, 'product_type', '', 0, 6),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 0),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_visibility', '', 0, 3),
(7, 7, 'product_visibility', '', 0, 3),
(8, 8, 'product_visibility', '', 0, 0),
(9, 9, 'product_visibility', '', 0, 0),
(10, 10, 'product_visibility', '', 0, 0),
(11, 11, 'product_visibility', '', 0, 0),
(12, 12, 'product_visibility', '', 0, 0),
(13, 13, 'product_visibility', '', 0, 0),
(14, 14, 'product_visibility', '', 0, 0),
(15, 15, 'product_cat', '', 0, 1),
(16, 16, 'product_cat', '', 0, 2),
(17, 17, 'product_cat', '', 0, 1),
(18, 18, 'nav_menu', '', 0, 6);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'mindengineer'),
(2, 1, 'first_name', 'sadsadsad'),
(3, 1, 'last_name', 'sadasdad'),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy,theme_editor_notice'),
(15, 1, 'show_welcome_panel', '1'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '150'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:10:\"172.30.0.0\";}'),
(20, 1, 'wc_last_active', '1564272000'),
(21, 1, 'wp_user-settings', 'libraryContent=browse&editor=html'),
(22, 1, 'wp_user-settings-time', '1559764241'),
(23, 1, 'meta-box-order_tribe_events', 'a:3:{s:4:\"side\";s:86:\"submitdiv,tagsdiv-post_tag,tribe_events_catdiv,tribe_events_event_options,postimagediv\";s:6:\"normal\";s:86:\"tribe_events_event_details,tribetickets,postexcerpt,commentstatusdiv,slugdiv,authordiv\";s:8:\"advanced\";s:0:\"\";}'),
(24, 1, 'screen_layout_tribe_events', '2'),
(25, 1, 'jetpack_tracks_anon_id', 'jetpack:0N402A8VuJhQ3j1fgkpgv5T7'),
(28, 1, 'dismissed_no_secure_connection_notice', '1'),
(31, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:\"link-target\";i:1;s:15:\"title-attribute\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";}'),
(32, 1, 'metaboxhidden_nav-menus', 'a:5:{i:0;s:21:\"add-post-type-product\";i:1;s:26:\"add-post-type-tribe_events\";i:2;s:12:\"add-post_tag\";i:3;s:15:\"add-product_cat\";i:4;s:15:\"add-product_tag\";}'),
(33, 1, 'tribe_setDefaultNavMenuBoxes', '1'),
(34, 1, 'nav_menu_recently_edited', '18'),
(39, 1, 'closedpostboxes_tribe_events', 'a:0:{}'),
(40, 1, 'metaboxhidden_tribe_events', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(43, 1, 'meta-box-order_product', 'a:3:{s:4:\"side\";s:84:\"submitdiv,product_catdiv,tagsdiv-product_tag,postimagediv,woocommerce-product-images\";s:6:\"normal\";s:55:\"woocommerce-product-data,postcustom,slugdiv,postexcerpt\";s:8:\"advanced\";s:0:\"\";}'),
(44, 1, 'screen_layout_product', '2'),
(45, 1, 'last_update', '1554945017'),
(46, 1, 'billing_first_name', 'Tobiasz'),
(47, 1, 'billing_last_name', 'Kochański'),
(48, 1, 'billing_country', 'NL'),
(49, 1, 'billing_email', 'tobiaszkochanski@gmail.com'),
(50, 1, 'shipping_method', ''),
(53, 1, 'billing_address_1', ''),
(54, 1, 'billing_city', ''),
(55, 1, 'billing_postcode', ''),
(56, 1, 'billing_phone', ''),
(59, 1, 'paying_customer', '1'),
(65, 1, 'session_tokens', 'a:1:{s:64:\"c17b492323c6d210602b0203e4efbde6da0e5eef12dc09e9920f62124881224f\";a:4:{s:10:\"expiration\";i:1564481429;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36\";s:5:\"login\";i:1564308629;}}'),
(77, 1, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'mindengineer', '$P$BIwX.J6eOhCwjWlZnE0pNH0HlLrJnZ.', 'mindengineer', 'tobiaszkochanski@gmail.com', '', '2019-03-18 22:30:56', '', 0, 'mindengineer');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_download_log`
--

CREATE TABLE `wp_wc_download_log` (
  `download_log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_ip_address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_webhooks`
--

CREATE TABLE `wp_wc_webhooks` (
  `webhook_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivery_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `topic` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_api_keys`
--

CREATE TABLE `wp_woocommerce_api_keys` (
  `key_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_attribute_taxonomies`
--

CREATE TABLE `wp_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_label` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_orderby` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_downloadable_product_permissions`
--

CREATE TABLE `wp_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `download_id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_log`
--

CREATE TABLE `wp_woocommerce_log` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `context` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_itemmeta`
--

CREATE TABLE `wp_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_order_itemmeta`
--

INSERT INTO `wp_woocommerce_order_itemmeta` (`meta_id`, `order_item_id`, `meta_key`, `meta_value`) VALUES
(194, 20, '_product_id', '68'),
(195, 20, '_variation_id', '0'),
(196, 20, '_qty', '1'),
(197, 20, '_tax_class', ''),
(198, 20, '_line_subtotal', '100'),
(199, 20, '_line_subtotal_tax', '0'),
(200, 20, '_line_total', '100'),
(201, 20, '_line_tax', '0'),
(202, 20, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(203, 20, 'contact_email', 'tobiasz@t-k.tech'),
(204, 20, 'contact_phone', '506950914'),
(205, 20, 'contact_message', 'sfsdfdsf');

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_items`
--

CREATE TABLE `wp_woocommerce_order_items` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_order_items`
--

INSERT INTO `wp_woocommerce_order_items` (`order_item_id`, `order_item_name`, `order_item_type`, `order_id`) VALUES
(20, 'Emergency Contact', 'line_item', 140);

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_payment_tokenmeta`
--

CREATE TABLE `wp_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_payment_tokens`
--

CREATE TABLE `wp_woocommerce_payment_tokens` (
  `token_id` bigint(20) UNSIGNED NOT NULL,
  `gateway_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_sessions`
--

CREATE TABLE `wp_woocommerce_sessions` (
  `session_id` bigint(20) UNSIGNED NOT NULL,
  `session_key` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_sessions`
--

INSERT INTO `wp_woocommerce_sessions` (`session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(517, '1', 'a:7:{s:4:\"cart\";s:6:\"a:0:{}\";s:11:\"cart_totals\";s:367:\"a:15:{s:8:\"subtotal\";i:0;s:12:\"subtotal_tax\";i:0;s:14:\"shipping_total\";i:0;s:12:\"shipping_tax\";i:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";i:0;s:12:\"discount_tax\";i:0;s:19:\"cart_contents_total\";i:0;s:17:\"cart_contents_tax\";i:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";i:0;s:7:\"fee_tax\";i:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";i:0;s:9:\"total_tax\";i:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:6:\"a:0:{}\";s:8:\"customer\";s:758:\"a:26:{s:2:\"id\";s:1:\"1\";s:13:\"date_modified\";s:25:\"2019-04-11T01:10:17+00:00\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:0:\"\";s:7:\"address\";s:0:\"\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:0:\"\";s:7:\"country\";s:2:\"NL\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:0:\"\";s:16:\"shipping_country\";s:2:\"NL\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:7:\"Tobiasz\";s:9:\"last_name\";s:10:\"Kochański\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:5:\"email\";s:26:\"tobiaszkochanski@gmail.com\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";}', 1564481468);

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zones`
--

CREATE TABLE `wp_woocommerce_shipping_zones` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `zone_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zone_locations`
--

CREATE TABLE `wp_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zone_methods`
--

CREATE TABLE `wp_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL,
  `method_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rates`
--

CREATE TABLE `wp_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_country` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rate_locations`
--

CREATE TABLE `wp_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10)),
  ADD KEY `woo_idx_comment_type` (`comment_type`);

--
-- Indexes for table `wp_failed_jobs`
--
ALTER TABLE `wp_failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_queue`
--
ALTER TABLE `wp_queue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  ADD PRIMARY KEY (`download_log_id`),
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `wp_wc_webhooks`
--
ALTER TABLE `wp_wc_webhooks`
  ADD PRIMARY KEY (`webhook_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(20));

--
-- Indexes for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `level` (`level`);

--
-- Indexes for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD UNIQUE KEY `session_key` (`session_key`);

--
-- Indexes for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Indexes for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`),
  ADD KEY `tax_rate_state` (`tax_rate_state`(2)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(10)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `wp_failed_jobs`
--
ALTER TABLE `wp_failed_jobs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2734;
--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2168;
--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=173;
--
-- AUTO_INCREMENT for table `wp_queue`
--
ALTER TABLE `wp_queue`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  MODIFY `download_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_wc_webhooks`
--
ALTER TABLE `wp_wc_webhooks`
  MODIFY `webhook_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206;
--
-- AUTO_INCREMENT for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  MODIFY `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=519;
--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  ADD CONSTRAINT `fk_wp_wc_download_log_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `wp_woocommerce_downloadable_product_permissions` (`permission_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
