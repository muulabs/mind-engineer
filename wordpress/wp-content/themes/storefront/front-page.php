<?php

get_header();
the_post();

global $post;

$ebooks = wc_get_products( [ 'limit' => 5, 'orderby' => 'date', 'order' => 'DESC', 'downloadable' => true ] );
$events = tribe_get_events( [ 'posts_per_page' => 5, ] );
?>

	<div class="hero">
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-10">
					<h2 class="hero-title"><?php the_title(); ?></h2>
					<div class="lead">
						<?php the_content(); ?>
						<div class="sign"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="section-info" id="events">
		<div class="container">
			<div class="row">
				<div class="col-lg-7 text-md-left text-center">
					<?php
					foreach ( $events as $post )
					{
						setup_postdata( $post );
						break;
					}
					?>

					<h3 class="pb-5"><?php the_title() ?></h3>
					<div class="mb-4 lead">
						<?php the_excerpt() ?>
						<time><?php echo tribe_get_start_date( $post ) ?></time>
						<address><?php echo tribe_get_venue( $post->ID ) ?></address>
					</div>
					<div class="text-lg-left text-center">
						<a href="<?php echo tribe_get_event_link( $post->ID ) ?>" class="btn btn-price btn-primary mr-2 mb-2">
							SCHRIJF JE NU IN <span class="badge badge-primary"><?php echo tribe_get_cost( $post->ID, true ) ?></span>
						</a>
						<a href="<?php echo tribe_get_event_link( $post->ID ) ?>" class="btn btn-outline-secondary mb-2">ONTDEK HET PROGRAMMA</a>
					</div>
				</div>
				<div class="col-lg-5">
					<?php if ( has_post_thumbnail() ) the_post_thumbnail( 'full' ); ?>
				</div>
			</div>
		</div>

		<div class="m-list mt-6 text-md-left text-center">
			<h5 class="m-list-title">Other events</h5>
			<?php foreach ( array_slice( $events, 1 ) as $post ): ?>
				<div class="m-list-item">
					<div class="container">
						<div>
							<h6><?php echo $post->post_title ?></h6>
							<p><?php echo tribe_get_start_date( $post ) ?></p>
						</div>
						<a href="<?php echo tribe_get_event_link( $post->ID ) ?>" class="btn btn-sm btn-outline-secondary mt-2 mt-md-0">
							meer informatie
						</a>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<div class="m-list mt-6 text-md-left text-center">
			<h3 class="m-list-title pb-5">Testimonials</h3>
			<div class="container">
				<ul class="testimonials">
					<?php if (have_rows('testimonial_01', 15)): ?>
						<?php while( have_rows('testimonial_01', 15)): the_row() ?>
							<?php if (get_field('testimonial_01', 15)['testimonial_01_image']): ?>
							<li class="testimonial">
								<div class="testimonial__wrapper">
									<div class="testimonial__image">
										<img src="<?php the_sub_field('testimonial_01_image', 15); ?>" alt="">
									</div>
									<i class="testimonial__comment">
										<?php the_sub_field('testimonial_01_comment', 15); ?>
									</i>
									<h5 class="testimonial__name">
										<?php the_sub_field('testimonial_01_name', 15); ?>
									</h5>
									<p class="testimonial__title">
										<?php the_sub_field('testimonial_01_title', 15); ?>
									</p>
								</div>
							</li>
							<?php endif; ?>
						<?php endwhile; ?>
					<?php endif; ?>
					<?php if (have_rows('testimonial_02', 15)): ?>
						<?php while( have_rows('testimonial_02', 15)): the_row() ?>
							<?php if (get_field('testimonial_02', 15)['testimonial_02_image']): ?>
								<li class="testimonial">
									<div class="testimonial__wrapper">
										<div class="testimonial__image">
											<img src="<?php the_sub_field('testimonial_02_image', 15); ?>" alt="">
										</div>
										<i class="testimonial__comment">
											<?php the_sub_field('testimonial_02_comment', 15); ?>
										</i>
										<h5 class="testimonial__name">
											<?php the_sub_field('testimonial_02_name', 15); ?>
										</h5>
										<p class="testimonial__title">
											<?php the_sub_field('testimonial_02_title', 15); ?>
										</p>
									</div>
								</li>
							<?php endif; ?>
						<?php endwhile; ?>
					<?php endif; ?>
					<?php if (have_rows('testimonial_03', 15)): ?>
						<?php while( have_rows('testimonial_03', 15)): the_row() ?>
							<?php if (get_field('testimonial_03', 15)['testimonial_03_image']): ?>
								<li class="testimonial">
									<div class="testimonial__wrapper">
										<div class="testimonial__image">
											<img src="<?php the_sub_field('testimonial_03_image', 15); ?>" alt="">
										</div>
										<i class="testimonial__comment">
											<?php the_sub_field('testimonial_03_comment', 15); ?>
										</i>
										<h5 class="testimonial__name">
											<?php the_sub_field('testimonial_03_name', 15); ?>
										</h5>
										<p class="testimonial__title">
											<?php the_sub_field('testimonial_03_title', 15); ?>
										</p>
									</div>
								</li>
							<?php endif; ?>
						<?php endwhile; ?>
					<?php endif; ?>
				</ul>
			</div>
		</div>
	</div>


	<div class="section-info dark" id="ebooks">
		<?php
		foreach ( $ebooks as $post )
		{
			setup_postdata( $post );
			break;
		}
		?>
		<div class="container">
			<div class="row position-relative text-md-left text-center">
				<div class="book-cover">
					<img src="<?php echo get_the_post_thumbnail_url( $post->get_id() ) ?>" class="img-fluid">
				</div>
				<div class="ml-auto col-md-6">
					<h3 class="pb-5"><?php echo $post->get_title(); ?></h3>
					<div class="mb-4 lead">
						<?php echo wpautop( $post->get_description() ) ?>
					</div>
					<a href="<?php echo get_permalink( $post->get_id() ) ?>" class="btn btn-primary">Koop een e-boek</a>
					<span class="btn btn-link"><?php echo wc_price( $post->get_price()) ?></span>
				</div>
			</div>
		</div>

		<div class="m-list dark mt-7 text-md-left text-center">
			<h5 class="m-list-title">Other books</h5>
			<?php foreach ( array_slice( $ebooks, 1 ) as $book ) : ?>

			<div class="m-list-item">
				<div class="container">
					<div>
						<h6><?php echo $book->get_title(); ?></h6>
						<p><?php echo $book->get_date_created()->format ('d M, Y'); ?></p>
					</div>
					<a href="<?php echo get_permalink( $book->get_id() ) ?>" class="btn btn-sm btn-outline-secondary mt-2 mt-md-0">meer informatie</a>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>

	<div class="pt-md-6 pt-5 text-center">
		<div class="container" id="contact">
			<div class="row justify-content-center">
				<div class="col-lg-8">
					<?php echo do_shortcode('[product_page id="68"]'); ?>
					<?php echo do_shortcode('[contact-form-7 id="172" title="Hur mij als spreker"]'); ?>
				</div>
			</div>
		</div>
	</div>

<?php
get_footer();
