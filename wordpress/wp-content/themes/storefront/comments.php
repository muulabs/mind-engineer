<?php
/**
 * The template for displaying comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package storefront
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() )
{
	return;
}
?>

<!-- <div id="comments" class="mt-5" aria-label="<?php esc_html_e( 'Post Comments', 'storefront' ); ?>">

	<h5 class="comments-title"><?php esc_html_e( 'Comments', 'storefront' ); ?></h5>

	<?php
	if ( have_comments() ) :
		?>
		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through. ?>
		<nav id="comment-nav-above" class="comment-navigation" role="navigation" aria-label="<?php esc_html_e( 'Comment Navigation Above', 'storefront' ); ?>">
			<span class="screen-reader-text"><?php esc_html_e( 'Comment navigation', 'storefront' ); ?></span>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'storefront' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'storefront' ) ); ?></div>
		</nav>
	<?php endif; // Check for comment navigation.
		?>

		<ol class="comment-list">
			<?php
			wp_list_comments(
				[
					'style'      => 'ol',
					'short_ping' => true,
					'callback'   => 'storefront_comment',
				]
			);
			?>
		</ol>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through. ?>
		<nav id="comment-nav-below" class="comment-navigation" role="navigation" aria-label="<?php esc_html_e( 'Comment Navigation Below', 'storefront' ); ?>">
			<span class="screen-reader-text"><?php esc_html_e( 'Comment navigation', 'storefront' ); ?></span>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'storefront' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'storefront' ) ); ?></div>
		</nav>
	<?php
	endif; // Check for comment navigation.

	endif;

	if ( !comments_open() && 0 !== intval( get_comments_number() ) && post_type_supports( get_post_type(), 'comments' ) ) :
		?>
		<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'storefront' ); ?></p>
	<?php
	endif;

	$args = apply_filters(
		'storefront_comment_form_args', [
			'title_reply_before' => '<span id="reply-title" class="gamma comment-reply-title">',
			'title_reply_after'  => '</span>',
			'comment_field'      => '<div class="form-group col-12"><textarea id="comment" name="comment" class="form-control" rows="1" maxlength="65525" required="required" placeholder="Join the discussion..."></textarea></div>',
			'comment_notes_before' => '',
			'title_reply'          => '',
			'submit_field'         => '<div class="w-100">%1$s %2$s</div>',
			'class_form'         => 'form-row',
			'class_submit'       => 'btn btn-primary mx-auto d-block w-auto',
			'logged_in_as' => '',
			'fields'             => [
				'author' => '<div class="form-group col-md-6"><input placeholder="' . __( 'Name' ) . '" class="form-control" id="author" name="author" type="text" value="' . esc_attr( $commenter[ 'comment_author' ] ) . '"/></div>',
				'email'  => '<div class="form-group col-md-6"><input placeholder="' . __( 'Email' ) . '" class="form-control" id="email" name="email" type="text" value="' . esc_attr( $commenter[ 'comment_author_email' ] ) . '"/></div>',
			]
		]

	);

	comment_form( $args );
	?>

</div>

