<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> data-spy="scroll" data-target="#main-nav" data-offset="0">

<?php if(!is_cart() && !is_checkout()): ?>
<div id="mini-cart">
	<button class="btn-cart d-none d-md-block" type="button">
		<span class="badge badge-light">
			<?php echo WC()->cart->get_cart_contents_count(); ?>
		</span>
		<span class="sr-only">cart</span>
	</button>
	<div class="wrapper">
		<?php the_widget( 'WC_Widget_Cart', 'title=' ); ?>
	</div>
	<div class="backdrop"></div>
</div>
<?php endif; ?>

<nav class="navbar navbar-expand-lg navbar-dark" role="navigation">
	<div class="container d-flex align-items-center">
		<a class="navbar-brand" href="/"><span class="logo"></span></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav">
			<span class="navbar-toggler-icon"></span>
		</button>

		<?php
		if ( storefront_is_woocommerce_activated() )
			$class = is_cart()
				? 'current-menu-item'
				: '';

		wp_nav_menu( [
			'menu'            => 'primary',
			'container_class' => 'collapse navbar-collapse',
			'container_id'    => 'main-nav',
			'menu_class'      => 'navbar-nav ml-auto'
		] );
		?>
	</div>
</nav>

<?php
