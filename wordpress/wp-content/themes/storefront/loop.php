<div class="container">
	<?php
	while ( have_posts() ) :
		the_post();
		get_template_part( 'content', get_post_format() );
	endwhile;

	the_posts_pagination( [
		'type'      => 'list',
		'next_text' => _x( 'Next', 'Next post', 'storefront' ),
		'prev_text' => _x( 'Previous', 'Previous post', 'storefront' ),
	] );
	?>
</div>