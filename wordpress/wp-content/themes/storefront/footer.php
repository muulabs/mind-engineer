<footer>
<div class="navbar navbar-expand-md navbar-dark">
	<div class="container">
		<a class="navbar-brand" href="#"><span class="logo"></span></a>

		<?php
		wp_nav_menu( [
			'menu'            => 'primary',
			'container_class' => 'collapse navbar-collapse show',
			'menu_class'      => 'navbar-nav ml-auto'
		] );
		?>
	</div>
	</div>
	<div class="navbar navbar-expand-md navbar-dark">
	<div class="container">


	<ul class="footer-social">
<!-- PN -->
		<?php if ( get_field( 'pinterest', 15 ) ): ?>
			<li>
				<a href="<?php the_field( 'pinterest', 15 ); ?>" target="_blank">
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					     viewBox="0 0 438.529 438.529" style="enable-background:new 0 0 438.529 438.529;"
					     xml:space="preserve">
						<path d="M409.141,109.203c-19.608-33.592-46.205-60.189-79.798-79.796C295.751,9.801,259.065,0,219.281,0
							C179.5,0,142.812,9.801,109.22,29.407c-33.597,19.604-60.194,46.201-79.8,79.796C9.809,142.8,0.008,179.485,0.008,219.267
							c0,44.35,12.085,84.611,36.258,120.767c24.172,36.172,55.863,62.912,95.073,80.232c-0.762-20.365,0.476-37.209,3.709-50.532
							l28.267-119.348c-4.76-9.329-7.139-20.93-7.139-34.831c0-16.175,4.089-29.689,12.275-40.541
							c8.186-10.85,18.177-16.274,29.979-16.274c9.514,0,16.841,3.14,21.982,9.42c5.142,6.283,7.705,14.181,7.705,23.7
							c0,5.896-1.099,13.084-3.289,21.554c-2.188,8.471-5.041,18.273-8.562,29.409c-3.521,11.132-6.045,20.036-7.566,26.692
							c-2.663,11.608-0.476,21.553,6.567,29.838c7.042,8.278,16.372,12.423,27.983,12.423c20.365,0,37.065-11.324,50.107-33.972
							c13.038-22.655,19.554-50.159,19.554-82.514c0-24.938-8.042-45.21-24.129-60.813c-16.085-15.609-38.496-23.417-67.239-23.417
							c-32.161,0-58.192,10.327-78.082,30.978c-19.891,20.654-29.836,45.352-29.836,74.091c0,17.132,4.854,31.505,14.56,43.112
							c3.235,3.806,4.283,7.898,3.14,12.279c-0.381,1.143-1.141,3.997-2.284,8.562c-1.138,4.565-1.903,7.522-2.281,8.851
							c-1.521,6.091-5.14,7.994-10.85,5.708c-14.654-6.085-25.791-16.652-33.402-31.689c-7.614-15.037-11.422-32.456-11.422-52.246
							c0-12.753,2.047-25.505,6.14-38.256c4.089-12.756,10.468-25.078,19.126-36.975c8.663-11.9,19.036-22.417,31.123-31.549
							c12.082-9.135,26.787-16.462,44.108-21.982s35.972-8.28,55.959-8.28c27.032,0,51.295,5.995,72.8,17.986
							c21.512,11.992,37.925,27.502,49.252,46.537c11.327,19.036,16.987,39.403,16.987,61.101c0,28.549-4.948,54.243-14.842,77.086
							c-9.896,22.839-23.887,40.778-41.973,53.813c-18.083,13.042-38.637,19.561-61.675,19.561c-11.607,0-22.456-2.714-32.548-8.135
							c-10.085-5.427-17.034-11.847-20.839-19.273c-8.566,33.685-13.706,53.77-15.42,60.24c-3.616,13.508-11.038,29.119-22.27,46.819
							c20.367,6.091,41.112,9.13,62.24,9.13c39.781,0,76.47-9.801,110.062-29.41c33.595-19.602,60.192-46.199,79.794-79.791
							c19.606-33.599,29.407-70.287,29.407-110.065C438.527,179.485,428.74,142.795,409.141,109.203z"/>
					</svg>
				</a>
			</li>
		<?php endif; ?>
<!-- FB -->
		<?php if ( get_field( 'facebook', 15 ) ): ?>
			<li>
				<a href="<?php the_field( 'facebook', 15 ); ?>" target="_blank">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					     viewBox="0 0 430.113 430.114" style="enable-background:new 0 0 430.113 430.114;"
					     xml:space="preserve">
						<path d="M158.081,83.3c0,10.839,0,59.218,0,59.218h-43.385v72.412h43.385v215.183h89.122V214.936h59.805
							c0,0,5.601-34.721,8.316-72.685c-7.784,0-67.784,0-67.784,0s0-42.127,0-49.511c0-7.4,9.717-17.354,19.321-17.354
							c9.586,0,29.818,0,48.557,0c0-9.859,0-43.924,0-75.385c-25.016,0-53.476,0-66.021,0C155.878-0.004,158.081,72.48,158.081,83.3z"/>
					</svg>
				</a>
			</li>
		<?php endif; ?>
<!-- TW -->
		<?php if ( get_field( 'twitter', 15 ) ): ?>
			<li>
				<a href="<?php the_field( 'twitter', 15 ); ?>" target="_blank">
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					     viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
							<path d="M612,116.258c-22.525,9.981-46.694,16.75-72.088,19.772c25.929-15.527,45.777-40.155,55.184-69.411
								c-24.322,14.379-51.169,24.82-79.775,30.48c-22.907-24.437-55.49-39.658-91.63-39.658c-69.334,0-125.551,56.217-125.551,125.513
								c0,9.828,1.109,19.427,3.251,28.606C197.065,206.32,104.556,156.337,42.641,80.386c-10.823,18.51-16.98,40.078-16.98,63.101
								c0,43.559,22.181,81.993,55.835,104.479c-20.575-0.688-39.926-6.348-56.867-15.756v1.568c0,60.806,43.291,111.554,100.693,123.104
								c-10.517,2.83-21.607,4.398-33.08,4.398c-8.107,0-15.947-0.803-23.634-2.333c15.985,49.907,62.336,86.199,117.253,87.194
								c-42.947,33.654-97.099,53.655-155.916,53.655c-10.134,0-20.116-0.612-29.944-1.721c55.567,35.681,121.536,56.485,192.438,56.485
								c230.948,0,357.188-191.291,357.188-357.188l-0.421-16.253C573.872,163.526,595.211,141.422,612,116.258z"/>
					</svg>
				</a>
			</li>
		<?php endif; ?>
<!-- IN -->
		<?php if ( get_field( 'instagram', 15 ) ): ?>
			<li>
				<a href="<?php the_field( 'instagram', 15 ); ?>" target="_blank">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
						<path d="M256,152c-57.9,0-105,47.1-105,105s47.1,105,105,105s105-47.1,105-105S313.9,152,256,152z M256,152   c-57.9,0-105,47.1-105,105s47.1,105,105,105s105-47.1,105-105S313.9,152,256,152z M437,0H75C33.6,0,0,33.6,0,75v362   c0,41.4,33.6,75,75,75h362c41.4,0,75-33.6,75-75V75C512,33.6,478.4,0,437,0z M256,392c-74.399,0-135-60.601-135-135   c0-74.401,60.601-135,135-135s135,60.599,135,135C391,331.399,330.399,392,256,392z M421,122c-16.5,0-30-13.5-30-30s13.5-30,30-30   s30,13.5,30,30S437.5,122,421,122z M256,152c-57.9,0-105,47.1-105,105s47.1,105,105,105s105-47.1,105-105S313.9,152,256,152z    M256,152c-57.9,0-105,47.1-105,105s47.1,105,105,105s105-47.1,105-105S313.9,152,256,152z M256,152c-57.9,0-105,47.1-105,105   s47.1,105,105,105s105-47.1,105-105S313.9,152,256,152z"/>
					</svg>
				</a>
			</li>
		<?php endif; ?>
		</ul>
	</div>
	</div>
</footer>

<?php wp_footer(); ?>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
	jQuery( function ()
	{
		jQuery( '#mini-cart > button' ).click( function ( e )
		{
			jQuery( '#mini-cart' ).toggleClass( 'show' );
		} );

		jQuery( '#mini-cart > .backdrop' ).click( function ( e )
		{
			jQuery( '#mini-cart' ).toggleClass( 'show' );
		} );

		if ( jQuery( 'tr.mentoring-type' ).length )
		{
			jQuery( 'tr.mentoring-type .value' ).attr( 'colspan', '2' );

			jQuery( 'input[name="mentoring_type"]' ).each( function ( index )
			{
				var input = jQuery( this );
				var price = input.data( 'price' ) || 0;
				var label = input.parent();
				var text  = label.clone().children().remove().end().text();

				text = text.replace( '()', '' ).split( ',' ).reduce( function ( acc, crr, index )
				{
					if ( index === 0 )
						return '<strong>' + crr + '</strong>';
					else if ( index === 1 )
						return acc + '<small>' + crr + '</small>';

					return acc;
				}, '' );

				input.next().remove();
				input[ 0 ].nextSibling.remove();

				if ( input[ 0 ].nextSibling )
					input[ 0 ].nextSibling.remove();

				label.append( '<span>' + text + '</span><span class="price-custom">€' + price + '</span>' );
			} );

			jQuery( 'input[name="mentoring_type"]' ).change( function ( e )
			{
				jQuery( 'input[name="mentoring_type"]' )
					.parent()
					.removeClass( 'active' );

				jQuery( 'input[name="mentoring_type"]:checked' )
					.parent()
					.toggleClass( 'active' );

				var input = jQuery( this );
				var price = input.data( 'price' ) || 0;

				var html  = price
					? 'Add to cart<span class="badge badge-primary"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">€</span>' + price + '</span></span>'
					: 'ASK FOR QUOTE<span class="badge badge-primary"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">€</span>0</span></span>';

				input.closest( 'form' ).find( 'button:submit' ).html( html );
			} );
		}
	} );
</script>

</body>
</html>
