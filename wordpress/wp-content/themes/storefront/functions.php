<?php

$theme              = wp_get_theme( 'storefront' );
$storefront_version = $theme[ 'Version' ];

if ( !isset( $content_width ) )
	$content_width = 980; /* pixels */

$storefront = (object)[
	'version'    => $storefront_version,
	'main'       => require 'inc/class-storefront.php',
	'customizer' => require 'inc/customizer/class-storefront-customizer.php',
];

require 'inc/storefront-functions.php';
require 'inc/storefront-template-functions.php';

if ( class_exists( 'Jetpack' ) )
{
	$storefront->jetpack = require 'inc/jetpack/class-storefront-jetpack.php';
}

if ( storefront_is_woocommerce_activated() )
{
	$storefront->woocommerce            = require 'inc/woocommerce/class-storefront-woocommerce.php';
	$storefront->woocommerce_customizer = require 'inc/woocommerce/class-storefront-woocommerce-customizer.php';

	require 'inc/woocommerce/class-storefront-woocommerce-adjacent-products.php';

	require 'inc/woocommerce/storefront-woocommerce-template-hooks.php';
	require 'inc/woocommerce/storefront-woocommerce-template-functions.php';
	require 'inc/woocommerce/storefront-woocommerce-functions.php';
}

/*if ( is_admin() )
{
	$storefront->admin = require 'inc/admin/class-storefront-admin.php';

	require 'inc/admin/class-storefront-plugin-install.php';
}*/

/**
 * NUX
 * Only load if wp version is 4.7.3 or above because of this issue;
 * https://core.trac.wordpress.org/ticket/39610?cversion=1&cnum_hist=2
 */
if ( version_compare( get_bloginfo( 'version' ), '4.7.3', '>=' ) && ( is_admin() || is_customize_preview() ) )
{
	require 'inc/nux/class-storefront-nux-admin.php';
	require 'inc/nux/class-storefront-nux-guided-tour.php';

	if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '3.0.0', '>=' ) )
	{
		require 'inc/nux/class-storefront-nux-starter-content.php';
	}
}


add_action( 'post_meta', 'post_meta', 0 );

if ( !function_exists( 'post_meta' ) )
{
	function post_meta()
	{
		if ( 'post' !== get_post_type() )
			return;

		$posted_on = sprintf(
			'<li class="breadcrumb-item" datetime="%1$s">%2$s</li>',
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		// Author.
		$author = sprintf(
			'<li class="breadcrumb-item"><a href="%2$s" class="url fn" rel="author">%3$s</a></li>',
			__( 'by', 'storefront' ),
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			esc_html( get_the_author() )
		);

		// Comments.
		$comments = '';

		if ( !post_password_required() && ( comments_open() || 0 !== intval( get_comments_number() ) ) )
		{
			$comments_number = get_comments_number_text( __( 'Leave a comment', 'storefront' ), __( '1 Comment', 'storefront' ), __( '% Comments', 'storefront' ) );

			$comments = sprintf(
				'<li class="breadcrumb-item"><a href="%1$s">%2$s</a></li>',
				esc_url( get_comments_link() ),
				$comments_number
			);
		}

		echo wp_kses(
			sprintf( '<ol class="breadcrumb">%1$s %2$s %3$s</ol>', $posted_on, $author, $comments ), [
				'ol'   => [
					'class' => [],
				],
				'li'   => [
					'class' => [],
				],
				'a'    => [
					'href'  => [],
					'title' => [],
					'rel'   => [],
				],
				'time' => [
					'datetime' => [],
					'class'    => [],
				],
			]
		);
	}
}


add_filter( 'nav_menu_link_attributes', 'add_class_to_href', 10, 2 );
function add_class_to_href( $classes, $item )
{
	if ( !is_front_page() && ( in_array( 'current_page_item', $item->classes ) ) )
		$classes[ 'class' ] = 'active';

	return $classes;
}

add_action( 'wp_enqueue_scripts', 'wsis_dequeue_stylesandscripts_select2', 100 );

function wsis_dequeue_stylesandscripts_select2()
{
	if ( class_exists( 'woocommerce' ) )
	{
		wp_dequeue_style( 'selectWoo' );
		wp_deregister_style( 'selectWoo' );

		wp_dequeue_script( 'selectWoo' );
		wp_deregister_script( 'selectWoo' );
	}
}

add_filter( 'woocommerce_default_address_fields', 'bbloomer_override_postcode_validation' );

function bbloomer_override_postcode_validation( $address_fields )
{
	$address_fields[ 'postcode' ][ 'required' ] = false;
	return $address_fields;
}


add_filter( 'thwepo_enqueue_public_scripts', 'extra_options_scripts' );

function extra_options_scripts()
{
	return is_front_page();
}