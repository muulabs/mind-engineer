<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
// do_action( 'woocommerce_before_single_product' );

if ( post_password_required() )
{
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}

do_action( 'woocommerce_before_single_product' );
?>

<div class="container">
	<h3 class="mb-4"><?php the_title(); ?> </h3>
	<?php if ( has_post_thumbnail() ): ?>
		<div class="row">
			<div class="col-md-8">
				<?php do_action( 'woocommerce_single_product_summary' ); ?>
				<?php the_content(); ?>
			</div>
			<div class="col-md-4">
				<?php the_post_thumbnail( 'full',  ['class'=> 'img-fluid'] ); ?>
			</div>
		</div>
	<?php else: ?>
		<?php do_action( 'woocommerce_single_product_summary' ); ?>
		<?php the_content(); ?>
	<?php endif; ?>
</div>

	<?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	do_action( 'woocommerce_after_single_product_summary' );
	?>




