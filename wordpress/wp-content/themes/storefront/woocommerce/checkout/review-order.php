<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.3.0
 */

if ( !defined( 'ABSPATH' ) )
{
	exit;
}
?>

<?php do_action( 'woocommerce_review_order_before_cart_contents' ); ?>

<ul class="checkout-review-order">
	<?php

	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item )
	{
		$_product = apply_filters( 'woocommerce_cart_item_product', $cart_item[ 'data' ], $cart_item, $cart_item_key );

		if ( $_product && $_product->exists() && $cart_item[ 'quantity' ] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) )
		{
			?>
			<li class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
				<span class="name">
					<?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity', '' . sprintf( '%s &times; ', $cart_item[ 'quantity' ] ) . '', $cart_item, $cart_item_key ); ?>
					<strong><?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;'; ?></strong>
					<?php echo wc_get_formatted_cart_item_data( $cart_item ); ?>
				</span>
				<span class="total">
					<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item[ 'quantity' ] ), $cart_item, $cart_item_key ); ?>
				</span>
			</li>
			<?php
		}
	}
	?>
</ul>

<?php do_action( 'woocommerce_review_order_after_cart_contents' ); ?>

<div class="checkout-review-order-total">
	<?php _e( 'Total', 'woocommerce' ); ?>
	<?php wc_cart_totals_order_total_html(); ?>
</div>
