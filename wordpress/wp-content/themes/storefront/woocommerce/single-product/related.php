<?php
if ( !defined( 'ABSPATH' ) ) exit;

if ( $related_products ) : ?>

	<div class="m-list mt-6">
		<h5 class="m-list-title">Other books</h5>
		<?php foreach ( $related_products as $related_product ): ?>

			<?php
			$post_object = get_post( $related_product->get_id() );
			setup_postdata( $GLOBALS[ 'post' ] =& $post_object );
			?>
			<div class="m-list-item">
				<div class="container">
					<div>
						<h6><?php the_title() ?></h6>
						<p><?php echo $post_object->post_date; ?></p>
					</div>
					<a href="<?php echo get_permalink( $related_product->get_id() ) ?>" class="btn btn-sm btn-outline-secondary mt-2 mt-md-0">
						meer informatie
					</a>
				</div>
			</div>
		<?php endforeach; ?>
	</div>

<?php endif; ?>

<?php wp_reset_postdata(); ?>
