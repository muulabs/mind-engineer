<?php
$title = get_the_title();
?>
<div class="post-thumbnail">
	<img src="<?php echo get_the_post_thumbnail_url($post->ID, 'large'); ?>" alt="">
</div>

<article data-letter="<?php echo $title[0] ?>" id="post-<?php the_ID(); ?>">
	<?php
	is_single()
		? the_title( '<h3 class="mb-2">', '</h1>' )
		: the_title( sprintf( '<h3 class="mb-2"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );

	do_action( 'post_meta' );

	// if ( has_post_thumbnail() )
	// 	the_post_thumbnail( 'full' );

	is_single()
		? the_content()
		: the_excerpt();

	if( !is_single() )
		echo '<a class="btn btn-sm btn-primary" href="' . get_permalink() . '">Read more</a>';

	wp_link_pages( [
		'before' => '<div class="page-links">' . __( 'Pages:', 'storefront' ),
		'after'  => '</div>',
	] );

	if ( comments_open() || 0 !== intval( get_comments_number() ) ) :
		comments_template();
	endif;
	?>

</article>