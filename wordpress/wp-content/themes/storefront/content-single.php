CONTENT-SINGLE
<div class="container">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php

	is_single()
		? the_title( '<h1 class="entry-title">', '</h1>' )
		: the_title( sprintf( '<h2 class="alpha entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );

	do_action( 'post_meta' );

	if ( has_post_thumbnail() )
		the_post_thumbnail( 'full' );

	the_content(
		sprintf(
		/* translators: %s: post title */
			__( 'Continue reading %s', 'storefront' ),
			'<span class="screen-reader-text">' . get_the_title() . '</span>'
		)
	);

	wp_link_pages(
		[
			'before' => '<div class="page-links">' . __( 'Pages:', 'storefront' ),
			'after'  => '</div>',
		]
	);

	the_post_navigation( [
		'next_text' => '<span class="screen-reader-text">' . esc_html__( 'Next post:', 'storefront' ) . ' </span>%title',
		'prev_text' => '<span class="screen-reader-text">' . esc_html__( 'Previous post:', 'storefront' ) . ' </span>%title',
	] );

	if ( comments_open() || 0 !== intval( get_comments_number() ) ) :
		comments_template();
	endif;

	?>
	</article><!-- #post-## -->
</div>