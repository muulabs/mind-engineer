<?php if(is_page()) : ?><div class="container"><?php endif; ?>

	<?php
	storefront_post_thumbnail( 'full' );
	the_title( '<h2 class="mb-4">', '</h2>' );
	the_content();

	wp_link_pages( [
		'before' => '<div class="page-links">' . __( 'Pages:', 'storefront' ),
		'after'  => '</div>',
	] );

	if ( comments_open() || 0 !== intval( get_comments_number() ) ) :
		comments_template();
	endif;
	?>


<?php if(is_page()) : ?></div><?php endif; ?>