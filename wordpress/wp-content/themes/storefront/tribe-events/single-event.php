<?php

if ( !defined( 'ABSPATH' ) ) die( '-1' );

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural   = tribe_get_event_label_plural();

$event_id = get_the_ID();
$program  = get_field( "program" );
$events   = tribe_get_events( [ 'posts_per_page' => 5, ] );
?>

<div class="container" id="event">
	<div class="row mb-5">
		<div class="col-md-8">
			<h3 class="pb-5"><?php the_title(); ?></h3>
			<div class="mb-4 lead">
				<?php the_excerpt(); ?>
				<time><? echo tribe_get_start_date( $post ) ?></time>
				<address><?php echo tribe_get_venue() ?></address>
			</div>
			<?php if ( tribe_get_cost() ) : ?>
				<a href="#tickets" class="btn btn-price btn-primary mr-2 mb-2">
					SCHRIJF JE NU IN <span class="badge badge-primary"><?php echo tribe_get_cost( $event_id, true ) ?></span>
				</a>
			<?php endif; ?>
			<?php if ( $program ) : ?>
				<a href="#program" class="btn btn-outline-secondary mb-2">
					ONTDEK HET PROGRAMMA
				</a>
			<?php endif; ?>
		</div>
		<div class="col-md-4">
			<?php
			if ( has_post_thumbnail() )
				the_post_thumbnail( 'full', [ 'class' => 'img-fluid' ] );
			?>
		</div>
	</div>
</div>


<div class="m-list mt-6 text-md-left text-center">
	<h5 class="m-list-title">Other events</h5>
	<?php foreach ( $events as $post ): ?>
		<?php if( $post->ID == $event_id ){ continue; } ?>
		<div class="m-list-item">
			<div class="container">
				<div>
					<h6><?php echo $post->post_title ?></h6>
					<p><?php echo tribe_get_start_date( $post ) ?></p>
				</div>
				<a href="<?php echo tribe_get_event_link( $post->ID ) ?>" class="btn btn-sm btn-outline-secondary mt-2 mt-md-0">
					meer informatie
				</a>
			</div>
		</div>
	<?php endforeach; ?>
</div>

<br/>
<br/>
<div class="container">
	<?php

	if ( $program )
	{
		echo '<h5>' . __( 'Programma' ) . '</h5><div id="program">' . $program . '</div>';

		if ( tribe_get_cost() )
			echo '<div class="text-center mb-6"><a href="#tickets" class="btn btn-primary btn-price">SCHRIJF JE NU IN <span class="badge badge-primary">' . tribe_get_cost( $event_id, true ) . '</span></a></div>';
	}
	?>

	<?php the_content(); ?>

	<?php if ( get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option( 'showComments', false ) ) comments_template() ?>

</div>

<?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>
