<?php
/**
 * Renders the WooCommerce tickets table/form
 *
 * Override this template in your own theme by creating a file at:
 *
 *     [your-theme]/tribe-events/wootickets/tickets.php
 *
 * @version 4.10
 *
 * @var bool $global_stock_enabled
 * @var bool $must_login
 */
global $woocommerce;

$is_there_any_product         = false;
$is_there_any_product_to_sell = false;
$unavailability_messaging     = is_callable( [ $this, 'do_not_show_tickets_unavailable_message' ] );

if ( !empty( $tickets ) )
	$tickets = tribe( 'tickets.handler' )->sort_tickets_by_menu_order( $tickets );

ob_start();

$cart_classes = (array)apply_filters( 'tribe_events_tickets_woo_cart_class', [ 'cart' ] );

?>

<div id="tickets">
	<div class="container">
		<h4 class="my-4"><?php esc_html_e( 'Tickets', 'event-tickets-plus' ) ?></h4>
	</div>

	<form id="buy-tickets" method="post" enctype='multipart/form-data' novalidate
	      action="<?php echo esc_url( tribe( 'tickets-plus.commerce.woo' )->get_cart_url() ) ?>"
	      class="<?php echo esc_attr( implode( ' ', $cart_classes ) ); ?>"
	>

		<?php
		foreach ( $tickets as $ticket ) :
			global $product;

			$product = class_exists( 'WC_Product_Simple' )
				? new WC_Product_Simple( $ticket->ID )
				: new WC_Product( $ticket->ID );

			$data_product_id = '';

			if ( $ticket->date_in_range() )
			{
				$is_there_any_product = true;

				echo sprintf( '<input type="hidden" name="product_id[]" value="%d">', esc_attr( $ticket->ID ) );

				$column_classes = (array)apply_filters( 'tribe_events_tickets_woo_quantity_column_class', [ 'woocommerce' ], $ticket->ID );
				$max_quantity   = $product->backorders_allowed() ? '' : $product->get_stock_quantity();
				$max_quantity   = $product->is_sold_individually() ? 1 : $max_quantity;
				$available      = $ticket->available();

				$row_classes = (array)apply_filters( 'tribe_events_tickets_row_class', [ 'woocommerce', 'tribe-tickets-form-row' ], $ticket->ID );
				echo '<div class="ticket-item ' . esc_attr( implode( ' ', $row_classes ) ) . '" data-product-id="' . esc_attr( $ticket->ID ) . '"><div class="container"><div class="row align-items-center">';

					echo '<div class="col-lg-8 col-md-6">';
						echo '<div class="ticket-name">' . $ticket->name . '</div>';
						echo '<div class="ticket-description">' . ( $ticket->show_description() ? $ticket->description : '' ) . '</div>';
					echo '</div>';

					echo '<div class="col-lg-2 col-md-3 text-center">';

						echo '<div class="ticket-price">' . ( method_exists( $product, 'get_price' ) && $product->get_price() ? $this->get_price_html( $product ) : esc_html_e( 'Free', 'event-tickets-plus' )). '</div>';

						if ( $available !== 0 )
						{
							$stock        = $ticket->stock();
							$max_quantity = $product->backorders_allowed() ? '' : $stock;
							$max_quantity = $product->is_sold_individually() ? 1 : $max_quantity;
							$available    = $ticket->available();

							$input = woocommerce_quantity_input( [
								'input_name'  => 'quantity_' . $ticket->ID,
								'input_class' => [ 'text-center', 'form-control', 'form-control-sm'],
								'input_value' => 1,
								'min_value'   => 1,
								'max_value'   => $max_quantity,
							], null, false );

							$is_there_any_product_to_sell = true;
							$disabled_attr                = disabled( $must_login, true, false );
							$input                        = str_replace( '<input type="number"', '<input type="number"' . $disabled_attr, $input );

							if ( $available )
							{
								$readable_amount = tribe_tickets_get_readable_amount( $available, null, false );
								echo '<div class="ticket-available">';
									echo sprintf( esc_html__( '%1$s available', 'event-tickets-plus' ), '<span class="available-stock" data-product-id="' . esc_attr( $ticket->ID ) . '">' . esc_html( $readable_amount ) . '</span>' );
								echo '</div>';
							}

							echo $input;
							do_action( 'wootickets_tickets_after_quantity_input', $ticket, $product );
						}
						else
						{
							echo '<div class="tickets-nostock">' . esc_html__( 'Out of stock!', 'event-tickets-plus' ) . '</div>';
						}

					echo '</div>';

					echo '<div class="col-lg-2 col-md-3 text-center">';

						if ( $is_there_any_product_to_sell )
						{
							if ( $must_login )
							{
								include Tribe__Tickets_Plus__Main::instance()->get_template_hierarchy( 'login-to-purchase' );
							}
							else
							{
								echo '<button type="submit" name="wootickets_process" value="1" class="btn btn-primary mt-3 mt-md-0">';
									echo esc_html_e( 'Add to cart', 'event-tickets-plus' );
								echo '</button>';
							}
						}
					echo '</div>';

				echo '</div></div></div>';

				if ( $product->is_in_stock() )
				{
					/**
					 * Use this filter to hide the Attendees List Optout
					 *
					 * @since 4.5.2
					 *
					 * @param bool
					 */
					$hide_attendee_list_optout = apply_filters( 'tribe_tickets_plus_hide_attendees_list_optout', false );
					if ( !$hide_attendee_list_optout
						&& class_exists( 'Tribe__Tickets_Plus__Attendees_List' )
						&& !Tribe__Tickets_Plus__Attendees_List::is_hidden_on( get_the_ID() )
					)
					{ ?>
						<div class="tribe-tickets-attendees-list-optout">
							<input type="checkbox" name="optout_<?php echo esc_attr( $ticket->ID ); ?>" id="tribe-tickets-attendees-list-optout-edd">
							<label for="tribe-tickets-attendees-list-optout-edd"><?php esc_html_e( "Don't list me on the public attendee list", 'event-tickets-plus' ); ?></label>
						</div>
						<?php
					}
				}
			}

		endforeach; ?>


</form>

	<?php

	$content = ob_get_clean();

	echo $content;

	$is_there_any_product
		? $this->do_not_show_tickets_unavailable_message()
		: $this->maybe_show_tickets_unavailable_message( $tickets );
	?>

</div>
